<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\VendedoresController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\VendedoresController Test Case
 */
class VendedoresControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vendedores',
        'app.usuarios'
    ];

    /**
     * Test ignoreListActions method
     *
     * @return void
     */
    public function testIgnoreListActions()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test autocomplete method
     *
     * @return void
     */
    public function testAutocomplete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test pesquisar method
     *
     * @return void
     */
    public function testPesquisar()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test abrir method
     *
     * @return void
     */
    public function testAbrir()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test incluir method
     *
     * @return void
     */
    public function testIncluir()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test salvar method
     *
     * @return void
     */
    public function testSalvar()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test excluir method
     *
     * @return void
     */
    public function testExcluir()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
