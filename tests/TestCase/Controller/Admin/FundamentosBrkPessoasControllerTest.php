<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\FundamentosBrkPessoasController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\FundamentosBrkPessoasController Test Case
 */
class FundamentosBrkPessoasControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fundamentos_brk_pessoas',
        'app.supervisores',
        'app.gerentes',
        'app.vendedores',
        'app.fundamentos_brk'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
