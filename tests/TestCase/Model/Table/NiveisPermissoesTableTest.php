<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\NiveisPermissoesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\NiveisPermissoesTable Test Case
 */
class NiveisPermissoesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\NiveisPermissoesTable
     */
    public $NiveisPermissoes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.niveis_permissoes',
        'app.niveis'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('NiveisPermissoes') ? [] : ['className' => NiveisPermissoesTable::class];
        $this->NiveisPermissoes = TableRegistry::getTableLocator()->get('NiveisPermissoes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->NiveisPermissoes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
