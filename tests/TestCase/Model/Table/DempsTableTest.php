<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DempsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DempsTable Test Case
 */
class DempsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DempsTable
     */
    public $Demps;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.demps'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Demps') ? [] : ['className' => DempsTable::class];
        $this->Demps = TableRegistry::getTableLocator()->get('Demps', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Demps);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
