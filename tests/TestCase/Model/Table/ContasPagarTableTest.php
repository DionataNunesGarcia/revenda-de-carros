<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContasPagarTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContasPagarTable Test Case
 */
class ContasPagarTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContasPagarTable
     */
    public $ContasPagar;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contas_pagar'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ContasPagar') ? [] : ['className' => ContasPagarTable::class];
        $this->ContasPagar = TableRegistry::getTableLocator()->get('ContasPagar', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContasPagar);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
