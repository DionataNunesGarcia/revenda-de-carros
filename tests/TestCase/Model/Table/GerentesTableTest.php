<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GerentesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GerentesTable Test Case
 */
class GerentesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GerentesTable
     */
    public $Gerentes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.gerentes',
        'app.usuarios',
        'app.pessoas_fisicas',
        'app.supervisores'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Gerentes') ? [] : ['className' => GerentesTable::class];
        $this->Gerentes = TableRegistry::getTableLocator()->get('Gerentes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Gerentes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test novo method
     *
     * @return void
     */
    public function testNovo()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buscar method
     *
     * @return void
     */
    public function testBuscar()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test salvar method
     *
     * @return void
     */
    public function testSalvar()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test montarDados method
     *
     * @return void
     */
    public function testMontarDados()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test excluir method
     *
     * @return void
     */
    public function testExcluir()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
