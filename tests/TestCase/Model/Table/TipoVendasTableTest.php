<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TipoVendasTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TipoVendasTable Test Case
 */
class TipoVendasTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TipoVendasTable
     */
    public $TipoVendas;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tipo_vendas'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TipoVendas') ? [] : ['className' => TipoVendasTable::class];
        $this->TipoVendas = TableRegistry::getTableLocator()->get('TipoVendas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TipoVendas);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
