<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContasReceberTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContasReceberTable Test Case
 */
class ContasReceberTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContasReceberTable
     */
    public $ContasReceber;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contas_receber'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ContasReceber') ? [] : ['className' => ContasReceberTable::class];
        $this->ContasReceber = TableRegistry::getTableLocator()->get('ContasReceber', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContasReceber);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
