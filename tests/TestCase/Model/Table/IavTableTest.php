<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IavTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IavTable Test Case
 */
class IavTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IavTable
     */
    public $Iav;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.iav',
        'app.clientes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Iav') ? [] : ['className' => IavTable::class];
        $this->Iav = TableRegistry::getTableLocator()->get('Iav', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Iav);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
