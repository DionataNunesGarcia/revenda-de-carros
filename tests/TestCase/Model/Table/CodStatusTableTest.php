<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CodStatusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CodStatusTable Test Case
 */
class CodStatusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CodStatusTable
     */
    public $CodStatus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cod_status'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CodStatus') ? [] : ['className' => CodStatusTable::class];
        $this->CodStatus = TableRegistry::getTableLocator()->get('CodStatus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CodStatus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
