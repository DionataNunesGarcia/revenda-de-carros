<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TiposAcordosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TiposAcordosTable Test Case
 */
class TiposAcordosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TiposAcordosTable
     */
    public $TiposAcordos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tipos_acordos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TiposAcordos') ? [] : ['className' => TiposAcordosTable::class];
        $this->TiposAcordos = TableRegistry::getTableLocator()->get('TiposAcordos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TiposAcordos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
