<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TipoVendaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TipoVendaTable Test Case
 */
class TipoVendaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TipoVendaTable
     */
    public $TipoVenda;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tipo_venda'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TipoVenda') ? [] : ['className' => TipoVendaTable::class];
        $this->TipoVenda = TableRegistry::getTableLocator()->get('TipoVenda', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TipoVenda);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
