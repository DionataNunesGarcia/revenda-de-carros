<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FabricantesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FabricantesTable Test Case
 */
class FabricantesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FabricantesTable
     */
    public $Fabricantes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.fabricantes',
        'app.veiculos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Fabricantes') ? [] : ['className' => FabricantesTable::class];
        $this->Fabricantes = TableRegistry::getTableLocator()->get('Fabricantes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fabricantes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
