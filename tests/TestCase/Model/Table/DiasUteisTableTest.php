<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DiasUteisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DiasUteisTable Test Case
 */
class DiasUteisTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DiasUteisTable
     */
    public $DiasUteis;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.dias_uteis'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DiasUteis') ? [] : ['className' => DiasUteisTable::class];
        $this->DiasUteis = TableRegistry::getTableLocator()->get('DiasUteis', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DiasUteis);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
