<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProprietariosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProprietariosTable Test Case
 */
class ProprietariosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProprietariosTable
     */
    public $Proprietarios;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.proprietarios',
        'app.veiculos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Proprietarios') ? [] : ['className' => ProprietariosTable::class];
        $this->Proprietarios = TableRegistry::getTableLocator()->get('Proprietarios', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Proprietarios);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
