<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContatosNewslettersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContatosNewslettersTable Test Case
 */
class ContatosNewslettersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContatosNewslettersTable
     */
    public $ContatosNewsletters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.contatos_newsletters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ContatosNewsletters') ? [] : ['className' => ContatosNewslettersTable::class];
        $this->ContatosNewsletters = TableRegistry::getTableLocator()->get('ContatosNewsletters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContatosNewsletters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
