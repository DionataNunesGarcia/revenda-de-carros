<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FormasPagamentoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FormasPagamentoTable Test Case
 */
class FormasPagamentoTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FormasPagamentoTable
     */
    public $FormasPagamento;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.formas_pagamento'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('FormasPagamento') ? [] : ['className' => FormasPagamentoTable::class];
        $this->FormasPagamento = TableRegistry::getTableLocator()->get('FormasPagamento', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FormasPagamento);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
