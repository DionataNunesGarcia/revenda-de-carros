<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OpcionaisVeiculosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OpcionaisVeiculosTable Test Case
 */
class OpcionaisVeiculosTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OpcionaisVeiculosTable
     */
    public $OpcionaisVeiculos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.opcionais_veiculos',
        'app.opcionals',
        'app.veiculos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OpcionaisVeiculos') ? [] : ['className' => OpcionaisVeiculosTable::class];
        $this->OpcionaisVeiculos = TableRegistry::getTableLocator()->get('OpcionaisVeiculos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OpcionaisVeiculos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
