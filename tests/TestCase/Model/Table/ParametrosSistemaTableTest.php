<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ParametrosSistemaTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ParametrosSistemaTable Test Case
 */
class ParametrosSistemaTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ParametrosSistemaTable
     */
    public $ParametrosSistema;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.parametros_sistema'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ParametrosSistema') ? [] : ['className' => ParametrosSistemaTable::class];
        $this->ParametrosSistema = TableRegistry::getTableLocator()->get('ParametrosSistema', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ParametrosSistema);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
