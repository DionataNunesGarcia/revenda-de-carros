<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OpcionaisTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OpcionaisTable Test Case
 */
class OpcionaisTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OpcionaisTable
     */
    public $Opcionais;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.opcionais',
        'app.veiculos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Opcionais') ? [] : ['className' => OpcionaisTable::class];
        $this->Opcionais = TableRegistry::getTableLocator()->get('Opcionais', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Opcionais);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
