<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * NomenclaturasOcorrenciasFixture
 *
 */
class NomenclaturasOcorrenciasFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'nomenclatura_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'ocorrencia_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_nomenclaturas_idx' => ['type' => 'index', 'columns' => ['nomenclatura_id'], 'length' => []],
            'fk_nomenclaturas_ocorrencias_idx' => ['type' => 'index', 'columns' => ['ocorrencia_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_nomenclaturas_ocorrencias1' => ['type' => 'foreign', 'columns' => ['nomenclatura_id'], 'references' => ['nomenclaturas_relatorios', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_ocorrencias1' => ['type' => 'foreign', 'columns' => ['nomenclatura_id'], 'references' => ['ocorrencias', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'nomenclatura_id' => 1,
                'ocorrencia_id' => 1
            ],
        ];
        parent::init();
    }
}
