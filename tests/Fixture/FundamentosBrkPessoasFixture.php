<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FundamentosBrkPessoasFixture
 *
 */
class FundamentosBrkPessoasFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'supervisores_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'gerentes_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'vendedores_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'fundamentos_brk_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_metas_pessoas_supervisores1_idx' => ['type' => 'index', 'columns' => ['supervisores_id'], 'length' => []],
            'fk_metas_pessoas_gerentes1_idx' => ['type' => 'index', 'columns' => ['gerentes_id'], 'length' => []],
            'fk_metas_pessoas_vendedores1_idx' => ['type' => 'index', 'columns' => ['vendedores_id'], 'length' => []],
            'fk_fundamentos_brk_pessoas_fundamentos_brk2_idx' => ['type' => 'index', 'columns' => ['fundamentos_brk_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_fundamentos_brk_pessoas_fundamentos_brk2' => ['type' => 'foreign', 'columns' => ['fundamentos_brk_id'], 'references' => ['fundamentos_brk', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_metas_pessoas_gerentes10' => ['type' => 'foreign', 'columns' => ['gerentes_id'], 'references' => ['gerentes', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_metas_pessoas_supervisores10' => ['type' => 'foreign', 'columns' => ['supervisores_id'], 'references' => ['supervisores', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_metas_pessoas_vendedores10' => ['type' => 'foreign', 'columns' => ['vendedores_id'], 'references' => ['vendedores', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'supervisores_id' => 1,
                'gerentes_id' => 1,
                'vendedores_id' => 1,
                'fundamentos_brk_id' => 1
            ],
        ];
        parent::init();
    }
}
