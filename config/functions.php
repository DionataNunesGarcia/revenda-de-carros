<?php

use Cake\I18n\Time;
use Cake\I18n\Date;

function dateToData($value)
{
    if (empty($value)) {
        return null;
    }
    if (get_class($value) == "Cake\I18n\FrozenTime") {
        return $value->i18nFormat('dd/MM/yyyy');
    }
    return $value->format('d/m/Y');
}

function dateToDataTime($value)
{
    if (empty($value)) {
        return null;
    }
    if (get_class($value) == "Cake\I18n\FrozenTime") {
        return $value->i18nFormat('dd/MM/yyyy HH:mm:ss');
    }
    return $value->format('d/m/Y H:');
}

function dateToTime($value)
{
    if (empty($value)) {
        return null;
    }
    return $value->i18nFormat('HH:mm');
}

function dateToMesAno($value)
{
    if (empty($value)) {
        return null;
    }
    return $value->i18nFormat('MM/yyyy');
}

function dataToDate($valor)
{
    if (empty($valor) || $valor == '00/00/0000') {
        return null;
    }
    $dateTime = \DateTime::createFromFormat('d/m/Y', $valor);

    return $dateTime->format('Y-m-d');
}

function dataToCakeTime($valor)
{
    if (empty($valor) || $valor == '00/00/0000') {
        return null;
    }
    $dateTime = \DateTime::createFromFormat('d/m/Y', $valor);

    return $dateTime;
}

function mesAnoToDate($valor)
{
    if (empty($valor) || $valor == '00/0000') {
        return null;
    }
    $dateTime = \DateTime::createFromFormat('d/m/Y', '01/' . $valor);

    return $dateTime->format('Y-m-d');
}

function mesAno($valor)
{
    if (empty($valor)) {
        return null;
    }

    return $valor->i18nFormat('MM/yyyy');
}

function boolSN($value)
{
    return intval($value) == 1 ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger"span>Não</span>';
}

function valorToValue1($valor)
{
    if (empty($valor)) {
        return null;
    }

    $verificaPonto = ".";
    if (strpos("[" . $valor . "]", "$verificaPonto")) {
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
    } else {
        $valor = str_replace(',', '.', $valor);
    }

    return $valor;
}

function valorToValue($str)
{
    if (strstr($str, ",")) {
        $str = str_replace(".", "", $str); // replace dots (thousand seps) with blancs
        $str = str_replace(",", ".", $str); // replace ',' with '.'
    }

    if (preg_match("#([0-9\.]+)#", $str, $match)) { // search for number that may contain '.'
        return floatval($match[0]);
    } else {
        return floatval($str); // take some last chances with floatval
    }
}

function str2num($str){
    if(strpos($str, '.') < strpos($str,',')){
        $str = str_replace('.','',$str);
        $str = strtr($str,',','.');
    } else {
        $str = str_replace(',','',$str);
    }
    return (float)$str ?? 0;
}

function valueToValor($valor, $decimais = 2)
{
    if (empty($valor)) {
        return 0;
    }
    return number_format($valor, $decimais, ',', '.');
}

function valueToValorCifrado($valor)
{
    if (empty(valueToValor($valor))) {
        return 'R$ 0,00';
    }
    return 'R$ ' . valueToValor($valor);
}

function slug($nome)
{

    // matriz de entrada
    $entrada = array('ä', 'ã', 'à', 'á', 'â', 'ê', 'ë', 'è', 'é', 'ï', 'ì', 'í', 'ö', 'õ', 'ò', 'ó', 'ô', 'ü', 'ù', 'ú', 'û', 'À', 'Á', 'É', 'Í', 'Ó', 'Ú', 'ñ', 'Ñ', 'ç', 'Ç', ' ', '-', '(', ')', ',', ';', ':', '|', '!', '"', '#', '$', '%', '&', '/', '=', '?', '~', '^', '>', '<', 'ª', 'º');

    // matriz de saída
    $saida = array('a', 'a', 'a', 'a', 'a', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'a', 'a', 'e', 'i', 'o', 'u', 'n', 'n', 'c', 'C', '-', '-', '-', '-', '-', '-', '', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-');

    //Converte as letras retornoando a URL limpa
    $palavra = str_replace($entrada, $saida, trim($nome));

    return strtolower($palavra);
}

function translateController($controller)
{
    $array = [
        'Usuarios' => 'Usuários',
        'Niveis' => 'Níveis',
        'Parametros' => 'Parâmetros do Sistema',
        'LogsAlteracoes' => 'Logs Alterações',
        'Precos' => 'Preços',
        'FundamentosExeDistOut' => 'Fundamentos de Execução Distribuição e Outros',
    ];

    if (array_key_exists($controller, $array)) {
        return tratarNome($array[$controller]);
    }

    return tratarNome($controller);
}

function correct_encoding($text) {
    $current_encoding = mb_detect_encoding($text, 'auto');

    $text = iconv($current_encoding, 'UTF-16BE//IGNORE', trim($text));
    return $text;
}

function translateActions($action)
{
    $array = [
    ];

    if (array_key_exists($action, $array)) {
        return tratarNome($array[$action]);
    }

    return tratarNome($action);
}

function tratarNome($nome)
{
    return ucfirst(preg_replace('/\B[A-Z]/', ' $0', $nome));
}

function somenteLetras($string)
{
    $array = [
        "/(á|à|ã|â|ä)/",
        "/(Á|À|Ã|Â|Ä)/",
        "/(é|è|ê|ë)/",
        "/(É|È|Ê|Ë)/",
        "/(í|ì|î|ï)/",
        "/(Í|Ì|Î|Ï)/",
        "/(ó|ò|õ|ô|ö)/",
        "/(Ó|Ò|Õ|Ô|Ö)/",
        "/(ú|ù|û|ü)/",
        "/(Ú|Ù|Û|Ü)/",
        "/(ñ)/",
        "/(Ñ)/"
    ];

    return strtolower(preg_replace($array,explode(" ","a A e E i I o O u U n N"),$string));
}

function tratarNomeSlug($nome)
{
    if (empty($nome) || $nome === '') {
        return '';
    }
    return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '-$0', $nome)), '-');
}

function limitarTexto($texto, $limite, $quebrar = true)
{
    //corta as tags do texto para evitar corte errado
    $contador = strlen(strip_tags($texto));

    if ($contador <= $limite) {
        //se o número do texto form menor ou igual o limite então retorna ele mesmo
        $newtext = $texto;
    } else {
        if ($quebrar == true) { //se for maior e $quebrar for true
            //corta o texto no limite indicado e retira o ultimo espaço branco
            $newtext = trim(mb_substr($texto, 0, $limite)) . "...";
        } else {
            //localiza ultimo espaço antes de $limite
            $ultimo_espaço = strrpos(mb_substr($texto, 0, $limite), " ");
            //corta o $texto até a posição lozalizada
            $newtext = trim(mb_substr($texto, 0, $ultimo_espaço)) . "...";
        }
    }

    return $newtext;
}

function sexo($valor = null)
{
    $array = ['m' => 'Masculino', 'f' => 'Feminino'];
    if (!empty($valor)) {
        return $array[$valor];
    }
    return $array;
}

function soNumeros($valor)
{
    if (empty($valor) || intval($valor) == 0) {
        return null;
    }
    return preg_replace('/[^0-9]/', '', $valor);
}

function somenteNumerosFone($valor)
{
    return preg_replace('/[^0-9]/', '', $valor);
}

function cpf($cpf)
{
    if (empty($cpf)) {
        return null;
    }
    return mask($cpf, '###.###.###-##');
}

function cnpj($cnpj)
{
    if (empty($cnpj)) {
        return null;
    }
    return mask($cnpj, '##.###.###/####-##');
}

function cpfCnpj($valor)
{
    if (empty($valor)) {
        return null;
    } else if (strlen($valor) > 11) {
        return cnpj($valor);
    } else {
        return cpf($valor);
    }
}

function cep($cep)
{
    if (empty($cep)) {
        return null;
    }
    return mask($cep, '#####-###');
}

function fone($fone)
{

    if (empty($fone)) {
        return null;
    } else if (strlen($fone) === 11) {
        return mask($fone, '(##) #####-####');
    } else {
        return mask($fone, '(##) ####-####');
    }
}

function mask($val, $mask)
{
    $maskared = '';
    $k = 0;
    for ($i = 0; $i <= strlen($mask) - 1; $i++) {
        if ($mask[$i] == '#') {
            if (isset($val[$k])) {
                $maskared .= $val[$k++];
            }
        } else if (isset($mask[$i])) {
            $maskared .= $mask[$i];
        }
    }
    return $maskared;
}

function validaCPF($cpf = null)
{
    // Verifica se um número foi informado
    if (empty($cpf)) {
        return false;
    }
    // Elimina possivel mascara
    $cpf = preg_replace("/[^0-9]/", "", $cpf);
    $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

    // Verifica se o numero de digitos informados é igual a 11
    if (strlen($cpf) != 11) {
        return false;
    }
    // Verifica se nenhuma das sequências invalidas abaixo
    // foi digitada. Caso afirmativo, retorna falso
    else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {
        return false;
        // Calcula os digitos verificadores para verificar se o
        // CPF é válido
    } else {
        for ($t = 9; $t < 11; $t++) {

            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }
}

function idade($data)
{
    if (empty($data)) {
        return null;
    }
    // Separa em dia, mês e ano
    list($ano, $mes, $dia) = explode('-', $data->i18nFormat('yyyy-MM-dd'));
    // Descobre que dia é hoje e retorna a unix timestamp
    $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
    // Descobre a unix timestamp da data de nascimento do fulano
    $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
    // Depois apenas fazemos o cálculo já citado :)
    $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
    return $idade;
}

function concatValueObject($array, $value, $separator)
{
    if (empty($array) || !is_array($array)) {
        return $array;
    }

    $text = '';
    foreach ($array as $item) {
        $text .= ($text !== '') ? "$separator" : '';
        $text .= $item->$value;
    }
    return $text;
}

//Verifica se o arquivo é uma imagem
function isImg($arquivo)
{

    $arrayImagens = array('jpg', 'png', 'jpeg');
    $ext = extensao($arquivo);

    return in_array(strtolower($ext), $arrayImagens);
}

function extensao($valor)
{
    if (empty($valor)) {
        return null;
    }
    $explode = explode('.', $valor);
    return end($explode);
}

function getIcon($ext)
{
    $ext = strtolower(extensao($ext));
    $array = [
        'gif' => 'fa-file-image-o',
        'jpeg' => 'fa-file-image-o',
        'jpg' => 'fa-file-image-o',
        'png' => 'fa-file-image-o',
        'pdf' => 'fa-file-pdf-o',
        'doc' => 'fa-file-word-o',
        'docx' => 'fa-file-word-o',
        'ppt' => 'fa-file-powerpoint-o',
        'pptx' => 'fa-file-powerpoint-o',
        'ods' => 'fa-file-powerpoint-o',
        'csv' => 'fa-file-excel-o',
        'xls' => 'fa-file-excel-o',
        'xlsx' => 'fa-file-excel-o',
        'aac' => 'fa-file-audio-o',
        'mp3' => 'fa-file-audio-o',
        'ogg' => 'fa-file-audio-o',
        'mp4' => 'fa-file-video-o',
        'avi' => 'fa-file-video-o',
        'flv' => 'fa-file-video-o',
        'mkv' => 'fa-file-video-o',
        'zip' => 'fa-file-zip-o',
        'gz' => 'fa-file-zip-o',
        'txt' => 'fa-file-text-o',
        'css' => 'fa-file-code-o',
        'sql' => 'fa-file-code-o',
        'js' => 'fa-file-code-o',
        'html' => 'fa-file-code-o',
        'file' => 'fa-file-o',
    ];
    if (array_key_exists($ext, $array)) {
        return $array[$ext];
    } else {
        return "fa-file-o";
    }
}

function montaMensagemErros($erros)
{

    if (!empty($erros) && is_array($erros)) {
        $error_msg = [];

        foreach ($erros as $key => $errors) {

            if (is_array($errors)) {
                $tabela = $key;
                foreach ($errors as $k => $error) {
                    if (!is_array($error)) {
                        $error_msg[] = 'Tabela: ' . $tabela . ', o campo ' . strtoupper($k) . ': ' . $error;
                    } else {
                        foreach ($error as $kk => $erro) {
                            $error_msg[] = 'Tabela: ' . $tabela . ', o campo ' . strtoupper($k) . ', : ' . $erro . "($kk)";
                            if (!is_array($erro)) {
                                $error_msg[] = 'Tabela: ' . $tabela . ', o campo ' . strtoupper($k) . ': ' . $error;
                            } else {
                                foreach ($erro as $ke => $er) {
                                    $error_msg[] = 'Tabela: ' . $tabela . ', o campo ' . strtoupper($ke) . ', : ' . $er . "($ke)";
                                }
                            }
                        }
                    }
                }
            } else {
                $error_msg[] = 'O campo ' . strtoupper($key) . ': ' . $errors;
            }
        }

        return montaMensagem($error_msg);
    }

    return $erros;
}

function montaMensagem($error_msg)
{
    $mensagem = '';
    foreach ($error_msg as $item) {
        $mensagem .= ($mensagem !== '') ? '<br/>' : '';

        $mensagem .= $item;
    }

    return $mensagem;
}

function canais($valor = null)
{
    $array = ['ASI', 'Frio', 'Premium'];
    $array = array_combine($array, $array);
    if (!empty($valor)) {
        return $array[$valor];
    }
    return $array;
}

function execInBackground($cmd) {
    if (substr(php_uname(), 0, 7) == "Windows"){
        pclose(popen("start /B ". $cmd, "r"));
    } else {
        exec($cmd . " > /dev/null &");
    }
}

function readCSV($csvFile, $separated = ';')
{
    ini_set('memory_limit', '-1');
    ini_set('max_execution_time','600');
    ini_set('max_allowed_packet','512MB');
    $csv = explode("\n", file_get_contents($csvFile));
    $csvArray = [];
    foreach ($csv as $key => $line) {
        $linhe = str_getcsv($line, $separated);
        $csvArray[$key] = $linhe;
    }
    return $csvArray;
}

function dataToDateCliente($value)
{
    $valor = trim($value);
    if (empty($valor) || $valor !== '0') {
        return null;
    } else if ($valor . strlen($valor) > 8) {
        $valor = str_pad($valor, 8, '0', STR_PAD_LEFT);
    }

    $ano = substr($valor, -4);
    $mes = substr($valor, 2, -4);
    $dia = substr($valor, 0, 2);
    $dateTime = \DateTime::createFromFormat('Y/m/d', $ano . '/' . $mes . '/' . $dia);

    return $dateTime->format('Y-m-d');
}

function converterTelefoneCliente($numero, $tipo = 'Comercial')
{
    if ($numero == '0' || $numero == '000-00000-0000' || $numero == '' || empty($numero)) {
        return;
    }
    //Pega os numeros de DDD
    $ddd = substr($numero, 1, 2);

    //Pega somente os numeros do telefone
    $telefone = substr($numero, 4);
    if (substr($telefone, 0, 1) == '0') {
        $telefone = substr($telefone, 1);
    }
    return [
        'ddd' => $ddd,
        'telefone' => soNumeros($telefone),
        'tipo' => $tipo
    ];
}

function diaSemana($data)
{

    $diasemana = [
        'Domingo',
        'Segunda',
        'Terça',
        'Quarta',
        'Quinta',
        'Sexta',
        'Sabado'
    ];

    if ($data instanceof DateTime || get_class($data) == 'Cake\I18n\FrozenDate') {
        $data = $data->i18nFormat('yyyy-MM-dd');
    }

    $w = date('w', strtotime($data));

    return $diasemana[$w];
}

function mesesAno()
{
    return [
        1 => 'Janeiro',
        2 => 'Fevereiro',
        3 => 'Março',
        4 => 'Abril',
        5 => 'Maio',
        6 => 'Junho',
        7 => 'Julho',
        8 => 'Agosto',
        9 => 'Setembro',
        10 => 'Outubro',
        11 => 'Novembro',
        12 => 'Dezembro',
    ];
}

function nomeMes($mes)
{
    $meses = mesesAno();

    return $meses[intval($mes)];
}

/**
 * Identifica se a data é um dia útil
 *
 * @param date $data - data no formato aaaa-mm-dd
 * @return boolean
 */
function isDiaUtil($data)
{

    //Colocamos em um array os dia de fim de semana (sábado e domingo)
    $fds = array('6', '0');

    //Verificamos qual é o dia da semana
    $diaSemana = date('w', strtotime($data));
    $feriados = diasFeriados(date('Y', strtotime($data)));

    //Aqui verficamos se é o dia útil
    if (in_array($diaSemana, $fds) || in_array($data, $feriados)) {
        return false;
    } else {
        return true;
    }
}

function diasFeriados($ano = null)
{
    if ($ano === null) {
        $ano = intval(date('Y'));
    }

    $pascoa = easter_date($ano); // Limite de 1970 ou após 2037 da easter_date PHP consulta http://www.php.net/manual/pt_BR/function.easter-date.php
    $dia_pascoa = date('j', $pascoa);
    $mes_pascoa = date('n', $pascoa);
    $ano_pascoa = date('Y', $pascoa);

    $feriados = array(
        // Tatas Fixas dos feriados Nacionail Basileiras
        mktime(0, 0, 0, 1, 1, $ano), // Confraternização Universal - Lei nº 662, de 06/04/49
        mktime(0, 0, 0, 4, 21, $ano), // Tiradentes - Lei nº 662, de 06/04/49
        mktime(0, 0, 0, 5, 1, $ano), // Dia do Trabalhador - Lei nº 662, de 06/04/49
        mktime(0, 0, 0, 9, 7, $ano), // Dia da Independência - Lei nº 662, de 06/04/49
        mktime(0, 0, 0, 10, 12, $ano), // N. S. Aparecida - Lei nº 6802, de 30/06/80
        mktime(0, 0, 0, 11, 2, $ano), // Todos os santos - Lei nº 662, de 06/04/49
        mktime(0, 0, 0, 11, 15, $ano), // Proclamação da republica - Lei nº 662, de 06/04/49
        mktime(0, 0, 0, 12, 25, $ano), // Natal - Lei nº 662, de 06/04/49
        // These days have a date depending on easter
        mktime(0, 0, 0, $mes_pascoa, $dia_pascoa - 48, $ano_pascoa), //2ºferia Carnaval
        mktime(0, 0, 0, $mes_pascoa, $dia_pascoa - 47, $ano_pascoa), //3ºferia Carnaval
        mktime(0, 0, 0, $mes_pascoa, $dia_pascoa - 2, $ano_pascoa), //6ºfeira Santa
        mktime(0, 0, 0, $mes_pascoa, $dia_pascoa, $ano_pascoa), //Pascoa
        mktime(0, 0, 0, $mes_pascoa, $dia_pascoa + 60, $ano_pascoa), //Corpus Cirist
    );

    sort($feriados);
    $array = array();

    foreach ($feriados as $a) {
        $array[] = date("Y-m-d", $a);
    }

    return $array;
}

function tipoCoringas($valor = null)
{
    $array = ['Produto', 'Categoria', 'Família', 'Marca'];
    $array = array_combine($array, $array);
    if (!empty($valor)) {
        return $array[$valor];
    }
    return $array;
}
function volumeCoringas($valor = null)
{
    $array = ['Qtde' => 'Quantidade', 'HL' => 'HL'];
    if (!empty($valor)) {
        return $array[$valor];
    }
    return $array;
}

function tipoNomenclaturas($valor = null)
{
    $array = [
        'Produtos' => 'Produtos',
        'Categorias' => 'Categorias',
        'Familias' => 'Famílias',
        'Marcas' => 'Marcas'
    ];

    if (!empty($valor)) {
        return $array[$valor];
    }
    return $array;
}

function volumeNomenclaturas($valor = null)
{
    $array = ['Cobertura', 'HL', 'Moeda', 'UMB'];
    $array = array_combine($array, $array);

    if (!empty($valor)) {
        return $array[$valor];
    }
    return $array;
}

function tipoCobertura($valor = null)
{
    $array = ['SKU', 'Unidade'];
    $array = array_combine($array, $array);

    if (!empty($valor)) {
        return $array[$valor];
    }
    return $array;
}


function verificaCodigoData($codigo)
{
    if(!strpos($codigo, '/')){
        return $codigo;
    }
    $mesesArray = mesesArray();

    $codigoArray = explode('/', $codigo);

    if (array_key_exists($codigoArray[1], $mesesArray)){
        $codigo = "00" . $codigoArray[0] . "-" . $mesesArray[$codigoArray[1]];
    }
    return $codigo;
}

function mesesArray(){
    return [
        'jan' => '0001',
        'fev' => '0002',
        'mar' => '0003',
        'abr' => '0004',
        'mai' => '0005',
        'jun' => '0006',
        'jul' => '0007',
        'ago' => '0008',
        'set' => '0009',
        'out' => '0010',
        'nov' => '0011',
        'dez' => '0012',
    ];
}

function fdsX($valor) {
    return ($valor == 1) ? '<i class="fa fa-times-circle-o"></i>' : '';
}

function convertDatasIntervalo($datas, $separator = ' - ') {
    $retorno = [
        'inicio' => null,
        'fim' => null,
    ];
    if (empty($datas) || !strpos($datas, $separator)) {
        return $retorno;
    }
    $explode = explode(' - ', $datas);
    $retorno['inicio'] = dataToDate($explode[0]) . ' 00:00:00';
    $retorno['fim'] = dataToDate($explode[1]) . ' 23:59:59';
    return $retorno;
}
