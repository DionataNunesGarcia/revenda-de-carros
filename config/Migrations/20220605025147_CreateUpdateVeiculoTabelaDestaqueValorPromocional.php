<?php
use Migrations\AbstractMigration;

class CreateUpdateVeiculoTabelaDestaqueValorPromocional extends AbstractMigration
{
    public function up()
    {

        $this->table('veiculos')
            ->addColumn('destaque', 'boolean', [
                'default' => 0,
                'null'    => false,
                'after' => 'serie',
            ])
            ->addColumn('valor_promocao', 'decimal', [
                'default' => 0,
                'null' => true,
                'precision' => 10,
                'scale' => 2,
                'after' => 'serie',
            ])
            ->update();
    }

    public function down()
    {
        $this->table('veiculos')
            ->removeColumn('destaque')
            ->removeColumn('valor_promocao')
            ->update();
    }
}
