<?php
use Migrations\AbstractMigration;

class UpdateTableVendedoresDataAdmissao extends AbstractMigration
{
    public function up()
    {
        $this->table('vendedores')

            ->addColumn('data_admissao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
                'after' => 'data_nascimento'
            ])
            ->addColumn('data_demissao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
                'after' => 'data_admissao'
            ])
            ->changeColumn('telefone', 'string', [
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('email', 'string', [
                'limit' => 255,
                'null' => true,
                'after' => 'nome'
            ])
            ->update();
    }

    public function down()
    {
        $this->table('vendedores')
            ->removeColumn('data_admissao')
            ->removeColumn('data_demissao')
            ->removeColumn('email')
            ->changeColumn('telefone', 'string', [
                'limit' => 15,
                'null' => false,
            ])
            ->update();
    }
}
