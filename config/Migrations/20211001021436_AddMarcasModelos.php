<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class AddMarcasModelos extends AbstractMigration
{
    private $path = WWW_ROOT . 'arquivos_sistema' . DS . 'importacoes' . DS;

    /**
     * @throws Exception
     */
    public function change()
    {
        $this->importMarcas('marcas-caminhao.csv', true);
        $this->importMarcas('marcas-carros.csv');
        $this->importMarcas('marcas-motos.csv');
        $this->importMarcas('marcas-nautica.csv');
    }

    private function importMarcas($name, $truncate = false)
    {
        $table = TableRegistry::getTableLocator()
            ->get('Marcas');

        if ($truncate) {
            $table->deleteAll(['id !=' => 0]);

            $modelosTable = TableRegistry::getTableLocator()
                ->get('Modelos');
            $modelosTable->deleteAll(['id !=' => 0]);

            $connection = $table->getConnection();
            $connection->query("ALTER TABLE modelos AUTO_INCREMENT = 1;");
        }

        $csv = readCSV($this->path . $name);
        foreach ($csv as $key => $data) {
            if ($key == 0) {
                continue;
            }

            if (!isset($data[1]) || !$data[0]) {
                continue;
            }

            $entity = $table->newEntity();
            $entity->id = intval($data[0]);
            $entity->nome = $data[1] !== '' ? utf8_encode($data[1]) : '  ';
            $entity->criado = Time::now();
            $entity->modificado = Time::now();

            if(!$table->save($entity)) {
                throw new \Exception('Error in save marca', 400);
            }
        }
    }
}
