<?php
use Migrations\AbstractMigration;

class UpdateTableArquivosAddLink extends AbstractMigration
{
    public function up()
    {
        $this->table('arquivos')
            ->changeColumn('usuario_id', 'integer', [
                'null' => false,
                'default' => null,
                'after' => 'model'
            ])
            ->addColumn('link', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
                'after' => 'model'
            ])
            ->update();
    }

    public function down()
    {
        $this->table('arquivos')
            ->removeColumn('link')
            ->update();
    }
}
