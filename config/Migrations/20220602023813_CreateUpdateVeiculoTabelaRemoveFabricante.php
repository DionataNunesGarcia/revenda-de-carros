<?php
use Migrations\AbstractMigration;

class CreateUpdateVeiculoTabelaRemoveFabricante extends AbstractMigration
{
    public function up()
    {
        $this->table('contas_receber')
            ->addColumn('numero_parcela', 'integer', [
                'default' => 1,
                'limit' => 11,
                'null' => false,
                'after' => 'anotacoes',
            ])
            ->addColumn('venda_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
                'after' => 'anotacoes',
            ])
            ->addColumn('conta_receber_parent_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
                'after' => 'anotacoes',
            ])
            ->update();

        $this->table('vendas')
            ->changeColumn('meses_garantia', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->update();

        $this->table('veiculos')
            ->changeColumn('numero_motor', 'string', [
                'limit' => 255,
                'null' => true,
                'after' => 'serie',
            ])
            ->addColumn('valor_reforma', 'decimal', [
                'default' => 0,
                'null' => true,
                'precision' => 10,
                'scale' => 2,
                'after' => 'serie',
            ])
            ->addColumn('valor_fipe', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 10,
                'scale' => 2,
                'after' => 'serie',
            ])
            ->addColumn('numero_portas', 'integer', [
                'default' => 0,
                'limit' => 11,
                'null' => false,
                'after' => 'serie',
            ])
            ->addColumn('combustivel', 'string', [
                'limit' => 100,
                'null' => true,
                'after' => 'serie',
            ])
            ->addColumn('mostrar_site', 'boolean', [
                'default' => 0,
                'null'    => false,
                'after' => 'serie',
            ])
            ->addColumn('disponivel_locacao', 'boolean', [
                'default' => 0,
                'null'    => false,
                'after' => 'serie',
            ])
            ->addColumn('valor_diaria_aluguel', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 10,
                'scale' => 2,
                'after' => 'serie',
            ])
            ->removeColumn('fabricante_id')
            ->update();
    }

    public function down()
    {
        $this->table('veiculos')
            ->removeColumn('valor_reforma')
            ->removeColumn('valor_fipe')
            ->removeColumn('numero_portas')
            ->removeColumn('combustivel')
            ->removeColumn('mostrar_site')
            ->removeColumn('disponivel_locacao')
            ->removeColumn('valor_diaria_aluguel')
            ->changeColumn('numero_motor', 'integer', [
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('fabricante_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->update();

        $this->table('contas_receber')
            ->removeColumn('numero_parcela')
            ->removeColumn('venda_id')
            ->removeColumn('conta_receber_parent_id')
            ->update();
    }
}
