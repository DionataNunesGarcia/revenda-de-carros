<?php
use Migrations\AbstractMigration;

class AddMarcaEmModelo extends AbstractMigration
{
    /**
     *
     */
    public function up()
    {

        $this->table('modelos')
            ->addColumn('marca_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addForeignKey(
                'marca_id',
                'marcas',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();
    }

    /**
     *
     */
    public function down()
    {
        $this->table('modelos')
            ->dropForeignKey('marca_id')
            ->removeColumn('marca_id');
    }
}
