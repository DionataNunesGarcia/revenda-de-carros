<?php
use Migrations\AbstractMigration;

class CreateTableFormasPagamento extends AbstractMigration
{

    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('formas_pagamento')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('slug', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('status', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('parcelas', 'integer', [
                'null' => false,
                'default' => 1,
                'after' => 'slug',
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('formas_pagamento');
    }
}
