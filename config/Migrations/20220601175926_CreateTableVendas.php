<?php
use Migrations\AbstractMigration;

class CreateTableVendas extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('vendas')
            ->addColumn('valor', 'decimal', [
                'default' => null,
                'null' => false,
                'precision' => 10,
                'scale' => 2,
            ])
            ->addColumn('valor_desconto', 'decimal', [
                'default' => null,
                'null' => false,
                'precision' => 10,
                'scale' => 2,
            ])
            ->addColumn('valor_acrescimo', 'decimal', [
                'default' => null,
                'null' => false,
                'precision' => 10,
                'scale' => 2,
            ])
            ->addColumn('status', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('dia_vencimento', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('primeiro_pagamento', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('data_retirada', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('meses_garantia', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('anotacoes', 'text', [
                'null' => true,
            ])
            ->addColumn('contrato_anexo', 'string', [
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('veiculo_id', 'integer', [
                'null' => false,
                'default' => null,
            ])
            ->addColumn('cliente_id', 'integer', [
                'null' => false,
                'default' => null,
            ])
            ->addColumn('vendedor_id', 'integer', [
                'null' => false,
                'default' => null,
            ])
            ->addColumn('forma_pagamento_id', 'integer', [
                'null' => false,
                'default' => null,
            ])
            ->addColumn('rascunho', 'boolean', [
                'default' => 1,
                'null'    => false,
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('vendas');
    }
}
