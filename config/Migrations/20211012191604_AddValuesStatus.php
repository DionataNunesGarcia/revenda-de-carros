<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddValuesStatus extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = TableRegistry::getTableLocator()
            ->get('Status');

        foreach ($this->getValues() as $alias => $name) {
            $entity = $table->newEntity();
            $entity->alias = $alias;
            $entity->nome = $name;

            $table->save($entity);
        }
    }

    private function getValues()
    {
        return [
            "DEFAULT" => 'Padrão',
            "ACTIVE" => 'Ativo',
            "INACTIVE" => 'Inativo',
            "PENDING" => 'Pendente',
            "CANCELED" => 'Cancelado',
            "INTERESTED" => 'Interessado',
            "DRAFT" => 'Rascunho',
            "EXCLUDED" => 'Excluído',
            "SUSPENDED" => 'Suspenso',
            "EXPIRED" => 'Expirado',
            "BLOCKED" => 'Bloqueado',
            "WAITING" => 'Aguardando',
            "FINISHED" => 'Finalizado',
        ];
    }
}
