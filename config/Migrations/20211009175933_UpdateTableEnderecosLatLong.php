<?php
use Migrations\AbstractMigration;

class UpdateTableEnderecosLatLong extends AbstractMigration
{
    /**
     *
     */
    public function up()
    {
        $this->table('enderecos')
            ->changeColumn('lat', 'string', [
                'limit' => 100,
                'null' => true,
            ])
            ->changeColumn('long', 'string', [
                'limit' => 100,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {
        $this->table('enderecos')
            ->changeColumn('lat', 'string', [
                'limit' => 100,
                'null' => true,
            ])
            ->changeColumn('long', 'string', [
                'limit' => 100,
                'null' => true,
            ])
            ->update();

    }
}
