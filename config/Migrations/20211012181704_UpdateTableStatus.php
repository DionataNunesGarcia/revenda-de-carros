<?php
use Migrations\AbstractMigration;

class UpdateTableStatus extends AbstractMigration
{

    public function up()
    {
        $this->table('status')
            ->dropForeignKey('clientes_id')
            ->removeColumn('clientes_id')
            ->removeColumn('status')
            ->removeColumn('observacoes')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('alias', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->update();
    }

    public function down()
    {
        $this->table('veiculos')
            ->addColumn('status', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('observacoes', 'text', [
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('clientes_id', 'integer', [
                'limit' => 11,
                'null' => false,
            ])
            ->dropForeignKey('cor_id')
            ->removeColumn('cor_id')
            ->dropForeignKey('usuario_id')
            ->removeColumn('usuario_id')
            ->update();
    }
}
