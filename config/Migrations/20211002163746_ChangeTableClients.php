<?php
use Migrations\AbstractMigration;

class ChangeTableClients extends AbstractMigration
{
    /**
     *
     */
    public function up()
    {

        $this->table('clientes')
            ->changeColumn('cpf_cnpj', 'integer', [
                'length' => 20,
                'default' => null,
            ])
            ->changeColumn('telefone', 'integer', [
                'limit' => 15,
                'null' => true,
            ])
            ->update();
    }

    /**
     *
     */
    public function down()
    {
        $this->table('clientes')
            ->changeColumn('cpf_cnpj', 'integer', [
                'length' => 11,
                'default' => null,
            ])
            ->changeColumn('telefone', 'integer', [
                'limit' => 15,
                'null' => false,
            ])
            ->update();
    }
}
