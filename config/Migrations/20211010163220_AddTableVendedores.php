<?php
use Migrations\AbstractMigration;

class AddTableVendedores extends AbstractMigration
{
    public function up()
    {
        $this->table('vendedores')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('rg', 'string', [
                'limit' => 14,
                'null' => true,
            ])
            ->addColumn('cpf', 'string', [
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_nascimento', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('sexo', 'enum', [
                'null' => false,
                'default' => 'm',
                'values' => [
                    'm',
                    'f',
                ]
            ])
            ->addColumn('celular', 'string', [
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('telefone', 'string', [
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('salario_fixo', 'decimal', [
                'default' => 0,
                'null' => true,
                'precision'=> 10,
                'scale' => 2
            ])
            ->addColumn('comissao', 'decimal', [
                'default' => 0,
                'null' => true,
                'precision'=> 10,
                'scale' => 2
            ])
            ->addColumn('usuario_id', 'integer', [
                'null' => false,
                'default' => null,
            ])
            ->addColumn('status', 'integer', [
                'null' => false,
                'default' => null,
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('vendedores')
            ->addForeignKey(
                'usuario_id',
                'usuarios',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'status',
                'status',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            );
    }

    public function down()
    {
        $this->table('vendedores')
            ->removeColumn('email')
            ->dropForeignKey('usuario_id')
            ->dropForeignKey('status');

        $this->dropTable('vendedores');
    }
}
