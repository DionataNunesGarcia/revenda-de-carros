<?php
use Migrations\AbstractMigration;

class AddTablesRevenda extends AbstractMigration
{
    public function up()
    {
        $this->table('enderecos')
            ->addColumn('model', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('cep', 'string', [
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('rua', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('numero', 'string', [
                'limit' => 45,
                'null' => false,
            ])
            ->addColumn('bairro', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('complemento', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('cidade_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('lat', 'string', [
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('long', 'string', [
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('tipo_veiculos')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('modelos')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->create();

        $this->table('tipo_vendas')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,

            ])
            ->create();

        $this->table('marcas')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,

            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('fabricantes')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,

            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('estados')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,

            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('cidades')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('uf', 'string', [
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('estado_id', 'string', [
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('opcionais')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,

            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('proprietarios')
            ->addColumn('nome', 'string', [
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('rg', 'string', [
                'limit' => 14,
                'null' => false,
            ])
            ->addColumn('cpf', 'string', [
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('celular', 'string', [
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('clientes')
            ->addColumn('nome', 'string', [
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('tipo', 'enum', [
                'null' => false,
                'default' => 'pf',
                'values' => [
                    'pf',
                    'pj',
                ]
            ])
            ->addColumn('cpf_cnpj', 'string', [
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'limit' => 150,
                'null' => false,
            ])
            ->addColumn('renda', 'decimal', [
                'default' => 0,
                'null' => true,
                'precision'=> 10,
                'scale' => 2
            ])
            ->addColumn('celular', 'string', [
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('veiculos')
            ->addColumn('ano', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('valor', 'decimal', [
                'default' => 0,
                'null' => true,
                'precision'=> 10,
                'scale' => 2
            ])
            ->addColumn('valor_compra', 'decimal', [
                'default' => 0,
                'null' => true,
                'precision'=> 10,
                'scale' => 2
            ])
            ->addColumn('renavam', 'string', [
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('placa', 'string', [
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('quilometragem', 'decimal', [
                'default' => 0,
                'null' => true,
                'precision'=> 10,
                'scale' => 2
            ])
            ->addColumn('chassi', 'string', [
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('imagem', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('data_entrada', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_venda', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('valor_venda', 'decimal', [
                'default' => 0,
                'null' => true,
                'precision'=> 10,
                'scale' => 2
            ])
            ->addColumn('situacao', 'enum', [
                'null' => false,
                'values' => [
                    'a venda',
                    'vendido',
                    'reservado',
                    'aguardando liberacao',
                    'aguardando manutencao',
                    'manutencao',
                    'devolvido',
                ]
            ])
            ->addColumn('observacoes', 'text', [
                'null' => true,
            ])
            ->addColumn('anotacoes', 'text', [
                'null' => true,
            ])
            ->addColumn('importado', 'boolean', [
                'default' => 0,
                'null'    => false,
            ])
            ->addColumn('modelo_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('marca_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('fabricante_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('tipo_veiculo_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('proprietario_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('cidade_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('meses_garantia', 'integer',[
                'limit' => 11,
                'null' => true,
                'after' => 'placa'
            ])
            ->addColumn('numero_motor', 'integer',[
                'limit' => 30,
                'null' => true,
                'after' => 'placa'
            ])
            ->addColumn('serie', 'string', [
                'limit' => 55,
                'null' => true,
                'after' => 'placa'
            ])
            ->addColumn('rastreador_bloqueador', 'boolean', [
                'default' => 0,
                'null'    => false,
            ])
            ->addColumn('acessorios_originais', 'boolean', [
                'default' => 0,
                'null'    => false,
            ])
            ->addColumn('descricao', 'text', [
                'null'    => true,
            ])
            ->addColumn('descricao_log', 'text', [
                'null'    => true,
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();

        $this->table('opcionais_veiculos')
            ->addColumn('opcional_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('veiculo_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->create();
    }

    public function down()
    {
        $this->dropTable('enderecos');
        $this->dropTable('tipo_veiculos');
        $this->dropTable('modelos');
        $this->dropTable('tipo_vendas');
        $this->dropTable('marcas');
        $this->dropTable('fabricantes');
        $this->dropTable('estados');
        $this->dropTable('cidades');
        $this->dropTable('opcionais');
        $this->dropTable('proprietarios');
        $this->dropTable('opcionais_veiculos');
        $this->dropTable('clientes');
        $this->dropTable('veiculos');
    }
}
