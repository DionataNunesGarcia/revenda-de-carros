<?php
use Migrations\AbstractMigration;

class CreateTableContasPagar extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->table('fornecedores')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('cnpj', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('anotacoes', 'text', [
                'null' => true,
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('contas_pagar')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('status', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('valor', 'decimal', [
                'default' => null,
                'null' => false,
                'precision' => 10,
                'scale' => 2,
            ])
            ->addColumn('model', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('data_vencimento', 'date', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('data_validade', 'date', [
                'default' => null,
                'null' => false,
            ])
            ->addColumn('data_pagamento', 'date', [
                'default' => null,
                'null' => true,
            ])
            ->addColumn('anotacoes', 'text', [
                'null' => true,
            ])
            ->addColumn('boleto', 'string', [
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('nota_fiscal', 'string', [
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('comprovante_pagamento', 'string', [
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->dropTable('contas_pagar');
        $this->dropTable('fornecedores');
    }
}
