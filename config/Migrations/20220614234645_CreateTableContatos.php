<?php
use Migrations\AbstractMigration;

class CreateTableContatos extends AbstractMigration
{
    public function up()
    {
        $this->table('contatos')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('assunto', 'text', [
                'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_REGULAR,
                'null' => false,
            ])
            ->addColumn('mensagem', 'text', [
                'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_LONG,
                'null' => false,
            ])
            ->addColumn('veiculo_id', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('criado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modificado', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->create();
    }

    public function down()
    {
        $this->dropTable('contatos');
    }
}
