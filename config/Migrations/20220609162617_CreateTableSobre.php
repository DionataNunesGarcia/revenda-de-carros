<?php
use Migrations\AbstractMigration;

class CreateTableSobre extends AbstractMigration
{
    public function up()
    {

        $this->table('sobre')
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('celular', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('celular_2', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('facebook', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('instagram', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('linkedin', 'string', [
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('sobre', 'text', [
                'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_LONG,
                'null' => false,
            ])
            ->addColumn('visao', 'text', [
                'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_LONG,
                'null' => false,
            ])
            ->addColumn('missao', 'text', [
                'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_LONG,
                'null' => false,
            ])
            ->addColumn('valores', 'text', [
                'limit' => \Phinx\Db\Adapter\MysqlAdapter::TEXT_LONG,
                'null' => false,
            ])
            ->create();
    }

    public function down()
    {
        $this->dropTable('sobre');
    }
}
