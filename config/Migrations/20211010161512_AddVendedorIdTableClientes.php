<?php
use Migrations\AbstractMigration;

class AddVendedorIdTableClientes extends AbstractMigration
{
    public function up()
    {
        $this->table('clientes')
            ->addColumn('vendedor_id', 'integer', [
                'null' => true,
                'after' => 'email'
            ])
            ->update();
    }

    public function down()
    {
        $this->table('veiculos')
            ->removeColumn('vendedor_id')
            ->update();
    }
}
