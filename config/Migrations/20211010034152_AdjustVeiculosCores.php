<?php
use Migrations\AbstractMigration;

class AdjustVeiculosCores extends AbstractMigration
{
    public function up()
    {
        $this->table('veiculos')
            ->addColumn('cor_id', 'integer', [
                'null' => false,
                'default' => null,
                'after' => 'cidade_id'
            ])
            ->addColumn('usuario_id', 'integer', [
                'null' => false,
                'default' => null,
                'after' => 'cor_id'
            ])
            ->addForeignKey(
                'cor_id',
                'cores',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'usuario_id',
                'usuarios',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('veiculos')
            ->dropForeignKey('cor_id')
            ->removeColumn('cor_id')
            ->dropForeignKey('usuario_id')
            ->removeColumn('usuario_id')
            ->update();
    }
}
