<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class AddModelos extends AbstractMigration
{
    private $path = WWW_ROOT . 'arquivos_sistema' . DS . 'importacoes' . DS;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->importModelos('modelos-caminhao.csv', true);
        $this->importModelos('modelos-carro.csv');
        $this->importModelos('modelos-moto.csv');
        $this->importModelos('modelos-nautica.csv');
    }

    /**
     * @param $name
     * @param false $truncate
     * @throws Exception
     */
    private function importModelos($name, $truncate = false)
    {
        $table = TableRegistry::getTableLocator()
            ->get('Modelos');

        if ($truncate) {
            $table->deleteAll(['id !=' => 0]);

            $connection = $table->getConnection();
            $connection->query("ALTER TABLE modelos AUTO_INCREMENT = 1;");
        }

        $csv = readCSV($this->path . $name);
        foreach ($csv as $key => $data) {
            if ($key == 0) {
                continue;
            }

            if (!isset($data[1]) || !$data[0]) {
                continue;
            }

            $entity = $table->newEntity();
            $entity->id = intval($data[0]);
            $entity->marca_id = intval($data[1]);
            $entity->nome = $data[2] !== '' ? utf8_encode($data[2]) : '  ';

            if(!$table->save($entity)) {
                throw new \Exception('Error in save modelo', 400);
            }
        }
    }
}
