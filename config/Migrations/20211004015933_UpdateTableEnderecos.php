<?php
use Migrations\AbstractMigration;

class UpdateTableEnderecos extends AbstractMigration
{
    /**
     *
     */
    public function up()
    {
        $this->table('enderecos')
            ->changeColumn('model', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->changeColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->changeColumn('complemento', 'string', [
                'limit' => 255,
                'null' => true,
            ])
            ->update();

        $this->table('estados')
            ->addColumn('uf', 'string', [
                'limit' => 2,
                'null' => false,
                'after' => 'nome'
            ])
            ->update();
    }

    public function down()
    {
        $this->table('enderecos')
            ->changeColumn('model', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->changeColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->changeColumn('complemento', 'string', [
                'limit' => 255,
                'null' => true,
            ])
            ->update();

        $this->table('estados')
            ->removeColumn('uf')
            ->update();

    }
}
