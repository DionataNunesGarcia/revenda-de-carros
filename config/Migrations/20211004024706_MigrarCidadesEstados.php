<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class MigrarCidadesEstados extends AbstractMigration
{
    private $path = WWW_ROOT . 'arquivos_sistema' . DS . 'importacoes' . DS;
    private $estadoTable;
    private $cidadeTable;

    /**
     * @throws Exception
     */
    public function change()
    {
        $this->estadoTable = TableRegistry::getTableLocator()
            ->get('Estados');
        $this->cidadeTable = TableRegistry::getTableLocator()
            ->get('Cidades');

        $this->importEstados();
        $this->importCidades();
    }

    /**
     * @throws Exception
     */
    private function importEstados()
    {
        $this->cidadeTable->deleteAll(['id !=' => 0]);
        $this->estadoTable->deleteAll(['id !=' => 0]);

        $connection = $this->estadoTable->getConnection();
        $connection->query("ALTER TABLE estados AUTO_INCREMENT = 1;");
        $connection->query("ALTER TABLE cidades AUTO_INCREMENT = 1;");

        $csv = readCSV($this->path . 'states.csv', ',');
        foreach ($csv as $key => $data) {

            if ($key == 0) {
                continue;
            }

            if (!isset($data[1]) || !$data[0]) {
                continue;
            }

            $entity = $this->estadoTable->newEntity();

            $entity->id = $data[0];
            $entity->nome = $data[1];
            $entity->uf = $data[2];
            $entity->criado = \Cake\I18n\Time::now();
            $entity->modificado = \Cake\I18n\Time::now();

            if(!$this->estadoTable->save($entity)) {
                throw new \Exception('Error in save state', 400);
            }
        }
    }

    /**
     * @throws Exception
     */
    private function importCidades()
    {
        $csv = readCSV($this->path . 'cities.csv', ',');

        foreach ($csv as $key => $data) {
            if ($key == 0) {
                continue;
            }

            if (!isset($data[1]) || !$data[0]) {
                continue;
            }

            $estado = $this->estadoTable->get($data[1]);

            $entity = $this->cidadeTable->newEntity();

            $entity->nome = $data[2];
            $entity->uf = $estado->uf;
            $entity->estado_id = $estado->id;
            $entity->criado = \Cake\I18n\Time::now();
            $entity->modificado = \Cake\I18n\Time::now();

            if(!$this->cidadeTable->save($entity)) {
                throw new \Exception('Error in save marca', 400);
            }
        }
    }
}
