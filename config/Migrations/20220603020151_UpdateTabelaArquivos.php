<?php
use Migrations\AbstractMigration;

class UpdateTabelaArquivos extends AbstractMigration
{
    public function up()
    {
        $this->table('arquivos')
            ->removeColumn('mais_parametros')
            ->removeColumn('tentativa_importacao')
            ->removeColumn('importando')
            ->removeColumn('inicio_importacao')
            ->removeColumn('importado')
            ->removeColumn('final_importacao')
            ->removeColumn('observacoes')
            ->removeColumn('observacoes_erro')
            ->addColumn('extensao', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
                'after' => 'tipo',
            ])
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
                'after' => 'titulo',
            ])
            ->addColumn('model', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
                'after' => 'tipo',
            ])
            ->addColumn('foreign_key', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
                'after' => 'tipo',
            ])
            ->addColumn('ordem', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
                'after' => 'tipo',
            ])
            ->addColumn('tamanho', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
                'after' => 'tipo',
            ])
            ->addColumn('capa', 'boolean', [
                'default' => 0,
                'null'    => false,
                'after' => 'tipo',
            ])
            ->update();
    }

    public function down()
    {
        $this->table('arquivos')
            ->removeColumn('model')
            ->removeColumn('foreign_key')
            ->removeColumn('ordem')
            ->removeColumn('tamanho')
            ->removeColumn('extensao')
            ->removeColumn('descricao')
            ->addColumn('tentativa_importacao', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('importando', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('inicio_importacao', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('importado', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('final_importacao', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('observacoes', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->addColumn('observacoes_erro', 'integer', [
                'null' => true,
                'default' => null,
            ])
            ->update();
    }
}
