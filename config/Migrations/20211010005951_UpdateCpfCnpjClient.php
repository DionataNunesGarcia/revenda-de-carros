<?php
use Migrations\AbstractMigration;

class UpdateCpfCnpjClient extends AbstractMigration
{
    /**
     *
     */
    public function up()
    {
        $this->table('clientes')
            ->changeColumn('cpf_cnpj', 'string', [
                'limit' => 15,
                'null' => true,
            ])
            ->changeColumn('celular', 'string', [
                'limit' => 15,
                'null' => true,
            ])
            ->changeColumn('telefone', 'string', [
                'limit' => 15,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {
        $this->table('clientes')
            ->changeColumn('cpf_cnpj', 'string', [
                'limit' => 11,
                'null' => false,
            ])
            ->changeColumn('celular', 'string', [
                'limit' => 15,
                'null' => false,
            ])
            ->changeColumn('telefone', 'string', [
                'limit' => 15,
                'null' => false,
            ])
            ->update();

    }
}
