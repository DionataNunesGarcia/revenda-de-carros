<?php
use Migrations\AbstractMigration;

class AddTableCores extends AbstractMigration
{
    public function up()
    {
        $this->table('cores')
            ->addColumn('codigo', 'string', [
                'limit' => 100,
                'null' => false,

            ])
            ->addColumn('nome', 'string', [
                'limit' => 255,
                'null' => false,

            ])
            ->addColumn('tipo_pintura', 'enum', [
                'null' => false,
                'default' => 'solida',
                'values' => [
                    'solida',
                    'metalica',
                    'perolizada',
                    'repintura',
                ]
            ])
            ->addColumn('cor', 'string', [
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('cor_denatran', 'enum', [
                'limit' => 255,
                'null' => false,
                'values' => [
                    '01-AMARELO',
                    '02-AZUL',
                    '03-BEGE',
                    '04-BRANCA',
                    '05-CINZA',
                    '06-DOURADA',
                    '07-GRENA',
                    '08-LARANJA',
                    '09-MARROM',
                    '10-PRATA',
                    '11-PRETA',
                    '12-ROSA',
                    '13-ROXA',
                    '14-VERDE',
                    '15-VERMELHA',
                    '16-FANTASIA',
                ]
            ])
            ->create();
    }

    public function down()
    {
        $this->dropTable('cores');
    }
}
