<?php
namespace App\Service\RelatoriosMatinais;

use App\Service\BaseRelatoriosService;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Service\CalculosGeraisSevice\CalculoClientesService;

class PlanejamentoVendedorService extends BaseRelatoriosService
{
    private $_vendedoresTable;
    private $_clientesTable;
    private $_pedidosTable;
    private $_pedidosHistoricosTable;
    private $_metasGeraisTable;
    private $_metasTable;

    private $vendedorId;
    private $vendedor;
    private $rotasVendedor;
    private $faturamentosVendedor;
    private $metasGeraisVendedor;
    private $metasVendedor;
    private $pastas;
    private $ultimosMeses;

    private $superior;
    private $inferior;
    private $clientes;
    private $cliente;

    public function __construct()
    {
        parent::__construct();
        $this->_vendedoresTable = TableRegistry::getTableLocator()->get('Vendedores');
        $this->_clientesTable = TableRegistry::getTableLocator()->get('Clientes');
        $this->_pedidosTable = TableRegistry::getTableLocator()->get('Pedidos');
        $this->_pedidosHistoricosTable = TableRegistry::getTableLocator()->get('PedidosHistoricos');
        $this->_metasGeraisTable = TableRegistry::getTableLocator()->get('MetasGerais');
        $this->_metasTable = TableRegistry::getTableLocator()->get('Metas');
    }

    public function setValores()
    {
        $this->setVendedor();
        $this->setMetasGeraisVendedor();
        $this->setMetasVendedor();
        $this->setUltimosMeses();
        $this->setClientes();
        $this->setSuperior();
        $this->setInferior();
    }

    public function getValores()
    {
        return [
            'lastUpdate' => $this->getLastUpdate(),
        ];
    }

    /**
     * @return mixed
     */
    public function getVendedorId()
    {
        return $this->vendedorId;
    }

    /**
     * @param mixed $vendedorId
     */
    public function setVendedorId($vendedorId): void
    {
        $this->vendedorId = $vendedorId;
    }

    /**
     * @return mixed
     */
    public function getPastas()
    {
        return $this->pastas;
    }

    /**
     * @param mixed $pastas
     */
    public function setPastas($pastas): void
    {
        $this->pastas = explode(',', $pastas);
    }

    /**
     * @return mixed
     */
    public function getSuperior()
    {
        return $this->superior;
    }

    /**
     *
     */
    public function setSuperior(): void
    {
        $this->setRotasVendedor();
        $this->setFaturamentosVendedor();

        $this->superior = [
            'data' => $this->getData(),
            'vendedor' => $this->getVendedor(),
            'rotasVendedor' => $this->getRotasVendedor(),
            'faturamentosVendedor' => $this->getFaturamentosVendedor(),
//            'volumesVendedor' => $this->getVolumesVendedor(),
            'totalDiasUteis' => $this->getTotalDiasUteis(),
            'totalDiasUteisMes' => $this->getTotalDiasUteisMes(),
        ];
    }

    /**
     * @return mixed
     */
    public function getInferior()
    {
        return $this->inferior;
    }

    /**
     *
     */
    public function setInferior(): void
    {
        $this->inferior = [
            'clientes' => $this->getClientes(),
            'ultimosMeses' => $this->ultimosMeses,
        ];
    }

    /**
     * @return mixed
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor(): void
    {
        $vendedor = $this->_vendedoresTable
            ->find()
            ->contain([
                'PessoaFisica'
            ])
            ->where([
                'Vendedores.id' => $this->getVendedorId(),
            ])
            ->first();

        $this->vendedor = $vendedor;
    }

    /**
     * @return mixed
     */
    public function getClientes()
    {
        return $this->clientes;
    }

    /**
     *
     */
    public function setClientes(): void
    {
        $clientesQuery = $this->_clientesTable
            ->find('list', [
                'keyField' => function($q) {},
                'valueField' => function($q) {
                    $this->setCliente($q);

                    $this->verificaClienteCoberto();
                    $this->verificaUltimosFaturamentos();
                    $this->setOutrosValoresCliente();

                    return $q;
                },
            ])
            ->contain([
                'PessoasJuridicas',
                'CategoriasVarejos.Demps'
            ])
            ->where([
                'vendedores_id' => $this->getVendedorId(),
                'pasta IN' => $this->getPastas(),
            ]);

        $this->clientes = $clientesQuery->toArray();
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente): void
    {
        $this->cliente = $cliente;
    }

    /**
     * @param $clientesQuery
     */
    private function setRotasVendedor()
    {
        $pdvs = 0;
        $naoCoberto = 0;
        $instaveis = 0;
        $estaveis = 0;
        $semCompras = 0;
        foreach ($this->clientes as $cliente) {
            //quando não e bloqueado, incrementa um
            if(empty($cliente->data_bloqueio)) {
                $pdvs++;
            }
            //quando foi coberto no mes atual, incrementa um
            if(!$cliente->coberto) {
                $naoCoberto++;
            }
            //FaturamentosAnteriores
            if($this->cliente->faturamentosAnteriores == 'Sem compra'){
                $instaveis++;
            }
            if($this->cliente->faturamentosAnteriores == 'Instáveis'){
                $estaveis++;
            }
            if($this->cliente->faturamentosAnteriores == 'Estáveis'){
                $semCompras++;
            }
        }

        $this->rotasVendedor = [
            'pdvs' => $pdvs,
            'naoCoberto' => $naoCoberto,
            'instaveis' => $instaveis,
            'estaveis' => $estaveis,
            'semCompras' => $semCompras,
        ];
    }

    /**
     * @return mixed
     */
    public function getRotasVendedor()
    {
        return $this->rotasVendedor;
    }

    private function verificaClienteCoberto()
    {
        $this->cliente->coberto = $this->_pedidosTable
            ->exists([
                'Pedidos.ocorrencia' => 1,
                'Pedidos.quantidade >=' => 1,
                'Pedidos.cliente_id' => $this->cliente->id,
            ]);
    }

    public function setUltimosMeses() {
        $data1 = Time::now();
        $data2 = Time::now();
        $data3 = Time::now();

        $umMesAnterior = $data1->modify('-1 month');
        $doisMesesAnteriores = $data2->modify('-2 month');;
        $tresMesesAnteriores = $data3->modify('-3 month');;

        $this->ultimosMeses = [
            1 => $umMesAnterior,
            2 => $doisMesesAnteriores,
            3 => $tresMesesAnteriores,
        ];
    }

    private function totalFaturamento(int $mes, $conditionsParam = [])
    {
        $conditionsFixed = [
            'cliente_id' => $this->getCliente()->id,
            'data_pedido >=' => $this->ultimosMeses[$mes]->i18nFormat('yyyy-MM-01'),
            'data_pedido <=' => $this->ultimosMeses[$mes]->modify('last day of this month')->i18nFormat('yyyy-MM-dd'),
            'ocorrencia' => 1,
            'Produtos.considera_relatorios' => true
        ];

        if (!empty($conditionsParam)) {
            $conditionsFixed = array_merge($conditionsFixed, $conditionsParam);
        }

        $query = $this->_pedidosHistoricosTable
            ->find()
            ->contain([
                'Produtos.Familias'
            ])
            ->where($conditionsFixed);

        return $query
            ->select([
                'total' => $query->func()->sum('valor')
            ])
            ->sumOf('total');
    }

    private function verificaUltimosFaturamentos()
    {
        $this->setUltimosFaturamentos();
        $compras = 0;
        if($this->cliente->ultimoMes > 0){
            $compras++;
        }
        if($this->cliente->penultimoMes > 0){
            $compras++;
        }
        if($this->cliente->antePenultimoMes > 0){
            $compras++;
        }

        switch ($compras) {
            case 0:
                $this->cliente->faturamentosAnteriores = 'Sem compra';
                break;
            case 1 || 2:
                $this->cliente->faturamentosAnteriores = 'Instáveis';
                break;
            case 3:
                $this->cliente->faturamentosAnteriores = 'Estáveis';
                break;
        }
    }

    private function setUltimosFaturamentos($conditions = [])
    {
        $this->cliente->ultimoMes = $this->totalFaturamento(1, $conditions);
        $this->cliente->penultimoMes = $this->totalFaturamento(2, $conditions);
        $this->cliente->antePenultimoMes = $this->totalFaturamento(3, $conditions);
    }

    /**
     * @return mixed
     */
    public function getFaturamentosVendedor()
    {
        return $this->faturamentosVendedor;
    }

    /**
     *
     */
    public function setMetasGeraisVendedor(): void
    {
        $metasGeraisVendedor = $this->_metasGeraisTable
            ->find()
            ->contain([
                'MetasGeraisPessoas',
            ])
            ->where([
                'MetasGerais.mes_ano' => Time::now()->i18nFormat('yyyy-MM-01'),
                'MetasGeraisPessoas.vendedores_id' => $this->getVendedorId(),
            ])
            ->first();

        if (!$metasGeraisVendedor) {
            throw new \Exception(__("O vendedor '{$this->vendedor->pessoaFisica->nome}' não possuí Metas Gerais no mês atual "));
        }

        $this->metasGeraisVendedor = $metasGeraisVendedor;
    }

    /**
     *
     */
    public function setMetasVendedor(): void
    {
        $metasVendedor = $this->_metasTable
            ->find()
            ->contain([
                'MetasPessoas',
            ])
            ->where([
                'Metas.mes_ano' => Time::now()->i18nFormat('yyyy-MM-01'),
                'MetasPessoas.vendedores_id' => $this->getVendedorId(),
            ])
            ->first();

        if (!$metasVendedor) {
            throw new \Exception(__("O vendedor '{$this->vendedor->pessoaFisica->nome}' não possuí Metas no mês atual "));
        }

        $this->metasVendedor = $metasVendedor;
    }

    /**
     *
     */
    public function setFaturamentosVendedor(): void
    {
        $total = $this->getFaturamentoTotal();
        $terceiros = $this->getFaturamentoTerceirosReal();
        $this->faturamentosVendedor = [
            'total' => $total,
            'retornaveis' => $this->getFaturamentoRetornaveis(),
            'terceiros_meta' => $this->getFaturamentoTerceirosMeta(),
            'terceiros_real' => $terceiros,
            'devolucoes_meta' => $this->getFaturamentoDevolucoesMeta(),
            'devolucoes_real' => $this->getFaturamentoDevolucoesReal($total, $terceiros),
            'trocas_meta' => $this->getFaturamentoTrocasMeta(),
            'trocas_real' => $this->getFaturamentoTrocasReal(),
        ];
    }

    /**
     * @return float
     */
    private function getFaturamentoTotal()
    {
        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos.Familias',
            ])
            ->where([
                'Pedidos.ocorrencia' => 1,
                'Clientes.vendedores_id' => $this->getVendedorId(),
                'Familias.grupo_id' => 1,
                'Pedidos.data_pedido >=' => Time::now()->i18nFormat('yyyy-MM-01'),
                'Pedidos.data_pedido <=' => Time::now()->modify('last day of this month')->i18nFormat('yyyy-MM-dd'),
                'Produtos.considera_relatorios' => true
            ]);

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.valor')
            ])
            ->sumOf('total');
    }

    /**
     * @return float
     */
    private function getFaturamentoRetornaveis()
    {
        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos',
            ])
            ->where([
                'Pedidos.ocorrencia' => 1,
                'Clientes.vendedores_id' => $this->getVendedorId(),
                'Produtos.familia_id IN' => [1, 2, 4],
                'Pedidos.data_pedido >=' => Time::now()->i18nFormat('yyyy-MM-01'),
                'Pedidos.data_pedido <=' => Time::now()->modify('last day of this month')->i18nFormat('yyyy-MM-dd'),
                'Produtos.considera_relatorios' => true
            ]);

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.valor')
            ])
            ->sumOf('total');
    }

    /**
     * @return float
     */
    private function getFaturamentoTerceirosMeta()
    {
        return $this->metasGeraisVendedor->meta_finan_terc;
    }

    /**
     * @return float
     */
    private function getFaturamentoTerceirosReal()
    {
        $condicoes['Clientes.vendedores_id'] = $this->vendedor->id;
        $condicoes['Pedidos.data_pedido >='] = Time::now()->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = Time::now()
            ->modify('last day of this month')
            ->i18nFormat('yyyy-MM-dd');

        $condicoes['Pedidos.ocorrencia IN'] = [1];
        $condicoes['Familias.grupo_id IN'] = [2];
        $condicoes['Produtos.considera_relatorios'] = true;

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos.Familias',
            ])
            ->where($condicoes)
            ->group('Pedidos.cliente_id');

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.valor')
            ])
            ->sumOf('total');
    }

    /**
     * @return float
     */
    private function getFaturamentoDevolucoesMeta()
    {
        return $this->metasGeraisVendedor->meta_dev;
    }

    /**
     * @return float
     */
    private function getFaturamentoDevolucoesReal($totalFaturamento, $terceiros)
    {
        $condicoes['Clientes.vendedores_id'] = $this->vendedor->id;
        $condicoes['Pedidos.data_pedido >='] = Time::now()->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = Time::now()
            ->modify('last day of this month')
            ->i18nFormat('yyyy-MM-dd');

        //PEDIDOS/OCORRENCIA = (1)
        $condicoes['Pedidos.ocorrencia IN'] = [1];

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
            ])
            ->where($condicoes)
            ->group('Pedidos.cliente_id');

        //[ (VALOR DEVOLUCOES) / (VALOR DEVOLUCOES + Faturamento total + Faturamento Terceiros) ] * 100
        //Apresentando assim o percentual de devolução 0.00%.
        $totalDevolucoes = $query
            ->select([
                'total' => $query->func()->sum('Pedidos.devolucao')
            ])
            ->sumOf('total');
        if ($totalDevolucoes == 0 || ($totalDevolucoes + $totalFaturamento + $terceiros) == 0) {
            return 0;
        }
        return ($totalDevolucoes / ($totalDevolucoes + $totalFaturamento + $terceiros) * 100);
    }

    /**
     * @return float
     */
    private function getFaturamentoTrocasMeta()
    {
        return $this->metasGeraisVendedor->meta_finan_troca;
    }

    /**
     * @return float
     */
    private function getFaturamentoTrocasReal()
    {
        $condicoes['Clientes.vendedores_id'] = $this->vendedor->id;
        $condicoes['Pedidos.data_pedido >='] = Time::now()->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = Time::now()
            ->modify('last day of this month')
            ->i18nFormat('yyyy-MM-dd');

        $condicoes['Pedidos.ocorrencia IN'] = [3];

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
            ])
            ->where($condicoes)
            ->group('Pedidos.cliente_id');

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.quantidade * Pedidos.preco_tabela')
            ])
            ->sumOf('total');
    }

    /**
     *
     */
    private function setOutrosValoresCliente()
    {
        $calculosService = new CalculoClientesService();

        $calculosService->setVendedorId($this->getVendedor()->id);
        $calculosService->setTotalHlClientes();
        $this->vendedor->totalHlClientes = $calculosService->getTotalHlClientes();

        $calculosService->setCliente($this->cliente);

        $this->cliente->inadimplencia = $calculosService->getInadimplencia();
        $this->cliente->top50 = $calculosService->getTop50();
        $this->cliente->top50Ret = $calculosService->getTop50Ret();

        $this->cliente->coberturas = $calculosService->getCoberturas();

        $this->cliente->fds = $calculosService->fdsCliente();

        $this->cliente->materiais = $calculosService->materiais();
    }

    private function getVolumesVendedor()
    {
        return [
            'hlTotal' => $this->getVolumeHlTotal(),
//            'retornavel' => $this->getVolumeRetornavel(),
        ];
    }

    private function getVolumeHlTotal()
    {
        $meta =  $this->metasVendedor->produto_meta;

        return [
            'meta' => $meta . '*',//verificar Regra 8
            'real' => '*',
            'tend' => '*',
            'necDia' => '*',
            'planejamento' => '*',
        ];
    }

    private function getHlTotalReal()
    {
//        $condicoes = $this->getCondicoesNomenclaturas('fund-volume-total');
//        $query = $this->getQueryReal($condicoes);
//        $valorMetaHl = $query
//            ->select([
//                'meta_hl' => $query->func()->sum('Familias.hl * Pedidos.quantidade'),
//            ])
//            ->sumOf('meta_hl');
//
//        return $valorMetaHl;
    }

    /**
     * @return float|int
     */
    public function getMediaUltimosMesesMateriais()
    {
        $this->setUltimosMeses();

        $conditions = [
            'Familias.grupo_id IN' => [1]
        ];
        $this->setUltimosFaturamentos($conditions);

        return ($this->cliente->ultimoMes +
                $this->cliente->penultimoMes +
                $this->cliente->antePenultimoMes) / 3;
    }

    /**
     * @return float|int
     */
    public function getMediaUltimosMesesChoperia()
    {
        $this->setUltimosMeses();

        $conditions = [
            'Familias.id IN' => [20, 21]
        ];
        $this->setUltimosFaturamentos($conditions);

        return ($this->cliente->ultimoMes +
                $this->cliente->penultimoMes +
                $this->cliente->antePenultimoMes) / 3;
    }

    /**
     * @return float|int
     */
    public function getMediaUltimosMesesVasilhames()
    {
        $this->setUltimosMeses();

        $conditions = [
            'Familias.id IN' => [1, 2]
        ];
        $this->setUltimosFaturamentos($conditions);

        return ($this->cliente->ultimoMes +
                $this->cliente->penultimoMes +
                $this->cliente->antePenultimoMes) / 3;
    }
}
