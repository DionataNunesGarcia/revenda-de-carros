<?php
namespace App\Service\RelatoriosMatinais;

use App\Service\BaseRelatoriosService;
use Cake\ORM\TableRegistry;

/**
 * Relatorio de Fundamentos volume
 * Cálculos https://docs.google.com/document/d/1SIobjESjVIh2R-Pak91TL3zlCLiKnHCe5B8DZfneQi0/edit
 *
 * Class FundamentosVolumeService
 * @package App\Service\RelatoriosMatinais
 */

class FundamentosVolumeService extends BaseRelatoriosService
{
    private $vendedoresFrio;
    private $vendedoresAsi;
    private $vendedoresPremium;
    private $vendedor;

    private $_vendedoresTable;
    private $_produtosTable;
    private $_pedidosTable;
    private $_metasTable;

    public function __construct()
    {
        parent::__construct();
        $this->_vendedoresTable = TableRegistry::getTableLocator()->get('Vendedores');
        $this->_produtosTable = TableRegistry::getTableLocator()->get('Produtos');
        $this->_pedidosTable = TableRegistry::getTableLocator()->get('Pedidos');
        $this->_metasTable = TableRegistry::getTableLocator()->get('Metas');
    }

    /**
     * @return array
     */
    public function getValores()
    {
        $this->setVendedoresFrio();
        $this->setVendedoresAsi();
        $this->setVendedoresPremium();
        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'vendedoresFrio' => $this->getVendedoresFrio(),
            'vendedoresAsi' => $this->getVendedoresAsi(),
            'vendedoresPremium' => $this->getVendedoresPremium(),
        ];
    }

    /**
     * @param string $tipo
     * @return array
     */
    private function getVendedores(string $tipo)
    {
        return $this->_vendedoresTable
            ->find('list', [
                'valueField' => function($vendedor) use ($tipo){
                    $this->setVendedor($vendedor);
                    $this->setValoresVendedor();

                    return $this->getVendedor();
                },
                'keyField' => function($q){}
            ])
            ->contain([
                'PessoaFisica'
            ])
            ->where([
                'Vendedores.canal' => $tipo,
                'Vendedores.relatorios' => true,
                'Vendedores.considera_relatorios' => true
            ])
            ->toArray();
    }

    /**
     * @return mixed
     */
    public function getVendedoresFrio()
    {
        return $this->vendedoresFrio;
    }

    /**
     *
     */
    public function setVendedoresFrio(): void
    {
        $this->vendedoresFrio = $this->getVendedores('Frio');
    }

    /**
     * @return mixed
     */
    public function getVendedoresAsi()
    {
        return $this->vendedoresAsi;
    }

    /**
     * @return mixed
     */
    public function getVendedoresPremium()
    {
        return $this->vendedoresPremium;
    }

    /**
     *
     */
    public function setVendedoresAsi(): void
    {
        $this->vendedoresAsi = $this->getVendedores('ASI');
    }

    /**
     *
     */
    public function setVendedoresPremium(): void
    {
        $this->vendedoresPremium = $this->getVendedores('Premium');
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor($vendedor): void
    {
        $this->vendedor = $vendedor;
    }

    /**
     * @return mixed
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     *
     */
    private function setValoresVendedor()
    {
        $this->vendedor->valores = [
            'trezentosSeicentosMil' => $this->getValoresPorSlug('600ml-+-1000ml-+-300ml'),
            'premium' => $this->getValoresPorSlug('funda-premium'),
            'volumeTotal' => $this->getValoresPorSlug('fund-volume-total'),
            'coringa1' => $this->getValoresPorSlug('fund-coringa1'),
            'coringa2' => $this->getValoresPorSlug('fund-coringa2'),
        ];
    }

    /**
     * @param string $slug
     * @return array
     */
    public function getValoresPorSlug(string $slug)
    {
        $condicoes = $this->getCondicoesNomenclaturas($slug);

        $meta = $this->getValorMeta($condicoes);
        $real = $this->getValorReal($condicoes);

        return [
            'meta' => round($meta, 1),
            'real' => round($real, 1),
            'porcentagem' => round($this->getPorcentagem($meta, $real), 1),
        ];
    }

    /**
     * @return mixed
     */
    private function getValorMeta(array $condicoes)
    {
        $query = $this->getQueryMetas($condicoes);
        $valorMetaHl = $query
            ->select([
                'meta' => $query->func()->sum('Familias.hl * Metas.produto_meta')
            ])
            ->sumOf('meta');

        return $valorMetaHl;
    }

    /**
     * @return mixed
     */
    private function getValorReal(array $condicoes)
    {
        $query = $this->getQueryReal($condicoes);
        $valorMetaHl = $query
            ->select([
                'meta_hl' => $query->func()->sum('Familias.hl * Pedidos.quantidade'),
                'hl_total' => $query->func()->sum('Familias.hl'),
                'quantidade_total' => $query->func()->sum('Pedidos.quantidade'),
            ])
            ->sumOf('meta_hl');

        return $valorMetaHl;
    }

    /**
     * @param array $condicoes
     * @return array|\Cake\ORM\Query
     */
    public function getQueryReal(array $condicoes)
    {
        $condicoes['Pedidos.data_pedido >='] = $this->getData()->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = $this->getData()->modify('last day of this month')->i18nFormat('yyyy-MM-dd');
        $condicoes['Clientes.vendedores_id'] = $this->vendedor->id;

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos' => [
                    'Familias.Categorias',
                    'Marcas',
                ],
            ])
            ->where($condicoes)
            ->group('Clientes.vendedores_id');

        return $query;
    }

    /**
     * @param array $condicoes
     * @return array|\Cake\ORM\Query
     */
    public function getQueryMetas(array $condicoes)
    {
        if (isset($condicoes['ocorrencia IN'])) {
            unset($condicoes['ocorrencia IN']);
        }
        $condicoes['Metas.mes_ano'] = $this->getData()->i18nFormat('yyyy-MM-01');
        $condicoes['MetasPessoas.vendedores_id'] = $this->vendedor->id;

        $query = $this->_metasTable
            ->find()
            ->contain([
                'MetasPessoas',
                'Produtos' => [
                    'Familias.Categorias',
                    'Marcas',
                ],
            ])
            ->where($condicoes)
            ->group('MetasPessoas.vendedores_id');

        return $query;
    }
}
