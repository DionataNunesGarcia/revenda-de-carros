<?php

namespace App\Service\RelatoriosMatinais;

use App\Model\Entity\Ranking;
use App\Service\BaseRelatoriosService;
use Cake\ORM\TableRegistry;

class RankingVendedoresService extends BaseRelatoriosService
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @var Ranking $ranking
     */
    private $ranking;

    /**
     * @return array
     */
    public function getValores()
    {
        $this->setRanking();

        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'ranking' => $this->getRanking(),
        ];
    }

    /**
     * @return mixed
     */
    public function getRanking()
    {
        return $this->ranking;
    }

    /**
     *
     */
    public function setRanking(): void
    {
        $data = $this->getData();
        $ranking = TableRegistry::getTableLocator()
            ->get("Ranking")
            ->find()
            ->contain([
                'Vendedores.PessoaFisica',
            ])
            ->where([
                'Ranking.mes_ano' => $data->i18nFormat('yyyy-MM-01'),
                'Vendedores.relatorios' => true,
                'Vendedores.considera_relatorios' => true
            ])
            ->order([
                'pontos' => 'DESC',
                'volume_300_600_1000' => 'DESC',
            ])
            ->toArray();

        $this->ranking = $ranking;
    }
}
