<?php

namespace App\Service\AtualizarRanking;

use App\Service\BaseService;
use App\Service\RelatoriosCoberturas\CoberturasGeraisService;
use App\Service\RelatoriosCoberturas\FdsVendedoresService;
use App\Service\RelatoriosMatinais\FundamentosVolumeService;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class AtualizarRankingVendedoresService extends BaseService
{
    private $_tableRanking;
    private $_tableVendedores;
    private $_tableMetasGerais;
    private $_coberturasGeraisService;

    private $mesAnoAtual;
    private $vendedor;
    private $metasGerais;

    public function __construct()
    {
        parent::__construct();

        $this->_tableRanking = TableRegistry::getTableLocator()->get("Ranking");
        $this->_tableVendedores = TableRegistry::getTableLocator()->get("Vendedores");
        $this->_tableMetasGerais = TableRegistry::getTableLocator()->get("MetasGerais");
        $this->_coberturasGeraisService = new CoberturasGeraisService();

        $this->mesAnoAtual = Time::now()->i18nFormat('yyyy-MM-01');
    }

    public function atualizaRankingVendedores()
    {
        $this->deletarMesAtual();
        foreach ($this->getVendedores() as $vendedor) {
            $this->setVendedor($vendedor);
            $this->setMetasGerais();

            $this->criarRankingVendedor();
        }
    }

    private function deletarMesAtual()
    {
        $this->_tableRanking
            ->deleteAll([
                'mes_ano' => $this->mesAnoAtual
            ]);
    }

    private function getVendedores()
    {
        return $this->_tableVendedores
            ->find()
            ->where([
                'relatorios' => true
            ])
            ->toArray();
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor($vendedor): void
    {
        $this->vendedor = $vendedor;
    }

    private function criarRankingVendedor()
    {
        $entidade = $this->_tableRanking->newEntity();

        $entidade->vendedores_id = $this->vendedor->id;
        //TODO @pablo e @dionata Colocar calculo do FDS
        $entidade->cob_fds = $this->getCobFds();
        $entidade->cob_total = $this->getCobTotal();
        $entidade->cob_devassa = $this->cobDevassa();
        $entidade->cob_eisenbahn = $this->cobEisenbahn();
        $entidade->volume_300_600_1000 = $this->volume3006001000();
        $entidade->cob_esp = 0;
        $entidade->cob_tendencia = 0;
        $entidade->tendencia = 0;//$this->getTendencia();
        $entidade->cob_retornaveis = 0;//$this->cobRetornaveis();
        $entidade->pontos = $this->getPontos($entidade);
        $entidade->mes_ano = $this->mesAnoAtual;

        if (!$this->_tableRanking->save($entidade)) {
            throw new \Exception(__('Erro ao salvar o ranking do vendedor'));
        }
    }

    /**
     * @return mixed
     */
    public function getMetasGerais()
    {
        return $this->metasGerais;
    }

    /**
     * @param mixed $metasGerais
     */
    public function setMetasGerais(): void
    {
        $metasGerais = TableRegistry::getTableLocator()
            ->get('MetasGerais')
            ->find()
            ->contain([
                'MetasGeraisPessoas',
            ])
            ->where([
                'MetasGerais.mes_ano' => $this->mesAnoAtual,
                'MetasGeraisPessoas.vendedores_id' => $this->vendedor->id,
            ])
            ->first();

        $this->metasGerais = $metasGerais;
    }

    /**
     * *******************************
     * Inicio Tendencia
     * *******************************
     *
     * @return float|int
     */
    private function getTendencia()
    {
        $pedidosHl = $this->getHlPedidosTendencia();
        $metasHl = $this->getTotalMetasProdutosTendencia();

        $diasUteis = $this->getTotalDiasUteis();
        $diasUteisMes = $this->getTotalDiasUteisMes();

        if ($metasHl == 0 || $pedidosHl == 0) {
            return 0;
        }

        return ($pedidosHl / $diasUteis) * $diasUteisMes / $metasHl * 100;
   }

    /**
     * @return float
     */
    private function getHlPedidosTendencia()
    {
        $query = TableRegistry::getTableLocator()
            ->get("Pedidos")
            ->find()
            ->contain([
                'Clientes',
                'Produtos' => [
                    'Marcas',
                    'Familias',
                ]
            ])
            ->where([
                'Pedidos.ocorrencia !=' => 3,
                'Pedidos.quantidade >=' => 1,
                'Clientes.canal NOT IN' => ['04', 'J1'],
                'Familias.id IN' => [1, 2, 3, 4, 5],
                'Clientes.vendedores_id' => $this->vendedor->id,
                'Produtos.considera_relatorios' => true
            ]);

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.quantidade * Familias.hl')
            ])
            ->sumOf('total');
    }

    /**
     * @return float
     */
    private function getTotalMetasProdutosTendencia()
    {
        $query = TableRegistry::getTableLocator()
            ->get('Produtos')
            ->find()
            ->contain([
                'Familias',
                'Metas.MetasPessoas',
            ])
            ->where([
                'Familias.id IN' => [1, 2, 3, 4, 5],
                'MetasPessoas.vendedores_id' => $this->vendedor->id,
                'mes_ano' => $this->mesAnoAtual,
                'Produtos.considera_relatorios' => true
            ]);

        return $query
            ->select([
                'total' => $query->func()->sum('Metas.produto_meta * Familias.hl')
            ])
            ->sumOf('total');
    }

    /**
     * *******************************
     * Inicio Cob Total
     * *******************************
     *
     * @return float|int
     */
    private function getCobTotal()
    {
        if (empty($this->getMetasGerais())) {
            return 0;
        }
        $cobTotal = $this->_coberturasGeraisService->getValorCobTotal($this->vendedor->id);
        $metasGerais = $this->getMetasGerais();

        if ($metasGerais->meta_total == 0 || $cobTotal == 0) {
            return 0;
        }
        $total = $cobTotal / $metasGerais->meta_total * 100;

        return round($total);
    }

    /**
     * *******************************
     * Inicio Cob Devassa
     * *******************************
     * @return float|int
     */
    private function cobDevassa()
    {
        if (empty($this->getMetasGerais())) {
            return 0;
        }
        $cobTotal = $this->_coberturasGeraisService->getValorCobDevassa($this->vendedor->id);
        $metaCobDevassa = $this->getMetasGerais()->meta_devassa;

        if ($cobTotal === 0 || $metaCobDevassa === 0) {
            return 0;
        }

        $total = ($cobTotal / $metaCobDevassa) * 100;
        return round($total);
    }

    /**
     * *******************************
     * Inicio Cob Eisenbahn
     * *******************************
     * @return float|int
     */
    private function cobEisenbahn()
    {
        if (empty($this->getMetasGerais())) {
            return 0;
        }
        $metaCobEisenbahn = $this->getMetasGerais()->meta_premium;
        $cobTotal = $this->_coberturasGeraisService->getValorCobEisenbahn($this->vendedor->id);

        if ($cobTotal === 0 || $metaCobEisenbahn === 0) {
            return 0;
        }
        $total = ($cobTotal / $metaCobEisenbahn) * 100;
        return round($total);
    }

    /**
     * *******************************
     * Inicio Cob Retornaveis
     * *******************************
     * @return float|int
     */
    private function cobRetornaveis()
    {
        if (empty($this->getMetasGerais())) {
            return 0;
        }
        $metaCobRetornaveis = $this->getMetasGerais()->meta_600_r;
        $cobTotal = $this->_coberturasGeraisService->getValorCobRetornaveis($this->vendedor->id);

        if ($cobTotal === 0 || $metaCobRetornaveis === 0) {
            return 0;
        }
        $total = ($cobTotal / $metaCobRetornaveis) * 100;
        return round($total);
    }

    /**
     * @param Ranking $ranking
     * @return float|int
     */
    private function getPontos($ranking)
    {
        $pontos = 0;
        if (round($ranking->cob_total) >= 100) {
            $pontos += 20;
        }

        if ($ranking->volume_300_600_1000 >= 100) {
            $pontos += 20;
        }

        if ($ranking->cob_fds >= 100) {
            $pontos += 20;
        }

        if ($ranking->cob_devassa >= 100) {
            $pontos += 20;
        }

        if ($ranking->cob_eisenbahn >= 100) {
            $pontos += 20;
        }

        return $pontos;
    }

    private function volume3006001000()
    {
        $service = new FundamentosVolumeService();

        $service->setVendedor($this->vendedor);
        $valor = $service->getValoresPorSlug('600ml-+-1000ml-+-300ml');

        return $valor['porcentagem'];
    }

    private function getCobFds()
    {
        $service = new FdsVendedoresService();
        $service->setVendedor($this->vendedor);
        $service->setValoresFdsVendedor();

        $valoresFds = $service->getValoresFds();

        return $valoresFds['porcentagem'];
    }

    /**
     *
     */
    public function executaAtualizarRanking()
    {
        $path = ROOT . DS . 'bin' . DS;
        execInBackground("{$path}cake.php importacoes_diarias atualizarRankingVendedores");
    }
}
