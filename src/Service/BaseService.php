<?php

namespace App\Service;

use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class BaseService
{
    /**
     * @var date $lastUpdate
     */
    private $lastUpdate;

    private $_nomenclaturasRelatoriosTable;

    private $parametros;
    private $_paramentrosTable;

    private $data;

    private $totalDiasUteis;

    private $totalDiasUteisMes;

    private $requestQuery;

    private $requestData;

    public function __construct()
    {
        $this->_nomenclaturasRelatoriosTable = TableRegistry::getTableLocator()
            ->get('NomenclaturasRelatorios');

        $this->_paramentrosTable = TableRegistry::getTableLocator()
            ->get('ParametrosSistema');

        $this->setParametros();
        $this->setTotalDiasUteis();
        $this->setTotalDiasUteisMes();
    }

    /**
     * @return mixed
     */
    protected function setLastUpdate()
    {
        $lastUpdate = TableRegistry::getTableLocator()
            ->get('Arquivos')
            ->getLastUpdate();

        $this->lastUpdate = $lastUpdate;
    }

    /**
     * @return mixed
     */
    protected function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * @param string $slug
     * @return array
     */
    protected function getCondicoesNomenclaturas(string $slug)
    {
        $this->_nomenclaturasRelatoriosTable = TableRegistry::getTableLocator()
            ->get('NomenclaturasRelatorios');

        $response = $this->_nomenclaturasRelatoriosTable
            ->getNomenclatura($slug);

        $condicoes = [];

        if (!empty($response['ocorrencias'])) {
            $condicoes["ocorrencia IN"] = $response['ocorrencias'];
        }

        $condicoes["{$response['model']}.id IN"] = $response['codigos'];

        return $condicoes;
    }

    /**
     * @return mixed
     */
    public function getParametros()
    {
        return $this->parametros;
    }

    /**
     *
     */
    public function setParametros(): void
    {
        $parametros = $this->_paramentrosTable
            ->find()
            ->first();

        $this->parametros = $parametros;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data = null): void
    {
        $this->data = $data ?? Time::now();
    }

    /**
     * @return mixed
     */
    public function getTotalDiasUteis()
    {
        return $this->totalDiasUteis;
    }

    /**
     * @param mixed $totalDiasUteis
     */
    public function setTotalDiasUteis($data = null): void
    {
        if (empty($data)) {
            $data = $this->getData();
        }

        $totalDiasUteis = TableRegistry::getTableLocator()
            ->get('DiasUteis')
            ->totalDiasUteis($data);

        $this->totalDiasUteis = $totalDiasUteis;
    }

    /**
     * @return mixed
     */
    public function getTotalDiasUteisMes()
    {
        return $this->totalDiasUteisMes;
    }

    /**
     *
     */
    protected function setTotalDiasUteisMes($data = null): void
    {
        if (empty($data)) {
            $data = $this->getData();
        }

        $totalDiasUteisMes = TableRegistry::getTableLocator()
            ->get('DiasUteis')
            ->totalDiasUteisMes($data);

        $this->totalDiasUteisMes = $totalDiasUteisMes;
    }

    /**
     * @param null $key
     * @return array
     */
    public function getRequestQuery($key = null)
    {
        if (!empty($key) && array_key_exists($key, $this->requestQuery) ) {
            return $this->requestQuery[$key];
        }
        return $this->requestQuery;
    }

    /**
     * @param mixed $requestQuery
     */
    public function setRequestQuery($requestQuery): void
    {
        $this->requestQuery = $requestQuery;
    }

    /**
     * @return mixed
     */
    public function getRequestData($key = null)
    {
        if (!empty($key) && array_key_exists($key, $this->requestData) ) {
            return $this->requestData[$key];
        }
        return $this->requestData;
    }

    /**
     * @param $requestData
     */
    public function setRequestData($requestData): void
    {
        $this->requestData = $requestData;
    }
}
