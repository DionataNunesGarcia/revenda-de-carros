<?php

namespace App\Service;

use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class BaseRelatoriosService extends BaseService
{

    public function __construct()
    {
        $this->setData();
        $this->setTotalDiasUteis();
        $this->setTotalDiasUteisMes();
        $this->setLastUpdate();
    }

    /**
     * @param float $meta
     * @param float $real
     * @return float|int
     */
    protected function getPorcentagem($meta, $real)
    {
        $porcentagem = 0;
        if ($meta != 0 && $real != 0) {
            $porcentagem = ( $real / $meta ) * 100;
        }
        return round($porcentagem, 1);
    }
}
