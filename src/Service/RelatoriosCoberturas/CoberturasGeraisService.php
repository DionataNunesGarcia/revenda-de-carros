<?php
namespace App\Service\RelatoriosCoberturas;

use App\Service\BaseRelatoriosService;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class CoberturasGeraisService extends BaseRelatoriosService
{
    private $vendedores = [];

    public function __construct()
    {
        parent::__construct();
    }

    public function getValores()
    {
        $this->setVendedores();

        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'vendedores' => $this->getVendedores(),
        ];
    }

    /**
     * @return mixed
     */
    public function getVendedores()
    {
        return $this->vendedores;
    }

    /**
     * @param mixed $vendedores
     */
    public function setVendedores(): void
    {
        $vendedoresTable = TableRegistry::getTableLocator()
            ->get("Vendedores");

        $data = $this->getData();

        $vendedores = $vendedoresTable
            ->find('list', [
                'keyField' => function($q){},
                'valueField' => function($q){

                    $q->meta_geral = $q->metas_gerais_pessoa->meta_geral;
                    $q->valores = $this->getValoresCobVendedores($q);

                    return $q;
                },
            ])
            ->contain([
                'PessoaFisica',
                'MetasGeraisPessoas.MetasGerais',
            ])
            ->where([
                'MetasGerais.mes_ano' => $data->i18nFormat('yyyy-MM-01'),
                'Vendedores.relatorios' => true,
                'Vendedores.considera_relatorios' => true
            ])
            ->toArray();

        $this->vendedores = $vendedores;
    }

    private function getValoresCobVendedores($vendedor)
    {
        return [
            'cobTotal' => $this->getValorCobTotal($vendedor->id),
            'cobCerveja' => $this->getValorCobCerveja($vendedor->id),
            'cobRetornavel' => $this->getValorCobRetornavel($vendedor->id),
            'cobEisen600' => $this->getValorCobEisen600($vendedor->id),
            'cobEspeciais' => $this->getValorCobEspeciais($vendedor->id),
            'cobNaoAlcool' => $this->getValorCobNaoAlcool($vendedor->id),
            'cobM1Schin' => $this->getValorCobM1Schin($vendedor->id),
            'cobDevassa' => $this->getValorCobDevassa($vendedor->id),
            'cob600' => $this->getValorCob600($vendedor->id),
            'cobDevassa600' => $this->getValorCobDevassa600($vendedor->id),
        ];
    }

    private function getPedidos(array $conditions)
    {
        return TableRegistry::getTableLocator()
            ->get("Pedidos")
            ->find()
            ->contain([
                'Clientes',
                'Produtos' => [
                    'Marcas',
                    'Familias.Categorias',
                ]
            ])
            ->where($conditions)
            ->group('Pedidos.cliente_id')
            ->orderAsc('Pedidos.quantidade')
            ->count();
    }

    /**
     * @param int|null $vendedorId
     * @param null $dataPedido
     * @return int|null
     */
    public function getValorCobTotal(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturas('info-cob-tt');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }

        return $this->getPedidos($conditions);
    }

    /**
     * Cob Cerveja
     * @param int $vendedorId
     * @return int|null
     */
    private function getValorCobCerveja(int $vendedorId)
    {
        $conditions = [
            'Pedidos.ocorrencia IN' => [1, 2, 4],
            'Clientes.canal NOT IN' => ['04', 'J1'],
            'Pedidos.quantidade >=' => 1,
            'Familias.categoria_id' => 1,
            'Clientes.vendedores_id' => $vendedorId,
        ];
        return $this->getPedidos($conditions);
    }

    /**
     * Cob RETORNAVEL
     * @param int $vendedorId
     * @return int|null
     */
    private function getValorCobRetornavel(int $vendedorId)
    {
        $conditions = [
            'Pedidos.ocorrencia IN' => [1, 2, 4],
            'Clientes.canal NOT IN' => ['04', 'J1'],
            'Pedidos.quantidade >=' => 1,
            'Familias.id IN' => [1, 2, 3, 4, 5, 20, 21, 14],
            'Clientes.vendedores_id' => $vendedorId,
        ];
        return $this->getPedidos($conditions);
    }

    /**
     * Cob EISEN 600
     * @param int $vendedorId
     * @return int|null
     */
    public function getValorCobEisen600(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = [
            'Pedidos.ocorrencia IN' => [1, 2, 4],
            'Clientes.canal NOT IN' => ['04', 'J1'],
            'Pedidos.quantidade >=' => 1,
            'Produtos.codigo' => '903024',
        ];

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob EISEN 600
     * @param int $vendedorId
     * @return int|null
     */
    public function getValorCob3006001000(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturas('info-vol-cerv-600+300+1000-ret-e-desc');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob ESPECIAIS
     * @param int $vendedorId
     * @return int|null
     */
    public function getValorCobEspeciais(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = [
            'Pedidos.ocorrencia IN' => [1, 2, 4],
            'Clientes.canal NOT IN' => ['04', 'J1'],
            'Pedidos.quantidade >=' => 1,
            'Marcas.id IN' => [4, 5, 6, 7, 19],
        ];

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob Eisenbahn
     * @param int $vendedorId
     * @return int|null
     */
    public function getValorCobEisenbahn(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturas('info-cob-eisenbahn');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob NÃO ALCOOL
     * @param int $vendedorId
     * @return int|null
     */
    public function getValorCobNaoAlcool(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturas('info-cob-nao-alcoolico');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob M1 SCHIN
     * @param int $vendedorId
     * @return int|null
     */
    public function getValorCobM1Schin(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturas('info-cob-schin');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }

        return $this->getPedidos($conditions);
    }

    /**
     * Cob M1 SCHIN
     * @param int $vendedorId
     * @return int|null
     */
    public function getValorCobSchin600(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturas('info-cob-schin-600ml');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }

        return $this->getPedidos($conditions);
    }

    /**
     * Cob DEVASSA
     * @param int $vendedorId
     * @return int|null
     */
    public function getValorCobDevassa(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturas('info-cob-devassa');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }

        return $this->getPedidos($conditions);
    }

    /**
     * Cob 600
     * @param int $vendedorId
     * @return int|null
     */
    private function getValorCob600(int $vendedorId)
    {
        $conditions = [
            'Pedidos.ocorrencia IN' => [1, 2, 4],
            'Clientes.canal NOT IN' => ['04', 'J1'],
            'Pedidos.quantidade >=' => 1,
            'Familias.id IN' => [6, 14],
            'Clientes.vendedores_id' => $vendedorId,
        ];
        return $this->getPedidos($conditions);
    }

    /**
     * Cob Devassa 600
     * @param int $vendedorId
     * @return int|null
     */
    public function getValorCobDevassa600(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = [
            'Pedidos.ocorrencia IN' => [1, 2, 4],
            'Clientes.canal NOT IN' => ['04', 'J1'],
            'Pedidos.quantidade >=' => 1,
            'Produtos.codigo' => '903061',
        ];

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob Retornaveis
     * @param int $vendedorId
     * @return int|null
     */
    public function getValorCobRetornaveis(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = [
            'Pedidos.ocorrencia IN' => [1, 2, 4],
            'Clientes.canal NOT IN' => ['04', 'J1'],
            'Pedidos.quantidade >=' => 1,
            'Familias.id IN' => [1, 2, 3, 4, 5],
        ];

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob Retornaveis
     * @param int $vendedorId
     * @return int|null
     */
    public function getValorCobRetornaveisGveGr(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturas('info-cob-600l-ret');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob Retornaveis 600
     *
     * @param int|null $vendedorId
     * @param null $dataPedido
     * @return int|null
     */
    public function getValorCobRetornaveisSeiscentos(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturas('info-cob-600l-ret');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob Retornaveis 1000
     *
     * @param int|null $vendedorId
     * @param null $dataPedido
     * @return int|null
     */
    public function getValorCobRetornaveisMil(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturas('info-vol-cerv-1000ml-ret');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob Retornaveis 300
     *
     * @param int|null $vendedorId
     * @param null $dataPedido
     * @return int|null
     */
    public function getValorCobRetornaveisTrezentos(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturas('info-vol-cerv-300ml-ret');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob Energetico K
     *
     * @param int|null $vendedorId
     * @param null $dataPedido
     * @return int|null
     */
    public function getValorCobEnergeticoK(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = [
            'Pedidos.ocorrencia IN' => [1, 2, 4],
            'Clientes.canal NOT IN' => ['04', 'J1'],
            'Pedidos.quantidade >=' => 1,
            'Marcas.id IN' => [18],
        ];

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    /**
     * Cob Todos Os Produtos
     *
     * @param int|null $vendedorId
     * @param null $dataPedido
     * @return int|null
     */
    public function getValorCobTodosProdutos(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = [
            'Pedidos.ocorrencia IN' => [1, 2, 4],
            'Clientes.canal NOT IN' => ['04', 'J1'],
            'Pedidos.quantidade >=' => 1,
            'Produtos.codigo IN' => $this->getTodosProdutos(),
        ];

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }
        return $this->getPedidos($conditions);
    }

    private function getTodosProdutos()
    {
        return [
            900090,
            903061,
            902411,
            902418,
            902426,
            903024,
            901133,
            903171,
            902432,
            902433,
            900416,
            902410,
            902504,
            902701,
            902503,
            902430,
            902406,
            900089,
            902428,
            902417,
            902413,
            902404,
            902502,
            902445,
            902121,
            903062,
            902832,
            902479,
            901162,
            902261,
            900500,
            900130,
            900121,
            900118,
            900115,
            900109,
            900106,
            903025,
            902541,
            902544,
            902501,
            902241,
            901781,
            901742,
            901741,
            901161,
            900850,
            900844,
            900841,
            900826,
            900820,
            900811,
            900808,
            900805,
            902478,
            902773,
            900829,
            900847,
            153991,
            153985,
            153989,
            900856,
            900859,
            900860,
            900861,
            903066,
            902420,
            902422,
            900855,
            903067,
            902421,
            900284,
            900296,
            900308,
            900323,
            900350,
            900522,
            900588,
            902633,
            902632,
            902991,
            902993,
            902795,
            902780,
            902815,
            902817,
            902778,
            902802,
            902809,
            902381,
            902383,
            903188,
            903189,
            903190,
            903201,
            900290,
            900314,
            900329,
            900341,
            900344,
            900591,
            902321,
            902805,
            902797,
            902812,
            902822,
            902821,
            902335,
            902336,
            900525,
            902334,
            902332,
            900026,
            900015,
            900020,
            900029,
            900023,
            900032,
            900362,
            900368,
            900227,
            900236,
            900245,
            900263,
            900272,
            901486,
            902481,
            901973,
            901972,
            902372,
            902377,
            902373,
            902375,
            902376,
            902374,
            902661,
            902662,
            902663,
            902665,
            902666,
            903124,
            903125,
            902311,
            902312,
            902451,
            902452,
            902800,
            903219,
            903212,
            903271,
        ];
    }
}
