<?php

namespace App\Service\RelatoriosCoberturas;

use App\Service\BaseRelatoriosService;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class IavService extends BaseRelatoriosService
{
    private $_iavTable;
    private $_vendedoresTable;
    private $_supervisoresTable;
    private $_gerentesTable;
    private $_pedidosTable;
    private $_modelPedidos;

    private $mesAno;
    private $vendedores;
    private $supervisores;
    private $gerentes;

    public function __construct()
    {
        parent::__construct();

        $this->_iavTable = TableRegistry::getTableLocator()
            ->get("Iav");

        $this->_vendedoresTable = TableRegistry::getTableLocator()
            ->get("Vendedores");

        $this->_supervisoresTable = TableRegistry::getTableLocator()
            ->get("Supervisores");

        $this->_gerentesTable = TableRegistry::getTableLocator()
            ->get("Gerentes");
    }

    public function getValores()
    {
        $this->setPedidosTable();
        $this->setGerentes();

        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'iav' => $this->getGerentes(),
        ];
    }

    /**
     * @return mixed
     */
    public function getMesAno()
    {
        return $this->mesAno;
    }

    /**
     *
     */
    public function setMesAno(): void
    {
        $mesAno = Time::now()->i18nFormat('yyyy-MM-01');

        $filtro = $this->getRequestQuery();
        if (isset($filtro['data']) && !empty($filtro['data'])) {
            $mesAno = mesAnoToDate($filtro['data']);
        }

        $this->mesAno = $mesAno;
    }

    /**
     *
     */
    public function setPedidosTable(): void
    {
        $this->_modelPedidos = "Pedidos";
        $mesAnoAtual = Time::now()->i18nFormat('yyyy-MM-01');

        if ($mesAnoAtual !== $this->getMesAno()) {
            $this->_modelPedidos = "PedidosHistoricos";
        }

        $this->_pedidosTable = TableRegistry::getTableLocator()
            ->get("{$this->_modelPedidos}");
    }

    /**
     * @return mixed
     */
    public function getQueryIav()
    {
        return $this->_iavTable
            ->find()
            ->where([
                'Iav.mes_ano' => $this->getMesAno(),
                'Vendedores.relatorios' => true,
                'Vendedores.considera_relatorios' => true,
            ])
            ->contain([
                'Vendedores' => [
                    'PessoaFisica',
                    'Supervisor' => [
                        'PessoasFisicas',
                        'Gerentes.PessoaFisica',
                    ]
                ]
            ])
            ->orderAsc('Iav.vendedor_id');;
    }

    /**
     *
     */
    private function setGerentes()
    {
        $query = $this->getQueryIav();

        $gerentesArray = $query
            ->select([
                'total_totcli' => $query->func()->sum('totcli'),
                'total_totcomp' => $query->func()->sum('totcomp'),
                'total_cob' => $query->func()->sum('cob'),
                'total_totvis' => $query->func()->sum('totvis'),
                'total_totped' => $query->func()->sum('totped'),
                'total_pos' => $query->func()->sum('pos'),
                'total_visit' => $query->func()->sum('visit'),
                'total_positiv' => $query->func()->sum('positiv'),
                'total_iav' => $query->func()->sum('iav'),
            ])
            ->select($this->_iavTable->Vendedores)
            ->select($this->_iavTable->Vendedores->Supervisor)
            ->select($this->_iavTable->Vendedores->Supervisor->Gerentes)
            ->group('Gerentes.id')
            ->orderAsc('Gerentes.codigo')
            ->toArray();

        $gerentes = [];
        foreach ($gerentesArray as $iav) {
            $gerenteId = $iav->vendedor->supervisor->gerente_id;

            $gerentes[] = [
                'nome' => $iav->vendedor->supervisor->gerente->pessoaFisica->nome,
                'visao' => $iav->vendedor->supervisor->gerente->codigo,
                'clientes' => $iav->total_totcli,
                'cobertos' => $iav->total_totcomp,
                'perc' => $iav->total_cob,
                'visitas' => $iav->total_totvis,
                'pedidos' => $iav->total_totped,
                'posit' => $iav->total_pos,
                'visitado' => $iav->total_visit,
                'iav' => $this->getPorcentagem($iav->visit, $iav->totped),
                'iv' => $this->getPorcentagem($iav->totvis, $iav->visit),
                'ticketMedio' => round($this->getPorcentagem($iav->totvis, $iav->visit), 2),
                'ped_dia' => $this->getPedidosDiaVendedor($iav),
                'class' => 'gerente',
            ];

            $this->setSupervisores($gerenteId);

            foreach ($this->getSupervisores() as $item) {
                $gerentes[] = $item;
            }
        }

        $this->gerentes = $gerentes;
    }

    /**
     * @return mixed
     */
    private function getGerentes()
    {
        return $this->gerentes;
    }

    /**
     * @param $gerenteId
     */
    private function setSupervisores($gerenteId)
    {
        $query = $this->getQueryIav();

        $supervisoresArray = $query
            ->select($this->_iavTable->Vendedores)
            ->select($this->_iavTable->Vendedores->Supervisor)
            ->select($this->_iavTable->Vendedores->Supervisor->PessoasFisicas)
            ->select([
                'total_totcli' => $query->func()->sum('totcli'),
                'total_totcomp' => $query->func()->sum('totcomp'),
                'total_cob' => $query->func()->sum('cob'),
                'total_totvis' => $query->func()->sum('totvis'),
                'total_totped' => $query->func()->sum('totped'),
                'total_pos' => $query->func()->sum('pos'),
                'total_visit' => $query->func()->sum('visit'),
                'total_positiv' => $query->func()->sum('positiv'),
                'total_iav' => $query->func()->sum('iav'),
            ])
            ->where([
                'Supervisor.gerente_id' => $gerenteId
            ])
            ->group('Supervisor.id')
            ->orderAsc('Supervisor.codigo')
            ->toArray();

        $supervisores = [];
        foreach ($supervisoresArray as $iav) {
            $supervisorId = $iav->vendedor->supervisor_id;
            $supervisores[] = [
                'nome' => $iav->vendedor->supervisor->pessoaFisica->nome,
                'visao' => $iav->vendedor->supervisor->codigo,
                'clientes' => $iav->total_totcli,
                'cobertos' => $iav->total_totcomp,
                'perc' => $iav->total_cob,
                'visitas' => $iav->total_totvis,
                'pedidos' => $iav->total_totped,
                'posit' => $iav->total_pos,
                'visitado' => $iav->total_visit,
                'iav' => $this->getPorcentagem($iav->visit, $iav->totped),
                'iv' => $this->getPorcentagem($iav->totvis, $iav->visit),
                'ticketMedio' => round($this->getPorcentagem($iav->totvis, $iav->visit), 2),
                'ped_dia' => $this->getPedidosDiaVendedor($iav),
                'class' => 'supervisor'
            ];

            $this->setVendedores($supervisorId);

            foreach ($this->getVendedores() as $vendedor) {
                $supervisores[] = $vendedor;
            }
        }

        $this->supervisores = $supervisores;
    }

    private function getSupervisores()
    {
        return $this->supervisores;
    }

    /**
     * @return mixed
     */
    public function getVendedores()
    {
        return $this->vendedores;
    }

    /**
     * @param int $supervisorId
     */
    public function setVendedores(int $supervisorId): void
    {
        $vendedoresArray = $this->getTotalVendedores($supervisorId);

        $vendedores = [];
        foreach ($vendedoresArray as $iav) {
            $vendedores[] = [
                'nome' => $iav->vendedor->pessoaFisica->nome,
                'visao' => $iav->vendedor->codigo,
                'clientes' => $iav->total_totcli,
                'cobertos' => $iav->total_totcomp,
                'perc' => $iav->total_cob,
                'visitas' => $iav->total_totvis,
                'pedidos' => $iav->total_totped,
                'posit' => $iav->total_pos,
                'visitado' => $iav->total_visit,
                'iav' => $this->getPorcentagem($iav->visit, $iav->totped),
                'iv' => $this->getPorcentagem($iav->totvis, $iav->visit),
                'ticketMedio' => round($this->getPorcentagem($iav->totvis, $iav->visit), 2),
                'ped_dia' => $this->getPedidosDiaVendedor($iav),
                'class' => 'vendedor',
            ];

            $pastasVendedores = $this->getPastasVendedores($iav->vendedor->id);
            foreach ($pastasVendedores as $pasta) {
                $vendedores[] = $pasta;
            }
        }

        $this->vendedores = $vendedores;
    }

    /**
     * @param int $supervisorId
     * @return mixed
     */
    private function getTotalVendedores(int $supervisorId)
    {
        $query = $this->getQueryIav();
        return $query
            ->select($this->_iavTable->Vendedores)
            ->select([
                'total_totcli' => $query->func()->sum('totcli'),
                'total_totcomp' => $query->func()->sum('totcomp'),
                'total_cob' => $query->func()->sum('cob'),
                'total_totvis' => $query->func()->sum('totvis'),
                'total_totped' => $query->func()->sum('totped'),
                'total_pos' => $query->func()->sum('pos'),
                'total_visit' => $query->func()->sum('visit'),
                'total_positiv' => $query->func()->sum('positiv'),
                'total_iav' => $query->func()->sum('iav'),
                'Vendedores.codigo',
            ])
            ->where([
                'Vendedores.supervisor_id' => $supervisorId
            ])
            ->group('Vendedores.id')
            ->orderAsc('Vendedores.codigo')
            ->toArray();
    }

    /**
     * @param int $vendedorId
     * @return mixed
     */
    private function getPastasVendedores(int $vendedorId)
    {
        $query = $this->getQueryIav();
        $pastasQuery = $query
            ->select($this->_iavTable->Vendedores)
            ->select([
                'totcli',
                'totcomp' ,
                'cob',
                'totvis',
                'totped',
                'pos',
                'visit' ,
                'positiv',
                'iav',
                'pasta',
                'vendedor_id',
            ])
            ->where([
                'vendedor_id' => $vendedorId
            ])
            ->group('Iav.pasta')
            ->orderAsc('Iav.pasta');

        $pastas = [];
        foreach ($pastasQuery->toArray() as $iav) {
            $pastas[] = [
                'nome' => $iav->vendedor->pessoaFisica->nome,
                'visao' => $iav->pasta,
                'clientes' => $iav->totcli,
                'cobertos' => $iav->totcomp,
                'perc' => $iav->cob,
                'visitas' => $iav->totvis,
                'pedidos' => $iav->totped,
                'posit' => $iav->pos,
                'visitado' => $iav->visit,
                'iav' => $this->getPorcentagem($iav->visit, $iav->totped),
                'iv' => $this->getPorcentagem($iav->totvis, $iav->visit),
                'ticketMedio' => round($this->getPorcentagem($iav->totvis, $iav->visit), 2),
                'ped_dia' => $this->getPedidosDiaVendedor($iav),
                'class' => 'pasta'
            ];
        }

        return $pastas;
    }

    public function getPedidosDiaVendedor($iav)
    {
        $mesAnoAtual = Time::now()->i18nFormat('yyyy-MM-01');
        $data = Time::now();
        if ($mesAnoAtual !== $this->getMesAno()) {
            $data = date("Y-m-t", strtotime($this->getMesAno()));
        }
        $this->setTotalDiasUteis($data);
        $diasUteis = $this->getTotalDiasUteis();

        if ($iav->totped == 0 || $diasUteis == 0) {
            return 0;
        }

        return round($iav->totped / $diasUteis, 1);
    }
}
