<?php

namespace App\Service\CalculosGeraisSevice;

use App\Service\BaseRelatoriosService;
use App\Service\RelatoriosCoberturas\FdsClientesService;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Class CalculoClientesService
 * @package App\Service\CalculosGeraisSevice
 */
class CalculoClientesService extends BaseRelatoriosService
{
    private $_inadimplenciasTable;
    private $_pedidosTable;
    private $_cevTable;

    private $cliente;
    private $vendedorId;

    private $totalHlClientes;

    /**
     * CalculoClientesService constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->_inadimplenciasTable =TableRegistry::getTableLocator()->get('Inadimplencias');
        $this->_pedidosTable =TableRegistry::getTableLocator()->get('Pedidos');
        $this->_cevTable =TableRegistry::getTableLocator()->get('Cev');
    }

    /**
     * @return mixed
     */
    public function getVendedorId()
    {
        return $this->vendedorId;
    }

    /**
     * @param mixed $vendedorId
     */
    public function setVendedorId(int $vendedorId): void
    {
        $this->vendedorId = $vendedorId;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente): void
    {
        $this->cliente = $cliente;
    }

    public function getInadimplencia()
    {
        // INADIMPLENCIA
        $query = $this->_inadimplenciasTable
            ->find()
            ->where([
                'clientes_id' => $this->getCliente()->id,
                'vencimento <=' => Time::now()->modify('-2 days'),
            ]);

        return $query
            ->select([
                'total' => $query->func()->sum('valor')
            ])
            ->sumOf('total');
    }

    /**
     * @return float|int
     */
    public function getTop50()
    {
        /*
         * Quando o HL do cliente (regra1) se for maior do que 0,4% aparece “X”
         * ONDE pedidos/ocorrencias =1
         * ONDE pedidos/produto buscar em produtos/id_familia que familias/id_categoria for !=10,11
         * SOMAR O HL DO CLIENTE pedidos/produto buscar em produtos/id_familia que familias/hl * qtde em pedidos/qtde_ved
         * DIVIDIR O HL DO CLIENTE PELO HL DA SOMA DE TODOS OS CLIENTES, para buscar o % de representatividade do cliente
         */
        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Produtos.Familias'
            ])
            ->where([
                'cliente_id' => $this->getCliente()->id,
                'ocorrencia IN' => [1],
                'categoria_id NOT IN' => [10, 11],
                'data_pedido >=' => Time::now()->i18nFormat('yyyy-MM-01'),
                'data_pedido <=' => Time::now()->modify('last day of this month')->i18nFormat('yyyy-MM-dd'),
                'Produtos.considera_relatorios' => true
            ]);

        $total = $query
            ->select([
                'total' => $query->func()->sum('Familias.hl * Pedidos.quantidade')
            ])
            ->sumOf('total');

        if ($total == 0 || $this->totalHlClientes == 0) {
            return 0;
        }

        return round(($total / $this->totalHlClientes) * 100, 2);
    }

    /**
     * @return float|int
     */
    public function getTop50Ret()
    {
        /*
         * Quando o HL do cliente (regra1) se for maior do que 0,4% aparece “X”
         * OONDE pedidos/ocorrencias =1
         * ONDE pedidos/produto buscar em produtos/id_familia que familias/id for <=5
         * SOMAR O HL DO CLIENTE pedidos/produto buscar em produtos/id_familia que familias/hl * qtde em pedidos/qtde_ved
         * DIVIDIR O HL DO CLIENTE PELO HL DA SOMA DE TODOS OS CLIENTES, para buscar o % de representatividade do cliente
         */
        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Produtos.Familias'
            ])
            ->where([
                'cliente_id' => $this->getCliente()->id,
                'ocorrencia IN' => [1],
                'Produtos.familia_id IN' => [1, 2, 3, 4, 5],
                'data_pedido >=' => Time::now()->i18nFormat('yyyy-MM-01'),
                'data_pedido <=' => Time::now()->modify('last day of this month')->i18nFormat('yyyy-MM-dd'),
                'Produtos.considera_relatorios' => true
            ]);

        $total = $query
            ->select([
                'total' => $query->func()->sum('Familias.hl * Pedidos.quantidade')
            ])
            ->sumOf('total');

        if ($total == 0 || $this->totalHlClientes == 0) {
            return 0;
        }

        return round(($total / $this->totalHlClientes) * 100, 2);
    }

    /**
     *
     */
    public function setTotalHlClientes()
    {
        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos.Familias',
            ])
            ->where([
                'Clientes.vendedores_id' => $this->getVendedorId(),
                'ocorrencia IN' => [1],
                'categoria_id NOT IN' => [10, 11],
                'data_pedido >=' => Time::now()->i18nFormat('yyyy-MM-01'),
                'data_pedido <=' => Time::now()->modify('last day of this month')->i18nFormat('yyyy-MM-dd'),
                'Produtos.considera_relatorios' => true
            ]);

        $this->totalHlClientes = $query
            ->select([
                'total' => $query->func()->sum('Familias.hl')
            ])
            ->sumOf('total');
    }

    public function getTotalHlClientes()
    {
        return $this->totalHlClientes;
    }

    public function getCoberturas()
    {
        $coberturas = new \stdClass();

        $coberturas->total = $this->getCoberturaNomenclatura('cob-mat-tt');
        $coberturas->m1 = $this->getCoberturaNomenclatura('cob-mat-m1');
        $coberturas->main = $this->getCoberturaNomenclatura('cob-mat-main');
        $coberturas->premium = $this->getCoberturaNomenclatura('cob-mat-premium');
        $coberturas->nao_alc = $this->getCoberturaNomenclatura('cob-mat-nao-alc');

        $coberturas->gla300 = $this->getCoberturaNomenclatura('cob-mat-gla300');
        $coberturas->gla600 = $this->getCoberturaNomenclatura('cob-mat-gla600');
        $coberturas->gla1l = $this->getCoberturaNomenclatura('cob-mat-gla1l');
        $coberturas->gla473 = $this->getCoberturaNomenclatura('cob-mat-gla473');

        $coberturas->schchopp = $this->getCoberturaNomenclatura('cob-mat-schchopp');
        $coberturas->sch300 = $this->getCoberturaNomenclatura('cob-mat-sch300');
        $coberturas->sch600 = $this->getCoberturaNomenclatura('cob-mat-sch600---fds-sc6');
        $coberturas->sch1l = $this->getCoberturaNomenclatura('cob-mat-sch1l---fds-s1l');
        $coberturas->sch350 = $this->getCoberturaNomenclatura('cob-mat-sch350');
        $coberturas->sch473Fds = $this->getCoberturaNomenclatura('cob-mat-sch473---fds-s47');

        $coberturas->devchopp = $this->getCoberturaNomenclatura('cob-mat-devchopp');
        $coberturas->dev600 = $this->getCoberturaNomenclatura('cob-mat-dev600---fds-d60');
        $coberturas->dev1l = $this->getCoberturaNomenclatura('cob-mat-dev1l');
        $coberturas->dev350 = $this->getCoberturaNomenclatura('cob-mat-dev350');
        $coberturas->dev355 = $this->getCoberturaNomenclatura('cob-mat-dev355---fds-dln');
        $coberturas->dev473 = $this->getCoberturaNomenclatura('cob-mat-dev473---fds-d47');

        $coberturas->eisenchopp = $this->getCoberturaNomenclatura('cob-mat-eisenchopp---fds-edr');
        $coberturas->eisen600 = $this->getCoberturaNomenclatura('cob-mat-eisen600---fds-e60');
        $coberturas->eisen350 = $this->getCoberturaNomenclatura('cob-mat-eisen350---fds-e35');
        $coberturas->eisen355 = $this->getCoberturaNomenclatura('cob-mat-eisen355---fds-eln');
        $coberturas->eisen_estilos = $this->getCoberturaNomenclatura('cob-mat-eisen-estilos---fds-she');

        $coberturas->baden_300 = $this->getCoberturaNomenclatura('cob-mat-baden-300');
        $coberturas->baden_600 = $this->getCoberturaNomenclatura('cob-mat-baden-600---fds-shb');

        $coberturas->fys_350 = $this->getCoberturaNomenclatura('cob-mat-fys-350');
        $coberturas->fys_2l = $this->getCoberturaNomenclatura('cob-mat-fys-2l---fds-p2l');

        $coberturas->itub350 = $this->getCoberturaNomenclatura('cob-mat-itub350');
        $coberturas->itub355 = $this->getCoberturaNomenclatura('cob-mat-itub355---fds-itu');
        $coberturas->itub2l = $this->getCoberturaNomenclatura('cob-mat-itub2l');

        $coberturas->skinka = $this->getCoberturaNomenclatura('cob-mat-skinka---fds-ski');
        $coberturas->dreher = $this->getCoberturaNomenclatura('cob-mat-dreher---fds-dre');

        return $coberturas;
    }

    public function getCoberturaNomenclatura(string $slug, $numero = false)
    {
        $condicoes = $this->getCondicoesNomenclaturas($slug);
        $condicoes['Pedidos.data_pedido >='] = Time::now()->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = Time::now()->modify('last day of this month')->i18nFormat('yyyy-MM-dd');
        $condicoes['Pedidos.cliente_id'] = $this->getCliente()->id;
        $condicoes['Produtos.considera_relatorios'] = true;

        $total = $this->_pedidosTable
            ->find()
            ->contain([
                'Produtos' => [
                    'Familias.Categorias',
                    'Marcas',
                ],
            ])
            ->where($condicoes)
            ->count();

        if ($numero) {
            return $total;
        }

        return $total > 0 ? 'C' : '';
    }

    public function fdsCliente()
    {
        $service = new FdsClientesService();
        $service->setClienteId($this->cliente->id);
        $service->setCanal($this->cliente->categoriaVarejo->demp->nome);
        $service->setMesAno();

        return $service->fdsPorCliente();
    }

    public function materiais()
    {
        /*
         * MATERIAL PESADO
         * qtde que o cliente tem nas colunas cev/ref_peq + cev/ref_gde + cev/visa + cev/freezer
         * ---------------------------------------------
         * MATERIAL LEVE
         * qtde que o cliente tem nas colunas cev/mesa_plast + cev/mesa_mad
         * ---------------------------------------------
         * CHOPEIRA
         * qtde que o cliente tem nas colunas cev/chopeira (estava como vago2 e eu ajustei)
        */
        $query = $this->_cevTable->find();
        $query->select(['material_pesado' => $query->func()->sum('ref_peq  + visa + freezer')]);
        $query->select(['material_leve' => $query->func()->sum('mesa_plastico + mesa_mad')]);
        $query->select(['chopeira' => $query->func()->sum('vago2')]);

        $query->where(['clientes_id' => $this->getCliente()->id]);

        return $query->first();
    }
}
