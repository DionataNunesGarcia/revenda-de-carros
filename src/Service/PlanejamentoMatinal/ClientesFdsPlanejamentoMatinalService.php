<?php

namespace App\Service\PlanejamentoMatinal;


class ClientesFdsPlanejamentoMatinalService
{
    public function converteCodigoCliente($codigo)
    {
        $cliente1 = substr($codigo, 0, 4);
        $cliente2 = substr($codigo, 4, 4);
        return $cliente1 . '-' . $cliente2;
    }

    public static function fdsPorCliente($codigoCliente, $canal)
    {
        $codigo = static::converteCodigoCliente($codigoCliente);

        switch ($canal) {
            case 'ASI 1-4':
                return static::totalASI14($codigo);
            case 'BAR A-B':
                return static::totalBarAB($codigo);
            case 'BAR C-D':
                return static::totalBarCD($codigo);
            case 'CHOPERIA':
                return static::totalCHOPERIA($codigo);
            case 'REST-LANCH':
                return static::totalRestLanch($codigo);
            case 'CONVENIENCIA':
                return static::totalCONVENIENCIA($codigo);
            case 'TRAD':
                return static::totalTRAD($codigo);
            case 'ADEGA':
                return static::totalADEGA($codigo);
        }
    }

    public function pertenceCanalFds($canal, $produtos)
    {
        $arrayProdutosCanal = $this->arrayProdutosCanal($canal);

        if (is_array($produtos)) {
            foreach ($produtos as $produto) {
                if (in_array($produto, $arrayProdutosCanal)) {
                    return 'canal';
                }
            }
        } else {
            if (in_array($produtos, $arrayProdutosCanal)) {
                return 'canal';
            }
        }

        return '';
    }

    public function arrayProdutosCanal($canal)
    {

        switch ($canal) {
            case 'ASI 1-4':
                return [
                    '900814',
                    '900832',
                    '902544',
                    '902541',
                    '902581',
                    '903222',
                    '901781',
                    '903762',
                    '903212',
                    '903211',
                    '903750',
                    '900687',
                    '900688',
                    '901161',
                    '901741',
                    '901742',
                    '901304',
                    '902563',
                    '903822',
                    '903128',
                    '902410',
                    '903769',
                    '903791',
                    '903792',
                    '903793',
                    '903795',
                    '903796',
                    '903797',
                    '903798',
                    '903800',
                    '903801',
                    '903802',
                    '903803',
                    '903832',
                    '903834',
                    '903836',
                    '902795',
                    '902815',
                    '902809',
                    '902780',
                    '902778',
                    '902802',
                    '902817',
                    '900350',
                    '219333',
                    '120366',
                ];
            case 'BAR A-B':
                return [
                    '900814',
                    '900832',
                    '902544',
                    '902541',
                    '902581',
                    '903222',
                    '901781',
                    '903762',
                    '903024',
                    '903025',
                    '903026',
                    '900704',
                    '903241',
                    '903061',
                    '903132',
                    '900020',
                    '900029',
                    '902641',
                    '902630',
                ];
            case 'BAR C-D':
                return [
                    '901781',
                    '903762',
                    '900704',
                    '903241',
                    '903061',
                    '903132',
                    '902031',
                    '902411',
                    '902418',
                    '902426',
                    '902786',
                    '903769',
                    '903791',
                    '903792',
                    '903793',
                    '903795',
                    '903796',
                    '903797',
                    '903798',
                    '903800',
                    '903801',
                    '903802',
                    '903803',
                    '903832',
                    '903834',
                    '903836',
                    '902795',
                    '902815',
                    '902809',
                    '902780',
                    '902778',
                    '902802',
                    '902817',
                    '900350',
                    '219333',
                    '120366',
                ];
            case 'CHOPERIA':
                return [
                    '901781',
                    '903762',
                    '900856',
                    '900859',
                    '900860',
                    '900861',
                    '900855',
                ];
            case 'REST-LANCH':
                return [
                    '901781',
                    '903762',
                    '900704',
                    '903241',
                    '903061',
                    '903132',
                    '902410',
                    '900525',
                    '900362',
                    '903122',
                    '900368',
                ];
            case 'CONVENIENCIA':
                return [
                    '900814',
                    '900832',
                    '902544',
                    '902541',
                    '902581',
                    '903222',
                    '900109',
                    '900500',
                    '902832',
                    '901162',
                    '901781',
                    '903762',
                    '903212',
                    '903211',
                    '903750',
                    '901304',
                    '902563',
                    '903822',
                    '903128',
                    '902410',
                    '900362',
                    '903122',
                    '900368',
                ];
            case 'TRAD':
                return [
                    '903212',
                    '903211',
                    '903750',
                    '901304',
                    '902563',
                    '903822',
                    '902410',
                    '900362',
                    '903122',
                    '900368',
                    '903769',
                    '903791',
                    '903792',
                    '903188',
                    '903189',
                    '903190',
                ];
            case 'ADEGA':
                return [
                    '903024',
                    '903025',
                    '903026',
                    '900704',
                    '903241',
                    '903061',
                    '903132',
                    '901304',
                    '902563',
                    '903822',
                    '903128',
                    '902410',
                    '902432',
                    '902433',
                    '903769',
                    '903791',
                    '903792',
                    '903793',
                    '903795',
                    '903796',
                    '903797',
                    '903798',
                    '903800',
                    '903801',
                    '903802',
                    '903803',
                    '903832',
                    '903834',
                    '903836',
                    '902795',
                    '902815',
                    '902809',
                    '902780',
                    '902778',
                    '902802',
                    '902817',
                    '900350',
                    '219333',
                    '120366',
                ];
        }

    }

    public function totalASI14($codigo)
    {
        return 0;
        die('totalASI14');
//        $query1 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900814' or pedidos.produto='900832' or pedidos.produto='902544' or pedidos.produto='902541' or pedidos.produto='902581' or pedidos.produto='903222') group by pedidos.cliente");
//        $total1 = mysql_num_rows($query1);
//
//        $query2 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='901781' or pedidos.produto='903762') group by pedidos.cliente");
//        $total2 = mysql_num_rows($query2);
//
//        $query3 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='903212' or pedidos.produto='903211' or pedidos.produto='903750') group by pedidos.cliente");
//        $total3 = mysql_num_rows($query3);
//
//        $query4 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900687' or pedidos.produto='900688' or pedidos.produto='901161' or pedidos.produto='901741' or pedidos.produto='901742') group by pedidos.cliente");
//        $total4 = mysql_num_rows($query4);
//
//        $query5 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='901304' or pedidos.produto='902563' or pedidos.produto='903822' or pedidos.produto='903128') group by pedidos.cliente");
//        $total5 = mysql_num_rows($query5);
//
//        $query6 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='902410') group by pedidos.cliente");
//        $total6 = mysql_num_rows($query6);
//
//        $query7 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='903769' or pedidos.produto='903791' or pedidos.produto='903792' or pedidos.produto='903793' or pedidos.produto='903795' or pedidos.produto='903796' or pedidos.produto='903797' or pedidos.produto='903798' or pedidos.produto='903800' or pedidos.produto='903801' or pedidos.produto='903802' or pedidos.produto='903803' or pedidos.produto='903832' or pedidos.produto='903834' or pedidos.produto='903836' or pedidos.produto='902795' or pedidos.produto='902815' or pedidos.produto='902809' or pedidos.produto='902780' or pedidos.produto='902778' or pedidos.produto='902802' or pedidos.produto='902817' or pedidos.produto='900350') group by pedidos.cliente");
//        $total7 = mysql_num_rows($query7);
//
//        $query8 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved >'0' and pedidos.cliente=clientes.codigo  and (pedidos.produto='219333' or pedidos.produto='120366') group by pedidos.cliente");
//        $total8 = mysql_num_rows($query8);

        return round((($total1 + $total2 + $total3 + $total4 + $total5 + $total6 + $total7 + $total8) / 8) * 100);
    }

    public function totalBarAB($codigo)
    {
        return 0;
        die('totalBarAB');

//        $query1 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900814' or pedidos.produto='900832' or pedidos.produto='902544' or pedidos.produto='902541' or pedidos.produto='902581' or pedidos.produto='903222') group by pedidos.cliente");
//        $total1 = mysql_num_rows($query1);
//
//        $query2 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='901781' or pedidos.produto='903762') group by pedidos.cliente");
//        $total2 = mysql_num_rows($query2);
//
//        $query3 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='903024' or pedidos.produto='903025' or pedidos.produto='903026') group by pedidos.cliente");
//        $total3 = mysql_num_rows($query3);
//
//        $query4 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900704' or pedidos.produto='903241' or pedidos.produto='903061' or pedidos.produto='903132') group by pedidos.cliente");
//        $total4 = mysql_num_rows($query4);
//
//        $query5 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900020' or pedidos.produto='900029' or pedidos.produto='902641' or pedidos.produto='902630') group by pedidos.cliente");
//        $total5 = mysql_num_rows($query5);

        return round((($total1 + $total2 + $total3 + $total4 + $total5 ) / 5) * 100);
    }

    public function totalBarCD($codigo)
    {
        return 0;
        die('totalBarCD');
//        $query1 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='901781' or pedidos.produto='903762') group by pedidos.cliente");
//        $total1 = mysql_num_rows($query1);
//
//        $query2 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900704' or pedidos.produto='903241' or pedidos.produto='903061' or pedidos.produto='903132') group by pedidos.cliente");
//        $total2 = mysql_num_rows($query2);
//
//        $query3 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='902031' or pedidos.produto='902411' or pedidos.produto='902418' or pedidos.produto='902426' or pedidos.produto='902786') group by pedidos.cliente");
//        $total3 = mysql_num_rows($query3);
//
//        $query4 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='903769' or pedidos.produto='903791' or pedidos.produto='903792' or pedidos.produto='903793' or pedidos.produto='903795' or pedidos.produto='903796' or pedidos.produto='903797' or pedidos.produto='903798' or pedidos.produto='903800' or pedidos.produto='903801' or pedidos.produto='903802' or pedidos.produto='903803' or pedidos.produto='903832' or pedidos.produto='903834' or pedidos.produto='903836' or pedidos.produto='902795' or pedidos.produto='902815' or pedidos.produto='902809' or pedidos.produto='902780' or pedidos.produto='902778' or pedidos.produto='902802' or pedidos.produto='902817' or pedidos.produto='900350') group by pedidos.cliente");
//        $total4 = mysql_num_rows($query4);
//
//        $query5 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved >'0' and pedidos.cliente=clientes.codigo  and (pedidos.produto='219333' or pedidos.produto='120366') group by pedidos.cliente");
//        $total5 = mysql_num_rows($query5);

        return round((($total1 + $total2 + $total3 + $total4 + $total5 ) / 5) * 100);
    }

    public function totalCHOPERIA($codigo)
    {
        return 0;
        die('totalCHOPERIA');
//        $query1 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='901781' or pedidos.produto='903762') group by pedidos.cliente");
//        $total1 = mysql_num_rows($query1);
//
//        $query2 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900856' or pedidos.produto='900859' or pedidos.produto='900860' or pedidos.produto='900861' or pedidos.produto='900855') group by pedidos.cliente");
//        $total2 = mysql_num_rows($query2);

        return round((($total1 + $total2 ) / 2) * 100);
    }

    public function totalRestLanch($codigo)
    {
        return 0;
        die('totalRestLanch');
//        $query1 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='901781' or pedidos.produto='903762') group by pedidos.cliente");
//        $total1 = mysql_num_rows($query1);
//
//        $query2 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900704' or pedidos.produto='903241' or pedidos.produto='903061' or pedidos.produto='903132') group by pedidos.cliente");
//        $total2 = mysql_num_rows($query2);
//
//        $query3 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='902410') group by pedidos.cliente");
//        $total3 = mysql_num_rows($query3);
//
//        $query4 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900525') group by pedidos.cliente");
//        $total4 = mysql_num_rows($query4);
//
//        $query5 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='".$codigo."' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900362' or pedidos.produto='903122' or pedidos.produto='900368') group by pedidos.cliente");
//        $total5 = mysql_num_rows($query5);

        return round((($total1 + $total2 + $total3 + $total4 + $total5 ) / 5) * 100);
    }

    public function totalCONVENIENCIA($codigo)
    {
        return 0;
        die('totalCONVENIENCIA');
//        $query1 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900814' or pedidos.produto='900832' or pedidos.produto='902544' or pedidos.produto='902541' or pedidos.produto='902581' or pedidos.produto='903222') group by pedidos.cliente");
//        $total1 = mysql_num_rows($query1);
//
//        $query2 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved >'0' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900109' or pedidos.produto='900500' or pedidos.produto='902832' or pedidos.produto='901162') group by pedidos.cliente");
//        $total2 = mysql_num_rows($query2);
//
//        $query3 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='901781' or pedidos.produto='903762') group by pedidos.cliente");
//        $total3 = mysql_num_rows($query3);
//
//        $query4 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='903212' or pedidos.produto='903211' or pedidos.produto='903750') group by pedidos.cliente");
//        $total4 = mysql_num_rows($query4);
//
//        $query5 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='901304' or pedidos.produto='902563' or pedidos.produto='903822' or pedidos.produto='903128') group by pedidos.cliente");
//        $total5 = mysql_num_rows($query5);
//
//        $query6 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='902410') group by pedidos.cliente");
//        $total6 = mysql_num_rows($query6);
//
//        $query7 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900362' or pedidos.produto='903122' or pedidos.produto='900368') group by pedidos.cliente");
//        $total7 = mysql_num_rows($query7);

        return round((($total1 + $total2 + $total3 + $total4 + $total5 + $total6 + $total7) / 7) * 100);
    }

    public function totalTRAD($codigo)
    {
        return 0;
        die('totalTRAD');
//        $query1 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='903212' or pedidos.produto='903211' or pedidos.produto='903750') group by pedidos.cliente");
//        $total1 = mysql_num_rows($query1);
//
//        $query2 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='901304' or pedidos.produto='902563' or pedidos.produto='903822' or pedidos.produto='903128') group by pedidos.cliente");
//        $total2 = mysql_num_rows($query2);
//
//        $query3 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='902410') group by pedidos.cliente");
//        $total3 = mysql_num_rows($query3);
//
//        $query4 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900362' or pedidos.produto='903122' or pedidos.produto='900368') group by pedidos.cliente");
//        $total4 = mysql_num_rows($query4);
//
//        $query5 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='903769' or pedidos.produto='903791' or pedidos.produto='903792' or pedidos.produto='903793' or pedidos.produto='903795' or pedidos.produto='903796' or pedidos.produto='903797' or pedidos.produto='903798' or pedidos.produto='903800' or pedidos.produto='903801' or pedidos.produto='903802' or pedidos.produto='903803' or pedidos.produto='903832' or pedidos.produto='903834' or pedidos.produto='903836' or pedidos.produto='902795' or pedidos.produto='902815' or pedidos.produto='902809' or pedidos.produto='902780' or pedidos.produto='902778' or pedidos.produto='902802' or pedidos.produto='902817' or pedidos.produto='900350') group by pedidos.cliente");
//        $total5 = mysql_num_rows($query5);
//
//        $query6 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='903188' or pedidos.produto='903189' or pedidos.produto='903190' or pedidos.produto='903201') group by pedidos.cliente");
//        $total6 = mysql_num_rows($query6);

        return round((($total1 + $total2 + $total3 + $total4 + $total5 + $total6) / 6) * 100);
    }

    public function totalADEGA($codigo)
    {
        return 0;
        die('totalADEGA');
//        $query1 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='903024' or pedidos.produto='903025' or pedidos.produto='903026') group by pedidos.cliente");
//        $total1 = mysql_num_rows($query1);
//
//        $query2 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='900704' or pedidos.produto='903241' or pedidos.produto='903061' or pedidos.produto='903132') group by pedidos.cliente");
//        $total2 = mysql_num_rows($query2);
//
//        $query3 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='901304' or pedidos.produto='902563' or pedidos.produto='903822' or pedidos.produto='903128') group by pedidos.cliente");
//        $total3 = mysql_num_rows($query3);
//
//        $query4 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='902410') group by pedidos.cliente");
//        $total4 = mysql_num_rows($query4);
//
//        $query5 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='902432' or pedidos.produto='902433') group by pedidos.cliente");
//        $total5 = mysql_num_rows($query5);
//
//        $query6 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>='1' and pedidos.cliente=clientes.codigo  and (pedidos.produto='903769' or pedidos.produto='903791' or pedidos.produto='903792' or pedidos.produto='903793' or pedidos.produto='903795' or pedidos.produto='903796' or pedidos.produto='903797' or pedidos.produto='903798' or pedidos.produto='903800' or pedidos.produto='903801' or pedidos.produto='903802' or pedidos.produto='903803' or pedidos.produto='903832' or pedidos.produto='903834' or pedidos.produto='903836' or pedidos.produto='902795' or pedidos.produto='902815' or pedidos.produto='902809' or pedidos.produto='902780' or pedidos.produto='902778' or pedidos.produto='902802' or pedidos.produto='902817' or pedidos.produto='900350') group by pedidos.cliente");
//        $total6 = mysql_num_rows($query6);
//
//        $query7 = mysql_query("select pedidos.cliente, pedidos.produto from clientes, produtos, pedidos where pedidos.cliente='$codigo' and (pedidos.ocorrencia='1' or pedidos.ocorrencia='2' or pedidos.ocorrencia='4') and pedidos.qtde_ved>'0' and pedidos.cliente=clientes.codigo  and (pedidos.produto='219333' or pedidos.produto='120366') group by pedidos.cliente");
//        $total7 = mysql_num_rows($query7);

        return round((($total1 + $total2 + $total3 + $total4 + $total5 + $total6 + $total7) / 7) * 100);
    }
}
