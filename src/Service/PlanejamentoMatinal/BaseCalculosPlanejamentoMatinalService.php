<?php


namespace App\Service\PlanejamentoMatinal;


use Cake\ORM\Locator\TableLocator;
use Cake\ORM\TableRegistry;

class BaseCalculosPlanejamentoMatinalService
{
    public static function converteCodigoVendedor($entidade)
    {
        $cliente1 = substr($entidade->codigo, 0, 4);
        $cliente2 = substr($entidade->codigo, 4, 4);

        return $cliente1 . '-' . $cliente2;
    }

    public static function totalInadimplencia($codigoCliente)
    {
//        $hoje = date('Y-m-d');
//        $dois_dias = date('Y-m-d', strtotime('' . $hoje . ' -2 days'));
//        $total_inadimplencia = 0;
//        $cartorio = 0;
//        $taxa = 0;
//        $multa = 0;
//        $tt = 0;
//        $principal = 0;
//
//        $PedidosHistoricos = TableRegistry::get('PedidosHistoricos');
//
//        $query2 = mysql_query("
//        select
//            inadimplencia.id,
//            inadimplencia.data_emissao,
//            inadimplencia.cliente,
//            inadimplencia.valor,
//            inadimplencia.data_vencimento,
//            clientes.codigo
//        from
//            inadimplencia, clientes
//        where clientes.codigo=inadimplencia.cliente
//        and clientes.codigo = '$codigoCliente'
//        and inadimplencia.data_vencimento<='$dois_dias'
//    ");
//
//        while ($row2 = mysql_fetch_array($query2)) {
//
//            @$total_inadimplencia += $row2['valor'];
//
//            $query3 = mysql_query("SELECT DATEDIFF(NOW(), data_vencimento) as total_dias from inadimplencia where id='" .  $row2['id']. "'");
//            $row3 = mysql_fetch_array($query3);
//
//            $total_dias = $row3['total_dias'];
//
//            $principal = $row2['valor'];
//            if ($total_dias >= 3) {
//                $cartorio = (109.9);
//            }
//            if ($total_dias >= 2) {
//                $taxa = 0.0020;
//                @$multa = $principal * 0.01;
//            }
//            //@$principal = $row2['valor'];
//            $juros = $principal * $taxa * $total_dias;
//            $tt = $juros;
//        } // end while
//
//
//
//
//
//        $total_devido = $tt + $cartorio + $principal + $multa;
//        $total_devido = round($total_devido, 2);
//
//        if($total_inadimplencia != 0) {
//            return number_format($total_devido,2,",",".");
//        }

        return 0;
    }


    public static function baseComprasUltimosMeses($codigo)
    {

        $exist1MesAtras = static::existeCompraMes($codigo, 1);
        $exist2MesesAtras = static::existeCompraMes($codigo, 2);
        $exist3MesesAtras = static::existeCompraMes($codigo, 3);

        $total = $exist1MesAtras + $exist2MesesAtras + $exist3MesesAtras;

        switch ($total) {
            case 0:
                return 'SEM COMPRA';
            case 1 || 2:
                return 'INSTÁVEIS';
            case 3:
                return 'ESTÁVEIS';
        }

    }

    private function existeCompraMes($codigo, $mesesAnteriores)
    {
        $PedidosHistoricos = TableRegistry::get('PedidosHistoricos');

        $query = $PedidosHistoricos->find();
        $query->where([
            'cliente_id' => $codigo,
            "data_pedido >= DATE_ADD(LAST_DAY(NOW() - INTERVAL {$mesesAnteriores} + 1 MONTH), INTERVAL 1 DAY)",
            "data_pedido <= (LAST_DAY(NOW() - INTERVAL {$mesesAnteriores} MONTH))"
        ]);

        $result = $query->limit(1)->first();

        return !empty($result) ? 1 : 0;
    }

    public static function coberturaProdutoMesAtual($codigo)
    {
        return [];
        die('coberturaProdutoMesAtual');
        /*
         * Colunas de COBERTURA,
         * o "C" significa que comprou e os que estão em branco é porque não tem compra no mês atual ainda.
         * As células pretas são os itens que integram o FDS do canal que o cliente é.
        */

//        $query = mysql_query("
//        select
//            ped.cliente,
//            case
//                when ped.ocorrencia = 1 and cat.id not in (10, 11) then 'C'
//                else ''
//            end total,
//            case
//                when ped.ocorrencia = 1 and prod.id_marca = 1 then 'C'
//                else ''
//            end m1,
//            case
//                when ped.ocorrencia = 1 and prod.id_marca = 3 then 'C'
//                else ''
//            end mainstrean,
//            case
//                when ped.ocorrencia = 1 and prod.id_marca in (4, 5) then 'C'
//                else ''
//            end premium,
//            case
//                when ped.ocorrencia = 1 and prod.id_marca in (8,9,10,11,12,13,14,15,16,18,20) then 'C'
//                else ''
//            end nao_alcoolico,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('902311', '902312') then 'C'
//                else ''
//            end glacial_300,
//            case
//                when ped.ocorrencia = 1 and ped.produto = '900090' then 'C'
//                else ''
//            end glacial_600,
//            case
//                when ped.ocorrencia = 1 and ped.produto = '901133' then 'C'
//                else ''
//            end glacial_litro,
//            case
//                when ped.ocorrencia = 1 and ped.produto = '900416' then 'C'
//                else ''
//            end glacial_473,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('902420', '902422', '903663', '902421', '903664') then 'C'
//                else ''
//            end schin_chopp,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('902451', '902452') then 'C'
//                else ''
//            end schin_300,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('902411', '902418', '902426') then 'C'
//                else ''
//            end schin_600,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('902432', '902433') then 'C'
//                else ''
//            end schin_litro,
//            case
//                when ped.ocorrencia = 1 and ped.produto = '902406' then 'C'
//                else ''
//            end schin_350,
//            case
//                when ped.ocorrencia = 1 and ped.produto = '902410' then 'C'
//                else ''
//            end schin_473,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('903066', '903665', '903067', '903666') then 'C'
//                else ''
//            end devassa_chopp,
//            case
//                when ped.ocorrencia = 1 and ped.produto = '903061' then 'C'
//                else ''
//            end devassa_600,
//            case
//                when ped.ocorrencia = 1 and ped.produto = '903171' then 'C'
//                else ''
//            end devassa_litro,
//            case
//                when ped.ocorrencia = 1 and ped.produto = '903131' then 'C'
//                else ''
//            end devassa_350,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('900687', '900688', '901161', '901741', '901742') then 'C'
//                else ''
//            end devassa_ln_355,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('901304', '902563', '903822', '903128') then 'C'
//                else ''
//            end devassa_473,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('900856', '900859', '900860', '900861', '900855', '903667', '903668') then 'C'
//                else ''
//            end eisen_chopp,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('903024', '903025') then 'C'
//                else ''
//            end eisen_600,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('903212', '903211', '903750') then 'C'
//                else ''
//            end eisen_350,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('901781', '903762') then 'C'
//                else ''
//            end eisen_355,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('902544', '902581', '903222') then 'C'
//                else ''
//            end eisen_estilos,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('903821', '903822', '903823') then 'C'
//                else ''
//            end baden_300,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('900109', '900500', '902832', '901162') then 'C'
//                else ''
//            end baden_600,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('903769', '903791', '903792', '903793', '903795', '903796', '903797', '903832') then 'C'
//                else ''
//            end fys_350,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('902795', '902815', '902809', '902780', '902778', '902802', '902817') then 'C'
//                else ''
//            end fys_2litros,
//            case
//                when ped.ocorrencia = 1 and ped.produto = '902321' then 'C'
//                else ''
//            end itubaina_350,
//            case
//                when ped.ocorrencia = 1 and ped.produto = '900525' then 'C'
//                else ''
//            end itubaina_355,
//            case
//                when ped.ocorrencia = 1 and ped.produto = '900350' then 'C'
//                else ''
//            end itubaina_2litros,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('900362', '900368', '903122') then 'C'
//                else ''
//            end skinka_450,
//            case
//                when ped.ocorrencia = 1 and ped.produto in ('219333', '120366') then 'C'
//                else ''
//            end dreher,
//            case
//                when ped.produto = '902312' then
//                    true
//                else
//                    false
//            end canal_glacial_300,
//            case
//                when ped.produto = '900090' then
//                    true
//                else
//                    false
//            end canal_glacial_600,
//            case
//                when ped.produto = '901133' then
//                    true
//                else
//                    false
//            end canal_glacial_litro
//        from pedidos ped
//        inner join clientes cli on cli.codigo = ped.cliente
//        inner join produtos prod on prod.codigo = ped.produto
//        inner join familias fam on fam.id = prod.id_familia
//        inner join categorias cat on fam.id_categoria = cat.id
//        join categoria_varejo cvc on cvc.codigo = cli.canal
//        where ped.cliente = $codigo
//        and ped.data_pedido >= date_add(date_add(LAST_DAY(current_date),interval 1 DAY),interval -1 MONTH)
//        and ped.data_pedido <= LAST_DAY(current_date)
//        group by ped.cliente
//    ");

//        $row = mysql_fetch_array($query);

//        return $row;
    }

    public static  function materiaisCliente($clienteId)
    {
        /*
         * MATERIAL PESADO
         * qtde que o cliente tem nas colunas cev/ref_peq + cev/ref_gde + cev/visa + cev/freezer
         * ---------------------------------------------
         * MATERIAL LEVE
         * qtde que o cliente tem nas colunas cev/mesa_plast + cev/mesa_mad
         * ---------------------------------------------
         * CHOPEIRA
         * qtde que o cliente tem nas colunas cev/chopeira (estava como vago2 e eu ajustei)
        */

        $Cev = TableRegistry::get('Cev');

        $query = $Cev->find();
        $query->select(['material_pesado' => $query->func()->sum('ref_peq  + visa + freezer')]);
        $query->select(['material_leve' => $query->func()->sum('mesa_plastico + mesa_mad')]);
        $query->select(['chopeira' => $query->func()->sum('vago2')]);

        $query->where(['clientes_id' => $clienteId]);

        return $query->first();
    }

    public static function buscar3MesesAnteriores()
    {
        $data = date('Y-m-01');

        // Modifica a data (7 dias atrás)
        $umMesAnterior = date('Y-m-d', strtotime($data. ' - 1 month'));
        $doisMesesAnteriores = date('Y-m-d', strtotime($data. ' - 2 month'));
        $tresMesesAnteriores = date('Y-m-d', strtotime($data. ' - 3 month'));

        return array(
            '1' => self::buscaMesAno($umMesAnterior),
            '2' => self::buscaMesAno($doisMesesAnteriores),
            '3' => self::buscaMesAno($tresMesesAnteriores),
        );
    }

    public static function buscaMesAno($data)
    {
        $mes = date( 'm', strtotime($data));
        $ano = date( 'Y', strtotime($data));

        $meses = [
            1 => 'JAN',
            2 => 'FEV',
            3 => 'MAR',
            4 => 'ABR',
            5 => 'MAI',
            6 => 'JUN',
            7 => 'JUL',
            8 => 'AGO',
            9 => 'SET',
            10 => 'OUT',
            11 => 'NOV',
            12 => 'DEZ',
        ];

        return $meses[intval($mes)] . '/' . $ano;
    }

    public static function buscaUltimosFaturamentos($codigo)
    {
        return [
            '1' => self::buscaFaturamentos($codigo, 1),
            '2' => self::buscaFaturamentos($codigo, 2),
            '3' => self::buscaFaturamentos($codigo, 3),
        ];
    }

    public static function buscaFaturamentos($codigo, $mesesAnteriores)
    {
        $PedidosHistoricos = TableRegistry::get('PedidosHistoricos');

        $query = $PedidosHistoricos->find();
        $query->select(['total' => $query->func()->sum('valor')]);
        $query->where([
            'cliente_id' => $codigo,
            'ocorrencia' => 1,
            "data_pedido >= DATE_ADD(LAST_DAY(NOW() - INTERVAL {$mesesAnteriores} + 1 MONTH), INTERVAL 1 DAY)",
            "data_pedido <= (LAST_DAY(NOW() - INTERVAL {$mesesAnteriores} MONTH))"
        ]);

        $result = $query->first();

        return !empty($result->total) ? 'R$ ' . number_format($result->total, 2,",",".") : 'R$ 0,00';
    }

    public function fdsCliente($codigo)
    {
        //- FDS: percentual dos campos que integram o FDS do canal do cliente com compra dividido pela quantidade possível.
    }
}
