<?php

namespace App\Service\Exportar;

use App\Service\ExportarService;
use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

class ExportarRelatorioEstoqueService extends ExportarService
{
    /**
     * @var string
     */
    private $model;

    /**
     * @var string
     */
    private $tipo;

    /**
     * @var int
     */
    private $id;

    /**
     * @var Entity
     */
    private $pessoa;

    /**
     * @var array
     */
    private $valoresHeader;

    /**
     * @var array
     */
    private $valoresBody;

    /**
     * @var string
     */
    private $colunaPessoaId;

    /**
     * @var \Cake\ORM\Table
     */
    private $_familiasTable;

    /**
     * @var \Cake\ORM\Table
     */
    private $_produtosTable;

    /**
     * @var \Cake\ORM\Table
     */
    private $_pedidosTable;

    /**
     * @var \Cake\ORM\Table
     */
    private $_metasTable;

    /**
     * @var Entity
     */
    private $produto;

    /**
     * @var array
     */
    private $produtos;

    public function __construct()
    {
        parent::__construct();
        $this->_familiasTable = TableRegistry::getTableLocator()
            ->get('Familias');
        $this->_produtosTable = TableRegistry::getTableLocator()
            ->get('Produtos');
        $this->_pedidosTable = TableRegistry::getTableLocator()
            ->get('Pedidos');
        $this->_metasTable = TableRegistry::getTableLocator()
            ->get('Metas');
    }

    public function gerarRelatorio()
    {
        $this->setTipo();
        $this->setModel();
        $this->setId();
        $this->setPessoa();

        $this->setProdutos();

        $this->setValoresHeader();
        $nome = "Estoque - {$this->getNomePessoa()}";

        $this->exportReport($nome, $this->getValoresHeader(), $this->getProdutos());
    }

    private function getNomePessoa()
    {
        return "{$this->getPessoa()->codigo} - {$this->getPessoa()->pessoaFisica->nome}";
    }

    private function getPessoa()
    {
        return $this->pessoa;
    }

    private function setPessoa()
    {
        $this->pessoa = TableRegistry::getTableLocator()
            ->get($this->getModel())
            ->buscar($this->getId());
    }

    private function setModel()
    {
        switch ($this->getTipo()) {
            case 'vendedor':
                $this->model = 'Vendedores';
                $this->colunaPessoaId = 'vendedores_id';
                break;
            case 'supervisor':
                $this->model = 'Supervisores';
                $this->colunaPessoaId = 'supervisores_id';
                break;
            case 'gerente':
                $this->model = 'Gerentes';
                $this->colunaPessoaId = 'gerentes_id';
                break;
            default:
                throw new \Exception('Tipo não configurado');
        }
    }

    private function getModel()
    {
        return $this->model;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @throws \Exception
     */
    public function setTipo(): void
    {
        if (empty($this->getRequestQuery('tipo'))) {
            throw new \Exception('Tipo não encontrado');
        }
        $this->tipo = $this->getRequestQuery('tipo');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @throws \Exception
     */
    public function setId(): void
    {
        if (empty($this->getRequestQuery('id'))) {
            throw new \Exception('Id não encontrado');
        }
        $this->id = $this->getRequestQuery('id');
    }

    /**
     * @return mixed
     */
    public function getValoresHeader()
    {
        return $this->valoresHeader;
    }

    /**
     * @param mixed $valoresHeader
     */
    public function setValoresHeader(): void
    {
        $this->valoresHeader = [
            'codigo' => 'Código',
            'descricao' => 'Descrição',
            'meta' => 'Meta',
            'real' => 'Real',
            'tend' => 'Tend %',
        ];
    }

    private function getProdutos()
    {
        return $this->produtos;
    }

    private function setProdutos()
    {
        $this->produtos = $this->_produtosTable
            ->find('list', [
                'valueField' => function($produto) {
                    $this->setProduto($produto);
                    $this->setValoresProduto();

                    return [
                        'codigo' => $this->produto->codigo,
                        'descricao' => $this->produto->nome,
                        'meta' => valueToValor($this->produto->meta, 2),
                        'real' => valueToValor($this->produto->real, 2),
                        'tendencia' => valueToValor($this->produto->tendencia, 2),
                    ];
                },
            ])
            ->select([
                'Produtos.id',
                'Produtos.codigo',
                'Produtos.nome',
            ])
            ->contain([
                'Familias'
            ])
            ->where($this->getCondicoes())
            ->group('Produtos.id')
            ->toArray();
    }

    private function getCondicoes()
    {
        $condicoes = [
            "Familias.grupo_id" => 1,
            "Familias.id NOT IN" => [7, 11, 34, 35, 36, 37, 90],
        ];

        return $condicoes;
    }

    /**
     * @return Entity
     */
    public function getProduto(): Entity
    {
        return $this->produto;
    }

    /**
     * @param Entity $produto
     */
    public function setProduto(Entity $produto): void
    {
        $this->produto = $produto;
    }

    private function setValoresProduto()
    {
        $this->produto->meta = $this->getMetaProduto();
        $this->produto->real = $this->getTotalVendaProduto();
        $this->produto->tendencia = $this->getTendenciaProduto();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    private function getMetaProduto()
    {
        $condicoes["Metas.mes_ano"] = Time::now()->i18nFormat('yyyy-MM-01');
        $condicoes["MetasPessoas.{$this->colunaPessoaId}"] = $this->getId();

        $meta = $this->_metasTable
            ->find()
            ->contain([
                'MetasPessoas',
            ])
            ->where($condicoes)
            ->first();

        if (empty($meta)) {
            throw new \Exception("Metas não configuradas");
        }

        return $meta->produto_meta;
    }

    private function getTotalVendaProduto()
    {
        $condicoes['Pedidos.data_pedido >='] = Time::now()->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = Time::now()->modify('last day of this month')->i18nFormat('yyyy-MM-dd');
        $condicoes['Produtos.id'] = $this->produto->id;

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Produtos'
            ])
            ->where($condicoes)
            ->group('Produtos.id');

        return $query
            ->select([
                'total_venda' => $query->func()->sum('Pedidos.quantidade'),
            ])
            ->sumOf('total_venda');
    }

    private function getTendenciaProduto()
    {
        $diasReal = 0;
        $diasMeta = 0;
        $totalDiasUteis = $this->getTotalDiasUteis();
        $totalDiasUteisMes = $this->getTotalDiasUteisMes();

        if ($this->produto->real !== 0 && $totalDiasUteis != 0) {
            $diasReal = ($this->produto->real / $totalDiasUteis);
        }

        if ($this->produto->meta !== 0 && $totalDiasUteisMes != 0) {
            $diasMeta = ($totalDiasUteisMes / $this->produto->meta);
        }

        return $diasReal * $diasMeta * 100;
    }
}
