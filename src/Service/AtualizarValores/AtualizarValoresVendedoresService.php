<?php


namespace App\Service\AtualizarValores;


use App\Model\Entity\Vendedore;
use App\Model\Entity\VendedoresValoresDiario;
use App\Service\BaseService;
use App\Service\RelatoriosCoberturas\FdsVendedoresService;
use App\Service\RelatoriosMatinais\FundamentosExeDistOutService;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

class AtualizarValoresVendedoresService extends BaseService
{
    private $_tableVendedores;
    private $_tableVendedoresValoresDiarios;

    private $_fdsVendedoresService;

    /**
     * @var Vendedore
     */
    private $vendedor;

    private $valoresFundamentos;

    /**
     * @var VendedoresValoresDiario
     */
    private $vendedorValorDiario;

    public function __construct()
    {
        parent::__construct();

        $this->_tableVendedores = TableRegistry::getTableLocator()
            ->get("Vendedores");

        $this->_tableVendedoresValoresDiarios = TableRegistry::getTableLocator()
                ->get("VendedoresValoresDiarios");

        $this->_fdsVendedoresService = new FdsVendedoresService();
    }

    public function atualizaValoresVendedores()
    {
        foreach ($this->getVendedores() as $vendedor) {
            $this->setVendedor($vendedor);

            $this->atualizaValores();
        }
    }


    private function getVendedores()
    {
        return $this->_tableVendedores
            ->find()
            ->where([
                'relatorios' => true
            ])
            ->toArray();
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor($vendedor): void
    {
        $this->vendedor = $vendedor;
    }

    private function atualizaValores()
    {
        $this->setVendedorValorDiario();
        $this->setValoresFundamentos();

        $entidade = $this->getVendedorValorDiario();

        //fds
        $entidade->fds = $this->_fdsVendedoresService->getFdsVendedor($this->vendedor);

        //fundamentos
        $entidade->fundamentos_refrigeracao = $this->valoresFundamentos['iav'];
        $entidade->fundamentos_iav = $this->valoresFundamentos['refrigeracao'];
        $entidade->fundamentos_cobTotal = $this->valoresFundamentos['cobTotal'];
        $entidade->fundamentos_fds = $this->valoresFundamentos['fds'];
        $entidade->fundamentos_devolucoes = $this->valoresFundamentos['devolucoes'];
        $entidade->fundamentos_trocas = $this->valoresFundamentos['trocas'];
        $entidade->fundamentos_terceiros = $this->valoresFundamentos['terceiros'];

        //data
        $entidade->modificado = Time::now();

        if (!$this->_tableVendedoresValoresDiarios->save($entidade)) {
            throw new \Exception('Erro ao atualizar valor', 400);
        }
    }

    /**
     * @return mixed
     */
    public function getVendedorValorDiario(): VendedoresValoresDiario
    {
        return $this->vendedorValorDiario;
    }

    /**
     *
     */
    public function setVendedorValorDiario(): void
    {
        $this->vendedorValorDiario = $this->_tableVendedoresValoresDiarios
            ->find()
            ->where([
                'vendedor_id' => $this->vendedor->id,
                'data' => Time::now()
            ])
            ->first();

        if (empty($this->vendedorValorDiario)) {
            $this->vendedorValorDiario = $this->_tableVendedoresValoresDiarios->newEntity();

            $this->vendedorValorDiario->vendedor_id = $this->vendedor->id;
            $this->vendedorValorDiario->data = Time::now();
        }
    }

    private function setValoresFundamentos()
    {
        $fundamentosService = new FundamentosExeDistOutService();
        $fundamentosService->setVendedor($this->vendedor);
        $fundamentosService->setValoresVendedor();

        $valores = $fundamentosService->getVendedor()->valores;

        $this->valoresFundamentos = [
            'iav' => $valores['iav']['porcentagem'],
            'refrigeracao' => $valores['refrigeracao']['porcentagem'],
            'cobTotal' => $valores['cobTotal']['porcentagem'],
            'fds' => $valores['fds']['porcentagem'],
            'devolucoes' => $valores['devolucoes']['porcentagem'],
            'trocas' => $valores['trocas']['porcentagem'],
            'terceiros' => $valores['terceiros']['porcentagem'],
        ];
    }
}
