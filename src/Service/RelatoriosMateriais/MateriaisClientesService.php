<?php

namespace App\Service\RelatoriosMateriais;

use App\Service\BaseRelatoriosService;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Service\RelatoriosMatinais\PlanejamentoVendedorService;
use Cake\Collection\Collection;

class MateriaisClientesService extends BaseRelatoriosService
{
    private $_clientesTable;
    private $_pedidosTable;
    private $_planejamentoVendedorService;

    private $clientes;
    private $cliente;

    public function __construct()
    {
        parent::__construct();

        $this->_clientesTable = TableRegistry::getTableLocator()->get('Clientes');
        $this->_pedidosTable = TableRegistry::getTableLocator()->get('Pedidos');

        $this->_planejamentoVendedorService = new PlanejamentoVendedorService();
    }

    /**
     * @return array
     */
    public function getValores($tipo)
    {
        $condicoes = [];
        if (!empty($this->getRequestQuery())) {
            $query = $this->getRequestQuery();
            if (!empty($query['cliente_id'])) {
                $condicoes['Clientes.id'] = $query['cliente_id'];
            }
            if (!empty($query['vendedor_id'])) {
                $condicoes['Clientes.vendedores_id'] = $query['vendedor_id'];
            }
        }

        $this->setClientes($tipo, $condicoes);
        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'clientes' => $this->getClientes(),
        ];
    }

    /**
     * @return mixed
     */
    public function getClientes()
    {
        return $this->clientes;
    }

    public function setClientes($tipo = 'materiais', $condicoes): void
    {
        $this->clientes = $this->_clientesTable
            ->find('list', [
                'keyField' => function($q) {},
                'valueField' => function($q) use($tipo) {
                    $this->setCliente($q);
                    $this->cliente->endereco = $this->montaEndereco();

                    switch ($tipo) {
                        case 'materiais';
                            $this->setValoresMateriais();
                            break;
                        case 'choperias';
                            $this->setValoresChoperias();
                            break;
                        case 'vasilhames';
                            $this->setValoresVasilhames();
                            break;
                    }

                    return $this->getCliente();
                },
            ])
            ->contain([
                'PessoasJuridicas.Enderecos',
                'Vendedores.PessoaFisica',
                'Cev',
            ])
            ->where($condicoes)
            ->innerJoinWith('Cev')
            ->matching('Cev', function ($q) {
                $q->select([
                    'tendencia_total' => $q->func()->sum('tendencia'),
                    'litro_total' => $q->func()->sum('litro'),
                    'ref_peq_total' => $q->func()->sum('ref_peq'),
                    'ref_grande_total' => $q->func()->sum('ref_grande'),
                    'mesa_plastico_total' => $q->func()->sum('mesa_plastico'),
                    'vago1_total' => $q->func()->sum('vago1'),
                    'vago2_total' => $q->func()->sum('vago2'),
                    'vago3_total' => $q->func()->sum('vago3'),
                    'visa_total' => $q->func()->sum('visa'),
                    'freezer_total' => $q->func()->sum('freezer'),
                    'mesa_mad_total' => $q->func()->sum('mesa_mad'),
                ]);

                return $q;
            })
            ->group('Clientes.id');
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente): void
    {
        $this->cliente = $cliente;
    }

    /**
     *
     */
    private function setValoresMateriais()
    {
        $this->_planejamentoVendedorService->setCliente($this->cliente);
        $this->cliente->meta = $this->getMeta();
        $this->cliente->mediasUltimosMeses = $this->_planejamentoVendedorService->getMediaUltimosMesesMateriais();
        $this->cliente->totalMesAtual = $this->getTotalMesAtualMateriais();
        $this->cliente->porcentagem = $this->getPorcentagemMetaReal();
    }

    /**
     * @return string
     */
    private function montaEndereco()
    {
        if (empty($this->cliente->pessoasJuridica->endereco)) {
            return '';
        }

        return ucfirst($this->cliente->pessoasJuridica->endereco->cidade) . '/' . $this->cliente->pessoasJuridica->endereco->uf;
    }

    /**
     * @return float|int
     */
    private function getMeta()
    {
        /**
         * Meta
         * Valor financeiro resultante do cálculo:
         * [ ( Qtde de “Ref Gde” * 1200 ) + ( Qtde de “Ref Peq” * 1000 ) + ( Qtde de “Visa” * 1000 ) ]
         */

        $refGrande = ($this->cliente->ref_grande_total * 1200);
        $refPequeno = ($this->cliente->ref_peq_total * 1000);
        $visa = ($this->cliente->visa_total * 1000);

        return $refGrande + $refPequeno + $visa;
    }

    /**
     * @return float
     */
    private function getTotalMesAtualMateriais()
    {
        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Produtos.Familias',
            ])
            ->where($this->getCondicoesMateriais());

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.valor')
            ])
            ->sumOf('total');
    }

    private function getCondicoesMateriais()
    {
        return [
            'Pedidos.ocorrencia' => 1,
            'Pedidos.cliente_id' => $this->cliente->id,
            'Familias.grupo_id' => 1,
            'Pedidos.data_pedido >=' => Time::now()->i18nFormat('yyyy-MM-01'),
            'Pedidos.data_pedido <=' => Time::now()->modify('last day of this month')->i18nFormat('yyyy-MM-dd'),
            'Produtos.considera_relatorios' => true
        ];
    }

    private function getPorcentagemMetaReal()
    {
        /**
         * %
         * [ ( “R$ mês atual” / “Meta” ) * 100]
         * obs.:
         * Este percentual será importante pois os clientes que atingirem determinado percentual contarão nos relatórios de refrigeração como “Real”.
         * Exemplo do cálculo com parâmetro em 70%, conforme descrevi no “Fundamentos Exe Dist Out”:
         * Meta nos fundamentos será os 70%.
         * Real nos fundamentos será:
         * [ ( Qtde de Clientes do vendedor que aqui tiveram o “%” acima de 100% / Qtde total de Clientes do vendedor com material ) * 100 ]
         */

        $real = $this->cliente->totalMesAtual;
        $meta = $this->cliente->meta;

        return $this->getPorcentagem($meta, $real);
    }

    /*
     *
     */
    private function setValoresChoperias()
    {
        $this->_planejamentoVendedorService->setCliente($this->cliente);
        $this->cliente->mediasUltimosMeses = $this->_planejamentoVendedorService->getMediaUltimosMesesChoperia();

        $this->cliente->meta = 0;
        $this->cliente->totalMesAtual = $this->getTotalMesAtualChoperias();
        $this->cliente->porcentagem = $this->getPorcentagemMetaReal();
    }

    /**
     * @return float
     */
    private function getTotalMesAtualChoperias()
    {
        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Produtos.Familias',
            ])
            ->where($this->getCondicoesChoperias());

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.valor')
            ])
            ->sumOf('total');
    }

    /**
     * @return array
     */
    private function getCondicoesChoperias()
    {
        return [
            'Pedidos.ocorrencia' => 1,
            'Pedidos.cliente_id' => $this->cliente->id,
            'Familias.id IN' => [20, 21],
            'Pedidos.data_pedido >=' => Time::now()->i18nFormat('yyyy-MM-01'),
            'Pedidos.data_pedido <=' => Time::now()->modify('last day of this month')->i18nFormat('yyyy-MM-dd'),
            'Produtos.considera_relatorios' => true
        ];
    }

    private function setValoresVasilhames()
    {
        $this->_planejamentoVendedorService->setCliente($this->cliente);
        $this->cliente->mediasUltimosMeses = $this->_planejamentoVendedorService->getMediaUltimosMesesVasilhames();

        $this->cliente->caixa600 = 0;
        if ($this->cliente->tendencia_total > 0) {
            $this->cliente->caixa600 = round($this->cliente->tendencia_total / 24, 2);
        }
        $this->cliente->caixa1Lt = 0;
        if ($this->cliente->litro_total > 0) {
            $this->cliente->caixa1Lt = round($this->cliente->litro_total / 12, 2);
        }

        $this->cliente->meta = ( $this->cliente->caixa600 + $this->cliente->caixa1Lt ) * 1;

        $this->cliente->totalMesAtual = $this->getTotalMesAtualVasilhames();
        $this->cliente->porcentagem = $this->getPorcentagemMetaReal();
    }

    /**
     * @return float
     */
    private function getTotalMesAtualVasilhames()
    {
        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Produtos.Familias',
            ])
            ->where($this->getCondicoesVasilhames());

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.valor')
            ])
            ->sumOf('total');
    }

    /**
     * @return array
     */
    private function getCondicoesVasilhames()
    {
        return [
            'Pedidos.ocorrencia' => 1,
            'Pedidos.cliente_id' => $this->cliente->id,
            'Familias.id IN' => [1, 2],
            'Pedidos.data_pedido >=' => Time::now()->i18nFormat('yyyy-MM-01'),
            'Pedidos.data_pedido <=' => Time::now()->modify('last day of this month')->i18nFormat('yyyy-MM-dd'),
            'Produtos.considera_relatorios' => true
        ];
    }

    public function getRealRefrigeracaoExeDistOut($vendedorId)
    {
        $condicoes['Clientes.vendedores_id'] = $vendedorId;

        $this->setClientes('materiais',  $condicoes);

        $clientes = $this->getClientes()->toArray();
        $collection = new Collection($clientes);

        return $collection->sumOf('porcentagem');
    }
}
