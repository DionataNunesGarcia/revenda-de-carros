<?php
namespace App\Service\RelatorioGerais;

use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\ORM\TableRegistry;
use App\Service\BaseRelatoriosService;

class RelatorioVendasService extends BaseRelatoriosService
{
    private $_pedidosTable;

    private $_metasTable;


    private $_modelPedidos;

    private $conditionsMetaCobertura;

    private $conditionsPedidosVolumeReal;

    private $conditionsPedidosPrecoMedio;

    /**
     * @var int $pessoa
     */
    private $pessoa;

    private $requestQuery;

    private $produto;

    /**
     * @var bool $isGerente
     */
    private $isGerente;

    /**
     * @var bool $isSupervisor
     */
    private $isSupervisor;

    /**
     * @var bool $isVendedor
     */
    private $isVendedor;

    public function __construct()
    {
        parent::__construct();

        $this->_metasTable = TableRegistry::getTableLocator()
            ->get('Metas');

    }

    /**
     * @return int
     */
    public function getPessoa(): int
    {
        return $this->pessoa;
    }

    /**
     *
     */
    public function setPessoa(): void
    {
        $requestQuery = $this->getRequestQuery();
        $pessoa = 1;
        if (!empty($requestQuery['tipo_pessoa'])) {
            $pessoa = $requestQuery['tipo_pessoa'];
        }
        $this->pessoa = $pessoa;
    }

    /**
     * @return boll
     */
    public function isGerente(): bool
    {
        return $this->isGerente;
    }

    /**
     *
     */
    public function setIsGerente(): void
    {
        $requestQuery = $this->getRequestQuery();
        $isGerente = false;
        if (!empty($requestQuery['gerente'])) {
            $isGerente = true;
            $this->pessoa = $requestQuery['gerente'];
        }
        $this->isGerente = $isGerente;
    }

    /**
     * @return bool
     */
    public function isSupervisor(): bool
    {
        return $this->isSupervisor;
    }

    /**
     *
     */
    public function setIsSupervisor(): void
    {
        $requestQuery = $this->getRequestQuery();
        $isSupervisor = false;
        if (!empty($requestQuery['supervisor'])) {
            $isSupervisor = true;
            $this->pessoa = $requestQuery['supervisor'];
        }
        $this->isSupervisor = $isSupervisor;
    }
    /**
     * @return bool
     */
    public function isVendedor(): bool
    {
        return $this->isVendedor;
    }

    /**
     *
     */
    public function setIsVendedor(): void
    {
        $requestQuery = $this->getRequestQuery();
        $setIsVendedor = false;
        if (!empty($requestQuery['vendedor'])) {
            $this->pessoa = $requestQuery['vendedor'];
            $setIsVendedor = true;
        }
        $this->isVendedor = $setIsVendedor;
    }

    /**
     * @return mixed
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param mixed $produto
     */
    public function setProduto($produto): void
    {
        $this->produto = $produto;
    }

    /**
     * @return array
     */
    public function getValores()
    {
        set_time_limit(1200);

        $requestQuery = $this->getRequestQuery();

        $data = Time::now();
        if (isset($requestQuery['data'])) {
            $dateTime = mesAnoToDate($requestQuery['data']);
            $data = new Date($dateTime);
        }


        $this->setData($data);
        $this->setPessoa();
        $this->setIsSupervisor();
        $this->setIsGerente();
        $this->setIsVendedor();
        $this->setPedidosTable();

        $this->setConditionsMetaCobertura();
        $this->setConditionsPedidosVolumeReal();
        $this->setConditionsPedidosPrecoMedio();

        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'produtos' => $this->getProdutos(),
        ];
    }

    /**
     * @return array
     */
    private function getProdutos()
    {
        $produtos = TableRegistry::getTableLocator()
            ->get('Produtos')
            ->find('list', [
                'keyField' => function(){},
                'valueField' => function($produto){

                    $this->setProduto($produto);
                    $this->setValoresVendas();

                    $this->produto->tipo = 'produto';

                    return $this->produto;
                },
            ])
            ->contain([
                'Familias',
            ])
            ->where([
                'Produtos.estoque' => 1,
                'Produtos.considera_relatorios' => true
            ])
//            ->innerJoinWith($this->_modelPedidos, function ($q) {
//                return $q->where([
//                    "{$this->_modelPedidos}.data_pedido >=" => $this->getData()
//                        ->i18nFormat('yyyy-MM-01'),
//                    "{$this->_modelPedidos}.data_pedido <=" => $this->getData()
//                        ->modify('last day of this month')
//                        ->i18nFormat('yyyy-MM-dd')
//                ]);
//            })
            ->group('Produtos.id')
            ->order([
                'Familias.id' => 'ASC',
                'Produtos.nome' => 'ASC',
            ])
            ->toArray();

        $retorno = $this->somarFamilias($produtos);

        return $retorno;
    }

    /**
     * @param array $produtos
     * @return array
     */
    private function somarFamilias(array $produtos)
    {
        $retorno = [];
        $familiaId = null;
        $familia = null;
        foreach ($produtos as $produto) {
            if (empty($familiaId)) {
                $familiaId = $produto->familia_id;
                $familia = $produto->familia;
            }

            if ($produto->familia_id != $familiaId) {
                //carrega os valores da familia
                $familia = $this->finalizaValoresFamilia($familia);

                $retorno[] = $familia;

                //troca de familia
                $familiaId = $produto->familia_id;
                $familia = $produto->familia;

                $familia = $this->zerarValoresFamilia($familia);
            }

            $familia = $this->somarValoresFamilia($familia, $produto);

            $retorno[] = $produto;
        }

        return $retorno;
    }

    /**
     * @param $familia
     * @return mixed
     */
    private function zerarValoresFamilia($familia)
    {
        $familia->total_meta = 0;
        $familia->total_meta_real = 0;
        $familia->tendencia = 0;
        $familia->total_meta_cobertura = 0;
        $familia->real_cobertura = 0;
        $familia->total_meta_pm = 0;
        $familia->real_pm = 0;

        return $familia;
    }

    /**
     * @param $familia
     * @param $produto
     * @return mixed
     */
    private function somarValoresFamilia($familia, $produto)
    {
        $familia->total_meta += $produto->total_meta;
        $familia->total_meta_real += $produto->total_meta_real;
        $familia->tendencia += $produto->tendencia;
        $familia->total_meta_cobertura += $produto->total_meta_cobertura;
        $familia->real_cobertura += $produto->real_cobertura;
        $familia->total_meta_pm += $produto->total_meta_pm;
        $familia->real_pm += $produto->real_pm;

        return $familia;
    }

    /**
     * @param $familia
     * @return mixed
     */
    private function finalizaValoresFamilia($familia)
    {
        $familia->codigo = '';

        $familia->tipo = 'familia';

        $familia->nome = 'TOTAL: ' . $familia->nome;

        $familia->total_meta_real = $familia->total_meta_real * $familia->hl;

        $familia->real_pm = ($familia->real_pm != 0 && $familia->hl !== 0) ? $familia->real_pm / $familia->hl : 0;

        return $familia;
    }

    /**
     *
     */
    private function setValoresVendas()
    {
        //total meta
        $this->produto->total_meta = $this->getTotalMetas();

        //total meta real
        $this->produto->total_meta_real = $this->getTotalMetasReal();

        //tendencia
        $this->produto->tendencia = $this->getTendenciaVenda();

        //meta cobertura
        $this->produto->total_meta_cobertura = $this->getMetaCobertura();

        //real cobertura
        $this->produto->real_cobertura = $this->getRealCobertura();

        //meta preco médio
        $this->produto->total_meta_pm = $this->getMetaPrecoMedio();

        //real preco médio
        $this->produto->real_pm = $this->getRealPrecoMedio();
    }

    /**
     * @return float
     */
    public function getTotalMetasReal()
    {
        $conditions = $this->getConditionsPedidosVolumeReal();
        $conditions['produtos_id'] = $this->produto->id;

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes.Vendedores.Supervisor'
            ])
            ->where($conditions)
            ->group("{$this->_modelPedidos}.produtos_id");

        $total = $query
            ->select([
                'total' => $query->func()->sum('quantidade')
            ])
            ->sumOf('total');

        return round($total);
    }

    /**
     * @return float
     */
    public function getTotalMetas()
    {
        $query = $this->getQueryMetas();

        return $query
            ->select([
                'produto_meta'
            ])
            ->sumOf('produto_meta');
    }

    private function getQueryMetas()
    {
        $data = $this->getData();
        $conditions = [
            'produtos_id' => $this->produto->id,
            'mes_ano' => $data->i18nFormat('yyyy-MM-01')
        ];

        if ($this->pessoa !== 1 && $this->isGerente()) {
            $conditions['MetasPessoas.gerentes_id'] = $this->pessoa;
        }

        if ($this->pessoa !== 1 && $this->isSupervisor()) {
            $conditions['MetasPessoas.supervisores_id'] = $this->pessoa;
        }

        if ($this->pessoa !== 1 && $this->isVendedor()) {
            $conditions['MetasPessoas.vendedores_id'] = $this->pessoa;
        }

        return $this->_metasTable
            ->find()
            ->contain('MetasPessoas')
            ->where($conditions)
            ->group('produtos_id');
    }

    /**
     * @return float
     */
    private function getTendenciaVenda()
    {
        $diasUteis = $this->getTotalDiasUteis();
        $diasUteisMes = $this->getTotalDiasUteisMes();
        $produtoMeta = $this->produto->total_meta;
        $total = $this->produto->total_meta_real;

        $tendencia = 0;
        if ($total != 0 && $diasUteis != 0 && $diasUteisMes != 0 && $produtoMeta != 0) {
            $tendencia = ($total / $diasUteis) * $diasUteisMes / $produtoMeta * 100;
        }

        return round($tendencia, 1);
    }

    /**
     * @return float
     */
    public function getMetaCobertura()
    {
        $query = $this->getQueryMetas();

        return $query
            ->select([
                'meta_cobertura'
            ])
            ->sumOf('meta_cobertura');
    }

    /**
     * @return float
     */
    private function getRealCobertura()
    {
        $conditions = $this->getConditionsMetaCobertura();
        $conditions['produtos_id'] = $this->produto->id;

        return $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes.Vendedores.Supervisor'
            ])
            ->where($conditions)
            ->group("{$this->_modelPedidos}.cliente_id")
            ->count();
    }

    /**
     * @return array
     */
    private function getConditionsMetaCobertura()
    {
        return $this->conditionsMetaCobertura;
    }

    /**
     * Cobertura soma cada cliente da “pessoa” que comprou ao menos 1,0 inteiro do produto e/ou família da linha,
     * somente ocorrência “1” e com base em:
     *
     * Se mês atual de produto: PEDIDOS / quantidade
     * Se mês atual de produto: PEDIDOS_HISTORICO / quantidade
     */
    private function setConditionsMetaCobertura()
    {
        $conditions = [
            "{$this->_modelPedidos}.ocorrencia IN" => [1],
            "{$this->_modelPedidos}.quantidade >=" => 1,
            "{$this->_modelPedidos}.data_pedido >=" => $this->getData()
                ->i18nFormat('yyyy-MM-01'),
            "{$this->_modelPedidos}.data_pedido <=" => $this->getData()
                ->modify('last day of this month')
                ->i18nFormat('yyyy-MM-dd')
        ];

        if ($this->pessoa !== 1 && $this->isGerente()) {
            $conditions['Supervisor.gerente_id'] = $this->pessoa;
        }

        if ($this->pessoa !== 1 && $this->isSupervisor()) {
            $conditions['Supervisor.id'] = $this->pessoa;
        }

        if ($this->pessoa !== 1 && $this->isVendedor()) {
            $conditions['Vendedores.id'] = $this->pessoa;
        }

        $this->conditionsMetaCobertura = $conditions;
    }

    /**
     * @return float
     */
    public function getMetaPrecoMedio()
    {
        $query = $this->getQueryMetas();

        return $query
            ->select([
                'meta_pm'
            ])
            ->sumOf('meta_pm');
    }

    /**
     * @return float
     */
    private function getRealPrecoMedio()
    {
        $conditions = $this->getConditionsPedidosPrecoMedio();
        $conditions['produtos_id'] = $this->produto->id;

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes.Vendedores.Supervisor'
            ])
            ->where($conditions)
            ->group("{$this->_modelPedidos}.produtos_id");

        $total = $query
            ->select([
                'total' => $query->func()->sum('valor')
            ])
            ->sumOf('total');

        $valor = 0;
        if ($this->produto->total_meta != 0 && $total != 0) {
            $valor = $total / $this->produto->total_meta;
        }

        return $valor;
    }

    private function setPedidosTable()
    {
        //verifica o mes atual
        $mesAtual = Time::now()->i18nFormat('YYYY-MM');

        //Se mês atual de produto: PEDIDOS
        $this->_modelPedidos  = 'Pedidos';

        //Se o mês for diferente, coloca a tabela Pedidos Historicos
        if ($this->getData()->i18nFormat('YYYY-MM') !== $mesAtual) {
            $this->_modelPedidos = "PedidosHistoricos";
        }

        $this->_pedidosTable = TableRegistry::getTableLocator()
            ->get($this->_modelPedidos);
    }

    /**
     * @return mixed
     */
    public function getConditionsPedidosVolumeReal()
    {
        return $this->conditionsPedidosVolumeReal;
    }

    /**
     *
     */
    public function setConditionsPedidosVolumeReal(): void
    {
        $data = $this->getData();

        $conditions = [
            "{$this->_modelPedidos}.ocorrencia" => 1,
            "{$this->_modelPedidos}.quantidade" => 1,
            "{$this->_modelPedidos}.data_pedido >=" => $data->i18nFormat('yyyy-MM-01'),
            "{$this->_modelPedidos}.data_pedido <=" => $data->modify('last day of this month')->i18nFormat('yyyy-MM-dd')
        ];

        if ($this->pessoa !== 1 && $this->isGerente()) {
            $conditions['Supervisor.gerente_id'] = $this->pessoa;
        }

        if ($this->pessoa !== 1 && $this->isSupervisor()) {
            $conditions['Supervisor.id'] = $this->pessoa;
        }

        if ($this->pessoa !== 1 && $this->isVendedor()) {
            $conditions['Vendedores.id'] = $this->pessoa;
        }

        $this->conditionsPedidosVolumeReal = $conditions;
    }

    /**
     * @return mixed
     */
    public function getConditionsPedidosPrecoMedio()
    {
        return $this->conditionsPedidosPrecoMedio;
    }

    /**
     *
     */
    public function setConditionsPedidosPrecoMedio(): void
    {
        $data = $this->getData();

        $conditions = [
            "{$this->_modelPedidos}.ocorrencia" => 1,
            "{$this->_modelPedidos}.quantidade" => 1,
            "{$this->_modelPedidos}.data_pedido >=" => $data->i18nFormat('yyyy-MM-01'),
            "{$this->_modelPedidos}.data_pedido <=" => $data->modify('last day of this month')->i18nFormat('yyyy-MM-dd')
        ];

        if ($this->pessoa !== 1 && $this->isGerente()) {
            $conditions['Supervisor.gerente_id'] = $this->pessoa;
        }

        if ($this->pessoa !== 1 && $this->isSupervisor()) {
            $conditions['Supervisor.id'] = $this->pessoa;
        }

        if ($this->pessoa !== 1 && $this->isVendedor()) {
            $conditions['Vendedores.id'] = $this->pessoa;
        }

        $this->conditionsPedidosPrecoMedio = $conditions;
    }
}
