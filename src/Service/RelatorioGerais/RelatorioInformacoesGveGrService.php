<?php
namespace App\Service\RelatorioGerais;

use App\Service\AtualizarRanking\AtualizarRankingVendedoresService;
use App\Service\RelatoriosCoberturas\CoberturasGeraisService;
use App\Service\RelatoriosCoberturas\FdsVendedoresService;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;
use App\Service\BaseRelatoriosService;
use Cake\Collection\Collection;

class RelatorioInformacoesGveGrService extends BaseRelatoriosService
{
    private $_metasMacroGveTable;

    private $_vendedoresTable;

    private $_metasGeraisTable;

    private $_nomenclaturasTable;

    private $_metasTable;

    private $_pedidosTable;

    private $_cevTable;

    private $_calculoRefrigeracaoTable;

    private $_iavTable;

    private $_tableVendedoresValoresDiarios;

    private $_coberturasGeraisService;

    private $_fdsVendedoresService;

    private $queryMetasMacroGve;

    private $vendedoresIds;

    private $dataFiltro;

    private $dataFiltroOntem;

    private $metasGerais;

    private $nomenclatura;

    private $cevVendedores;

    private $quantidadeTotalPdvs;

    private $queryCevs;

    private $valoresCalculoRefrigeracao;

    private $queryIavVendedores;

    private $totalVendedores;

    /**
     * @var int
     */
    private $metaCob = 0;

    public function __construct()
    {
        parent::__construct();

        $this->_vendedoresTable = TableRegistry::getTableLocator()
            ->get('Vendedores');

        $this->_metasMacroGveTable = TableRegistry::getTableLocator()
            ->get('MetasMacrosGves');

        $this->_metasTable = TableRegistry::getTableLocator()
            ->get('Metas');

        $this->_metasGeraisTable = TableRegistry::getTableLocator()
            ->get('MetasGerais');

        $this->_cevTable = TableRegistry::getTableLocator()
            ->get('Cev');

        $this->_iavTable = TableRegistry::getTableLocator()
            ->get('Iav');

        $this->_nomenclaturasTable = TableRegistry::getTableLocator()
            ->get('NomenclaturasRelatorios');

        $this->_tableVendedoresValoresDiarios = TableRegistry::getTableLocator()
            ->get('VendedoresValoresDiarios');

        $this->_pedidosTable = TableRegistry::getTableLocator()
            ->get('Pedidos');

        $this->_calculoRefrigeracaoTable = TableRegistry::getTableLocator()
            ->get('CalculoRefrigeracao');

        $this->_coberturasGeraisService = new CoberturasGeraisService();

        $this->_fdsVendedoresService = new FdsVendedoresService();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getValores()
    {
        $this->setVendedoresFiltro();

        $this->setDataFiltro();
        $this->setDataFiltroOntem();

        $this->setMetasGerais();

        $this->setQueryMetasMacroGve();

        return [
            'lastUpdate' => $this->getLastUpdate(),
            'data' => $this->getData(),
            'valoresDistribuicao' => $this->getValoresDistribuicao(),
            'valoresVolume' => $this->getValoresVolume(),
            'execucaoPdvsRefrigeracao' => $this->getExecucaoPdvsRefrigeracao(),
            'execucaoIavIv' => $this->getExecucaoIavIv(),
            'totalVendedores' => $this->totalVendedores,
        ];
    }

    public function setDataFiltro()
    {
        if (empty($this->getRequestQuery('data'))) {
            throw new \Exception('Não foi passado uma data como parametro');
        }

        $data = $this->getRequestQuery('data');
        $this->dataFiltro  = Time::parseDate($data, 'dd/MM/yyyy');
    }

    public function setDataFiltroOntem()
    {
        $data = $this->getRequestQuery('data');
        $this->dataFiltroOntem = Time::parseDate($data, 'dd/MM/yyyy')->modify('-1 day');
    }

    /**
     * @return mixed
     */
    public function getMetasGerais()
    {
        return $this->metasGerais;
    }

    /**
     *
     */
    public function setMetasGerais(): void
    {
        $query = $this->_metasGeraisTable
            ->find();

        $metasGerais = $query
            ->select([
                'total_meta_rob_hei' => $query->func()->sum('meta_rob_hei'),
                'total_meta_rob_3' => $query->func()->sum('meta_rob_3'),
                'total_meta_dev' => $query->func()->sum('meta_dev'),
                'total_meta_finan_troca' => $query->func()->sum('meta_finan_troca'),
                'total_meta_cerveja' => $query->func()->sum('meta_cerveja'),
                'total_meta_total' => $query->func()->sum('meta_total'),
                'total_meta_300_600_1000' => $query->func()->sum('meta_300_600_1000'),
                'total_meta_retornavel' => $query->func()->sum('meta_retornavel'),
                'total_meta_nao_alcoolico' => $query->func()->sum('meta_nao_alcoolico'),
                'total_meta_eisenbahn_600_r' => $query->func()->sum('meta_eisenbahn_600_r'),
                'total_meta_premium' => $query->func()->sum('meta_premium'),
                'total_meta_devassa' => $query->func()->sum('meta_devassa'),
                'total_meta_schin' => $query->func()->sum('meta_schin'),
                'total_meta_schin_600' => $query->func()->sum('meta_schin_600'),
                'total_meta_600_r' => $query->func()->sum('meta_600_r'),
                'total_meta_300_r' => $query->func()->sum('meta_300_r'),
                'total_meta_1000_r' => $query->func()->sum('meta_1000_r'),
                'total_meta_devassa_600_r' => $query->func()->sum('meta_devassa_600_r'),
                'total_meta_eisenbahn' => $query->func()->sum('meta_eisenbahn'),
                'total_fds' => $query->func()->sum('fds'),
            ])
            ->contain([
                'MetasGeraisPessoas'
            ])
            ->where([
                'mes_ano' => $this->dataFiltro->i18nFormat('yyyy-MM-01'),
                'vendedores_id IN' => $this->vendedoresIds
            ])
            ->first();

        $this->metasGerais = $metasGerais;
    }

    /***
     * INICIO DISTRIBUIÇÃO
     * =================================================================================================================
     */
    /**
     * @return array
     */
    private function getValoresDistribuicao()
    {
        return [
            'cobFds' => $this->getCobFds(),
            'cobTotal' => $this->getCobTotal(),
            'cobSchin' => $this->getCobSchin(),
            'cobDevassa' => $this->getCobDevassa(),
            'cobEisenbahn' => $this->getCobEisenbahn(),
            'cob3006001000' => $this->getValorCob3006001000(),
            'cobRetornaveisSeiscentos' => $this->getCobRetornaveisSeiscentos(),
            'cobRetornaveisMil' => $this->getCobRetornaveisMil(),
            'cobRetornaveisTrezentos' => $this->getCobRetornaveisTrezentos(),
            'cobSchinSeiscentos' => $this->getCobSchinSeiscentos(),
            'cobEisenbahnSeiscentos' => $this->getCobEisenbahnSeiscentos(),
            'cobDevassaSeiscentos' => $this->getCobDevassaSeiscentos(),
            'cobPremium' => $this->getCobPremium(),
            'cobNaoAlcoolico' => $this->getNaoAlcoolico(),
        ];
    }

    /**
     * @return mixed
     */
    public function getQueryMetasMacroGve()
    {
        return $this->queryMetasMacroGve;
    }

    /**
     *
     */
    public function setQueryMetasMacroGve(): void
    {
        $queryMetasMacroGve = $this->_metasMacroGveTable
            ->find()
            ->where([
                'mes_validade' => $this->dataFiltroOntem->i18nFormat('yyyy-MM-01')
            ]);

        $this->queryMetasMacroGve = $queryMetasMacroGve;
    }

    /**
     * @return mixed
     */
    public function getMetaCob()
    {
        return $this->metaCob;
    }

    /**
     * @param string $sigla
     */
    public function setMetaCob(string $sigla): void
    {
        $query = $this->getQueryMetasMacroGve();

        $metaCob = $query
            ->where([
                'sigla' => $sigla,
                'sigla' => $sigla,
            ])
            ->first();

        $this->metaCob = $metaCob;
    }

    private function setVendedoresFiltro()
    {
        $conditions = $this->getCondicoesVendedores();

        $vendedoresIds = $this->_vendedoresTable
            ->find('list', [
                'valueField' => 'id',
                'keyField' => 'id',
            ])
            ->contain([
                'Supervisor.Gerentes'
            ])
            ->where($conditions)
            ->toArray();

        $this->totalVendedores = count($vendedoresIds);

        if (empty($vendedoresIds)) {
            $vendedoresIds = [0];
        }
        $this->vendedoresIds = !empty($vendedoresIds) ? $vendedoresIds : [0];
    }

    private function getCondicoesVendedores()
    {
        $condicoes = [
            'Vendedores.relatorios' => true,
            'Vendedores.considera_relatorios' => true
        ];

        $query = $this->getRequestQuery();
        if (isset($query['tipo']) && $query['tipo'] == 'supervisor') {
            $condicoes['Supervisor.id'] = $query['supervisor'];
        }
        if (isset($query['tipo']) && $query['tipo'] == 'gerente') {
            $condicoes['Gerentes.id'] = $query['gerente'];
        }
        return $condicoes;
    }

    /**
     * Inicio COB FDS
     *
     * @return array
     */
    private function getCobFds()
    {
        $metaCobTotal = $this->getMetasGerais()->total_fds;

        $cobTotalDia = $this->getCobDiaFds();
        $cobTotalAnterior = $this->getCobFdsOntem();

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCobTotal,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCobTotal),
        ];
    }

    private function getCobDiaFds()
    {
        return $this->_tableVendedoresValoresDiarios
            ->find()
            ->where([
                'vendedor_id IN' => $this->vendedoresIds,
                'data' => $this->dataFiltro->i18nFormat('yyyy-MM-dd')
            ])
            ->sumOf('fds');
    }

    private function getCobFdsOntem()
    {
        return $this->_tableVendedoresValoresDiarios
            ->find()
            ->where([
                'vendedor_id IN' => $this->vendedoresIds,
                'data' => $this->dataFiltroOntem->i18nFormat('yyyy-MM-dd')
            ])
            ->sumOf('fds');
    }

    /**
     * Inicio COB TOTAL
     *
     * @return array
     */
    private function getCobTotal()
    {
        $metaCobTotal = $this->getMetasGerais()->total_meta_total;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->getValorCobTotal($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->getValorCobTotal($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCobTotal,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCobTotal),
        ];
    }

    /**
     * @param int|null $vendedorId
     * @param null $dataPedido
     * @return int|null
     */
    public function getValorCobTotal(int $vendedorId = null, $dataPedido = null, $somenteNoDia = null)
    {
        $conditions = $this->getCondicoesNomenclaturaSlug('info-cob-tt');

        if ($vendedorId) {
            $conditions['Clientes.vendedores_id'] = $vendedorId;
        }

        if ($dataPedido) {
            $conditions['Pedidos.data_pedido <='] = $dataPedido;
        }

        if ($somenteNoDia) {
            $conditions['Pedidos.data_pedido'] = $somenteNoDia;
        }

        return $this->getPedidos($conditions);
    }

    private function getPedidos(array $conditions)
    {
        $conditions['Produtos.considera_relatorios'] = true;

        $query = TableRegistry::getTableLocator()
            ->get("Pedidos")
            ->find()
            ->contain([
                'Clientes',
                'Produtos' => [
                    'Marcas',
                    'Familias.Categorias',
                ]
            ])
            ->where($conditions)
            ->group('Pedidos.cliente_id')
            ->orderAsc('Pedidos.quantidade')
            ->count();

        return $query;
    }

    /**
     * @param string $slug
     * @return array
     */
    protected function getCondicoesNomenclaturaSlug(string $slug)
    {
        $nomenclaturasRelatoriosTable = TableRegistry::getTableLocator()
            ->get('NomenclaturasRelatorios');

        $response = $nomenclaturasRelatoriosTable
            ->getNomenclatura($slug);

        $entidade = $response['entidade'];

        $valorCobertura = 0.1;
        if ($entidade->tipo_cobertura == 'SKU') {
            $valorCobertura = 1;
        }

        $condicoes['Pedidos.quantidade >='] = $valorCobertura;

        if (!empty($response['ocorrencias'])) {
            $condicoes["ocorrencia IN"] = $response['ocorrencias'];
        }

        $condicoes["{$response['model']}.id IN"] = $response['codigos'];

        return $condicoes;
    }

    /**
     * Inicio COB SCHIN
     *
     * @return array
     */
    private function getCobSchin()
    {
        $metaCob = $this->getMetasGerais()->total_meta_schin;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobM1Schin($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobM1Schin($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB DEVASSA
     *
     * @return array
     */
    private function getCobDevassa()
    {
        $metaCob = $this->getMetasGerais()->total_meta_devassa;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobDevassa($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobDevassa($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB Eisenbahn
     *
     * @return array
     */
    private function getCobEisenbahn()
    {
        $metaCob = $this->getMetasGerais()->total_meta_eisenbahn;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobEisenbahn($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobEisenbahn($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB Eisenbahn
     *
     * @return array
     */
    private function getCobRetornaveis()
    {
        $metaCob = $this->getMetasGerais();

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobRetornaveisGveGr($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobRetornaveisGveGr($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB Retornaveis Seiscentos
     *
     * @return array
     */
    private function getCobRetornaveisSeiscentos()
    {
        $metaCob = $this->getMetasGerais()->total_meta_600_r;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobRetornaveisSeiscentos($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobRetornaveisSeiscentos($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB Retornaveis Mil
     *
     * @return array
     */
    private function getCobRetornaveisMil()
    {
        $metaCob = $this->getMetasGerais()->total_meta_1000_r;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobRetornaveisMil($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobRetornaveisMil($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB Retornaveis Trezentos
     *
     * @return array
     */
    private function getCobRetornaveisTrezentos()
    {
        $metaCob = $this->getMetasGerais()->total_meta_300_r;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobRetornaveisTrezentos($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobRetornaveisTrezentos($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB SCHIN Seiscentos
     *
     * @return array
     */
    private function getCobSchinSeiscentos()
    {
        $metaCob = $this->getMetasGerais()->total_meta_schin_600;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobSchin600($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobSchin600($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB Eisenbahn Seiscentos
     *
     * @return array
     */
    private function getCobEisenbahnSeiscentos()
    {
        $metaCob = $this->getMetasGerais()->total_meta_eisenbahn_600_r;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobEisen600($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobEisen600($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB 300 + 600 + 1000
     *
     * @return array
     */
    private function getValorCob3006001000()
    {
        $metaCob = $this->getMetasGerais()->total_meta_300_600_1000;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCob3006001000($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCob3006001000($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB Devassa Seiscentos
     *
     * @return array
     */
    private function getCobDevassaSeiscentos()
    {
        $metaCob = $this->getMetasGerais()->total_meta_devassa_600_r;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobDevassa600($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobDevassa600($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB Especiais
     *
     * @return array
     */
    private function getCobPremium()
    {
        $metaCob = $this->getMetasGerais()->total_meta_premium;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobEspeciais($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobEspeciais($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }

    /**
     * Inicio COB Nao Alcoolico
     *
     * @return array
     */
    private function getNaoAlcoolico()
    {
        $metaCob = $this->getMetasGerais()->total_meta_nao_alcoolico;

        $cobTotalDia = 0;
        $cobTotalAnterior = 0;
        foreach ($this->vendedoresIds as $id) {
            if ($id == 0) {
                continue;
            }
            $cobTotalDia += $this->_coberturasGeraisService->getValorCobNaoAlcool($id, null, $this->dataFiltro);
            $cobTotalAnterior += $this->_coberturasGeraisService->getValorCobNaoAlcool($id, $this->dataFiltroOntem);
        }

        $acumuladoAtual = $cobTotalAnterior + $cobTotalDia;

        return [
            'meta' => $metaCob,
            'acumuladoDiaAnterior' => $cobTotalAnterior,
            'cobDia' => $cobTotalDia,
            'acumuladoAtual' => $acumuladoAtual,
            'porcentagem' => $this->getPorcentagem($acumuladoAtual, $metaCob),
        ];
    }
    /***
     * =================================================================================================================
     * FIM DISTRIBUIÇÃO
     */

    /***
     * INICIO VOLUME
     * =================================================================================================================
     */

    /**
     * @return array
     */
    private function getValoresVolume()
    {
        return [
            'volume3006001000' => $this->getValoresVolumePorSlug('info-vol-cerv-600+300+1000-ret-e-desc', 'hl'),
            'volume600UmbRetor' => $this->getValoresVolumePorSlug('info-vol-cerv-600ml-ret', 'umb'),
            'volume1000UmbRetor' => $this->getValoresVolumePorSlug('info-vol-cerv-1000ml-ret', 'umb'),
            'volume300UmbRetor' => $this->getValoresVolumePorSlug('info-vol-cerv-300ml-ret', 'umb'),
            'volumePremium' => $this->getValoresVolumePorSlug('info-vol-premium', 'hl'),
            'volumeEisenbahn' => $this->getValoresVolumePorSlug('info-vol-eisenbahn', 'hl'),
            'volumeDevassa' => $this->getValoresVolumePorSlug('info-vol-devassa', 'hl'),
            'volumeOutrosAlcoolicos' => $this->getValoresVolumePorSlug('info-vol-outros-alcoolicos', 'hl'),
            'volumeNaoAlcoolicos' => $this->getValoresVolumePorSlug('info-vol-nao-alcoolicos', 'hl'),
            'volumeTotal' => $this->getValoresVolumePorSlug('info-vol-tt', 'hl'),
            'volumeRobTotal' => $this->getValoresVolumePorSlugValor('info-rob-tt'),
        ];
    }

    /**
     * @param string $slug
     * @return array
     */
    private function getValoresVolumePorSlug(string $slug, string $tipoCalculo = 'hl')
    {
        $this->setNomenclatura($slug);

        $condicoes = $this->getCondicoesNomenclaturaSlug($slug);

        $meta = $this->getValorMeta($condicoes);

        $vendasDia = $this->getValorVendasDia($condicoes);

        $diaAnterior = $this->getValorRealizadoDiaAnterior($condicoes, $tipoCalculo);

        $vendaAcumulada = $this->getValorRealizadoTodoMes($condicoes);

        $tendencia = $this->getTendencia($vendaAcumulada, $meta);

        $decimais = ($tipoCalculo == 'hl') ? 2 : 1;

        return [
            'vendaDia' => round($vendasDia, $decimais),
            'tend' => round($tendencia, $decimais),
            'meta' => round($meta, $decimais),
            'diaAnterior' => round($diaAnterior, $decimais),
            'vendaAcumulada' => round($vendaAcumulada, $decimais),
        ];
    }

    /**
     * @param string $slug
     * @return array
     */
    private function getValoresVolumePorSlugValor(string $slug, string $tipoCalculo = 'valor')
    {
        $this->setNomenclatura($slug);

        $condicoes = $this->getCondicoesNomenclaturas($slug);

        $meta = $this->getValorMeta($condicoes);

        $vendasDia = $this->getValorVendasDia($condicoes);

        $diaAnterior = $this->getValorRealizadoDiaAnterior($condicoes, 'valor');

        $vendaAcumulada = $this->getValorRealizadoTodoMes($condicoes);

        $tendencia = $this->getTendencia($vendaAcumulada, $meta);

        return [
            'vendaDia' => valueToValorCifrado($vendasDia),
            'tend' => round($tendencia, 1),
            'meta' => round($meta, 1),
            'diaAnterior' => valueToValorCifrado($diaAnterior),
            'vendaAcumulada' => valueToValorCifrado($vendaAcumulada),
        ];
    }

    /**
     * @param array $condicoes
     * @return float
     */
    private function getValorRealizadoDiaAnterior(array $condicoes, $tipoCalculo)
    {
        $condicoes['Pedidos.data_pedido >='] = $this->dataFiltroOntem->i18nFormat('yyyy-MM-01');
        $condicoes['Pedidos.data_pedido <='] = $this->dataFiltroOntem->i18nFormat('yyyy-MM-dd');
        $query = $this->getQueryReal($condicoes);

        if ($tipoCalculo == 'hl') {
            return $query
                ->select([
                    'total' => $query->func()->sum('Pedidos.quantidade_hl')
                ])
                ->sumOf('total');
        }

        if ($tipoCalculo == 'umb') {
            return $query
                ->select([
                    'total' => $query->func()->sum('Pedidos.quantidade'),
                ])
                ->sumOf('total');
        }

        if ($tipoCalculo == 'valor') {
            return $query
                ->select([
                    'total' => $query->func()->sum('Pedidos.valor'),
                ])
                ->sumOf('total');
        }
    }

    /**
     * @return mixed
     */
    private function getValorRealizadoTodoMes(array $condicoes)
    {
        $condicoes['Pedidos.data_pedido >='] = $this->dataFiltroOntem
            ->i18nFormat('yyyy-MM-01');

        $condicoes['Pedidos.data_pedido <='] = $this->dataFiltroOntem
            ->modify('last day of this month')
            ->i18nFormat('yyyy-MM-dd');

        $query = $this->getQueryReal($condicoes);

        return $query
            ->select([
                'total' => $query->func()->sum('Pedidos.quantidade'),
            ])
            ->sumOf('total');
    }

    /**
     * @param array $condicoes
     * @return array|\Cake\ORM\Query
     */
    public function getQueryReal(array $condicoes)
    {
        $condicoes['Clientes.vendedores_id IN'] = $this->vendedoresIds;
        $conditions['Produtos.considera_relatorios'] = true;

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos' => [
                    'Familias.Categorias',
                    'Marcas',
                ],
            ])
            ->where($condicoes);

        return $query;
    }

    /**
     * @param float $vendaAcumulado
     * @param float $meta
     * @return float|int
     */
    private function getTendencia(float $vendaAcumulado, float $meta)
    {
        $this->setTotalDiasUteis();
        $this->setTotalDiasUteisMes();

        $diasUteis = $this->getTotalDiasUteis();
        $diasUteisMes = $this->getTotalDiasUteisMes();

        if ($vendaAcumulado == 0 || $meta == 0) {
            return 0;
        }

        return ($vendaAcumulado / $diasUteis) * $diasUteisMes / $meta * 100;

    }

    /**
     * @return mixed
     */
    private function getValorMeta(array $condicoes)
    {
        if (isset($condicoes['Pedidos.quantidade >='])) {
            unset($condicoes['Pedidos.quantidade >=']);
        }
        $query = $this->getQueryMetas($condicoes);
        $valorMetaHl = $query
            ->select([
                'meta' => $query->func()->sum('Familias.hl * Metas.produto_meta')
            ])
            ->sumOf('meta');

        return $valorMetaHl;
    }

    /**
     * @param array $condicoes
     * @return array|\Cake\ORM\Query
     */
    public function getQueryMetas(array $condicoes)
    {
        if (isset($condicoes['ocorrencia IN'])) {
            unset($condicoes['ocorrencia IN']);
        }
        $condicoes['Metas.mes_ano'] = $this->getData()->i18nFormat('yyyy-MM-01');
        $condicoes['MetasPessoas.vendedores_id IN'] = $this->vendedoresIds;
        $conditions['Produtos.considera_relatorios'] = true;

        $query = $this->_metasTable
            ->find()
            ->contain([
                'MetasPessoas',
                'Produtos' => [
                    'Familias.Categorias',
                    'Marcas',
                ],
            ])
            ->where($condicoes);

        return $query;
    }

    /**
     * @param array $condicoes
     * @return float|int
     */
    private function getValorVendasDia(array $condicoes)
    {
        $valor = 0;

        $condicoes['Pedidos.data_pedido'] = $this->dataFiltro->i18nFormat('yyyy-MM-dd');
        $condicoes['Clientes.vendedores_id IN'] = $this->vendedoresIds;
        $conditions['Produtos.considera_relatorios'] = true;

        $query = $this->_pedidosTable
            ->find()
            ->contain([
                'Clientes',
                'Produtos' => [
                    'Familias.Categorias',
                    'Marcas',
                ],
            ])
            ->where($condicoes);

        if ($this->nomenclatura->volume == 'HL') {
            $valor = $query
                ->select([
                    'valor' => $query->func()->sum('Pedidos.quantidade_hl'),
                ])
                ->sumOf('valor');
        }

        if ($this->nomenclatura->volume == 'UMB') {
            $valor = $query
                ->select([
                    'valor' => $query->func()->sum('Pedidos.quantidade'),
                ])
                ->sumOf('valor');
        }

        if ($this->nomenclatura->volume == 'Moeda') {
            $valor = $query
                ->select([
                    'valor' => $query->func()->sum('Pedidos.valor'),
                ])
                ->sumOf('valor');
        }

        return $valor;
    }

    /**
     * @param string $slug
     */
    private function setNomenclatura(string $slug)
    {
        $response = $this->_nomenclaturasTable
            ->getNomenclatura($slug);

        $this->nomenclatura = $response['entidade'];
    }

    /**
     * @return array
     */
    private function getExecucaoPdvsRefrigeracao()
    {
        $this->setQueryCevs();
        $this->setCevVendedores();
        $this->setValoresCalculoRefrigeracao();

        return [
            'totalEquipamentosPdvs' => $this->getTotalEquipamentosPdvs(),
            'pdvsEquipamentosRefrigerador' => $this->getPdvsEquipamentosRefrigerador(),
            'refrigeracaoAltaPerformance' => $this->getRefrigeracaoAltaPerformance(),
            'refrigeracaoBaixaPerformance' => $this->getRefrigeracaoBaixaPerformance(),
            'refrigeracaoGiroZero' => $this->getRefrigeracaoGiroZero(),
        ];
    }

    /**
     * Soma da quantidade de materiais de refrigeração em clientes (Ref_Gde, Ref_Peq, Freezer e Visa)
     *
     * @return int[]
     */
    private function getTotalEquipamentosPdvs()
    {
        $query = $this->queryCevs;

        $this->quantidadeTotalPdvs = $query
            ->select([
                'quantidade' => $query->func()->sum('ref_peq + ref_grande + visa + freezer')
            ])
            ->sumOf('quantidade');

        return [
            'quantidade' => $this->quantidadeTotalPdvs,
        ];
    }

    /**
     * Quantidade de clientes com algum destes refrigeradores comodatados.
     * O percentual deve ser desta quantidade de clientes com material dividido pelo número de clientes ativos (sem bloqueio),
     * multiplicado por 100.
     *
     * @return int[]
     */
    private function getPdvsEquipamentosRefrigerador()
    {
        $query = $this->queryCevs;

        $quantidade = $query
            ->where([
                'clientes_id IS NOT' => NULL
            ])
            ->group('clientes_id')
            ->count();

        $ativos = $query
            ->where([
                'clientes_id IS NOT' => NULL,
                'Clientes.codigo_bloqueio' => ''
            ])
            ->group('clientes_id')
            ->count();

        $porcentagem = $this->getPorcentagem($quantidade, $ativos);

        return [
            'quantidade' => $quantidade,
            'porcentagem' => $porcentagem,
        ];
    }

    /**
     * Quantidade de material de refrigeração que estão comodatados em clientes que alcançaram 100% de sua meta do mês,
     * vide relatório de Refrigeração por cliente.
     * Percentual destes sobre a quantidade total de material de refrigeração comodatado.
     *
     * @return int[]
     */
    private function getRefrigeracaoAltaPerformance()
    {
        $quantidade = $this->valoresCalculoRefrigeracao['maior_100_ref_grande']
            + $this->valoresCalculoRefrigeracao['visa_maior_100'];

        $porcentagem = $this->getPorcentagem($this->quantidadeTotalPdvs, $quantidade);

        return [
            'quantidade' => $quantidade,
            'porcentagem' => $porcentagem,
        ];
    }

    /**
     * Quantidade de material de refrigeração que estão comodatados em clientes que estão entre 1% e 99% de sua meta do mês,
     * vide relatório de Refrigeração por cliente.
     * Percentual destes sobre a quantidade total de material de refrigeração comodatado.
     *
     * @return int[]
     */
    private function getRefrigeracaoBaixaPerformance()
    {
        $quantidade = $this->valoresCalculoRefrigeracao['menor_80_ref_grande']
            + $this->valoresCalculoRefrigeracao['entre_80_100_ref_grande']
            + $this->valoresCalculoRefrigeracao['visa_menor_80']
            + $this->valoresCalculoRefrigeracao['visa_maior_80'];

        $porcentagem = $this->getPorcentagem($this->quantidadeTotalPdvs, $quantidade);

        return [
            'quantidade' => $quantidade,
            'porcentagem' => $porcentagem,
        ];
    }

    /**
     * Quantidade de material de refrigeração que estão comodatados em clientes que estão com 0% de sua meta do mês,
     * vide relatório de Refrigeração por cliente.
     * Percentual destes sobre a quantidade total de material de refrigeração comodatado.
     *
     * @return int[]
     */
    private function getRefrigeracaoGiroZero()
    {
        $quantidade = $this->valoresCalculoRefrigeracao['giro_zero_ref_grande']
            + $this->valoresCalculoRefrigeracao['giro_zero_visa'];

        $porcentagem = $this->getPorcentagem($this->quantidadeTotalPdvs, $quantidade);

        return [
            'quantidade' => $quantidade,
            'porcentagem' => $porcentagem,
        ];
    }

    public function setValoresCalculoRefrigeracao()
    {
        $this->valoresCalculoRefrigeracao = $this->_calculoRefrigeracaoTable
            ->getValoresVendedores($this->vendedoresIds);
    }

    /**
     *
     */
    public function setQueryCevs(): void
    {
        $queryCevs = $this->_cevTable
            ->find()
            ->contain([
                'Clientes'
            ])
            ->where([
                'OR' => [
                    'Cev.ref_peq !=' => 0,
                    'Cev.ref_grande !=' => 0,
                    'Cev.visa !=' => 0,
                    'Cev.freezer !=' => 0,
                ],
                'Clientes.vendedores_id IN' => $this->vendedoresIds,
            ]);

        $this->queryCevs = $queryCevs;
    }

    /**
     *
     */
    private function setCevVendedores()
    {
        $query = $this->queryCevs;

        $this->cevVendedores = $query->toArray();
    }

    /**
     * @return int[]
     */
    private function getExecucaoIavIv()
    {
        $this->setQueryIavVendedores();

        return [
            'iav' => $this->getExecucaoIav(),
            'iv' => $this->getExecucaoIv(),
        ];
    }

    /**
     * IAV
     * Vide relatório de Cobertura / IAV (ou tabela IAV)
     * [
     *      ( Tabela IAV coluna ”totped” quando mês selecionado para abrir ) / ( Tabela IAV coluna ”visit” quando mês selecionado para abrir )
     * ] * 100
     *
     * @return float
     */
    private function getExecucaoIav()
    {
        $query = $this->queryIavVendedores;

        $totalTotped = $query
            ->select([
                'total_totped' => $query->func()->sum('totped'),
            ])
            ->sumOf('total_totped');

        $totalVisit = $query
            ->select([
                'total_visit' => $query->func()->sum('visit'),
            ])
            ->sumOf('total_visit');

        return $this->getPorcentagem($totalTotped, $totalVisit);
    }

    private function getExecucaoIv()
    {
        $query = $this->queryIavVendedores;

        $totalVisit = $query
            ->select([
                'total_visit' => $query->func()->sum('visit'),
            ])
            ->sumOf('total_visit');

        $totalTotvis = $query
            ->select([
                'total_totvis' => $query->func()->sum('totvis'),
            ])
            ->sumOf('total_totvis');

        return $this->getPorcentagem($totalTotvis, $totalVisit);

    }

    private function setQueryIavVendedores()
    {
        $query = $this->_iavTable
            ->find()
            ->where([
                'vendedor_id IN' => $this->vendedoresIds,
                'mes_ano' => $this->dataFiltro->i18nFormat('yyyy-MM-01')
            ]);

        $this->queryIavVendedores = $query;
    }
}
