<?php

namespace App\Controller;

use App\Enum\SituacaoEnum;

class HomeController extends FrontController {

    public function initialize() {
        parent::initialize();
        $this->Auth->allow();
        $this->loadModel('Veiculos');
        $this->loadModel('Sobre');
        $this->loadModel('Contatos');
        $this->loadModel('ContatosNewsletters');
    }

    public function home()
    {
        $this->setVariablesCommon();
        $destaques = $this->Veiculos->getDestaques(4);
        $ultimos = $this->Veiculos->getUltimos(4);

        $this->set(compact('destaques', 'ultimos'));
    }

    public function veiculo($id, $slug = null)
    {
        $veiculo = $this->Veiculos->buscar($id);
        if (!$veiculo->mostrar_site || $veiculo->situacao != SituacaoEnum::A_VENDA) {
            $this->Flash->error(__('Carro não está disponivel.'));
        }

        $this->set(compact('veiculo'));
    }

    public function veiculos($limit = 9)
    {
        $this->setVariablesCommon();
        $query = $this->Veiculos
            ->getListFront($this->request->getQuery());

        $this->paginate = [
            'limit' => $limit,
        ];
        $veiculos = $this->paginate(
            $query
        );

        $this->set(compact('veiculos'));
    }

    public function sobre()
    {
    }

    public function contato()
    {

    }

    public function salvarContato()
    {
        if ($this->request->is('post')) {
            $retorno = $this->Contatos->salvar($this->request->getData());
            if ($retorno['status']) {
                $this->Flash->success(__('A sua mensagem foi enviada com sucesso, aguarde que em breve entraremos em contato.'));
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('Aconteceu um erro ao tentar enviar a sua mensagem, tente novamente.'));
            return $this->redirect($this->referer());
        }
    }

    public function saveNewsletters()
    {
        if ($this->request->is('post')) {
            $this->RequestHandler->renderAs($this, 'json');
            $response = $this->ContatosNewsletters->salvar($this->request->getData());
            $this->set(compact('response'));
            $this->set('_serialize', 'response');
        }
    }
}
