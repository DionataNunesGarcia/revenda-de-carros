<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Contatos Controller
 *
 * @property \App\Model\Table\ContatosTable $Contatos
 *
 * @method \App\Model\Entity\Contato[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContatosController extends AppController
{

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {
        $contitions = [];

        if (!empty($this->request->getQuery())) {
            if ($this->request->getQuery('nome')) {
                $contitions['or'] = [
                    'Contatos.nome like' => '%' . $this->request->getQuery('nome') . '%',
                ];
            }
            if ($this->request->getQuery('email')) {
                $contitions['or'] = [
                    'Contatos.email like' => '%' . $this->request->getQuery('email') . '%',
                ];
            }
            if ($this->request->getQuery('veiculo_id')) {
                $contitions['Contatos.veiculo_id'] = $this->request->getQuery('veiculo_id');
            }
        }
        $this->paginate = [
            'conditions' => $contitions,
            'contain' => [
                'Veiculos.Modelos',
            ],
            'order' => ['Contatos.criado' => 'DESC']
        ];
        $entidade = $this->paginate($this->Contatos);

        $this->set(compact('entidade'));
    }

    public function ver($id) {
        $entidade = $this->Contatos->buscar($id);

        $this->layout = '';
        $this->set(compact('entidade'));
        $this->render('ver');
    }
}
