<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Cidades Controller
 *
 * @property \App\Model\Table\CidadesTable $Cidades
 *
 * @method \App\Model\Entity\Cidade[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CidadesController extends AppController
{
    /**
     * @return string[]
     */
    public static function ignoreListActions() {
        return [
            'getCidade',
        ];
    }

    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function autocomplete() {

        $this->autoRender = false;

        $retorno = $this->Cidades->autocomplete(@$this->request->getQuery('term'), @$this->request->getQuery('id'));

        echo json_encode($retorno);
    }

    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function getCidade() {

        $this->autoRender = false;

        $term = somenteLetras($this->request->getQuery('cidade'));
        $retorno = $this->Cidades
            ->find()
            ->where([
                'lower(nome) like' => '%' . $term . '%',
            ])
            ->first();

        echo json_encode($retorno);
    }
}
