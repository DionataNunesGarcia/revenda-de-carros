<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Veiculos Controller
 *
 * @property \App\Model\Table\VeiculosTable $Veiculos
 *
 * @method \App\Model\Entity\Veiculo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VeiculosController extends AppController
{
    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function autocomplete() {

        $this->autoRender = false;

        $retorno = $this->Veiculos->autocomplete(@$this->request->getQuery('term'), @$this->request->getQuery('id'));

        echo json_encode($retorno);
    }

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {
        $contitions = [];

        if (!empty($this->request->getQuery('placa'))) {
            $contitions['or'] = [
                'placa like' => '%' . $this->request->getQuery('placa') . '%',
            ];
        }

        if (!empty($this->request->getQuery('ano'))) {
            $contitions['or'] = [
                'ano' => $this->request->getQuery('ano')
            ];
        }

        if (!empty($this->request->getQuery('cor_id'))) {
            $contitions['or'] = [
                'cor_id' => $this->request->getQuery('cor_id')
            ];
        }

        $this->paginate = [
            'contain' => [
                'Cores',
                'Cidades',
                'Usuarios',
                'Modelos.Marcas'
            ],
            'conditions' => $contitions,
            'order' => ['Veiculos.nome' => 'ASC']
        ];

        $entidade = $this->paginate($this->Veiculos);

        $this->set(compact('entidade'));
    }

    public function abrir($id) {
        $entidade = $this->Veiculos->buscar($id);

        $this->set(compact('entidade'));
    }

    public function incluir() {
        $entidade = $this->Veiculos->novo();

        $this->set(compact('entidade'));
        $this->render('abrir');
    }

    public function salvar() {

        $id = $this->request->getData('id');
        $dados = $this->request->getData();

        try {
            $retorno = $this->Veiculos->salvar($dados, $id);
            $entidade = $retorno['entidade'];

            if (!$retorno['status']) {
                $this->Flash->error(__($retorno['mensagem']));

                $this->set(compact('entidade'));
                $this->render('abrir');
            } else {
                $this->Flash->success(__('O conteúdo foi salvo com sucesso'));
                return $this->redirect(['action' => 'abrir', $entidade->id]);
            }
        } catch (Exception $ex) {

            $this->Flash->error(__($ex->getMessage()));

            $this->set(compact('entidade'));
            $this->render('abrir');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $ids Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function excluir($ids = null) {

        if (empty($ids)) {
            $this->Flash->error(__('Nenhum registro foi selecionado.'));
            return $this->redirect(['action' => 'pesquisar']);
        }

        $resultado = $this->Veiculos->excluir($ids);

        if ($resultado) {
            $this->Flash->success(__('Registro(s) excluídos com sucesso.'));
        } else {
            $mensagens = '';
            foreach ($resultado as $i) {
                $mensagens .= $i;
            }
            $this->Flash->error(__($mensagens));
        }

        return $this->redirect(['action' => 'pesquisar']);
    }

    public function excluirImagem($id) {

        $this->Veiculos->excluirImagem($id);
        $this->Flash->success(__('Imagem excluída com sucesso.'));

        return $this->redirect($this->referer());
    }
}
