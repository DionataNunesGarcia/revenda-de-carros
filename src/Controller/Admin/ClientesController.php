<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Clientes Controller
 *
 * @property \App\Model\Table\ClientesTable $Clientes
 *
 * @method \App\Model\Entity\Cliente[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClientesController extends AppController
{
    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function autocomplete() {

        $this->autoRender = false;

        $retorno = $this->Clientes->autocomplete(@$this->request->getQuery('term'), @$this->request->getQuery('id'));

        echo json_encode($retorno);
    }

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {
        $contitions = [];

        if (!empty($this->request->getQuery('nome'))) {
            $contitions['or'] = [
                'nome like' => '%' . $this->request->getQuery('nome') . '%',
            ];
        }

        $this->paginate = [
            'conditions' => $contitions,
            'order' => ['nome' => 'ASC']
        ];

        $entidade = $this->paginate($this->Clientes);

        $this->set(compact('entidade'));
    }

    public function abrir($id) {
        $entidade = $this->Clientes->buscar($id);

        $this->set(compact('entidade'));
    }

    public function incluir() {
        $entidade = $this->Clientes->novo();

        $this->set(compact('entidade'));
        $this->render('abrir');
    }

    public function salvar() {

        $id = $this->request->getData('id');
        $dados = $this->request->getData();

        try {
            $retorno = $this->Clientes->salvar($dados, $id);
            $entidade = $retorno['entidade'];

            if (!$retorno['status']) {
                $this->Flash->error(__($retorno['mensagem']));

                $this->set(compact('entidade'));
                $this->render('abrir');
            } else {
                $this->Flash->success(__('O conteúdo foi salvo com sucesso'));
                return $this->redirect(['action' => 'abrir', $entidade->id]);
            }
        } catch (Exception $ex) {

            $this->Flash->error(__($ex->getMessage()));

            $this->set(compact('entidade'));
            $this->render('abrir');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $ids Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function excluir($ids = null) {

        if (empty($ids)) {
            $this->Flash->error(__('Nenhum registro foi selecionado.'));
            return $this->redirect(['action' => 'pesquisar']);
        }

        $resultado = $this->Clientes->excluir($ids);

        if ($resultado) {
            $this->Flash->success(__('Registro(s) excluídos com sucesso.'));
        } else {
            $mensagens = '';
            foreach ($resultado as $i) {
                $mensagens .= $i;
            }
            $this->Flash->error(__($mensagens));
        }

        return $this->redirect(['action' => 'pesquisar']);
    }
}
