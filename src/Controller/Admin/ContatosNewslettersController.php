<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * ContatosNewsletters Controller
 *
 * @property \App\Model\Table\ContatosNewslettersTable $ContatosNewsletters
 *
 * @method \App\Model\Entity\ContatosNewsletter[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContatosNewslettersController extends AppController
{
    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {
        $contitions = [];

        if (!empty($this->request->getQuery())) {
            if ($this->request->getQuery('nome')) {
                $contitions['or'] = [
                    'ContatosNewsletters.nome like' => '%' . $this->request->getQuery('nome') . '%',
                ];
            }
        }

        $this->paginate = [
            'conditions' => $contitions,
            'order' => ['data_hora' => 'DESC']
        ];

        $entidade = $this->paginate($this->ContatosNewsletters);

        $this->set(compact('entidade'));
    }

    public function ver($id) {
        $entidade = $this->ContatosNewsletters->buscar($id);

        $this->layout = '';
        $this->set(compact('entidade'));
        $this->render('ver');
    }

    /**
     * Excluir method
     *
     * @param string|null $id Contatos Newsletter id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function excluir($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contatosNewsletter = $this->ContatosNewsletters->get($id);
        if ($this->ContatosNewsletters->delete($contatosNewsletter)) {
            $this->Flash->success(__('The contatos newsletter has been deleted.'));
        } else {
            $this->Flash->error(__('The contatos newsletter could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
