<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Sobre Controller
 *
 * @property \App\Model\Table\SobreTable $Sobre
 *
 * @method \App\Model\Entity\Sobre[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SobreController extends AppController
{
    public function abrir() {
        $entidade = $this->Sobre->buscar();

        $this->set(compact('entidade'));
    }

    public function salvar() {

        $id = $this->request->getData('id');
        $dados = $this->request->getData();

        try {
            $retorno = $this->Sobre->salvar($dados, $id);
            $entidade = $retorno['entidade'];

            if (!$retorno['status']) {
                $this->Flash->error(__($retorno['mensagem']));

                $this->set(compact('entidade'));
                $this->render('abrir');
            } else {
                $this->Flash->success(__('O conteúdo foi salvo com sucesso'));
                return $this->redirect(['action' => 'abrir', $entidade->id]);
            }
        } catch (Exception $ex) {

            $this->Flash->error(__($ex->getMessage()));

            $this->set(compact('entidade'));
            $this->render('abrir');
        }
    }

    public function banners() {
        $entidade = $this->Sobre->buscar();

        if (empty($entidade)) {
            $this->Flash->error(__('Precisa registrar um conteúdo Sobre para salvar os banners'));
            $this->redirect('abrir');
        }
        $this->set(compact('entidade'));
    }
}
