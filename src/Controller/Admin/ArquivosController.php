<?php

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;

/**
 * Arquivos Controller
 *
 * @property \App\Model\Table\ArquivosTable $Arquivos
 *
 * @method \App\Model\Entity\Arquivo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArquivosController extends AppController
{
    private $_table = 'Arquivos';

    public static function ignoreListActions() {
        return [
            'download',
            'multipleFileUploads',
            'multipleFileUploadsList',
            'multipleFileUploadsDelete',
            'multipleFileUploadsUpdateFields',
        ];
    }

    public function download($id)
    {
        $entidade = $this->{$this->_table}->buscar($id);

        $file_path = WWW_ROOT . $entidade->arquivo;
        $this->response->file($file_path, array(
            'download' => true,
            'name' => Time::now()->i18nFormat('yyyy-MM-dd HH-mm-ss')
                . " {$entidade->tipo}."
                . extensao($entidade->arquivo),
        ));

        return $this->response;
    }

    public function multipleFileUploads()
    {
        ini_set('post_max_size', '1024M');
        ini_set('upload_max_filesize', '1024M');
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $this->RequestHandler->renderAs($this, 'json');
        $response = [];
        try {
            $this->request->allowMethod(['post']);
            $data = $this->request->getData();
            $response = $this->Arquivos->salvarArquivos($data);
            $conn->commit();
        } catch (\Exception $ex) {
            $conn->rollback();
            $this->response = $this->response->withStatus($ex->getCode());
            $response['message'] = $ex->getMessage();
        }
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function multipleFileUploadsUpdateFields()
    {
        ini_set('post_max_size', '1024M');
        ini_set('upload_max_filesize', '1024M');
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $this->RequestHandler->renderAs($this, 'json');
        $response = [];
        try {
            $this->request->allowMethod(['post']);
            $data = $this->request->getData();
            $response = $this->Arquivos->updateFields($data);
            $conn->commit();
        } catch (\Exception $ex) {
            $conn->rollback();
            $this->response = $this->response->withStatus($ex->getCode());
            $response['message'] = $ex->getMessage();
        }
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function multipleFileUploadsList()
    {
        $this->RequestHandler->renderAs($this, 'json');
        $response = [];
        try {
            $this->request->allowMethod(['get']);
            $data = $this->request->getQuery();
            $response['data'] = $this->Arquivos->listarArquivos($data);
        } catch (\Exception $ex) {
            $this->response = $this->response->withStatus($ex->getCode());
            $response['message'] = $ex->getMessage();
        }
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }

    public function multipleFileUploadsDelete()
    {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $this->RequestHandler->renderAs($this, 'json');
        $response = [];
        try {
            $this->request->allowMethod(['post']);
            $data = $this->request->getData();
            $response['data'] = $this->Arquivos->deleteFile($data);
            $conn->commit();
        } catch (\Exception $ex) {
            $conn->rollback();
            $this->response = $this->response->withStatus($ex->getCode());
            $response['message'] = $ex->getMessage();
        }
        $this->set(compact('response'));
        $this->set('_serialize', 'response');
    }
}
