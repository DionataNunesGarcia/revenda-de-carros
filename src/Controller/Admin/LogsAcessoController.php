<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * LogsAcesso Controller
 *
 * @property \App\Model\Table\LogsAcessoTable $LogsAcesso
 *
 * @method \App\Model\Entity\LogsAcesso[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LogsAcessoController extends AppController {

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {
        $contitions = [];

        if (!empty($this->request->query())) {
            if ($this->request->query('usuario')) {
                $contitions['or'] = [
                    'usuario like' => '%' . $this->request->query('usuario') . '%',
                ];
            }
            if ($this->request->query('ip')) {
                $contitions['or'] = [
                    'ip like' => '%' . $this->request->query('ip') . '%',
                ];
            }
            if ($this->request->query('data_hora')) {
                $data_hora = explode(' - ', $this->request->query('data_hora'));
                
                $contitions['or'] = [
                    'data_hora >=' => dataToDate(trim($data_hora[0])),
                ];
                $contitions['and'] = [
                    'data_hora <=' => dataToDate(trim($data_hora[1])),
                ];
            }
        }

        $this->paginate = [
            'conditions' => $contitions,
            'order' => ['data_hora' => 'DESC']
        ];

        $entidade = $this->paginate($this->LogsAcesso);

        $this->set(compact('entidade'));
    }

}
