<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Vendedores Controller
 *
 * @property \App\Model\Table\VendedoresTable $Vendedores
 *
 * @method \App\Model\Entity\Vendedore[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VendedoresController extends AppController
{
    /**
     * @return array
     */
    public static function ignoreListActions()
    {
        return [];
    }

    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Clientes');
    }
    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function autocomplete()
    {

        $this->autoRender = false;

        $retorno = $this->Vendedores->autocomplete(@$this->request->query['term'], @$this->request->query['id']);

        echo json_encode($retorno);
    }

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {
        $contitions = [];

        if (!empty($this->request->getQuery('nome'))) {
            $contitions['or'] = [
                'nome like' => '%' . $this->request->getQuery('nome') . '%',
            ];
        }

        $this->paginate = [
            'conditions' => $contitions,
            'contain' => [
                'Endereco',
                'Usuario',
            ],
            'order' => ['nome' => 'ASC']
        ];

        $entidade = $this->paginate($this->Vendedores);

        $this->set(compact('entidade'));
    }

    public function abrir($id) {
        $entidade = $this->Vendedores->buscar($id);

        $this->set(compact('entidade'));
    }

    public function incluir()
    {
        $entidade = $this->Vendedores->novo();

        $this->set(compact('entidade'));
        $this->render('abrir');
    }

    public function salvar()
    {

        $id = $this->request->getData('id');
        $dados = $this->request->getData();

        try {
            $retorno = $this->Vendedores->salvar($dados, $id);
            $entidade = $retorno['entidade'];

            if (!$retorno['status']) {
                $this->Flash->error(__($retorno['mensagem']));

                $this->set(compact('entidade'));
                $this->render('abrir');
            } else {
                $this->Flash->success(__('O conteúdo foi salvo com sucesso'));
                return $this->redirect(['action' => 'abrir', $entidade->id]);
            }
        } catch (Exception $ex) {

            $this->Flash->error(__($ex->getMessage()));

            $this->set(compact('entidade'));
            $this->render('abrir');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $ids Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function excluir($ids = null)
    {

        if (empty($ids)) {
            $this->Flash->error(__('Nenhum registro foi selecionado.'));
            return $this->redirect(['action' => 'pesquisar']);
        }

        $resultado = $this->Vendedores->excluir($ids);

        if ($resultado) {
            $this->Flash->success(__('Registro(s) excluídos com sucesso.'));
        } else {
            $mensagens = '';
            foreach ($resultado as $i) {
                $mensagens .= $i;
            }
            $this->Flash->error(__($mensagens));
        }

        return $this->redirect(['action' => 'pesquisar']);
    }
}
