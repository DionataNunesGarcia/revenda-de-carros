<?php

namespace App\Controller\Admin;

use App\Controller\AppController;

class UsuariosController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->Auth->allow(['logout']);
    }

    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function autocomplete() {

        $this->autoRender = false;

        $retorno = $this->Usuarios->autocomplete(@$this->request->query['term'], @$this->request->query['id']);

        echo json_encode($retorno);
    }


    public static function ignoreListActions() {
        return [
            'login',
            'logout',
            'perfil',
            'renovarSenha',
            'primeiroAcesso',
            'excluirImagemPerfil',
        ];
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {

        $contitions = [];

        if (!empty($this->request->getQuery())) {
            if ($this->request->getQuery('nome')) {
                $contitions['or'] = [
                    'Niveis.nome like' => '%' . $this->request->getQuery('nome') . '%',
                ];
            }
        }

        $this->paginate = [
            'conditions' => $contitions,
            'contain' => ['Niveis'],
            'order' => ['data_hora' => 'DESC']
        ];

        $entidade = $this->paginate($this->Usuarios);

        $this->set(compact('entidade'));
    }

    public function abrir($id) {
        $entidade = $this->Usuarios->buscar($id);
        $this->set(compact('entidade'));
    }

    public function incluir() {
        $entidade = $this->Usuarios->novo();
        $this->set(compact('entidade'));
        $this->render('abrir');
    }

    public function salvar() {

        $id = $this->request->data['id'];
        $dados = $this->request->data;

        try {
            $entidade = $this->Usuarios->salvar($dados, $id);

            if (!$entidade['status']) {
                $this->Flash->error(__($entidade['mensagem']));
                $this->render('abrir');
            } else {
                $this->Flash->success(__('O conteudo foi salvo com sucesso'));
                return $this->redirect(['action' => 'abrir', $entidade['entidade']->id]);
            }
        } catch (Exception $ex) {

            $entidade = $this->request->getData();
            $this->Flash->error(__('O conteudo não pôde ser salvo. Por favor, tente novamente.'));

            $this->set(compact('entidade'));
            $this->render('abrir');
        }
    }

    public function perfil() {

        if ($this->request->is(['patch', 'post', 'put'])) {
            $dados = $this->request->getData();

            $id = $this->Auth->user('id');

            $dados['id'] = $id;
            $retorno = $this->Usuarios->salvar($dados, $id);

            $userSession = $this->Usuarios->montarUsuarioLogado($id);
            $this->Auth->setUser($userSession);

            $this->set(compact('userSession'));

            if (!$retorno['status']) {
                $this->Flash->error(__($retorno['mensagem']));
            } else {
                $this->Flash->success(__('O conteúdo foi salvo com sucesso'));
            }
        }
    }

    public function excluirImagemPerfil() {
        $id = $this->Auth->user('id');

        $this->Usuarios->excluirImagem($id);

        $usuario = $this->Usuarios->montarUsuarioLogado($id);
        $this->Auth->setUser($usuario);

        return $this->redirect(['action' => 'perfil', $id]);
    }

    /**
     * Excluir method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function excluir($ids = null) {

        if (empty($ids)) {
            $this->Flash->error(__('Nenhum registro foi selecionado.'));
        } else if ($this->Usuarios->excluir($ids)) {
            $this->Flash->success(__('The usuario has been deleted.'));
        } else {
            $this->Flash->error(__('The usuario could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'pesquisar']);
    }

    public function excluirImagem($id) {

        $this->Usuarios->excluirImagem($id);
        $this->Flash->success(__('Imagem excluída com sucesso.'));

        return $this->redirect($this->referer());
    }

    public function login() {
        if (!empty($this->Auth->user())) {
            return $this->redirect(['controller' => 'home', 'action' => 'index']);
        }
        $this->viewBuilder()->layout('login');
        $this->loadModel('LogsAcesso');
        if ($this->request->is('post')) {

            $usuario = $this->Auth->identify();

            if (!$usuario) {

                $this->Flash->error('Seu nome ou a senha está incorreta');
            } else if (!$usuario['ativo']) {
                $this->Flash->error('Seu usuário foi desativado, consulte o administrador.');
            } else {

                $usuario = $this->Usuarios->montarUsuarioLogado($usuario['id']);

                $ip = $this->request->clientIp();

                $this->LogsAcesso->gerarLog($ip, $usuario['usuario']);

                $this->Auth->setUser($usuario);
                $this->Flash->success('Você entrou no sistema com sucesso.');
                return $this->redirect($this->Auth->redirectUrl());
            }
        }
    }

    public function logout() {
        $this->Flash->success('Você saiu do sistema.');
        return $this->redirect($this->Auth->logout());
    }

    public function deleteArquivo($id) {

        try {
            $entidade = $this->Usuarios->deletar_arquivo($id);
            $this->Flash->success(__('O arquivo foi deletado com sucesso'));

            return $this->redirect(['action' => 'abrir', $id]);
        } catch (Exception $ex) {
            $this->Flash->error(__('O arquivo não pode ser deletado.'));
            return $this->redirect(['action' => 'abrir']);
        }
        $this->set(compact('entidade'));
    }

}
