<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * ContasPagar Controller
 *
 * @property \App\Model\Table\ContasPagarTable $ContasPagar
 *
 * @method \App\Model\Entity\ContasPagar[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContasPagarController extends AppController
{
    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function autocomplete() {

        $this->autoRender = false;

        $retorno = $this->ContasPagar->autocomplete(@$this->request->getQuery('term'), @$this->request->getQuery('id'));

        echo json_encode($retorno);
    }

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {
        $contitions = [];
        $search = $this->request->getQuery();
        if (isset($search['nome']) && !empty($search['nome'])) {
            $contitions['ContasPagar.nome like'] = '%' . $search['nome'] . '%';
        }
        if (isset($search['datas_vencimento']) && !empty($search['datas_vencimento'])) {
            $intervalo = convertDatasIntervalo($search['datas_vencimento']);
            $contitions['ContasPagar.data_vencimento >='] = '%' . $intervalo['inicio'] . '%';
            $contitions['ContasPagar.data_vencimento <='] = '%' . $intervalo['fim'] . '%';
        }
        if (isset($search['datas_validade']) && !empty($search['datas_validade'])) {
            $intervalo = convertDatasIntervalo($search['datas_validade']);
            $contitions['ContasPagar.data_validade >='] = '%' . $intervalo['inicio'] . '%';
            $contitions['ContasPagar.data_validade <='] = '%' . $intervalo['fim'] . '%';
        }
        if (isset($search['datas_pagamento']) && !empty($search['datas_pagamento'])) {
            $intervalo = convertDatasIntervalo($search['datas_validade']);
            $contitions['ContasPagar.data_pagamento >='] = '%' . $intervalo['inicio'] . '%';
            $contitions['ContasPagar.data_pagamento <='] = '%' . $intervalo['fim'] . '%';
        }

        $this->paginate = [
            'conditions' => $contitions,
            'order' => ['nome' => 'ASC']
        ];

        $entidade = $this->paginate($this->ContasPagar);

        $this->set(compact('entidade'));
    }

    public function abrir($id) {
        $entidade = $this->ContasPagar->buscar($id);

        $this->set(compact('entidade'));
    }

    public function incluir() {
        $entidade = $this->ContasPagar->novo();
        $entidade->model = 'Fornecedores';

        $this->set(compact('entidade'));
        $this->render('abrir');
    }

    public function salvar() {

        $id = $this->request->getData('id');
        $dados = $this->request->getData();

        try {
            $retorno = $this->ContasPagar->salvar($dados, $id);
            $entidade = $retorno['entidade'];

            if (!$retorno['status']) {
                $this->Flash->error(__($retorno['mensagem']));

                $this->set(compact('entidade'));
                $this->render('abrir');
            } else {
                $this->Flash->success(__('O conteúdo foi salvo com sucesso'));
                return $this->redirect(['action' => 'abrir', $entidade->id]);
            }
        } catch (Exception $ex) {

            $this->Flash->error(__($ex->getMessage()));

            $this->set(compact('entidade'));
            $this->render('abrir');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $ids Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function excluir($ids = null) {

        if (empty($ids)) {
            $this->Flash->error(__('Nenhum registro foi selecionado.'));
            return $this->redirect(['action' => 'pesquisar']);
        }

        $resultado = $this->ContasPagar->excluir($ids);

        if ($resultado) {
            $this->Flash->success(__('Registro(s) excluídos com sucesso.'));
        } else {
            $mensagens = '';
            foreach ($resultado as $i) {
                $mensagens .= $i;
            }
            $this->Flash->error(__($mensagens));
        }

        return $this->redirect(['action' => 'pesquisar']);
    }

    public function download($field, $encode)
    {
        $file = base64_decode($encode);
        $ext = $file;
        $dateTime = Time::now()->i18nFormat('yyyy-MM-dd HH-mm-ss');
        $name = "{$field}-{$dateTime}{$ext}";
        $filePath = WWW_ROOT . $file;

        return $this->response->withFile(
            $filePath,
            [
                'download' => true,
                'name' => $name
            ]
        );
    }

    public function excluirArquivo($id, $field) {
        try {
            $entidade = $this->ContasPagar->buscar($id);
            $file = $entidade->{$field};
            $entidade->{$field} = null;
            $entidade->modificado = Time::now();
            $this->ContasPagar->save($entidade);

            @unlink(WWW_ROOT . $file);
            $this->Flash->success(__('O arquivo foi deletado com sucesso'));

            return $this->redirect(['action' => 'abrir', $id]);
        } catch (Exception $ex) {
            $this->Flash->error(__('O arquivo não pode ser deletado.'));
            return $this->redirect(['action' => 'abrir']);
        }
        $this->set(compact('entidade'));
    }
}
