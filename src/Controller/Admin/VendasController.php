<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * Vendas Controller
 *
 * @property \App\Model\Table\VendasTable $Vendas
 *
 * @method \App\Model\Entity\Venda[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class VendasController extends AppController
{
    /**
     * autocomplete method
     *
     * @return \Cake\Http\Response|void
     */
    public function autocomplete() {

        $this->autoRender = false;

        $retorno = $this->Vendas->autocomplete(@$this->request->getQuery('term'), @$this->request->getQuery('id'));

        echo json_encode($retorno);
    }

    /**
     * Pesquisar method
     *
     * @return \Cake\Http\Response|void
     */
    public function pesquisar() {
        $contitions = [];
        $search = $this->request->getQuery();
        if (isset($search['nome']) && !empty($search['nome'])) {
            $contitions['Vendas.nome like'] = '%' . $search['nome'] . '%';
        }
        if (isset($search['criado']) && !empty($search['criado'])) {
            $intervalo = convertDatasIntervalo($search['criado']);
            $contitions['Vendas.criado >='] = '%' . $intervalo['inicio'] . '%';
            $contitions['Vendas.criado <='] = '%' . $intervalo['fim'] . '%';
        }

        $this->paginate = [
            'contain' => [
                'Veiculo' => [
                    'Modelos'
                ],
                'Vendedor',
                'Cliente',
                'FormaPagamento',
            ],
            'conditions' => $contitions,
            'order' => ['nome' => 'ASC']
        ];

        $entidade = $this->paginate($this->Vendas);

        $this->set(compact('entidade'));
    }

    public function abrir($id) {
        $entidade = $this->Vendas->buscar($id);

        $this->set(compact('entidade'));
    }

    public function incluir() {
        $entidade = $this->Vendas->novo();
        $entidade->model = 'Clientes';

        $this->set(compact('entidade'));
        $this->render('abrir');
    }

    public function salvar() {

        $id = $this->request->getData('id');
        $dados = $this->request->getData();

        try {
            $retorno = $this->Vendas->salvar($dados, $id);
            $entidade = $retorno['entidade'];

            if (!$retorno['status']) {
                $this->Flash->error(__($retorno['mensagem']));

                $this->set(compact('entidade'));
                $this->render('abrir');
            } else {
                $this->Flash->success(__('O conteúdo foi salvo com sucesso'));
                return $this->redirect(['action' => 'abrir', $entidade->id]);
            }
        } catch (Exception $ex) {

            $this->Flash->error(__($ex->getMessage()));

            $this->set(compact('entidade'));
            $this->render('abrir');
        }
    }

    /**
     * Delete method
     *
     * @param string|null $ids Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function excluir($ids = null) {

        if (empty($ids)) {
            $this->Flash->error(__('Nenhum registro foi selecionado.'));
            return $this->redirect(['action' => 'pesquisar']);
        }

        $resultado = $this->Vendas->excluir($ids);

        if ($resultado) {
            $this->Flash->success(__('Registro(s) excluídos com sucesso.'));
        } else {
            $mensagens = '';
            foreach ($resultado as $i) {
                $mensagens .= $i;
            }
            $this->Flash->error(__($mensagens));
        }

        return $this->redirect(['action' => 'pesquisar']);
    }

    public function download($field, $encode)
    {
        $file = base64_decode($encode);
        $ext = $file;
        $dateTime = Time::now()->i18nFormat('yyyy-MM-dd HH-mm-ss');
        $name = "{$field}-{$dateTime}{$ext}";
        $filePath = WWW_ROOT . $file;

        return $this->response->withFile(
            $filePath,
            [
                'download' => true,
                'name' => $name
            ]
        );
    }

    public function excluirArquivo($id, $field) {
        try {
            $entidade = $this->Vendas->buscar($id);
            $file = $entidade->{$field};
            $entidade->{$field} = null;
            $entidade->modificado = Time::now();
            $this->Vendas->save($entidade);

            @unlink(WWW_ROOT . $file);
            $this->Flash->success(__('O arquivo foi deletado com sucesso'));

            return $this->redirect(['action' => 'abrir', $id]);
        } catch (Exception $ex) {
            $this->Flash->error(__('O arquivo não pode ser deletado.'));
            return $this->redirect(['action' => 'abrir']);
        }
        $this->set(compact('entidade'));
    }
}
