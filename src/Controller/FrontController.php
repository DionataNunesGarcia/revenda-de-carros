<?php

namespace App\Controller;

use Cake\Event\Event;

class FrontController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->Auth->allow();
        $this->loadModel('TipoVeiculos');
        $this->loadModel('ParametrosSistema');
        $this->loadModel('Modelos');
        $this->loadModel('Marcas');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->viewBuilder()
            ->setLayout('front');
    }

    protected function setVariablesCommon()
    {
        $categorias = $this->TipoVeiculos->getList();
        $modelos = $this->Modelos->getList();
        $marcas = $this->Marcas->getList();

        $this->set(compact(
            'categorias',
            'modelos',
            'marcas',
        ));
    }

}
