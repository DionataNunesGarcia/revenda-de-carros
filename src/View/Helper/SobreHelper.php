<?php

namespace App\View\Helper;
use Cake\View\Helper;
use Cake\View\View;
use Cake\ORM\TableRegistry;

class sobreHelper extends Helper {

    public function buscar() {
        return TableRegistry::getTableLocator()
            ->get('Sobre')
            ->buscar();
    }

    public function banners() {
        return TableRegistry::getTableLocator()
            ->get('Arquivos')
            ->find()
            ->where([
                'model' => 'Banners'
            ])
            ->toArray();
    }
}
