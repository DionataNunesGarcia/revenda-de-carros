<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * NiveisPermisso Entity
 *
 * @property int $id
 * @property int $nivel_id
 * @property string $prefix
 * @property string $controller
 * @property string $action
 *
 * @property \App\Model\Entity\Nivei $nivei
 */
class NiveisPermisso extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nivel_id' => true,
        'prefix' => true,
        'controller' => true,
        'action' => true,
        'nivei' => true
    ];
}
