<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * UsuariosMensagen Entity
 *
 * @property int $id
 * @property string $titulo
 * @property string $assunto
 * @property string $mensagem
 * @property string $tipo
 * @property int $usuario_id
 * @property int $enviado_por_id
 * @property bool $visualizado
 * @property \Cake\I18n\FrozenTime $data_visualizacao
 * @property \Cake\I18n\FrozenTime $criado
 * @property \Cake\I18n\FrozenTime $modificado
 *
 * @property \App\Model\Entity\Usuario $usuario
 */
class UsuariosMensagen extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'titulo' => true,
        'assunto' => true,
        'mensagem' => true,
        'tipo' => true,
        'usuario_id' => true,
        'enviado_por_id' => true,
        'visualizado' => true,
        'data_visualizacao' => true,
        'criado' => true,
        'modificado' => true,
        'usuario' => true
    ];
}
