<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OpcionaisVeiculo Entity
 *
 * @property int $id
 * @property int|null $opcional_id
 * @property int|null $veiculo_id
 *
 * @property \App\Model\Entity\Opcional $opcional
 * @property \App\Model\Entity\Veiculo $veiculo
 */
class OpcionaisVeiculo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'opcional_id' => true,
        'veiculo_id' => true,
        'opcional' => true,
        'veiculo' => true
    ];
}
