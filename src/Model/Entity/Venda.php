<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Venda Entity
 *
 * @property int $id
 * @property float $valor
 * @property float $valor_desconto
 * @property float $valor_acrescimo
 * @property int $status
 * @property int $dia_vencimento
 * @property \Cake\I18n\FrozenTime $primeiro_pagamento
 * @property \Cake\I18n\FrozenTime $data_retirada
 * @property int $meses_garantia
 * @property string|null $anotacoes
 * @property string|null $contrato_anexo
 * @property int $veiculo_id
 * @property int $cliente_id
 * @property int $vendedor_id
 * @property int $forma_pagamento_id
 * @property bool $rascunho
 * @property \Cake\I18n\FrozenTime $criado
 * @property \Cake\I18n\FrozenTime $modificado
 *
 * @property \App\Model\Entity\Veiculo $veiculo
 * @property \App\Model\Entity\Cliente $cliente
 * @property \App\Model\Entity\Vendedor $vendedor
 * @property \App\Model\Entity\FormaPagamento $forma_pagamento
 */
class Venda extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'valor' => true,
        'valor_desconto' => true,
        'valor_acrescimo' => true,
        'status' => true,
        'dia_vencimento' => true,
        'primeiro_pagamento' => true,
        'data_retirada' => true,
        'meses_garantia' => true,
        'anotacoes' => true,
        'contrato_anexo' => true,
        'contrato_anexo_upload' => true,
        'veiculo_id' => true,
        'cliente_id' => true,
        'vendedor_id' => true,
        'forma_pagamento_id' => true,
        'rascunho' => true,
        'criado' => true,
        'modificado' => true,
        'veiculo' => true,
        'cliente' => true,
        'vendedor' => true,
        'forma_pagamento' => true
    ];
}
