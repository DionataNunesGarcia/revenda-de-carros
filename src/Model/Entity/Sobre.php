<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Sobre Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $email
 * @property string $telefone
 * @property string $celular
 * @property string $celular_2
 * @property string $facebook
 * @property string $instagram
 * @property string $linkedin
 * @property string $sobre
 * @property string $visao
 * @property string $missao
 * @property string $valores
 */
class Sobre extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'email' => true,
        'telefone' => true,
        'celular' => true,
        'celular_2' => true,
        'facebook' => true,
        'instagram' => true,
        'linkedin' => true,
        'sobre' => true,
        'visao' => true,
        'missao' => true,
        'valores' => true
    ];
}
