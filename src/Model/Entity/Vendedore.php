<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vendedore Entity
 *
 * @property int $id
 * @property string $nome
 * @property string|null $rg
 * @property string|null $cpf
 * @property \Cake\I18n\FrozenTime|null $data_nascimento
 * @property string $sexo
 * @property string|null $celular
 * @property string $telefone
 * @property float|null $salario_fixo
 * @property float|null $comissao
 * @property int $usuario_id
 * @property int $status
 * @property \Cake\I18n\FrozenTime|null $criado
 * @property \Cake\I18n\FrozenTime|null $modificado
 *
 * @property \App\Model\Entity\Usuario $usuario
 */
class Vendedore extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'rg' => true,
        'cpf' => true,
        'email' => true,
        'data_nascimento' => true,
        'data_admissao' => true,
        'data_demissao' => true,
        'sexo' => true,
        'celular' => true,
        'telefone' => true,
        'salario_fixo' => true,
        'comissao' => true,
        'usuario_id' => true,
        'status' => true,
        'criado' => true,
        'modificado' => true,
        'usuario' => true
    ];
}
