<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Core Entity
 *
 * @property int $id
 * @property string $codigo
 * @property string $nome
 * @property string $tipo_pintura
 * @property string $cor
 * @property string $cor_denatran
 */
class Core extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'codigo' => true,
        'nome' => true,
        'tipo_pintura' => true,
        'cor' => true,
        'cor_denatran' => true
    ];
}
