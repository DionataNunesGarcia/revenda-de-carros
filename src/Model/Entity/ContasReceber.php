<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContasReceber Entity
 *
 * @property int $id
 * @property string $nome
 * @property int $status
 * @property float $valor
 * @property string|null $model
 * @property int|null $foreign_key
 * @property int|null $numero_parcela
 * @property int|null $venda_id
 * @property int|null $conta_receber_parent_id
 * @property \Cake\I18n\FrozenDate $data_vencimento
 * @property \Cake\I18n\FrozenDate $data_validade
 * @property \Cake\I18n\FrozenDate|null $data_pagamento
 * @property string|null $anotacoes
 * @property string|null $boleto
 * @property string|null $nota_fiscal
 * @property string|null $comprovante_pagamento
 * @property \Cake\I18n\FrozenTime $criado
 * @property \Cake\I18n\FrozenTime $modificado
 */
class ContasReceber extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'status' => true,
        'valor' => true,
        'model' => true,
        'foreign_key' => true,
        'data_vencimento' => true,
        'data_validade' => true,
        'data_pagamento' => true,
        'anotacoes' => true,
        'numero_parcela' => true,
        'venda_id' => true,
        'conta_receber_parent_id' => true,
        'boleto' => true,
        'boleto_upload' => true,
        'nota_fiscal' => true,
        'nota_fiscal_upload' => true,
        'comprovante_pagamento' => true,
        'comprovante_pagamento_upload' => true,
        'criado' => true,
        'modificado' => true
    ];
}
