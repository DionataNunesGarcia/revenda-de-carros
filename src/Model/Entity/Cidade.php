<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cidade Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $uf
 * @property string $estado_id
 * @property \Cake\I18n\FrozenTime|null $criado
 * @property \Cake\I18n\FrozenTime|null $modificado
 *
 * @property \App\Model\Entity\Estado $estado
 * @property \App\Model\Entity\Endereco[] $enderecos
 * @property \App\Model\Entity\Veiculo[] $veiculos
 */
class Cidade extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'uf' => true,
        'estado_id' => true,
        'criado' => true,
        'modificado' => true,
        'estado' => true,
        'enderecos' => true,
        'veiculos' => true
    ];
}
