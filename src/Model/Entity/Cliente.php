<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Cliente Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $tipo
 * @property string $cpf_cnpj
 * @property string $email
 * @property float|null $renda
 * @property string $celular
 * @property string $telefone
 * @property \Cake\I18n\FrozenTime|null $criado
 * @property \Cake\I18n\FrozenTime|null $modificado
 */
class Cliente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'tipo' => true,
        'cpf_cnpj' => true,
        'email' => true,
        'renda' => true,
        'celular' => true,
        'telefone' => true,
        'criado' => true,
        'modificado' => true,
        'endereco' => true,
    ];
}
