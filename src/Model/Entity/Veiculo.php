<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Veiculo Entity
 *
 * @property int $id
 * @property int|null $ano
 * @property float|null $valor
 * @property float|null $valor_compra
 * @property string $renavam
 * @property string $placa
 * @property float|null $quilometragem
 * @property string $chassi
 * @property string $imagem
 * @property \Cake\I18n\FrozenTime|null $data_entrada
 * @property \Cake\I18n\FrozenTime|null $data_venda
 * @property float|null $valor_venda
 * @property string $situacao
 * @property string|null $observacoes
 * @property string|null $anotacoes
 * @property bool $importado
 * @property int|null $modelo_id
 * @property int|null $marca_id
 * @property int|null $tipo_veiculo_id
 * @property int|null $proprietario_id
 * @property int|null $cidade_id
 * @property int $cor_id
 * @property int $usuario_id
 * @property int|null $meses_garantia
 * @property int|null $numero_motor
 * @property string|null $serie
 * @property bool $rastreador_bloqueador
 * @property bool $acessorios_originais
 * @property string|null $descricao
 * @property string|null $descricao_log
 * @property float|null $valor_reforma
 * @property float|null $valor_fipe
 * @property int|null $numero_portas
 * @property string|null $combustivel
 * @property bool $mostrar_site
 * @property bool $disponivel_locacao
 * @property float|null $valor_diaria_aluguel
 * @property \Cake\I18n\FrozenTime|null $criado
 * @property \Cake\I18n\FrozenTime|null $modificado
 *
 * @property \App\Model\Entity\Modelo $modelo
 * @property \App\Model\Entity\Marca $marca
 * @property \App\Model\Entity\TipoVeiculo $tipo_veiculo
 * @property \App\Model\Entity\Proprietario $proprietario
 * @property \App\Model\Entity\Cidade $cidade
 * @property \App\Model\Entity\Opcionai[] $opcionais
 */
class Veiculo extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'ano' => true,
        'valor' => true,
        'valor_compra' => true,
        'renavam' => true,
        'placa' => true,
        'quilometragem' => true,
        'chassi' => true,
        'imagem' => true,
        'imagem_upload' => true,
        'data_entrada' => true,
        'data_venda' => true,
        'valor_venda' => true,
        'situacao' => true,
        'observacoes' => true,
        'anotacoes' => true,
        'importado' => true,
        'modelo_id' => true,
        'marca_id' => true,
        'tipo_veiculo_id' => true,
        'proprietario_id' => true,
        'cidade_id' => true,
        'cor_id' => true,
        'usuario_id' => true,
        'meses_garantia' => true,
        'numero_motor' => true,
        'serie' => true,
        'destaque' => true,
        'valor_promocao' => true,
        'rastreador_bloqueador' => true,
        'acessorios_originais' => true,
        'descricao' => true,
        'descricao_log' => true,
        'valor_reforma' => true,
        'valor_fipe' => true,
        'numero_portas' => true,
        'combustivel' => true,
        'mostrar_site' => true,
        'disponivel_locacao' => true,
        'valor_diaria_aluguel' => true,
        'criado' => true,
        'modificado' => true,
        'modelo' => true,
        'marca' => true,
        'tipo_veiculo' => true,
        'proprietario' => true,
        'cidade' => true,
        'opcionais' => true
    ];
}
