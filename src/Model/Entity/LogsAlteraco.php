<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LogsAlteraco Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime $data_hora
 * @property string $tabela
 * @property string $usuario
 * @property string $acao
 * @property string $valor_novo
 * @property string $valor_antigo
 */
class LogsAlteraco extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'data_hora' => true,
        'tabela' => true,
        'usuario' => true,
        'acao' => true,
        'valor_novo' => true,
        'valor_antigo' => true,
        'id_tabela' => true,
    ];
}
