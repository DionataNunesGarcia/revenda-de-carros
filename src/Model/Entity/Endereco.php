<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Endereco Entity
 *
 * @property int $id
 * @property string|null $model
 * @property int|null $foreign_key
 * @property string $cep
 * @property string $rua
 * @property string $numero
 * @property string $bairro
 * @property string $complemento
 * @property int|null $cidade_id
 * @property string $lat
 * @property string $long
 * @property \Cake\I18n\FrozenTime|null $criado
 * @property \Cake\I18n\FrozenTime|null $modificado
 *
 * @property \App\Model\Entity\Cidade $cidade
 */
class Endereco extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'model' => true,
        'foreign_key' => true,
        'cep' => true,
        'rua' => true,
        'numero' => true,
        'bairro' => true,
        'complemento' => true,
        'cidade_id' => true,
        'lat' => true,
        'long' => true,
        'criado' => true,
        'modificado' => true,
        'cidade' => true
    ];
}
