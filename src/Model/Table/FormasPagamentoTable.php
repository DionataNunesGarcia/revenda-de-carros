<?php
namespace App\Model\Table;

use App\Enum\StatusEnum;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * FormasPagamento Model
 *
 * @method \App\Model\Entity\FormasPagamento get($primaryKey, $options = [])
 * @method \App\Model\Entity\FormasPagamento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FormasPagamento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FormasPagamento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FormasPagamento|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FormasPagamento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FormasPagamento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FormasPagamento findOrCreate($search, callable $callback = null, $options = [])
 */
class FormasPagamentoTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'criado' => 'new',
                    'modificado' => 'always',
                ],
            ]
        ]);

        $this->setTable('formas_pagamento');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->allowEmpty('slug');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->integer('parcelas')
            ->requirePresence('parcelas', 'create')
            ->notEmpty('parcelas');

        $validator
            ->dateTime('criado')
            ->requirePresence('criado', 'create')
            ->notEmpty('criado');

        $validator
            ->dateTime('modificado')
            ->requirePresence('modificado', 'create')
            ->notEmpty('modificado');

        return $validator;
    }

    /**
     * @param $dados
     * @param null $id
     * @return array
     */
    public function salvar($dados, $id = NULL) {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $entidade = $this->montar($dados, $id);

        try {
            if ($entidade->isNew()) {
                $entidade->criado = Time::now();
                $entidade->status = StatusEnum::ACTIVE;
            }
            $entidade->modificado = Time::now();
            $retorno = $this->save($entidade);
            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }
        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }
        $conn->commit();
        return $this->retorno;
    }
}
