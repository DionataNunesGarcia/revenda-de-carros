<?php
namespace App\Model\Table;

use App\Enum\StatusEnum;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Vendas Model
 *
 * @property \App\Model\Table\VeiculosTable|\Cake\ORM\Association\BelongsTo $Veiculos
 * @property \App\Model\Table\ClientesTable|\Cake\ORM\Association\BelongsTo $Clientes
 * @property \App\Model\Table\FormaPagamentosTable|\Cake\ORM\Association\BelongsTo $FormaPagamentos
 *
 * @method \App\Model\Entity\Venda get($primaryKey, $options = [])
 * @method \App\Model\Entity\Venda newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Venda[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Venda|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Venda|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Venda patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Venda[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Venda findOrCreate($search, callable $callback = null, $options = [])
 */
class VendasTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vendas');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Xety/Cake3Upload.Upload', [
            'fields' => [
                'contrato_anexo' => [
                    'path' => '/upload/vendas/:md5',
                ],
            ],
            'suffix' => '_upload',
            'prefix' => '../'
        ]);

        $this->belongsTo('Veiculo', [
            'className' => 'Veiculos',
            'foreignKey' => 'veiculo_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cliente', [
            'className' => 'Clientes',
            'foreignKey' => 'cliente_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Vendedor', [
            'className' => 'Vendedores',
            'foreignKey' => 'vendedor_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('FormaPagamento', [
            'className' => 'FormasPagamento',
            'foreignKey' => 'forma_pagamento_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->decimal('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');

        $validator
            ->decimal('valor_desconto')
            ->requirePresence('valor_desconto', 'create')
            ->notEmpty('valor_desconto');

        $validator
            ->decimal('valor_acrescimo')
            ->requirePresence('valor_acrescimo', 'create')
            ->notEmpty('valor_acrescimo');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->integer('dia_vencimento')
            ->requirePresence('dia_vencimento', 'create')
            ->notEmpty('dia_vencimento');

        $validator
            ->dateTime('primeiro_pagamento')
            ->requirePresence('primeiro_pagamento', 'create')
            ->notEmpty('primeiro_pagamento');

        $validator
            ->dateTime('data_retirada')
            ->requirePresence('data_retirada', 'create')
            ->notEmpty('data_retirada');

        $validator
            ->scalar('anotacoes')
            ->allowEmpty('anotacoes');

        $validator
            ->scalar('contrato_anexo')
            ->maxLength('contrato_anexo', 255)
            ->allowEmpty('contrato_anexo');

        $validator
            ->boolean('rascunho')
            ->requirePresence('rascunho', 'create')
            ->notEmpty('rascunho');

        $validator
            ->dateTime('criado')
            ->requirePresence('criado', 'create')
            ->notEmpty('criado');

        $validator
            ->dateTime('modificado')
            ->requirePresence('modificado', 'create')
            ->notEmpty('modificado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    // In a table or behavior class
    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        if (isset($data['valor']) && !empty($data['valor'])) {
            $data['valor'] = valorToValue($data['valor']);
        }
        if (isset($data['valor_desconto']) && !empty($data['valor_desconto'])) {
            $data['valor_desconto'] = valorToValue($data['valor_desconto']);
        }
        if (isset($data['valor_acrescimo']) && !empty($data['valor_acrescimo'])) {
            $data['valor_acrescimo'] = valorToValue($data['valor_acrescimo']);
        }
        if (isset($data['data_retirada']) && !empty($data['data_retirada'])) {
            $data['data_retirada'] = dataToCakeTime($data['data_retirada']);
        }
        if (isset($data['primeiro_pagamento']) && !empty($data['primeiro_pagamento'])) {
            $data['primeiro_pagamento'] = dataToCakeTime($data['primeiro_pagamento']);
        }
    }

    /**
     * @param $id
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function buscar($id) {
        $entity = $this
            ->find()
            ->where([
                'Vendas.id' => $id
            ])
            ->first();

        return $entity;
    }

    /**
     * @param $dados
     * @param null $id
     * @return array
     */
    public function salvar($dados, $id = NULL) {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $entidade = $this->montar($dados, $id);

        try {
            if ($entidade->isNew()) {
                $entidade->status = StatusEnum::ACTIVE;
                $entidade->rascunho = StatusEnum::ACTIVE;
                $entidade->criado = Time::now();
            }
            $entidade->modificado = Time::now();

            $retorno = $this->save($entidade);
            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }

        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }
        $conn->commit();
        return $this->retorno;
    }
}
