<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Opcionais Model
 *
 * @property \App\Model\Table\VeiculosTable|\Cake\ORM\Association\BelongsToMany $Veiculos
 *
 * @method \App\Model\Entity\Opcionai get($primaryKey, $options = [])
 * @method \App\Model\Entity\Opcionai newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Opcionai[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Opcionai|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Opcionai|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Opcionai patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Opcionai[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Opcionai findOrCreate($search, callable $callback = null, $options = [])
 */
class OpcionaisTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('opcionais');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Veiculos', [
            'foreignKey' => 'opcionai_id',
            'targetForeignKey' => 'veiculo_id',
            'joinTable' => 'opcionais_veiculos'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->dateTime('criado')
            ->allowEmpty('criado');

        $validator
            ->dateTime('modificado')
            ->allowEmpty('modificado');

        return $validator;
    }
}
