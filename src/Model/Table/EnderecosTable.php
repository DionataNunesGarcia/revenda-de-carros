<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Enderecos Model
 *
 * @property \App\Model\Table\CidadesTable|\Cake\ORM\Association\BelongsTo $Cidades
 *
 * @method \App\Model\Entity\Endereco get($primaryKey, $options = [])
 * @method \App\Model\Entity\Endereco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Endereco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Endereco|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Endereco|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Endereco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Endereco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Endereco findOrCreate($search, callable $callback = null, $options = [])
 */
class EnderecosTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('enderecos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Cidade', [
            'className' => 'Cidades',
            'foreignKey' => 'cidade_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('model')
            ->maxLength('model', 100)
            ->allowEmpty('model');

        $validator
            ->integer('foreign_key')
            ->allowEmpty('foreign_key');

        $validator
            ->scalar('cep')
            ->maxLength('cep', 10)
            ->requirePresence('cep', 'create')
            ->notEmpty('cep');

        $validator
            ->scalar('rua')
            ->maxLength('rua', 255)
            ->requirePresence('rua', 'create')
            ->notEmpty('rua');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 45)
            ->requirePresence('numero', 'create')
            ->notEmpty('numero');

        $validator
            ->scalar('bairro')
            ->maxLength('bairro', 255)
            ->requirePresence('bairro', 'create')
            ->notEmpty('bairro');

        $validator
            ->scalar('complemento')
            ->maxLength('complemento', 255)
            ->allowEmpty('complemento', 'create');

        $validator
            ->scalar('lat')
            ->maxLength('lat', 100)
            ->allowEmpty('lat');

        $validator
            ->scalar('long')
            ->maxLength('long', 100)
            ->allowEmpty('long');

        $validator
            ->dateTime('criado')
            ->allowEmpty('criado');

        $validator
            ->dateTime('modificado')
            ->allowEmpty('modificado');

        return $validator;
    }

    // In a table or behavior class
    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        if (isset($data['cep'])) {
            $data['cep'] = soNumeros($data['cep']);
        }
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        return $rules;
    }

    public function salvarEndereco($dados)
    {
        $entidade = $this->montar($dados, $dados['id']);
        try {
            $retorno = $this->save($entidade);

            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                return $this->retorno;
            }
        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            return $this->retorno;
        }
    }
}
