<?php
namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Vendedores Model
 *
 * @property \App\Model\Table\UsuariosTable|\Cake\ORM\Association\BelongsTo $Usuarios
 *
 * @method \App\Model\Entity\Vendedore get($primaryKey, $options = [])
 * @method \App\Model\Entity\Vendedore newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Vendedore[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Vendedore|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vendedore|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Vendedore patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Vendedore[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Vendedore findOrCreate($search, callable $callback = null, $options = [])
 */
class VendedoresTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vendedores');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Usuario', [
            'className' => 'Usuarios',
            'foreignKey' => 'usuario_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Status', [
            'propertyName' => 'status',
            'className' => 'Status',
            'foreignKey' => 'status'
        ]);

        $this->hasOne('Endereco', [
            'foreignKey' => 'foreign_key',
            'joinType' => 'LEFT',
            'className' => 'Enderecos',
            'conditions' => [
                'Endereco.model' => 'Vendedores',
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->scalar('rg')
            ->maxLength('rg', 14)
            ->allowEmpty('rg');

        $validator
            ->scalar('cpf')
            ->maxLength('cpf', 11)
            ->allowEmpty('cpf');

        $validator
            ->dateTime('data_nascimento')
            ->allowEmpty('data_nascimento');

        $validator
            ->dateTime('data_admissao')
            ->allowEmpty('data_admissao');

        $validator
            ->dateTime('data_demissao')
            ->allowEmpty('data_demissao');

        $validator
            ->scalar('sexo')
            ->requirePresence('sexo', 'create')
            ->notEmpty('sexo');

        $validator
            ->scalar('celular')
            ->maxLength('celular', 15)
            ->allowEmpty('celular');

        $validator
            ->scalar('telefone')
            ->maxLength('telefone', 15)
            ->allowEmpty('telefone');

        $validator
            ->decimal('salario_fixo')
            ->allowEmpty('salario_fixo');

        $validator
            ->decimal('comissao')
            ->allowEmpty('comissao');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->dateTime('criado')
            ->allowEmpty('criado');

        $validator
            ->dateTime('modificado')
            ->allowEmpty('modificado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
//        $rules->add($rules->existsIn(['usuario_id'], 'Usuarios'));
        return $rules;
    }

    // In a table or behavior class
    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        if (isset($data['cpf'])) {
            $data['cpf'] = soNumeros($data['cpf']);
        }
        if (isset($data['comissao'])) {
            $data['comissao'] = valorToValue($data['comissao']);
        }
        if (isset($data['salario_fixo'])) {
            $data['salario_fixo'] = valorToValue($data['salario_fixo']);
        }
        if (isset($data['celular'])) {
            $data['celular'] = somenteNumerosFone($data['celular']);
        }
        if (isset($data['telefone']) && !empty($data['telefone'])) {
            $data['telefone'] = somenteNumerosFone($data['telefone']);
        }
        if (isset($data['data_nascimento'])) {
            $data['data_nascimento'] = dataToCakeTime($data['data_nascimento']);
        }
        if (isset($data['data_admissao']) && !empty($data['data_admissao'])) {
            $data['data_admissao'] = dataToCakeTime($data['data_admissao']);
        }
        if (isset($data['data_demissao']) && !empty($data['data_demissao'])) {
            $data['data_demissao'] = dataToCakeTime($data['data_demissao']);
        }
    }

    /**
     * @return \App\Model\Entity\Vendedore
     */
    public function novo() {
        $entity = $this->newEntity();
        $entity->endereco = TableRegistry::getTableLocator()
            ->get('Enderecos')
            ->newEntity();

        $entity->usuario = TableRegistry::getTableLocator()
            ->get('Usuarios')
            ->newEntity();

        return $entity;
    }

    /**
     * @param $id
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function buscar($id) {
        $entity = $this
            ->find()
            ->contain([
                'Endereco',
                'Usuario',
            ])
            ->where([
                'Vendedores.id' => $id
            ])
            ->first();

        return $entity;
    }

    /**
     * @param $dados
     * @param null $id
     * @return array
     */
    public function salvar($dados, $id = NULL) {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $entidade = $this->montar($dados, $id);

        try {
            if (isset($dados['usuario'])) {
                $usuario = TableRegistry::getTableLocator()
                    ->get('Usuarios')
                    ->salvar($dados['usuario']);

                if ($usuario['status']) {
                    $entidade->usuario_id = $usuario['entidade']->id;
                }
            }

            $retorno = $this->save($entidade);
            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }
            if (isset($dados['endereco'])) {
                $dados['endereco']['foreign_key'] = $entidade->id;
                TableRegistry::getTableLocator()
                    ->get('Enderecos')
                    ->salvarEndereco($dados['endereco']);
            }

        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }
        $conn->commit();
        return $this->retorno;
    }
}
