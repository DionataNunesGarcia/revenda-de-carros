<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;

/**
 * Arquivos Model
 *
 * @method \App\Model\Entity\Arquivo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Arquivo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Arquivo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Arquivo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Arquivo|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Arquivo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Arquivo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Arquivo findOrCreate($search, callable $callback = null, $options = [])
 */
class ArquivosTable extends AppTable {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('arquivos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Xety/Cake3Upload.Upload', [
            'fields' => [
                'arquivo' => [
                    'path' => '/upload/arquivos/:md5',
                ]
            ],
            'suffix' => '_upload',
            'prefix' => '../'
        ]);

        $this->belongsTo('Usuario', [
            'foreignKey' => 'usuario_id',
            'className' => 'Usuarios',
            'joinType' => 'LEFT'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('arquivo')
                ->maxLength('arquivo', 255)
                ->requirePresence('arquivo', 'create')
                ->notEmpty('arquivo');

        $validator
                ->scalar('tipo')
                ->allowEmpty('tipo');

        $validator
                ->scalar('titulo')
                ->maxLength('titulo', 255)
                ->allowEmpty('titulo');

        $validator
                ->dateTime('criado')
                ->allowEmpty('criado');

        return $validator;
    }

    public function novo() {
        return $this->newEntity();
    }

    public function buscar($id) {
        return $this->get($id, ['contain' => ['Usuario']]);
    }

    public function salvar($arquivo, $tipo = '', $titulo = '', ...$args) {

        $conn = ConnectionManager::get('default');
        $conn->begin();
        $userSession = Configure::read('SessionUser');

        ini_set('post_max_size', '64M');
        ini_set('upload_max_filesize', '64M');

        $entidade = $this->novo();
        try {

            $entidade->arquivo_upload = $arquivo;
            $entidade->tipo = $tipo;
            $entidade->titulo = $titulo;
            $entidade->descricao = $titulo;
            $entidade->usuario_id = $userSession['id'];
            $entidade->criado = Time::now();
            $entidade->modificado = Time::now();

            $retorno = $this->save($entidade);

            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }
        } catch (\PDOException $e) {

            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }

        $conn->commit();

        return $this->retorno;
    }

    public function salvarArquivos($data)
    {
        $response = [];
        $userSession = Configure::read('SessionUser');
        foreach ($data['files'] as $file) {
            if ($file['error'] > 0) {
                throw new \Exception("Arquivo inválido", 400);
            }
            $entidade = $this->novo();
            $entidade->arquivo_upload = $file;
            $entidade->tipo = $file['type'];
            $entidade->foreign_key = $data['foreign_key'];
            $entidade->model = $data['model'];
            $entidade->link = $data['link'];
            $entidade->titulo = $file['name'];
            $entidade->extensao = extensao($file['name']);
            $entidade->tamanho = $file['size'];
            $entidade->descricao = '';
            $entidade->usuario_id = $userSession['id'];
            $entidade->criado = Time::now();
            $entidade->modificado = Time::now();
            if (!$this->save($entidade)) {
                throw new \Exception("Erro ao salvar o arquivo", 400);
            }
            $response['data'][] = $entidade;
        }
        return $response;
    }

    public function updateFields($data)
    {
        $response = [];
        $arquivos = $data['arquivo'];
        foreach ($arquivos['id'] as $key => $id) {
            $entidade = $this->buscar($id);
            $entidade->titulo = $arquivos['titulo'][$key];
            $entidade->link = $arquivos['link'][$key];
            $entidade->modificado = Time::now();
            if (!$this->save($entidade)) {
                throw new \Exception("Erro ao salvar o arquivo", 400);
            }
            $response['data'][] = $entidade;
        }
        return $response;
    }

    public function listarArquivos($data)
    {
        return $this
            ->find()
            ->where([
                'foreign_key' => $data['foreign_key'],
                'model' => $data['model'],
            ])
            ->orderDesc('criado')
            ->toArray();
    }

    public function deleteFile($data)
    {
        $entidade = $this
            ->find()
            ->where([
                'id' => $data['id'],
            ])
            ->first();
        @unlink(WWW_ROOT . $entidade->arquivo);
        $this->delete($entidade);

        return [
            'message' => 'Arquivo deletado com sucesso',
            'code' => 200
        ];
    }

    public function enviarMensagem($entidade)
    {
        $mensagensTable = TableRegistry::get('UsuariosMensagens');
        $mensagem = $mensagensTable->novo();

        $mensagem->titulo = $entidade->titulo;
        $mensagem->assunto = "Arquivo do tipo \"{$entidade->tipo}\" importado na data de " . Time::now() . ".";
        $mensagem->mensagem = $entidade->observacoes;
        $mensagem->tipo = $entidade->titulo;
        $mensagem->usuario_id = $entidade->usuario_id;
        $mensagem->enviado_por_id = null;
        $mensagem->visualizado = false;
        $mensagem->data_visualizacao = null;
        $mensagem->criado = Time::now();

        $mensagensTable->save($mensagem);
    }
}
