<?php
namespace App\Model\Table;

use App\Enum\SituacaoEnum;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Veiculos Model
 *
 * @property \App\Model\Table\ModelosTable|\Cake\ORM\Association\BelongsTo $Modelos
 * @property \App\Model\Table\MarcasTable|\Cake\ORM\Association\BelongsTo $Marcas
 * @property \App\Model\Table\TipoVeiculosTable|\Cake\ORM\Association\BelongsTo $TipoVeiculos
 * @property \App\Model\Table\ProprietariosTable|\Cake\ORM\Association\BelongsTo $Proprietarios
 * @property \App\Model\Table\CidadesTable|\Cake\ORM\Association\BelongsTo $Cidades
 * @property |\Cake\ORM\Association\BelongsTo $Cores
 * @property |\Cake\ORM\Association\BelongsTo $Usuarios
 * @property \App\Model\Table\OpcionaisTable|\Cake\ORM\Association\BelongsToMany $Opcionais
 *
 * @method \App\Model\Entity\Veiculo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Veiculo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Veiculo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Veiculo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Veiculo|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Veiculo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Veiculo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Veiculo findOrCreate($search, callable $callback = null, $options = [])
 */
class VeiculosTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('veiculos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Modelos', [
            'foreignKey' => 'modelo_id'
        ]);
        $this->belongsTo('Marcas', [
            'foreignKey' => 'marca_id'
        ]);
        $this->belongsTo('TipoVeiculos', [
            'foreignKey' => 'tipo_veiculo_id'
        ]);
        $this->belongsTo('Proprietarios', [
            'foreignKey' => 'proprietario_id'
        ]);
        $this->belongsTo('Cidades', [
            'foreignKey' => 'cidade_id'
        ]);
        $this->belongsTo('Cores', [
            'foreignKey' => 'cor_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Usuarios', [
            'foreignKey' => 'usuario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Opcionais', [
            'foreignKey' => 'veiculo_id',
            'targetForeignKey' => 'opcional_id',
            'joinTable' => 'opcionais_veiculos'
        ]);

        $this->hasMany('Arquivos', [
            'className' => 'Arquivos',
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'conditions' => [
                'Arquivos.model' => 'Veiculos',
            ],
            'sort' => [
                'Arquivos.ordem' => 'ASC',
            ],
            'propertyName' => 'arquivos',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->addBehavior('Xety/Cake3Upload.Upload', [
            'fields' => [
                'imagem' => [
                    'path' => '/upload/veiculo/:md5',
                ]
            ],
            'suffix' => '_upload',
            'prefix' => '../'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('ano')
            ->allowEmpty('ano');

        $validator
            ->decimal('valor')
            ->allowEmpty('valor');

        $validator
            ->decimal('valor_compra')
            ->allowEmpty('valor_compra');

        $validator
            ->scalar('renavam')
            ->maxLength('renavam', 100)
            ->requirePresence('renavam', 'create')
            ->notEmpty('renavam');

        $validator
            ->scalar('placa')
            ->maxLength('placa', 10)
            ->requirePresence('placa', 'create')
            ->notEmpty('placa');

        $validator
            ->decimal('quilometragem')
            ->allowEmpty('quilometragem');

        $validator
            ->scalar('chassi')
            ->maxLength('chassi', 10)
            ->requirePresence('chassi', 'create')
            ->notEmpty('chassi');

        $validator
            ->dateTime('data_entrada')
            ->allowEmpty('data_entrada');

        $validator
            ->dateTime('data_venda')
            ->allowEmpty('data_venda');

        $validator
            ->decimal('valor_venda')
            ->allowEmpty('valor_venda');

        $validator
            ->scalar('situacao')
            ->requirePresence('situacao', 'create')
            ->notEmpty('situacao');

        $validator
            ->scalar('observacoes')
            ->allowEmpty('observacoes');

        $validator
            ->scalar('anotacoes')
            ->allowEmpty('anotacoes');

        $validator
            ->boolean('importado')
            ->requirePresence('importado', 'create')
            ->notEmpty('importado');

        $validator
            ->dateTime('criado')
            ->allowEmpty('criado');

        $validator
            ->dateTime('modificado')
            ->allowEmpty('modificado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
//        $rules->add($rules->existsIn(['modelo_id'], 'Modelos'));
//        $rules->add($rules->existsIn(['marca_id'], 'Marcas'));
//        $rules->add($rules->existsIn(['tipo_veiculo_id'], 'TipoVeiculos'));
//        $rules->add($rules->existsIn(['proprietario_id'], 'Proprietarios'));
//        $rules->add($rules->existsIn(['cidade_id'], 'Cidades'));
//        $rules->add($rules->existsIn(['cor_id'], 'Cores'));
//        $rules->add($rules->existsIn(['usuario_id'], 'Usuarios'));

        return $rules;
    }

    public function autocomplete($term = null, $id = null) {

        $query = $this
            ->find('all')
            ->contain([
                'Modelos'
            ])
            ->limit(20);

        if (!empty($id)) {
            //Se carregar o id, busca por ele
            $query->where(['Veiculos.id in ' => explode(',',$id)]);
        } else if (!empty($term)) {
            //se pesquisar, busca pelo termo
            $query->where([
                'OR' => [
                    'upper(Veiculos.placa) like' => '%' . strtoupper($term) . '%',
                    'upper(Modelos.nome) like' => '%' . strtoupper($term) . '%'
                ]
            ]);
        }

        $retorno = [];
        foreach ($query as $result) {
            $retorno[] = [
                'id' => $result->id,
                'value' => "{$result->placa} - {$result->modelo->nome}",
            ];
        }

        return $retorno;
    }

    // In a table or behavior class
    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        if (isset($data['valor'])) {
            $data['valor'] = valorToValue($data['valor']);
        }
        if (isset($data['valor_compra'])) {
            $data['valor_compra'] = valorToValue($data['valor_compra']);
        }
        if (isset($data['data_entrada'])) {
            $data['data_entrada'] = dataToDate($data['data_entrada']);
            $data['data_entrada'] = new Time($data['data_entrada']);
        }
        if (isset($data['data_venda'])) {
            $data['data_venda'] = dataToDate($data['data_venda']);
            $data['data_venda'] = new Time($data['data_venda']);
        }
        if (isset($data['valor_venda'])) {
            $data['valor_venda'] = valorToValue($data['valor_venda']);
        }
        if (isset($data['valor_reforma'])) {
            $data['valor_reforma'] = valorToValue($data['valor_reforma']);
        }
        if (isset($data['valor_fipe'])) {
            $data['valor_fipe'] = valorToValue($data['valor_fipe']);
        }
        if (isset($data['valor_diaria_aluguel'])) {
            $data['valor_diaria_aluguel'] = valorToValue($data['valor_diaria_aluguel']);
        }
        if (isset($data['valor_promocao'])) {
            $data['valor_promocao'] = valorToValue($data['valor_promocao']);
        }
        if (isset($data['descricao'])) {
            $data['descricao'] = utf8_encode($data['descricao']);
        }
    }

    /**
     * @param $id
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function buscar($id) {
        $entity = $this
            ->find()
            ->contain([
                'Cores',
                'Cidades',
                'Usuarios',
                'Modelos.Marcas',
                'Arquivos',
                'Opcionais',
            ])
            ->where([
                'Veiculos.id' => $id
            ])
            ->first();
        $entity->opcionais_ids = [];
        foreach ($entity->opcionais as $opcional) {
            $entity->opcionais_ids[] = $opcional->id;
        }
        return $entity;
    }

    /**
     * @param $dados
     * @param null $id
     * @return array
     */
    public function salvar($dados, $id = NULL)
    {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $entidade = $this->montar($dados, $id);
        try {
            $entidade->usuario_id = $this->userSession['id'];

            $retorno = $this->save($entidade);
            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }
            $this->saveOpcionais($dados, $entidade);
        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }
        $conn->commit();
        return $this->retorno;
    }

    private function saveOpcionais($dados, $entidade)
    {
        $table = TableRegistry::getTableLocator()->get("OpcionaisVeiculos");

        if (isset($dados['opcionais']['ids'])) {
            $table->deleteAll([
                'veiculo_id' => $entidade->id
            ]);
            foreach ($dados['opcionais']['ids'] as $id) {
                $opcional = $table->newEntity();
                $opcional->opcional_id = $id;
                $opcional->veiculo_id = $entidade->id;

                $table->save($opcional);
            }
        }
    }

    public function excluirImagem($id) {
        $entidade = $this->buscar($id);

        @unlink(WWW_ROOT . $entidade->imagem);

        $entidade->imagem = '';

        $this->save($entidade);
    }

    public function getDestaques($limit = 5)
    {
        $conditions = $this->getConditionsFront();
        return $this
            ->find()
            ->where($conditions)
            ->contain([
                'Modelos.Marcas'
            ])
            ->limit($limit)
            ->toArray();
    }

    public function getUltimos($limit = 5)
    {
        $conditions = $this->getConditionsFront();
        return $this
            ->find()
            ->where($conditions)
            ->contain([
                'Modelos.Marcas'
            ])
            ->limit($limit)
            ->orderDesc('Veiculos.criado')
            ->toArray();
    }

    public function getListFront($searchFilter = [])
    {
        $conditions = $this->getFilterFront($searchFilter);
        $order = $this->getOrderFront($searchFilter);
        return $this
            ->find()
            ->where($conditions)
            ->contain([
                'Modelos.Marcas'
            ])
            ->order($order);
    }

    private function getFilterFront($searchFilter)
    {
        $conditions = $this->getConditionsFront();
        if (isset($searchFilter['modelo']) && !empty($searchFilter['modelo'])) {
            $conditions["Veiculos.modelo_id"] = $searchFilter['modelo'];
        }
        if (isset($searchFilter['categoria']) && !empty($searchFilter['categoria'])) {
            $conditions["Veiculos.tipo_veiculo_id"] = $searchFilter['categoria'];
        }
        if (isset($searchFilter['marca']) && !empty($searchFilter['marca'])) {
            $conditions["Modelos.marca_id"] = $searchFilter['marca'];
        }
        if (isset($searchFilter['ano_min']) && !empty($searchFilter['ano_min'])) {
            $conditions["Veiculos.ano >="] = $searchFilter['ano_min'];
        }
        if (isset($searchFilter['ano_max']) && !empty($searchFilter['ano_max'])) {
            $conditions["Veiculos.ano <="] = $searchFilter['ano_max'];
        }
        if (isset($searchFilter['importado']) && $searchFilter['importado'] != '') {
            $conditions["Veiculos.importado"] = $searchFilter['importado'];
        }
        if (isset($searchFilter['combustivel']) && !empty($searchFilter['combustivel'])) {
            $conditions["Veiculos.combustivel"] = $searchFilter['combustivel'];
        }

        return $conditions;
    }

    private function getOrderFront($searchFilter)
    {
        $order = [];
        if (isset($searchFilter['ordem_preco']) && !empty($searchFilter['ordem_preco'])) {
            $order["Veiculos.valor"] = $searchFilter['ordem_preco'];
        }
        if (empty($order)) {
            $order['Veiculos.destaque'] = 'DESC';
        }
        return $order;
    }

    private function getConditionsFront()
    {
        return [
            'Veiculos.destaque' => true,
            'Veiculos.mostrar_site' => true,
            'Veiculos.situacao' => SituacaoEnum::A_VENDA,
        ];
    }
}
