<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * VwNiveis Model
 *
 * @method \App\Model\Entity\VwNivei get($primaryKey, $options = [])
 * @method \App\Model\Entity\VwNivei newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\VwNivei[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\VwNivei|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VwNivei|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VwNivei patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\VwNivei[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\VwNivei findOrCreate($search, callable $callback = null, $options = [])
 */
class VwNiveisTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vw_niveis');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->requirePresence('id', 'create')
            ->notEmpty('id');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->allowEmpty('nome');

        $validator
            ->boolean('ativo')
            ->requirePresence('ativo', 'create')
            ->notEmpty('ativo');

        $validator
            ->boolean('deletavel')
            ->requirePresence('deletavel', 'create')
            ->notEmpty('deletavel');

        $validator
            ->boolean('deletado')
            ->allowEmpty('deletado');

        $validator
            ->requirePresence('usuarios', 'create')
            ->notEmpty('usuarios');

        return $validator;
    }
}
