<?php
namespace App\Model\Table;

use App\Enum\StatusEnum;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContasReceber Model
 *
 * @method \App\Model\Entity\ContasReceber get($primaryKey, $options = [])
 * @method \App\Model\Entity\ContasReceber newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ContasReceber[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ContasReceber|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContasReceber|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContasReceber patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ContasReceber[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ContasReceber findOrCreate($search, callable $callback = null, $options = [])
 */
class ContasReceberTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contas_receber');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Xety/Cake3Upload.Upload', [
            'fields' => [
                'boleto' => [
                    'path' => '/upload/contas-receber/:md5',
                ],
                'nota_fiscal' => [
                    'path' => '/upload/contas-receber/:md5',
                ],
                'comprovante_pagamento' => [
                    'path' => '/upload/contas-receber/:md5',
                ],
            ],
            'suffix' => '_upload',
            'prefix' => '../'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->decimal('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');

        $validator
            ->scalar('model')
            ->maxLength('model', 100)
            ->allowEmpty('model');

        $validator
            ->integer('foreign_key')
            ->allowEmpty('foreign_key');

        $validator
            ->date('data_vencimento')
            ->requirePresence('data_vencimento', 'create')
            ->notEmpty('data_vencimento');

        $validator
            ->date('data_pagamento')
            ->allowEmpty('data_pagamento');

        $validator
            ->dateTime('criado')
            ->requirePresence('criado', 'create')
            ->notEmpty('criado');

        $validator
            ->dateTime('modificado')
            ->requirePresence('modificado', 'create')
            ->notEmpty('modificado');

        return $validator;
    }

    // In a table or behavior class
    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        if (isset($data['valor']) && !empty($data['valor'])) {
            $data['valor'] = valorToValue($data['valor']);
        }
        if (isset($data['data_vencimento']) && !empty($data['data_vencimento'])) {
            $data['data_vencimento'] = dataToCakeTime($data['data_vencimento']);
        }
        if (isset($data['data_validade']) && !empty($data['data_validade'])) {
            $data['data_validade'] = dataToCakeTime($data['data_validade']);
        }
        if (isset($data['data_pagamento']) && !empty($data['data_pagamento'])) {
            $data['data_pagamento'] = dataToCakeTime($data['data_pagamento']);
        }
    }

    /**
     * @param $id
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function buscar($id) {
        $entity = $this
            ->find()
            ->where([
                'ContasReceber.id' => $id
            ])
            ->first();

        return $entity;
    }

    /**
     * @param $dados
     * @param null $id
     * @return array
     */
    public function salvar($dados, $id = NULL) {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $entidade = $this->montar($dados, $id);

        try {
            if ($entidade->isNew()) {
                $entidade->model = 'Clientes';
                $entidade->status = StatusEnum::ACTIVE;
                $entidade->criado = Time::now();
            }
            $entidade->modificado = Time::now();

            $retorno = $this->save($entidade);
            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }

        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }
        $conn->commit();
        return $this->retorno;
    }
}
