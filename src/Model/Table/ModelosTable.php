<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Modelos Model
 *
 * @property \App\Model\Table\MarcasTable|\Cake\ORM\Association\BelongsTo $Marcas
 * @property \App\Model\Table\VeiculosTable|\Cake\ORM\Association\HasMany $Veiculos
 *
 * @method \App\Model\Entity\Modelo get($primaryKey, $options = [])
 * @method \App\Model\Entity\Modelo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Modelo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Modelo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Modelo|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Modelo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Modelo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Modelo findOrCreate($search, callable $callback = null, $options = [])
 */
class ModelosTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('modelos');
        $this->setDisplayField('nome');
        $this->setPrimaryKey('id');

        $this->belongsTo('Marcas', [
            'foreignKey' => 'marca_id',
            'type' => 'INNER'
        ]);
        $this->hasMany('Veiculos', [
            'foreignKey' => 'modelo_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['marca_id'], 'Marcas'));

        return $rules;
    }

    public function autocomplete($term = null, $id = null) {

        $query = $this
            ->find('all')
            ->contain(['Marcas'])
            ->limit(20);

        if (!empty($id)) {
            //Se carregar o id, busca por ele
            $query->where(['Modelos.id in ' => explode(',',$id)]);
        } else if (!empty($term)) {
            //se pesquisar, busca pelo termo
            $query->where([
                'OR' => [
                    'upper(Modelos.nome) like' => '%' . strtoupper($term) . '%',
                    'upper(Marcas.nome) like' => '%' . strtoupper($term) . '%'
                ]
            ]);
        }

        $retorno = [];
        foreach ($query as $result) {
            $retorno[] = [
                'id' => $result->id,
                'value' => $result->nome . " ({$result->marca->nome})",
            ];
        }

        return $retorno;
    }

    public function getList()
    {
        return $this
            ->find('list')
            ->matching('Veiculos', function(\Cake\ORM\Query $q) {
                return $q
                    ->where([
                        'Veiculos.mostrar_site' => true
                    ]);
            })
            ->toArray();
    }
}
