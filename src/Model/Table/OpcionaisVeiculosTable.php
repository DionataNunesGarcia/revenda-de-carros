<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OpcionaisVeiculos Model
 *
 * @property \App\Model\Table\OpcionalsTable|\Cake\ORM\Association\BelongsTo $Opcionals
 * @property \App\Model\Table\VeiculosTable|\Cake\ORM\Association\BelongsTo $Veiculos
 *
 * @method \App\Model\Entity\OpcionaisVeiculo get($primaryKey, $options = [])
 * @method \App\Model\Entity\OpcionaisVeiculo newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OpcionaisVeiculo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OpcionaisVeiculo|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OpcionaisVeiculo|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OpcionaisVeiculo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OpcionaisVeiculo[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OpcionaisVeiculo findOrCreate($search, callable $callback = null, $options = [])
 */
class OpcionaisVeiculosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('opcionais_veiculos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Opcionais', [
            'foreignKey' => 'opcional_id'
        ]);
        $this->belongsTo('Veiculos', [
            'foreignKey' => 'veiculo_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['opcional_id'], 'Opcionais'));
        $rules->add($rules->existsIn(['veiculo_id'], 'Veiculos'));

        return $rules;
    }
}
