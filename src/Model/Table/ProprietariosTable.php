<?php
namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Proprietarios Model
 *
 * @property \App\Model\Table\VeiculosTable|\Cake\ORM\Association\HasMany $Veiculos
 *
 * @method \App\Model\Entity\Proprietario get($primaryKey, $options = [])
 * @method \App\Model\Entity\Proprietario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Proprietario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Proprietario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Proprietario|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Proprietario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Proprietario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Proprietario findOrCreate($search, callable $callback = null, $options = [])
 */
class ProprietariosTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('proprietarios');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Veiculos', [
            'foreignKey' => 'proprietario_id'
        ]);

        $this->hasOne('Endereco', [
            'foreignKey' => 'foreign_key',
            'className' => 'Enderecos',
            'conditions' => [
                'Endereco.model' => 'Proprietarios',
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 50)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->scalar('rg')
            ->maxLength('rg', 14)
            ->requirePresence('rg', 'create')
            ->notEmpty('rg');

        $validator
            ->scalar('cpf')
            ->maxLength('cpf', 11)
            ->requirePresence('cpf', 'create')
            ->notEmpty('cpf');

        $validator
            ->scalar('celular')
            ->maxLength('celular', 15)
            ->requirePresence('celular', 'create')
            ->notEmpty('celular');

        $validator
            ->scalar('telefone')
            ->maxLength('telefone', 15)
            ->requirePresence('telefone', 'create')
            ->notEmpty('telefone');

        $validator
            ->dateTime('criado')
            ->allowEmpty('criado');

        $validator
            ->dateTime('modificado')
            ->allowEmpty('modificado');

        return $validator;
    }

    // In a table or behavior class
    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        if (isset($data['cpf'])) {
            $data['cpf'] = soNumeros($data['cpf']);
        }
        if (isset($data['celular'])) {
            $data['celular'] = somenteNumerosFone($data['celular']);
        }
        if (isset($data['telefone']) && !empty($data['telefone'])) {
            $data['telefone'] = somenteNumerosFone($data['telefone']);
        }
    }

    /**
     * @param $id
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function buscar($id) {
        $entity = $this
            ->find()
            ->contain([
                'Endereco'
            ])
            ->where([
                'Proprietarios.id' => $id
            ])
            ->first();

        return $entity;
    }

    /**
     * @param $dados
     * @param null $id
     * @return array
     */
    public function salvar($dados, $id = NULL)
    {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $entidade = $this->montar($dados, $id);
        try {
            $retorno = $this->save($entidade);

            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }
            if (isset($dados['endereco'])) {
                $dados['endereco']['foreign_key'] = $entidade->id;
                TableRegistry::getTableLocator()
                    ->get('Enderecos')
                    ->salvarEndereco($dados['endereco']);
            }
        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }
        $conn->commit();
        return $this->retorno;
    }
}
