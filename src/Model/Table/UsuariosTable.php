<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Routing\Router;

/**
 * Usuarios Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Niveis
 * @property |\Cake\ORM\Association\HasMany $Gerentes
 * @property |\Cake\ORM\Association\HasMany $Vendedores
 *
 * @method \App\Model\Entity\Usuario get($primaryKey, $options = [])
 * @method \App\Model\Entity\Usuario newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Usuario[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Usuario|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usuario|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Usuario patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Usuario findOrCreate($search, callable $callback = null, $options = [])
 */
class UsuariosTable extends AppTable {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('usuarios');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Niveis', [
            'foreignKey' => 'niveis_id',
            'joinType' => 'INNER',
            'className' => 'Niveis',
            'propertyName' => 'niveis',
        ]);

        $this->addBehavior('Xety/Cake3Upload.Upload', [
            'fields' => [
                'imagem' => [
                    'path' => '/upload/usuario/:md5',
                ]
            ],
            'suffix' => '_upload',
            'prefix' => '../'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

//        $validator
//                ->scalar('usuario')
//                ->maxLength('usuario', 45)
//                ->requirePresence('usuario', 'create')
//                ->notEmpty('usuario')
//                ->add('usuario', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
                ->scalar('senha')
                ->maxLength('senha', 255)
                ->allowEmpty('senha');

        $validator
                ->scalar('imagem')
                ->maxLength('imagem', 255)
                ->allowEmpty('imagem');

        $validator
                ->dateTime('criado')
                ->allowEmpty('criado');

        $validator
                ->dateTime('modificado')
                ->allowEmpty('modificado');

        $validator
                ->boolean('ativo')
                ->requirePresence('ativo', 'create')
                ->notEmpty('ativo');

        $validator
                ->boolean('deletado')
                ->requirePresence('deletado', 'create')
                ->notEmpty('deletado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
//        $rules->add($rules->isUnique(['usuario']));
        $rules->add($rules->existsIn(['niveis_id'], 'Niveis'));

        return $rules;
    }

    public function autocomplete($term = null, $id = null) {

        $query = $this->find('all')
            ->andWhere([
                'deletado' => false,
            ])
            ->limit(50);

        if (!empty($id)) {
            //Se carregar o id, busca por ele
            $query->where(['id in ' => explode(',',$id)]);
        } else if (!empty($term)) {
            //se pesquisar, busca pelo termo
            $query->where(['upper(usuario) like' => '%' . strtoupper($term) . '%']);
        }

        $query
            ->order('usuario');

        $retorno = [];
        foreach ($query as $result) {
            $retorno[] = [
                'id' => $result->id,
                'value' => $result->usuario,
            ];
        }

        return $retorno;
    }

    public function novo() {
        return $this->newEntity();
    }

    public function buscar($id) {
        return $this->get($id,[
            'contain' => [
                'Niveis'
            ]
        ]);
    }

    public function salvar($dados, $id = NULL) {
        $conn = ConnectionManager::get('default');
        $conn->begin();

        if (!empty($id) && $dados['senha'] === '') {
            unset($dados['senha']);
        }

        if(!empty($dados['imagem_upload_temp'])){
            $dados['imagem_upload']['tmp_name'] = $dados['imagem_upload_temp'];
        }

        $entidade = $this->montar($dados, $id);

        try {
            if (empty($id)) {
                $entidade->deletado = false;
            }

            $this->retorno['entidade'] = $this->save($entidade);

            if (!$this->retorno['entidade']) {
                $this->retorno['status'] = false;
                $this->retorno['entidade'] = $entidade;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());

                $conn->rollback();
            }
        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
            $conn->rollback();
            return $this->retorno;
        }

        $conn->commit();

        return $this->retorno;
    }

    public function excluirImagem($id) {
        $entidade = $this->buscar($id);

        @unlink(WWW_ROOT . $entidade->imagem);

        $entidade->imagem = NULL;

        $this->save($entidade);
    }

    public function montarUsuarioLogado($id) {
        $usuario = $this->get($id, ['contain' => ['Niveis', 'Niveis.NiveisPermissao']])->toArray();

        $url = Router::url('/', true);
        $permissoes = [];

        //Busca qual tipo de pessoa é

        //Monta as permissoes
        if (!empty($usuario['niveis']['niveisPermissoes'])) {
            foreach ($usuario['niveis']['niveisPermissoes'] as $item) {
                $permissoes[] = $url . $item['prefix'] . '/' . tratarNomeSlug($item['controller']) . '/' . tratarNomeSlug($item['action']);
            }
        }

        $usuario['permissoes'] = $permissoes;

        return $usuario;
    }

}
