<?php
namespace App\Model\Table;

use App\Enum\StatusEnum;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use App\Enum\TipoPinturaEnum;
use App\Enum\CorDenatranEnum;

/**
 * Cores Model
 *
 * @method \App\Model\Entity\Core get($primaryKey, $options = [])
 * @method \App\Model\Entity\Core newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Core[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Core|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Core|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Core patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Core[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Core findOrCreate($search, callable $callback = null, $options = [])
 */
class CoresTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('cores');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('codigo')
            ->maxLength('codigo', 100)
            ->requirePresence('codigo', 'create')
            ->notEmpty('codigo');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->scalar('tipo_pintura')
            ->requirePresence('tipo_pintura', 'create')
            ->notEmpty('tipo_pintura');

        $validator
            ->scalar('cor')
            ->maxLength('cor', 10)
            ->requirePresence('cor', 'create')
            ->notEmpty('cor');

        $validator
            ->scalar('cor_denatran')
            ->requirePresence('cor_denatran', 'create')
            ->notEmpty('cor_denatran');

        return $validator;
    }

    /**
     * @param null $term
     * @param null $id
     * @return array
     */
    public function autocomplete($term = null, $id = null) {

        $query = $this
            ->find('all')
            ->contain([])
            ->limit(20);

        if (!empty($id)) {
            //Se carregar o id, busca por ele
            $query->where(['Cores.id in ' => explode(',',$id)]);
        } else if (!empty($term)) {
            //se pesquisar, busca pelo termo
            $query->where([
                'OR' => [
                    'upper(Cores.codigo) like' => '%' . strtoupper($term) . '%',
                    'upper(Cores.tipo_pintura) like' => '%' . strtoupper($term) . '%',
                    'upper(Cores.cor) like' => '%' . strtoupper($term) . '%',
                    'upper(Cores.nome) like' => '%' . strtoupper($term) . '%',
                ]
            ]);
        }

        $retorno = [];
        foreach ($query as $result) {
            $tipo = TipoPinturaEnum::getType($result->tipo_pintura);
            $retorno[] = [
                'id' => $result->id,
                'value' => "{$result->nome} | {$tipo} | Denatran: {$result->cor_denatran}",
            ];
        }

        return $retorno;
    }

    /**
     * @param $dados
     * @param null $id
     * @return array
     */
    public function salvar($dados, $id = NULL) {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $entidade = $this->montar($dados, $id);

        try {
            $retorno = $this->save($entidade);
            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }

        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }
        $conn->commit();
        return $this->retorno;
    }
}
