<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\NotFoundException;
use Cake\ORM\TableRegistry;

/**
 * Niveis Model
 *
 * @method \App\Model\Entity\Nivel get($primaryKey, $options = [])
 * @method \App\Model\Entity\Nivel newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Nivei[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Nivei|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Nivei|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Nivel patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Nivei[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Nivel findOrCreate($search, callable $callback = null, $options = [])
 */
class NiveisTable extends AppTable {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('niveis');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('NiveisPermissao', [
            'foreignKey' => 'nivel_id',
            'className' => 'NiveisPermissoes',
            'propertyName' => 'niveisPermissoes',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 45)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->dateTime('criado')
            ->requirePresence('criado', 'create')
            ->notEmpty('criado');

        $validator
            ->dateTime('modificado')
            ->allowEmpty('modificado');

        $validator
            ->boolean('ativo')
            ->requirePresence('ativo', 'create')
            ->notEmpty('ativo');

        $validator
            ->boolean('deletavel')
            ->requirePresence('deletavel', 'create')
            ->notEmpty('deletavel');

        return $validator;
    }

    public function novo() {
        return $this->newEntity();
    }

    public function buscar($id) {
        return $this->get($id, ['contain' => ['NiveisPermissao']]);
    }

    public function salvar($dados, $id = NULL) {
        $entidade = $this->montar($dados, $id);

        $permissoesTable = TableRegistry::get('NiveisPermissoes');

        $conn = ConnectionManager::get('default');
        $conn->begin();

        try {
            if (empty($entidade->id)) {
                $entidade->criado = Time::now();
                $entidade->deletavel = 1;
            } else {
                $entidade->modificado = Time::now();
                $permissoesTable->salvarTodos($dados, $entidade->id);
            }

            $retorno = $this->save($entidade);
            if (!$retorno) {
                $conn->rollback();
                throw new NotFoundException(__('Não foi possivel salvar o registro'));
            }
        } catch (\PDOException $e) {
            $conn->rollback();
            throw new NotFoundException(__($e->getMessage()));
        }

        $conn->commit();
        return $retorno;
    }

    public function excluir($ids) {
        $resultado = true;
        $error = [];
        
        $permissoesTable = TableRegistry::get('NiveisPermissoes');

        $conn = ConnectionManager::get('default');
        $conn->begin();

        if (!empty($ids)) {
            foreach (explode(',', $ids) as $i) {

                try {
                    $entidade = $this->get($i);
                    
                    $permissoesTable->excluirPermissoesNivel($i);
        
                    $resultado = $this->delete($entidade);
                } catch (\PDOException $e) {
                    $error[] = $e->getMessage();
                }
            }
        }
        if (!empty($error)) {
            $conn->rollback();
            return $error;
        }

        $conn->commit();
        return $resultado;
    }

}
