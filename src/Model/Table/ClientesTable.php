<?php
namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Clientes Model
 *
 * @method \App\Model\Entity\Cliente get($primaryKey, $options = [])
 * @method \App\Model\Entity\Cliente newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Cliente[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Cliente|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cliente|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Cliente patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Cliente[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Cliente findOrCreate($search, callable $callback = null, $options = [])
 */
class ClientesTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('clientes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasOne('Endereco', [
            'foreignKey' => 'foreign_key',
            'joinType' => 'LEFT',
            'className' => 'Enderecos',
            'conditions' => [
                'Endereco.model' => 'Clientes',
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 50)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->scalar('tipo')
            ->requirePresence('tipo', 'create')
            ->notEmpty('tipo');

        $validator
            ->scalar('cpf_cnpj')
            ->maxLength('cpf_cnpj', 14)
            ->requirePresence('cpf_cnpj', 'create')
            ->notEmpty('cpf_cnpj');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->decimal('renda')
            ->allowEmpty('renda');

        $validator
            ->scalar('celular')
            ->maxLength('celular', 15)
            ->allowEmpty('celular');

        $validator
            ->scalar('telefone')
            ->maxLength('telefone', 15)
            ->allowEmpty('telefone');

        $validator
            ->dateTime('criado')
            ->allowEmpty('criado');

        $validator
            ->dateTime('modificado')
            ->allowEmpty('modificado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

    // In a table or behavior class
    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        if (isset($data['cpf_cnpj'])) {
            if (empty($data['cpf_cnpj'])) {
                $data['cpf_cnpj'] = !empty($data['cpf']) ? $data['cpf'] : $data['cnpj'];
            }
            $data['cpf_cnpj'] = soNumeros($data['cpf_cnpj']);
        }
        if (isset($data['renda'])) {
            $data['renda'] = valorToValue($data['renda']);
        }
        if (isset($data['celular'])) {
            $data['celular'] = somenteNumerosFone($data['celular']);
        }
        if (isset($data['telefone']) && !empty($data['telefone'])) {
            $data['telefone'] = somenteNumerosFone($data['telefone']);
        }
    }

    public function novo() {
        $entity = $this->newEntity();
        $entity->endereco = TableRegistry::getTableLocator()
            ->get('Enderecos')
            ->newEntity();

        return $entity;
    }

    /**
     * @param $id
     * @return array|\Cake\Datasource\EntityInterface|null
     */
    public function buscar($id) {
        $entity = $this
            ->find()
            ->contain([
                'Endereco'
            ])
            ->where([
                'Clientes.id' => $id
            ])
            ->first();

        return $entity;
    }

    /**
     * @param $dados
     * @param null $id
     * @return array
     */
    public function salvar($dados, $id = NULL) {
        $conn = ConnectionManager::get('default');
        $conn->begin();
        $entidade = $this->montar($dados, $id);

        try {
            $retorno = $this->save($entidade);
            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }
            if (isset($dados['endereco'])) {
                $dados['endereco']['foreign_key'] = $entidade->id;
                TableRegistry::getTableLocator()
                    ->get('Enderecos')
                    ->salvarEndereco($dados['endereco']);
            }

        } catch (\PDOException $e) {
            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }
        $conn->commit();
        return $this->retorno;
    }
}
