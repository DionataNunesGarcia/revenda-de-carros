<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;

/**
 * LogsAlteracoes Model
 *
 * @method \App\Model\Entity\LogsAlteraco get($primaryKey, $options = [])
 * @method \App\Model\Entity\LogsAlteraco newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\LogsAlteraco[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LogsAlteraco|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LogsAlteraco|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LogsAlteraco patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LogsAlteraco[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\LogsAlteraco findOrCreate($search, callable $callback = null, $options = [])
 */
class LogsAlteracoesTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('logs_alteracoes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('data_hora')
            ->requirePresence('data_hora', 'create')
            ->notEmpty('data_hora');

        $validator
            ->scalar('tabela')
            ->maxLength('tabela', 45)
            ->requirePresence('tabela', 'create')
            ->notEmpty('tabela');

        $validator
            ->scalar('usuario')
            ->maxLength('usuario', 45)
            ->requirePresence('usuario', 'create')
            ->notEmpty('usuario');

        $validator
            ->scalar('acao')
            ->requirePresence('acao', 'create')
            ->notEmpty('acao');

        $validator
            ->scalar('valor_novo')
            ->maxLength('valor_novo', 4294967295)
            ->allowEmpty('valor_novo');

        $validator
            ->scalar('valor_antigo')
            ->maxLength('valor_antigo', 4294967295)
            ->allowEmpty('valor_antigo');

        return $validator;
    }

    public function novo() {
        return $this->newEntity();
    }

    public function buscar($id) {
        return $this->get($id);
    }
    
    public function gerarLogs($dados) {
        
        $entidade = $this->montar($dados);
        
        $entidade->data_hora = Time::now();
        
        $this->save($entidade);
        
    }
}
