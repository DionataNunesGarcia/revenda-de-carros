<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\I18n\Time;

/**
 * LogsAcesso Model
 *
 * @method \App\Model\Entity\LogsAcesso get($primaryKey, $options = [])
 * @method \App\Model\Entity\LogsAcesso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\LogsAcesso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LogsAcesso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LogsAcesso|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LogsAcesso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LogsAcesso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\LogsAcesso findOrCreate($search, callable $callback = null, $options = [])
 */
class LogsAcessoTable extends AppTable {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('logs_acesso');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->dateTime('data_hora')
                ->requirePresence('data_hora', 'create')
                ->notEmpty('data_hora');

        $validator
                ->scalar('ip')
                ->maxLength('ip', 45)
                ->requirePresence('ip', 'create')
                ->notEmpty('ip');

        $validator
                ->scalar('usuario')
                ->maxLength('usuario', 45)
                ->requirePresence('usuario', 'create')
                ->notEmpty('usuario');

        return $validator;
    }

    public function novo() {
        return $this->newEntity();
    }

    public function buscar($id) {
        return $this->get($id);
    }

    public function gerarLog($ip, $usuario) {

        $entidade = $this->novo();
        
        $entidade->ip = $ip;
        $entidade->usuario = $usuario;
        $entidade->data_hora = Time::now();
        
        $this->save($entidade);
    }

}
