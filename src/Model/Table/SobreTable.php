<?php
namespace App\Model\Table;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Sobre Model
 *
 * @method \App\Model\Entity\Sobre get($primaryKey, $options = [])
 * @method \App\Model\Entity\Sobre newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Sobre[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Sobre|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sobre|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Sobre patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Sobre[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Sobre findOrCreate($search, callable $callback = null, $options = [])
 */
class SobreTable extends AppTable
{
    private $enderecoTable;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sobre');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasOne('Endereco', [
            'foreignKey' => 'foreign_key',
            'joinType' => 'LEFT',
            'className' => 'Enderecos',
            'conditions' => [
                'Endereco.model' => 'Sobre',
            ]
        ]);

        $this->hasMany('Arquivos', [
            'className' => 'Arquivos',
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'conditions' => [
                'Arquivos.model' => 'Sobre',
            ],
            'sort' => [
                'Arquivos.ordem' => 'ASC',
            ],
            'propertyName' => 'arquivos',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->enderecoTable = TableRegistry::getTableLocator()
            ->get('Enderecos');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('telefone')
            ->maxLength('telefone', 255)
            ->requirePresence('telefone', 'create')
            ->notEmpty('telefone');

        $validator
            ->scalar('celular')
            ->maxLength('celular', 255)
            ->requirePresence('celular', 'create')
            ->notEmpty('celular');

        $validator
            ->scalar('celular_2')
            ->maxLength('celular_2', 255)
            ->requirePresence('celular_2', 'create')
            ->notEmpty('celular_2');

        $validator
            ->scalar('facebook')
            ->maxLength('facebook', 255)
            ->requirePresence('facebook', 'create')
            ->notEmpty('facebook');

        $validator
            ->scalar('instagram')
            ->maxLength('instagram', 255)
            ->requirePresence('instagram', 'create')
            ->notEmpty('instagram');

        $validator
            ->scalar('linkedin')
            ->maxLength('linkedin', 255)
            ->requirePresence('linkedin', 'create')
            ->notEmpty('linkedin');


        return $validator;
    }

    // In a table or behavior class
    public function beforeMarshal(\Cake\Event\Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        if (isset($data['telefone'])) {
            $data['telefone'] = somenteNumerosFone($data['telefone']);
        }
        if (isset($data['celular'])) {
            $data['celular'] = somenteNumerosFone($data['celular']);
        }
        if (isset($data['celular_2'])) {
            $data['celular_2'] = somenteNumerosFone($data['celular_2']);
        }
        if (isset($data['sobre'])) {
            $data['sobre'] = utf8_encode($data['sobre']);
        }
        if (isset($data['missao'])) {
            $data['missao'] = utf8_encode($data['missao']);
        }
        if (isset($data['visao'])) {
            $data['visao'] = utf8_encode($data['visao']);
        }
        if (isset($data['valores'])) {
            $data['valores'] = utf8_encode($data['valores']);
        }
    }

    public function buscar($id = null) {
        $entidade = $this
            ->find()
            ->contain([
                'Endereco.Cidade',
                'Arquivos'
            ])
            ->first();
        if (!$entidade) {
            $entidade = $this->novo();
            $entidade->endereco = $this->enderecoTable->novo();
            return $entidade;
        }
        return $entidade;
    }

    public function salvar($dados, $id = NULL) {

        $conn = ConnectionManager::get('default');
        $conn->begin();

        $entidade = $this->montar($dados, $id);
        try {

            $retorno = $this->save($entidade);

            $this->retorno['entidade'] = $entidade;

            if (!$retorno) {
                $this->retorno['status'] = false;
                $this->retorno['mensagem'] = montaMensagemErros($entidade->getErrors());
                $conn->rollback();
                return $this->retorno;
            }
            if (isset($dados['endereco'])) {
                $dados['endereco']['foreign_key'] = $entidade->id;
                TableRegistry::getTableLocator()
                    ->get('Enderecos')
                    ->salvarEndereco($dados['endereco']);
            }
        } catch (\PDOException $e) {

            $this->retorno['status'] = false;
            $this->retorno['entidade'] = $entidade;
            $this->retorno['mensagem'] = $e->getMessage();
            $conn->rollback();
            return $this->retorno;
        }

        $conn->commit();

        return $this->retorno;
    }
}
