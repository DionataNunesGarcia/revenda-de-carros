<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Datasource\ConnectionManager;

/**
 * NiveisPermissoes Model
 *
 * @property \App\Model\Table\NiveisTable|\Cake\ORM\Association\BelongsTo $Niveis
 *
 * @method \App\Model\Entity\NiveisPermissao get($primaryKey, $options = [])
 * @method \App\Model\Entity\NiveisPermissao newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\NiveisPermisso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\NiveisPermisso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NiveisPermisso|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\NiveisPermissao patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\NiveisPermisso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\NiveisPermissao findOrCreate($search, callable $callback = null, $options = [])
 */
class NiveisPermissoesTable extends AppTable {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('niveis_permissoes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Niveis', [
            'foreignKey' => 'nivel_id',
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('controller')
            ->maxLength('controller', 255)
            ->requirePresence('controller', 'create')
            ->notEmpty('controller');

        $validator
            ->scalar('action')
            ->maxLength('action', 255)
            ->requirePresence('action', 'create')
            ->notEmpty('action');

        $validator
            ->scalar('prefix')
            ->maxLength('prefix', 255)
            ->allowEmpty('prefix');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['nivel_id'], 'Niveis'));

        return $rules;
    }

    public function novo() {
        return $this->newEntity();
    }

    public function buscar($id) {
        return $this->get($id);
    }

    public function salvarTodos($dados, $nivelId) {

        $this->excluirPermissoesNivel($nivelId);
        
        if(!empty($dados['action'])){            
            foreach ($dados['action'] as $controller => $actions) {

                foreach ($actions as $action) {

                    $entidade = $this->novo();

                    $entidade->controller = $controller;
                    $entidade->action = $action;
                    $entidade->prefix = $dados['prefix'];
                    $entidade->nivel_id = $nivelId;

                    $this->save($entidade);
                }
            }
        }
    }

    public function excluirPermissoesNivel($nivelId) {
        $this->query()->delete()
            ->where(['nivel_id' => $nivelId])
            ->execute();
    }

    
    
    
}
