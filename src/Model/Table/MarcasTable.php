<?php
namespace App\Model\Table;

use App\Enum\SituacaoEnum;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Marcas Model
 *
 * @property \App\Model\Table\VeiculosTable|\Cake\ORM\Association\HasMany $Veiculos
 *
 * @method \App\Model\Entity\Marca get($primaryKey, $options = [])
 * @method \App\Model\Entity\Marca newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Marca[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Marca|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Marca|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Marca patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Marca[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Marca findOrCreate($search, callable $callback = null, $options = [])
 */
class MarcasTable extends AppTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('marcas');
        $this->setDisplayField('nome');
        $this->setPrimaryKey('id');

        $this->hasMany('Modelos', [
            'foreignKey' => 'marca_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome')
            ->maxLength('nome', 255)
            ->requirePresence('nome', 'create')
            ->notEmpty('nome');

        $validator
            ->dateTime('criado')
            ->allowEmpty('criado');

        $validator
            ->dateTime('modificado')
            ->allowEmpty('modificado');

        return $validator;
    }

    public function getList()
    {
        return $this
            ->find('list')
            ->matching('Modelos.Veiculos', function(\Cake\ORM\Query $q) {
                return $q
                    ->where([
                        'Veiculos.destaque' => true,
                        'Veiculos.mostrar_site' => true,
                        'Veiculos.situacao' => SituacaoEnum::A_VENDA,
                    ]);
            })
            ->toArray();
    }
}
