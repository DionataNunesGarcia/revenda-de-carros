<?php


namespace App\Enum;


class TipoCombustivelEnum
{
    /*
     * Database entries boolean
     */
    const GASOLINA  = 'gasolina';
    const DIESEL = 'diesel';
    const GNV = 'gnv';
    const FLEX = 'flex';
    const ALCOOL = 'alcool';
    const ELETRICO = 'eletrico';
    const OUTRO = 'outro';

    const ARRAY_STR = [
        self::GASOLINA => 'Gasolina',
        self::DIESEL => 'Diesel',
        self::GNV => 'GNV (Gás Natural Veicular)',
        self::FLEX => 'Flex',
        self::ALCOOL => 'Alcool',
        self::ELETRICO => 'Elétrico',
        self::OUTRO => 'Outro',
    ];

    public static function getType($type) {
        if (is_bool($type)) {
            $type = $type ? 1 : 0;
        }

        return self::ARRAY_STR[$type];
    }

}
