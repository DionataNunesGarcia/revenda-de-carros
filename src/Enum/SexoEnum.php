<?php


namespace App\Enum;


class SexoEnum
{
    /*
     * Database entries boolean
     */
    const MASCULINO = 'm';
    const FEMININO = 'f';

    const ARRAY_STR = [
        self::MASCULINO => 'Masculino',
        self::FEMININO => 'Feminino',
    ];

    public static function getType($type)
    {
        return self::ARRAY_STR[$type];
    }

}
