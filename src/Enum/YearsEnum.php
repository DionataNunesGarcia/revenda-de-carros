<?php

namespace App\Enum;


use Cake\Collection\Collection;
use Cake\I18n\Time;

class YearsEnum
{
    public static function getList()
    {
        $maxYear = 1960;
        $year = date('Y');
        $years = [];
        while ($year >= $maxYear) {
            $years[$year] = intval($year);
            $year--;
        }
        return $years;
    }
}
