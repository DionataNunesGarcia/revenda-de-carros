<?php


namespace App\Enum;


class TipoPessoaEnum
{
    /*
     * Database entries boolean
     */
    const PESSOA_FISICA = 'pf';
    const PESSOA_JURIDICA = 'vendido';

    const ARRAY_STR = [
        self::PESSOA_FISICA => 'P. Física',
        self::PESSOA_JURIDICA => 'P. Jurídica',
    ];

    public static function getType($type)
    {
        return self::ARRAY_STR[$type];
    }

}
