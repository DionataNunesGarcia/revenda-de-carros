<?php

namespace App\Enum;


use Cake\I18n\Time;

class ContasReceberSituacaoEnum
{
    public static function getSituacao($dataVencimento, $dataValidade, $dataPagamento = null) {
        $hoje = Time::now()->i18nFormat('yyyy-MM-dd');
        if (!empty($dataPagamento)) {
            return self::getHtml('Pago', 'success');
        }

        if ($dataVencimento->i18nFormat('yyyy-MM-dd') > $hoje) {
            return self::getHtml('À vencer', 'primary');
        }

        if ($dataVencimento->i18nFormat('yyyy-MM-dd') == $hoje) {
            return self::getHtml('Vence hoje', 'primary');
        }

        if ($dataVencimento->i18nFormat('yyyy-MM-dd') < $hoje && $dataValidade == $hoje) {
            return self::getHtml('Vence hoje | Atrasada', 'primary');
        }

        if ($dataVencimento->i18nFormat('yyyy-MM-dd') < $hoje && $dataValidade > $hoje) {
            return self::getHtml('À vencer | Atrasada', 'primary');
        }

        if ($dataVencimento->i18nFormat('yyyy-MM-dd') < $hoje && $dataValidade < $hoje) {
            return self::getHtml('Atrasada', 'danger');
        }
        return self::getHtml('Sem Definição', 'danger');
    }

    private static function getHtml($situacao, $class)
    {
        return "<label class='label label-{$class}'>{$situacao}</label>";
    }
}
