<?php

namespace App\Enum;

class SimNaoEnum
{
    public static function getEmpty() {
        return [
            '' => ''
        ];
    }

    public static function getArray() {
        return [
            1 => 'Sim',
            0 => 'Não',
        ];
    }

    /**
     * @param $type
     * @return mixed
     */
    public static function getType($type) {
        return self::getArray()[$type];
    }
}
