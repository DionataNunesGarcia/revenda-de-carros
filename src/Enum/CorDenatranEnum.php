<?php


namespace App\Enum;


class CorDenatranEnum
{

    /*
     * Database entries boolean
     */
    const AMARELO = '01-AMARELO';
    const AZUL = '02-AZUL';
    const BEGE = '03-BEGE';
    const BRANCA = '04-BRANCA';
    const CINZA = '05-CINZA';
    const DOURADA = '06-DOURADA';
    const GRENA = '07-GRENA';
    const LARANJA = '08-LARANJA';
    const MARROM = '09-MARROM';
    const PRATA = '10-PRATA';
    const PRETA = '11-PRETA';
    const ROSA = '12-ROSA';
    const ROXA = '13-ROXA';
    const VERDE = '14-VERDE';
    const VERMELHA = '15-VERMELHA';
    const FANTASIA = '16-FANTASIA';

    const ARRAY_STR = [
        self::AMARELO => '01-AMARELO',
        self::AZUL => '02-AZUL',
        self::BEGE => '03-BEGE',
        self::BRANCA => '04-BRANCA',
        self::CINZA => '05-CINZA',
        self::DOURADA => '06-DOURADA',
        self::GRENA => '07-GRENA',
        self::LARANJA => '08-LARANJA',
        self::MARROM => '09-MARROM',
        self::PRATA => '10-PRATA',
        self::PRETA => '11-PRETA',
        self::ROSA => '12-ROSA',
        self::ROXA => '13-ROXA',
        self::VERDE => '14-VERDE',
        self::VERMELHA => '15-VERMELHA',
        self::FANTASIA => '16-FANTASIA',
    ];

    public static function getType($type) {
        if (is_bool($type)) {
            $type = $type ? 1 : 0;
        }

        return self::ARRAY_STR[$type];
    }

}
