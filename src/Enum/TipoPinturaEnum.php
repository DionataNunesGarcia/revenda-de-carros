<?php


namespace App\Enum;


class TipoPinturaEnum
{
    /*
     * Database entries boolean
     */
    const SOLIDA = 'solida';
    const METALICA = 'metalica';
    const PEROLIZADA = 'perolizada';
    const REPINTURA = 'repintura';

    const ARRAY_STR = [
        self::SOLIDA => 'Sólida',
        self::METALICA => 'Metálica',
        self::PEROLIZADA => 'Perolizada',
        self::REPINTURA => 'Repintura',
    ];

    public static function getType($type) {
        if (is_bool($type)) {
            $type = $type ? 1 : 0;
        }

        return self::ARRAY_STR[$type];
    }

}
