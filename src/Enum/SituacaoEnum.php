<?php


namespace App\Enum;


class SituacaoEnum
{
    /*
     * Database entries boolean
     */
    const A_VENDA = 'a venda';
    const VENDIDO = 'vendido';
    const AGUARDANDO_MANUTENCAO = 'aguardando manutencao';
    const MANUTENCAO = 'manutencao';
    const DEVOLVIDO = 'devolvido';

    const ARRAY_STR = [
        self::A_VENDA => 'À Venda',
        self::VENDIDO => 'Vendido',
        self::AGUARDANDO_MANUTENCAO => 'Aguardando Manutenção',
        self::MANUTENCAO => 'Manutenção',
        self::DEVOLVIDO => 'Devolvido',
    ];

    public static function getType($type) {
        if (is_bool($type)) {
            $type = $type ? 1 : 0;
        }

        return self::ARRAY_STR[$type];
    }

}
