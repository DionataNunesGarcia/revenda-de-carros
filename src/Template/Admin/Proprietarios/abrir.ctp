<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <div class="form-group col-md-4">
                <?= $this->Form->control('nome'); ?>
            </div>
            <div class="form-group col-md-4">
                <?= $this->Form->control('rg'); ?>
            </div>
            <div class="form-group col-md-4">
                <?= $this->Form->control('cpf', ['class' => 'cpf', 'maxlength' => 14]); ?>
            </div>
            <div class="form-group col-md-4">
                <?= $this->Form->control('celular', ['class' => 'fone']); ?>
            </div>
            <div class="form-group col-md-4">
                <?= $this->Form->control('telefone', ['class' => 'fone']); ?>
            </div>
        </fieldset>
    </div>
    <!--Formulario endereço-->
    <?= $this->element('admin/pessoas/endereco', ['entidade' => $entidade->endereco, 'model' => 'Proprietarios']) ?>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
