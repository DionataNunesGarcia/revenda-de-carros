
<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">

        <div class="col-md-4">
            <?=
            $this->Form->input('nome', ['class' => 'form-control', 'label' => false,
                'placeholder' => 'Pesquise por Nome',
                'autofocus' => true,
                'value' => $this->request->query('nome')]);
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $this->Form->input('email', ['class' => 'form-control', 'label' => false,
                'placeholder' => 'Pesquse por E-mail',
                'value' => $this->request->query('email')]);
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $this->Form->input('cpf', ['class' => 'form-control cpf', 'label' => false,
                'placeholder' => 'Pesquse por CPF',
                'value' => $this->request->query('cpf')]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">
        <?= $this->element('admin/pesquisar/botoes') ?>
        <div class="table-responsive no-padding">
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                    <tr>
                        <th><?= $this->Form->checkbox('select-all', ['hiddenField' => false]); ?></th>
                        <th><?= $this->Paginator->sort('nome') ?></th>
                        <th><?= $this->Paginator->sort('email','E-mail') ?></th>
                        <th><?= $this->Paginator->sort('cpf','CPF') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($entidade as $item) {
                        $disabled = '';
                        $img = 'user-default.png';
                        if (!empty($item->usuario->imagem) && file_exists(WWW_ROOT . $item->usuario->imagem)) {
                            $img = $item->usuario->imagem;
                        }
                        ?>
                        <tr>
                            <td><?= $this->Form->checkbox('id', ['hiddenField' => false, 'value' => $item->id, $disabled]); ?></td>
                            <td class="image">
                                <?= $this->Html->image($img, ['class' => 'img-circle']); ?>
                            </td>
                            <td><?= h($item->nome) ?></td>
                            <td><?= h($item->email) ?></td>
                            <td><?= cpf($item->cpf) ?></td>
                            <td class="actions">
                                <?= $this->element('admin/pesquisar/botoes-acoes', ['id' => $item->id, 'deletavel' => $item->deletavel]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>
