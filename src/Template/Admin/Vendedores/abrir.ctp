<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <!--Formulario pessoa fisica-->
            <div class="row">
                <div class="col-md-12 no-padding">
                    <div class="form-group col-md-3">
                        <?=
                        $this->Form->control('nome', [
                            'required' => true,
                            'label' => 'Nome',
                            'value' => $entidade->nome
                        ]);
                        ?>
                    </div>
                    <div class="form-group col-md-3">
                        <?=
                        $this->Form->control('email', [
                            'required' => true,
                            'label' => 'E-mail',
                            'value' => $entidade->email
                        ]);
                        ?>
                    </div>
                    <div class="form-group col-md-3">
                        <?=
                        $this->Form->input('cpf', [
                            'class' => 'form-control cpf',
                            'label' => 'CPF',
                            'required' => true,
                            'maxlength' => 100,
                            'type' => 'text',
                            'value' => cpf($entidade->cpf)
                        ]);
                        ?>
                    </div>
                    <div class="form-group col-md-3">
                        <?=
                        $this->Form->control('data_nascimento', [
                            'label' => 'Data Nascimento',
                            'class' => 'datepicker',
                            'required' => true,
                            'type' => 'text',
                            'value' => dateToData($entidade->data_nascimento)
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-12 no-padding">
                    <div class="form-group col-md-3">
                        <?=
                        $this->Form->control('celular', [
                            'value' => fone($entidade->celular),
                            'label' => 'Celular',
                            'class' => 'fone',
                            'type' => 'text',
                        ]);
                        ?>
                    </div>
                    <div class="form-group col-md-3">
                        <?=
                        $this->Form->control('telefone', [
                            'value' => fone($entidade->telefone),
                            'label' => 'Telefone',
                            'class' => 'fone',
                            'type' => 'text',
                        ]);
                        ?>
                    </div>
                    <div class="form-group col-md-3">
                        <?=
                        $this->Form->control('data_admissao', [
                            'required' => false,
                            'type' => 'text',
                            'label' => 'Data Admissão',
                            'class' => 'datepicker',
                            'value' => dateToData($entidade->data_admissao)
                        ]);
                        ?>
                    </div>
                    <div class="form-group col-md-3">
                        <?=
                        $this->Form->control('data_demissao', [
                            'required' => false,
                            'type' => 'text',
                            'label' => 'Data Demissão',
                            'class' => 'datepicker',
                            'value' => dateToData($entidade->data_demissao)
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-12 no-padding">
                    <div class="form-group col-md-3">
                        <label class=" no-padding" for="sexo">Sexo</label>
                        <?=
                        $this->Form->select('sexo', \App\Enum\SexoEnum::ARRAY_STR, [
                            'multiple' => false,
                            'hiddenField' => false,
                            'required' => true,
                        ]);
                        ?>
                    </div>
                    <div class="form-group col-md-3">
                        <?=
                        $this->Form->control('salario_fixo', [
                            'class' => 'nf2',
                            'type' => 'text',
                            'value' => valueToValor($entidade->salario_fixo)
                        ]);
                        ?>
                    </div>
                    <div class="form-group col-md-3">
                        <?=
                        $this->Form->control('comissao', [
                            'label' => 'Comissão %',
                            'required' => false,
                            'class' => 'nf2',
                            'type' => 'text',
                            'value' => valueToValor($entidade->comissao)
                        ]);
                        ?>
                    </div>
                    <div class="form-group col-md-3">
                        <label class=" no-padding" for="status">Situação</label>
                        <?=
                        $this->Form->select('status', \App\Enum\StatusEnum::ARRAY_VENDEDORES, [
                            'multiple' => false,
                            'hiddenField' => false,
                            'required' => true,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <!--Formulario endereço-->
    <?= $this->element('admin/pessoas/endereco', ['entidade' => $entidade->endereco, 'model' => 'Vendedores']) ?>

    <!--Formulario Usuário-->
    <?= $this->element('admin/titulo-box', ['titulo' => 'Usuário', 'collapse' => false]) ?>
    <?= $this->element('admin/pessoas/usuario', ['entidade' => $entidade->usuario]) ?>

    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
