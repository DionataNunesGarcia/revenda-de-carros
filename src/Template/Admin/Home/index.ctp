<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Olá</h3>
                <!-- /. tools -->
            </div>
            <div class="box-body">
                <h3 class="box-title">
                    Sejá bem vindo  <?= ucfirst($userSession['usuario']); ?>!
                </h3>
            </div>
        </div>
    </div>
</div>