<style>
    ul.menu li a[data-visualizado='0'] {
        background: #ccffff;
    }
</style>
<script>
    var total = "<?= h($retorno['total']) ?>";
    document.getElementById("total-mensagens").innerHTML = total;

    var title = $('body').data('title');
    $('html head title').html('(' + total + ') ' + title);

    if(parseInt(total) == 0) {
        $('html head title').html(title);
    }

</script>
<li class="header">
    <?= __('Você tem {total} mensagens', ['total' => $retorno['total']]) ?>
</li>
<li>
    <ul class="menu">
        <?php
        if (!empty($retorno['mensagens'])) {
            foreach ($retorno['mensagens'] as $mensagem) {
                $url = $this->Url->build(['prefix' => 'admin', 'controller' => 'Home', 'action' => 'mensagem', $mensagem->id], true);
                $visualizado = ($mensagem->visualizado) ? 1 : 0;
                ?>
                <li>
                    <a href="<?= $url; ?>" data-open-modal-id='visualizar_mensagem' data-visualizado='<?= $visualizado ?>'>
                        <div class="pull-left">
                            <?= $this->Html->image('user-default.png', ['class' => 'img-circle']); ?>
                        </div>
                        <h4>
                            <?= h($mensagem->titulo) ?>
                            <small>
                                <i class="fa fa-clock-o"></i>
                                <?= h($mensagem->criado) ?>
                            </small>
                        </h4>
                        <p>
                            <?= h($mensagem->assunto) ?>
                        </p>
                    </a>
                </li>
                <?php
            }
        }
        ?>
    </ul>
</li>
<li class="footer">
    <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Home', 'action' => 'pesquisarMensagens'], true); ?>" title="Ver todas as mensagens" data-placement="right">
        Ver todas mensagens
    </a>
</li>
