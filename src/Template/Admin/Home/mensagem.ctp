<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">
                <?= h($entidade->titulo) ?>
            </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-12">
                    <strong>Assunto:</strong>
                    <span class="help-block"><?= h($entidade->assunto) ?></span>
                </div>
                <div class="form-group col-md-12">
                    <strong>Mensagem:</strong>
                    <span class="string-to-json">
                        <?= $this->Text->autoParagraph($entidade->mensagem) ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Fechar</button>
        </div>
    </div>
</div>
<script>
    setup();
</script>