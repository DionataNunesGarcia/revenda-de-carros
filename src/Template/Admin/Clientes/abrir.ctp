<?php
use App\Enum\TipoPessoaEnum;
?>
<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <?= $this->Form->hidden('id') ?>
        <?= $this->Form->hidden('cpf_cnpj', ['id' => 'cpf-cnpj', 'value' => $entidade->cpf_cnpj]) ?>
        <div class="col-md-12 no-padding">
            <div class="form-group col-md-3">
                <?= $this->Form->control('nome'); ?>
            </div>
            <div class="form-group col-md-3">
                <?=
                $this->Form->input('email', [
                    'label' => 'E-mail',
                    'required' => true,
                    'type' => 'email',
                    'value' => $entidade->email
                ]);
                ?>
            </div>
            <div class="form-group col-md-3">
                <?=
                $this->Form->input('celular', [
                    'value' => fone($entidade->celular),
                    'label' => 'Celular',
                    'class' => 'fone',
                    'type' => 'text',
                ]);
                ?>
            </div>
            <div class="form-group col-md-3">
                <?=
                $this->Form->input('telefone', [
                    'value' => fone($entidade->telefone),
                    'label' => 'Telefone',
                    'class' => 'fone',
                    'type' => 'text',
                ]);
                ?>
            </div>
        </div>
        <div class="col-md-12 no-padding">
            <div class="form-group col-md-3 radio-button">
                <label class="no-padding" for="tipo">
                    Tipo Cliente
                </label>
                <div class="radio">
                    <?=
                    $this->Form->radio('tipo', TipoPessoaEnum::ARRAY_STR, [
                        'label' => 'Tipo Pessoa'
                    ]);
                    ?>
                </div>
            </div>
            <div class="form-group col-md-3" id="hide-cpf">
                <?=
                $this->Form->input('cpf', [
                    'class' => 'form-control cpf',
                    'label' => 'CPF',
                    'required' => true,
                    'value' => cpfCnpj($entidade->cpf_cnpj)
                ]);
                ?>
            </div>
            <div class="form-group col-md-3" id="hide-cnpj">
                <?=
                $this->Form->input('cnpj', [
                    'class' => 'form-control cnpj',
                    'label' => 'CNPJ',
                    'required' => true,
                    'value' => $entidade->cpf_cnpj
                ]);
                ?>
            </div>
            <div class="form-group col-md-3">
                <?=
                $this->Form->input('renda', [
                    'label' => 'Renda Mensal',
                    'class' => 'nf2',
                    'value' => valueToValor($entidade->renda),
                    'type' => 'text',
                ]);
                ?>
            </div>
        </div>
    </div>
    <?= $this->element('admin/pessoas/endereco', ['entidade' => $entidade->endereco, 'model' => 'Clientes']) ?>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
<script>
    $(document).ready(function(e) {
        $("body").on('change',"[name=tipo]", function () {
            validaTipoPessoa($(this).val())
        });
        validaTipoPessoa($('[name=tipo]:checked').val());

        $("body").on('change',"[name=cpf]", function () {
            if ($(this).val() !== '') {
                $('[name=cpf_cnpj').val($(this).val())
            }
        });
        $("body").on('change',"[name=cnpj]", function () {
            if ($(this).val() !== '') {
                $('[name=cpf_cnpj').val($(this).val())
            }
        });
    });

    function validaTipoPessoa(tipo) {
        if(tipo == 'pf') {
            $('#hide-cnpj').hide();
            $('#cnpj').attr('required', false).val('');
            $('#cpf').attr('required', true);
            $('#hide-cpf').fadeIn();
            return;
        }
        $('#hide-cpf').hide();
        $('#cpf').attr('required', false).val('');
        $('#cnpj').attr('required', true);
        $('#hide-cnpj').fadeIn();
        return;
    }
</script>
