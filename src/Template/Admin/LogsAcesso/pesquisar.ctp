<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">
        <div class="col-md-4">    
            <?=
            $this->Form->input('usuario', ['class' => 'form-control', 'label' => false,
                'placeholder' => 'Pesquise por Usuário',
                'autofocus' => true,
                'value' => $this->request->query('usuario')]);
            ?>
        </div>
        <div class="col-md-4">    
            <?=
            $this->Form->input('ip', ['class' => 'form-control', 'label' => false,
                'placeholder' => 'Pesquise por IP',
                'data-inputmask' => "'alias': 'ip'",
                'value' => $this->request->query('ip')]);
            ?>
        </div>
        <div class="col-md-4">    
            <?=
            $this->Form->input('data_hora', ['class' => 'form-control intervalo-tempo', 'label' => false,
                'placeholder' => 'Pesquise por Data Hora',
                'value' => $this->request->query('data_hora')]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">        
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">            
        <?= $this->element('admin/pesquisar/botoes') ?>
        <div class="table-responsive no-padding">
            <table cellpadding="0" cellspacing="0" class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Form->checkbox('select-all', ['hiddenField' => false]); ?></th>
                        <th scope="col"><?= $this->Paginator->sort('usuario') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('ip') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('data_hora') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($entidade as $item) { ?>
                        <tr>
                            <td><?= $this->Form->checkbox('id', ['hiddenField' => false, 'value' => $item->id]); ?></td>
                            <td><?= h($item->usuario) ?></td>  
                            <td><?= h($item->ip) ?></td>
                            <td><?= ($item->data_hora) ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>