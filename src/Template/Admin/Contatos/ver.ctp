<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">
                Contato - <?= h($entidade->nome) ?>
            </h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-4">
                    <strong>Nome</strong>
                    <span class="help-block">
                        <?= h($entidade->nome) ?>
                    </span>
                </div>
                <div class="form-group col-md-4">
                    <strong>E-mail</strong>
                    <span class="help-block">
                        <?= h($entidade->email) ?>
                    </span>
                </div>
                <div class="form-group col-md-4">
                    <strong>Telefone</strong>
                    <span class="help-block">
                        <?= fone($entidade->telefone) ?>
                    </span>
                </div>
                <div class="form-group col-md-4">
                    <strong>Criado</strong>
                    <span class="help-block">
                        <?= dateToDataTime($entidade->criado) ?>
                    </span>
                </div>
                <?php if ($entidade->veiculo) { ?>
                    <div class="form-group col-md-8">
                        <strong>Veículo</strong>
                        <span class="help-block">
                            <?= h($entidade->veiculo->placa . ' | ' . $entidade->veiculo->modelo->nome) ?>
                        </span>
                    </div>
                <?php } ?>
                <div class="form-group col-md-6">
                    <strong>Assunto</strong>
                    <span class="help-block">
                        <?= h($entidade->assunto) ?>
                    </span>
                </div>
                <div class="form-group col-md-12">
                    <strong>Mensagem</strong>
                    <br/>
                    <span class="">
                        <?= \Cake\Utility\Text::wordWrap($entidade->mensagem) ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Fechar</button>
        </div>
    </div>
</div>
<script>
    setup();
</script>
