<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">

        <div class="col-md-4">
            <?=
            $this->Form->input('nome', [
                'class' => 'form-control',
                'label' => 'Nome',
                'placeholder' => 'Pesquise por Nome',
                'autofocus' => true,
                'value' => $this->request->query('nome')
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $this->Form->input('email', [
                'class' => 'form-control',
                'label' => 'E-mail',
                'type' => 'text',
                'placeholder' => 'Pesquse por e-mail',
                'autofocus' => false,
                'value' => $this->request->query('email')
            ]);
            ?>
        </div>
        <div class="form-group col-md-4">
            <?=
            $this->element('admin/select2', [
                'controller' => 'Veiculos',
                'name' => 'veiculo_id',
                'label' => 'Veículo',
                'placeholder' => 'Pesquise pela Placa do Veículo',
                'multiple' => false,
                'required' => false,
                'value' => $this->request->query('veiculo_id')
            ])
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">
        <?= $this->element('admin/pesquisar/botoes') ?>
        <div class="table-responsive no-padding">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th><?= $this->Paginator->sort('nome') ?></th>
                        <th><?= $this->Paginator->sort('email', 'E-mail') ?></th>
                        <th><?= $this->Paginator->sort('assunto', 'Assunto') ?></th>
                        <th><?= h('Veículo') ?></th>
                        <th><?= $this->Paginator->sort('criado') ?></th>
                        <th class="actions"><?= __('Ações') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($entidade as $item): ?>
                        <tr>
                            <td><?= h($item->nome) ?></td>
                            <td><?= h($item->email) ?></td>
                            <td><?= h($item->assunto) ?></td>
                            <td><?= $item->veiculo ? h($item->veiculo->placa . ' | ' . $item->veiculo->modelo->nome) : '' ?></td>
                            <td><?= dateToDataTime($item->criado) ?></td>
                            <td class="actions">
                                <?=
                                $this->Html->link(__('<i class="fa fa-eye"></i>'), [
                                        'action' => 'ver',
                                        $item->id,
                                        '_full' => true
                                    ], [
                                        'class' => 'btn btn-primary btn-xs',
                                        'escape' => false,
                                        'data-open-modal-id' => 'visualizar_contato'
                                    ]);
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>
<div class="modal fade" id="visualizar_contato" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true"></div>
