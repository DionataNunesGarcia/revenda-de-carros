<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Log Alteração</h4>
        </div>
        <div class="modal-body">
            <fieldset>

                <div class="form-group col-md-4">
                    <strong>Usuário</strong>
                    <span class="help-block"><?= h($entidade->usuario->usuario) ?></span>
                </div>
                <div class="form-group col-md-4">
                    <strong>Título</strong>
                    <span class="help-block"><?= h($entidade->titulo) ?></span>
                </div>
                <div class="form-group col-md-4">
                    <strong>Tipo</strong>
                    <span class="help-block"><?= h($entidade->tipo) ?></span>
                </div>
                <div class="form-group col-md-4">
                    <strong>Criado</strong>
                    <span class="help-block"><?= dateToDataTime($entidade->criado) ?></span>
                </div>
                <div class="form-group col-md-4">
                    <strong>Modificado</strong>
                    <span class="help-block"><?= dateToDataTime($entidade->modificado) ?></span>
                </div>
                <div class="form-group col-md-4">
                    <strong>Tentativas de Importação</strong>
                    <span class="help-block"><?= h($entidade->tentativa_importacao) ?></span>
                </div>
                <div class="form-group col-md-4">
                    <strong>Importando</strong>
                    <span class="help-block"><?= boolSN($entidade->importando) ?></span>
                </div>
                <div class="form-group col-md-4">
                    <strong>Importado</strong>
                    <span class="help-block"><?= boolSN($entidade->importado) ?></span>
                </div>
                <div class="form-group col-md-4">
                    <strong>
                        Início:
                        <?= dateToDataTime($entidade->inicio_importacao) ?>
                        <br/>
                        Fim: <?= dateToDataTime($entidade->final_importacao) ?>
                    </strong>
                </div>
                <div class="form-group col-md-12">
                    <strong>
                        Formato: <?= strtoupper(extensao($entidade->arquivo)) ?>
                    </strong> 
                    <br/>
                    <?php if(file_exists(WWW_ROOT.$entidade->arquivo)){ ?>
                        <i class="fa fa-4x <?= h(getIcon(extensao($entidade->arquivo))) ?>"></i>
                        <a href="<?= $this->Url->build([
                            'prefix' => 'admin', 
                            'controller' => 'Arquivos', 
                            'action' => 'download',
                            $entidade->id
                        ]); ?>"
                        class="btn btn-success btn-xs" data-auth="false" style="margin-left: 15px">
                            <i class="fa fa-download"></i>
                        </a>                        
                    <?php } else { ?>
                        <strong class="text-danger">
                            ARQUIVO NÃO ENCONTRADO                            
                        </strong>
                    <?php } ?>
                </div>
                <div class="col-md-12 no-padding">
                    <div class="form-group col-md-12">
                        <strong>Observações</strong>
                        <br/>
                        <p>
                            <?= html_entity_decode($entidade->observacoes) ?>
                        </p>
                    </div>
                    <div class="form-group col-md-12">
                        <strong>Observações Erro</strong>
                        <br/>
                        <p>
                            <?= html_entity_decode($entidade->observacoes_erro) ?>
                        </p>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Fechar</button>
        </div>
    </div>
</div>
<script>
    setup();
</script>