
<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">
        <div class="col-md-3">
            <?=
            $this->Form->control('usuario', [
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Pesquise por Usuário',
                'autofocus' => true,
                'value' => $this->request->query('usuario')]);
            ?>
        </div>
        <div class="col-md-3">
            <?=
            $this->Form->control('tipo', [
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Pesquise por Tipo',
                'autofocus' => true,
                'value' => $this->request->query('tipo')]);
            ?>
        </div>
        <div class="col-md-6">
            <?=
            $this->Form->control('datas', [
                'class' => 'intervalo-tempo',
                'label' => false,
                'placeholder' => 'Pesquise por Mês Ano',
                'value' => $this->request->getQuery('datas')
            ]);
            ?>
            <?=
            $this->Form->hidden('datas_load', [
                'class' => 'intervalo-tempo-load',
                'value' => $this->request->getQuery('datas'),
                'disabled' => true
            ]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">
        <div class="botoes">
            <?=
            $this->Html->link(__('<i class="fa fa-upload"></i> Importar/Atualizar'), ['action' => 'importarAtualizar', '_full' => true], [
                'class' => 'btn btn-primary',
                'escape' => false
            ])
            ?>
        </div>
        <div class="table-responsive no-padding">
            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                    <tr>
                        <th><?= $this->Paginator->sort('usuario') ?></th>
                        <th><?= $this->Paginator->sort('tipo') ?></th>
                        <th><?= $this->Paginator->sort('criado') ?></th>
                        <th class="text-center"><?= $this->Paginator->sort('importando') ?></th>
                        <th class="text-center"><?= $this->Paginator->sort('importado') ?></th>
                        <th class="actions"><?= __('Ações') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($entidade as $item) { ?>
                        <tr>
                            <td>
                                <?= h(@$item->usuario->usuario) ?>
                            </td>
                            <td>
                                <?= h($item->tipo) ?>
                                <br/>
                                <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => $item->tipo, 'action' => 'pesquisar'], true); ?>" title="Pesquisar <?= h($item->tipo) ?>" data-placement="right" target="_blank">
                                    <?= __('Listagem') ?>
                                </a>
                            </td>
                            <td>
                                <?= dateToDataTime($item->criado) ?>
                            </td>
                            <td class="text-center">
                                <?= boolSN($item->importando) ?>
                            </td>
                            <td class="text-center">
                                <?= boolSN($item->importado) ?><br/>
                                <?=
                                !empty($item->inicio_importacao) ? 'Inicio: '
                                        . dateToDataTime($item->inicio_importacao) : ''
                                ?>
                                <br/>
                                <?=
                                !empty($item->final_importacao) ? 'Fim: '
                                        . dateToDataTime($item->final_importacao) : ''
                                ?>
                            </td>
                            <td class="actions">
                                <?=
                                $this->Html->link(__('<i class="fa fa-eye"></i>'), [
                                    'action' => 'ver', $item->id, '_full' => true
                                        ], [
                                    'class' => 'btn btn-primary btn-xs',
                                    'escape' => false,
                                    'data-open-modal-id' => 'visualizar'
                                ]);
                                ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>
<div class="modal fade" id="visualizar" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true"></div>
