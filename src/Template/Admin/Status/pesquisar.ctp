
<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">

        <div class="col-md-4">    
            <?=
            $this->Form->input('status', [
                'class' => 'form-control',
                'label' => false,
                'placeholder' => 'Pesquise por status',
                'autofocus' => true,
                'value' => $this->request->getQuery('status')]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">        
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">            
        <?= $this->element('admin/pesquisar/botoes') ?>
        <div class="table-responsive no-padding">
            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                    <tr>
                        <th><?= $this->Form->checkbox('select-all', ['hiddenField' => false]); ?></th>
                        <th><?= $this->Paginator->sort('status') ?></th>
                        <th><?= $this->Paginator->sort('cliente') ?></th>
                        <th class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($entidade as $item) {
                        ?>
                        <tr>
                            <td><?= $this->Form->checkbox('id', ['hiddenField' => false, 'value' => $item->id]); ?></td>
                            <td><?= h($item->status) ?></td>  
                            <td>
                                <?= h($item->cliente->pessoasJuridica->razao_social) ?>
                                (<?= h($item->cliente->pessoasJuridica->nome_fantasia) ?>)
                            </td>
                            <td class="actions">
                                <?= $this->element('admin/pesquisar/botoes-acoes', ['id' => $item->id]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>