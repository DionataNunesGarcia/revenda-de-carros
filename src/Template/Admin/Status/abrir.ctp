<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <div class="form-group col-md-4">
                <?= $this->Form->control('status'); ?>
            </div>
            <div class="form-group col-md-8">  
                <?=
                $this->element('admin/select2', [
                    'controller' => 'Clientes',
                    'name' => 'clientes_id',
                    'label' => 'Clientes',
                    'placeholder' => 'Pesquise por (Razão Social, Nome Fantasia, CNPJ/CPF, N°, Região...)',
                    'multiple' => false,
                    'required' => true,
                    'value' => $entidade->clientes_id,
                ])
                ?>
            </div>
            <div class="form-group col-md-12">
                <?= $this->Form->control('observacoes', ['type' => 'textarea', 'class' => 'ckeditor']); ?>
            </div>
        </fieldset>
    </div>
    <div class="box-footer">                    
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>