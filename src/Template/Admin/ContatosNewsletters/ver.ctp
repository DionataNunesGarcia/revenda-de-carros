<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Log Alteração</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <?php // debug($entidade) ?>
                <div class="form-group col-md-3">
                    <strong>Nome</strong>
                    <span class="help-block"><?= h($entidade->nome) ?></span>
                </div>
                <div class="form-group col-md-3">
                    <strong>Email</strong>
                    <span class="help-block"><?= h($entidade->email) ?></span>
                </div>
                <div class="form-group col-md-3">
                    <strong>Telefone</strong>
                    <span class="help-block"><?= fone($entidade->fone) ?></span>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Fechar</button>
        </div>
    </div>
</div>
<script>
    setup();
</script>
