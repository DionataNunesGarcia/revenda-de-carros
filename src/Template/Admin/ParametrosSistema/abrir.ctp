<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Dados Sistema', 'collapse' => false]) ?>
    <div class="box-body">
        <?= $this->Form->hidden('id') ?>
        <div class="row">
            <div class="form-group col-md-6">
                <?= $this->Form->control('razao_social'); ?>
            </div>
            <div class="form-group col-md-6">
                <?= $this->Form->control('nome_fantasia'); ?>
            </div>
            <div class="form-group col-md-6">
                <?= $this->Form->control('cnpj_cpf', ['value' => cpfCnpj($entidade->cnpj_cpf), 'label' => 'CNPJ/CPF']); ?>
            </div>
            <div class="form-group col-md-6">
                <?=
                $this->element('admin/selectTags', [
                    'nome' => 'emails',
                    'label' => 'E-mails Sistemas',
                    'value' => $entidade->emails
                ]);
                ?>
            </div>
        </div>
    </div>
    <?= $this->element('admin/titulo-box', ['titulo' => 'Configurações Gerais', 'collapse' => false]) ?>
    <div class="box-body">
        <div class="row">
            <div class="form-group col-md-4 radio-booton">
                <label class="no-padding" for="gerar_log">Gerar Logs</label>
                <div class="radio">
                    <?= $this->Form->radio('gerar_log', ['1' => 'Sim', '0' => 'Não']); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <?= $this->Form->button('<i class="fa fa-save"></i> Salvar', ['class' => 'btn btn-primary', 'escape' => false]); ?>
    </div>
</div>
<?= $this->Form->end() ?>
