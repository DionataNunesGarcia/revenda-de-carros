<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']); ?>
<div class="box">
    <div class="box-body">
        <?= $this->Form->input('id', ['type' => 'hidden']) ?>
        <div class="form-group col-md-4">
            <?= $this->Form->input('nome') ?>
        </div>
        <div class="form-group col-md-4">
            <?= $this->Form->input('email', ['type' => 'email']) ?>
        </div>
        <div class="form-group col-md-4">
            <?= $this->Form->input('telefone', ['class' => 'fone', 'type' => 'text']) ?>
        </div>
        <div class="form-group col-md-4">
            <?= $this->Form->input('celular', ['class' => 'fone', 'type' => 'text', 'label' => 'Celular/Whatsapp']) ?>
        </div>
        <div class="form-group col-md-4">
            <?= $this->Form->input('celular_2', ['class' => 'fone', 'type' => 'text']) ?>
        </div>
        <div class="form-group col-md-4">
            <?= $this->Form->input('facebook') ?>
        </div>
        <div class="form-group col-md-4">
            <?= $this->Form->input('linkedin') ?>
        </div>
        <div class="form-group col-md-4">
            <?= $this->Form->input('instagram') ?>
        </div>
    </div>
    <?= $this->element('admin/pessoas/endereco', ['entidade' => $entidade->endereco, 'model' => 'Sobre']) ?>

    <?= $this->element('admin/titulo-box', ['titulo' => 'Textos']) ?>
    <div class="box-body">
        <div class="form-group col-md-12">
            <?= $this->Form->input('sobre', ['class' => 'ckeditor']) ?>
        </div>
        <div class="form-group col-md-12">
            <?= $this->Form->input('visao', ['class' => 'ckeditor', 'text' => 'Visão']) ?>
        </div>
        <div class="form-group col-md-12">
            <?= $this->Form->input('missao', ['class' => 'ckeditor', 'text' => 'Missão']) ?>
        </div>
        <div class="form-group col-md-12">
            <?= $this->Form->input('valores', ['text' => 'Valores', 'class' => 'ckeditor', 'type' => 'textarea']) ?>
        </div>
    </div>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
<?php if ($entidade->id) { ?>
    <div class="box">
        <?= $this->element('admin/titulo-box', ['titulo' => 'Galeria de Imagens']) ?>
        <div class="box-body">
            <?=
            $this->element('admin/multi-upload', [
                'foreignKey' => $entidade->id,
                'model' => 'Sobre',
                'accept' => 'image/*',
            ]);
            ?>
        </div>
    </div>
<?php } ?>
