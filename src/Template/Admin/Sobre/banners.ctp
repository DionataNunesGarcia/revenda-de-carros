<?php if ($entidade->id) { ?>
    <div class="box">
        <?= $this->element('admin/titulo-box', ['titulo' => 'Banners']) ?>
        <div class="box-body">
            <?=
            $this->element('admin/multi-upload', [
                'foreignKey' => $entidade->id,
                'model' => 'Banners',
                'accept' => 'image/*',
                'link' => true,
                'tamanhoImagens' => '1349 × 646 px',
            ]);
            ?>
        </div>
    </div>
<?php } ?>
