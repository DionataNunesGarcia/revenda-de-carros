<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">

        <div class="col-md-6">
            <?=
            $this->Form->input('usuario', ['class' => 'form-control', 'label' => false,
                'placeholder' => 'Pesquise por usuário',
                'autofocus' => true,
                'value' => $this->request->getQuery('usuario')]);
            ?>
        </div>
        <div class="col-md-6">
            <?=
            $this->Form->input('email', ['class' => 'form-control', 'label' => false,
                'placeholder' => 'Pesquse por e-mail',
                'autofocus' => true,
                'value' => $this->request->getQuery('email')]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">
        <?= $this->element('admin/pesquisar/botoes') ?>
        <div class="table-responsive no-padding">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col"></th>
                        <th scope="col"><?= $this->Form->checkbox('select-all', ['hiddenField' => false]); ?></th>
                        <th scope="col"><?= $this->Paginator->sort('usuario', 'Usuário') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Nivel.nome', 'Nível') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('ativo') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('created', 'Criado') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('modified', 'Modificado') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($entidade as $item):
                        $img = 'user-default.png';
                        if (!empty($item->imagem) && file_exists(WWW_ROOT . $item->imagem)) {
                            $img = $item->imagem;
                        }
                        ?>
                        <tr>
                            <td>
                                <?= $this->Form->checkbox('id', ['hiddenField' => false, 'value' => $item->id]); ?>
                            </td>
                            <td class="image">
                                <?= $this->Html->image($img, ['class' => 'img-circle']); ?>
                            </td>
                            <td><?= h($item->usuario) ?></td>
                            <td><?= h($item->niveis->nome) ?></td>
                            <td><?= boolSN($item->ativo) ?></td>
                            <td><?= dateToDataTime($item->criado) ?></td>
                            <td><?= dateToDataTime($item->modificado) ?></td>
                            <td class="actions">
                                <?= $this->element('admin/pesquisar/botoes-acoes', ['id' => $item->id]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>
