<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Log Alteração</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <?php // debug($entidade) ?>
                <div class="form-group col-md-3">
                    <strong>Usuário</strong>
                    <span class="help-block"><?= h($entidade->usuario) ?></span>
                </div>
                <div class="form-group col-md-3">
                    <strong>Data/Hora</strong>
                    <span class="help-block"><?= dateToDataTime($entidade->data_hora) ?></span>
                </div>
                <div class="form-group col-md-3">
                    <strong>Tabela</strong>
                    <span class="help-block"><?= h($entidade->tabela) ?></span>
                </div>
                <div class="form-group col-md-3">
                    <strong>Ação</strong>
                    <span class="help-block"><?= h($entidade->acao) ?></span>
                </div>
                <div class="form-group col-md-6">
                    <strong>Valores Novos</strong>
                    <span class="string-to-json">
                        <?= h($entidade->valor_novo) ?>
                    </span>
                </div>
                <div class="form-group col-md-6">
                    <strong>Valores Antigos</strong>
                    <span class="string-to-json">
                        <?= h($entidade->valor_antigo) ?>
                    </span>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Fechar</button>
        </div>
    </div>
</div>
<script>
    setup();
</script>