<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">

        <div class="col-md-6">    
            <?=
            $this->Form->input('nome', ['class' => 'form-control', 'label' => false,
                'placeholder' => 'Pesquise por Nome',
                'autofocus' => true,
                'value' => $this->request->query('nome')]);
            ?>
        </div>
        <div class="col-md-6">  
            <?=
            $this->Form->input('email', ['class' => 'form-control', 'label' => false,
                'placeholder' => 'Pesquse por e-mail',
                'autofocus' => true,
                'value' => $this->request->query('email')]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">        
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">            
        <?= $this->element('admin/pesquisar/botoes') ?>
        <div class="table-responsive no-padding">
            <table cellpadding="0" cellspacing="0" class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('tabela') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('usuario', 'Usuário') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('data_hora') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('acao') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($entidade as $item): ?>
                        <tr>
                            <td><?= ucfirst($item->tabela) ?></td>  
                            <td><?= h($item->usuario) ?></td>
                            <td><?= dateToDataTime($item->data_hora) ?></td>
                            <td><?= h($item->acao) ?></td>
                            <td class="actions">
                                <?=
                                $this->Html->link(__('<i class="fa fa-eye"></i>'), [
                                    'action' => 'ver', $item->id, '_full' => true
                                    ], [
                                    'class' => 'btn btn-primary btn-xs',
                                    'escape' => false,
                                    'data-open-modal-id' => 'visualizar_log'
                                ]);
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>
<div class="modal fade" id="visualizar_log" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true"></div>