<?php
use App\Enum\SimNaoEnum;
?>
<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('placa', [
                        'required' => true,
                        'class' => 'placa'
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'Cidades',
                        'name' => 'cidade_id',
                        'label' => 'Cidades',
                        'multiple' => false,
                        'required' => true,
                        'value' => $entidade->cidade_id,
                    ])
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'tipoVeiculos',
                        'name' => 'tipo_veiculo_id',
                        'label' => 'Tipo de Veículo',
                        'multiple' => false,
                        'required' => true,
                        'value' => $entidade->tipo_veiculo_id,
                    ])
                    ?>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-4">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'cores',
                        'name' => 'cor_id',
                        'label' => 'Cor',
                        'multiple' => false,
                        'required' => true,
                        'value' => $entidade->cor_id,
                    ])
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('ano', [
                        'type' => 'text',
                        'class' => 'ano',
                        'label' => 'Ano Fabricação',
                        'required' => true
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'modelos',
                        'name' => 'modelo_id',
                        'label' => 'Modelo',
                        'multiple' => false,
                        'required' => true,
                        'value' => $entidade->modelo_id,
                    ])
                    ?>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-12">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'Opcionais',
                        'name' => 'opcionais.ids',
                        'label' => 'Opcionais e acessórios',
                        'multiple' => true,
                        'required' => true,
                        'value' => $entidade->opcionais_ids,
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <label for="importado">Importado</label>
                    <?= $this->Form->select('importado', SimNaoEnum::getArray(), ['label' => 'Importado']); ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'Proprietarios',
                        'name' => 'proprietario_id',
                        'label' => 'Proprietários',
                        'multiple' => false,
                        'required' => true,
                        'value' => $entidade->proprietario_id,
                    ])
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('quilometragem', [
                        'type' => 'text',
                        'class' => 'integer',
                        'label' => 'KM rodados',
                        'required' => true,
                        'value' => ($entidade->valor_compra)
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('valor', [
                        'type' => 'text',
                        'class' => 'nf2Cifrado',
                        'required' => true,
                        'label' => 'Valor de Venda',
                        'value' => valueToValor($entidade->valor)
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('valor_promocao', [
                        'type' => 'text',
                        'class' => 'nf2Cifrado',
                        'required' => false,
                        'label' => 'Valor Promoção',
                        'value' => valueToValor($entidade->valor_promocao)
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('valor_compra', [
                        'type' => 'text',
                        'class' => 'nf2Cifrado',
                        'required' => true,
                        'value' => valueToValor($entidade->valor_compra)
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('renavam', [
                        'required' => true
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('valor_reforma', [
                        'type' => 'text',
                        'class' => 'nf2Cifrado',
                        'required' => true,
                        'value' => valueToValor($entidade->valor_reforma)
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('valor_fipe', [
                        'type' => 'text',
                        'class' => 'nf2Cifrado',
                        'required' => true,
                        'value' => valueToValor($entidade->valor_fipe)
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('numero_portas', [
                        'type' => 'text',
                        'class' => 'integer',
                        'label' => 'Nº de Portas',
                        'required' => true,
                        'value' => $entidade->numero_portas
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <label for="situacao">
                        Tipo de Combustível
                    </label>
                    <?=
                    $this->Form->select('combustivel', \App\Enum\TipoCombustivelEnum::ARRAY_STR, [
                        'required' => true
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('chassi', [
                        'required' => true
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('data_entrada', [
                        'type' => 'text',
                        'class' => 'datepicker',
                        'required' => true,
                        'value' => dateToData($entidade->data_entrada)
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('data_venda', [
                        'type' => 'text',
                        'class' => 'datepicker',
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('valor_venda', [
                        'type' => 'text',
                        'class' => 'nf2Cifrado',
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <label for="situacao">
                        Situação
                    </label>
                    <?=
                    $this->Form->select('situacao', \App\Enum\SituacaoEnum::ARRAY_STR, [
                        'label' => 'Situação'
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('serie', [
                        'type' => 'text',
                        'required' => true,
                        'label' => 'Série',
                        'value' => $entidade->serie
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('meses_garantia', [
                        'type' => 'text',
                        'class' => 'integer',
                        'required' => true,
                        'value' => $entidade->meses_garantia
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('numero_motor', [
                        'type' => 'text',
                        'class' => 'integer',
                        'label' => 'N° Motor',
                        'required' => true,
                        'value' => $entidade->numero_motor
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <label for="rastreador-bloqueador">Rastreador e/ou Bloqueador</label>
                    <?= $this->Form->select('rastreador_bloqueador', SimNaoEnum::getArray()); ?>
                </div>
                <div class="form-group col-md-4">
                    <label for="acessorios-originais">Acessórios Originais</label>
                    <?= $this->Form->select('acessorios_originais', SimNaoEnum::getArray()); ?>
                </div>
                <div class="form-group col-md-4">
                    <label for="mostrar-site">Mostrar no site</label>
                    <?= $this->Form->select('mostrar_site', SimNaoEnum::getArray()); ?>
                </div>
                <div class="form-group col-md-4">
                    <label for="destaque">Destaque</label>
                    <?= $this->Form->select('destaque', SimNaoEnum::getArray()); ?>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-12">
                    <?=
                    $this->Form->control('observacoes', [
                        'label' => 'Observações internas',
                        'class' => 'form-control',
                        'type' => 'textArea',
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-12">
                    <?=
                    $this->Form->control('descricao', [
                        'type' => 'textarea',
                        'class' => 'ckeditor'
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-4">
                    <?php
                    if (empty($entidade->imagem)) {
                        echo "<label for='imagem-upload'>Imagem Principal</label>";
                        echo $this->Form->file('imagem_upload', [
                            'class' => 'upload_crop',
                            'multiple' => false,
                            'accept' => 'image/*',
                            'label' => 'Imagem',
                            'data-width' => 500,
                            'data-height' => 350,
                        ]);
                    } else {
                        ?>
                        <div class="col-md-8 no-padding">
                            <strong>Imagem Principal</strong><br/>
                            <?= $this->Html->image('../' . $entidade->imagem, ['class' => 'img-responsive img-usuario img-thumbnail']); ?>
                        </div>
                        <div class="col-md-4 text-rigth">
                            <?=
                            $this->Html->link("<i class='fa fa-trash'></i> Excluir", ['action' => 'excluir_imagem', $entidade->id, '_full' => true], [
                                "alt" => "Imagem",
                                'escape' => false,
                                'confirm' => __('Tem certeza de que deseja excluir o arquivo?'),
                                'class' => 'btn btn-xs btn-danger tex'
                            ]);
                            ?>
                        </div>
                    <?php } ?>
                    <div id="uploaded_image"></div>
                </div>
                <div class="form-group col-md-4">
                    <label for="disponivel-locacao">Disponível para Locação</label>
                    <?= $this->Form->select('disponivel_locacao', SimNaoEnum::getArray()); ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('valor_diaria_aluguel', [
                        'type' => 'text',
                        'class' => 'nf2Cifrado',
                        'required' => true,
                        'label' => 'Valor Diário de Locação',
                        'value' => valueToValor($entidade->valor_diaria_aluguel)
                    ]);
                    ?>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
<?= $this->element('admin/imagem-crop-modal') ?>
<?php if ($entidade->id) { ?>
    <div class="box">
        <?= $this->element('admin/titulo-box', ['titulo' => 'Galeria de Imagens']) ?>
        <div class="box-body">
            <?=
            $this->element('admin/multi-upload', [
                'foreignKey' => $entidade->id,
                'model' => 'Veiculos',
                'accept' => 'image/*',
            ]);
            ?>
        </div>
    </div>
<?php } ?>
