
<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">

        <div class="col-md-3">
            <?=
            $this->Form->control('placa', [
                'class' => 'form-control placa',
                'label' => false,
                'placeholder' => 'Pesquise por Placa',
                'autofocus' => true,
                'value' => $this->request->getQuery('placa')]);
            ?>
        </div>
        <div class="col-md-3">
            <?=
            $this->Form->control('ano', [
                'type' => 'text',
                'class' => 'ano',
                'label' => false,
                'placeholder' => 'Pesquise por Ano',
                'autofocus' => true,
                'value' => $this->request->getQuery('ano')]);
            ?>
        </div>
        <div class="col-md-3">
            <?=
            $this->element('admin/select2', [
                'controller' => 'cores',
                'name' => 'cor_id',
                'label' => false,
                'placeholder' => 'Pesquise por Cor',
                'multiple' => false,
                'value' => $this->request->getQuery('cor_id')
            ])
            ?>
        </div>
        <div class="col-md-3">
            <?=
            $this->Form->control('renavam', [
                'type' => 'text',
                'label' => false,
                'placeholder' => 'Pesquise por Renavam',
                'autofocus' => true,
                'value' => $this->request->getQuery('renavam')]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">
        <?= $this->element('admin/pesquisar/botoes') ?>
        <div class="table-responsive no-padding">
            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                    <tr>
                        <th class="checkbox-id">
                            <?= $this->Form->checkbox('select-all', ['hiddenField' => false]); ?>
                        </th>
                        <th class="table-image"></th>
                        <th><?= $this->Paginator->sort('placa') ?></th>
                        <th><?= $this->Paginator->sort('ano') ?></th>
                        <th><?= $this->Paginator->sort('combustivel') ?></th>
                        <th><?= $this->Paginator->sort('cor') ?></th>
                        <th><?= $this->Paginator->sort('data_entrada') ?></th>
                        <th><?= $this->Paginator->sort('renavam') ?></th>
                        <th class="actions">
                            <?= __('Ações') ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($entidade as $item) {
                        $img = $item->imagem ?? 'car-default.png';
                        ?>
                        <tr>
                            <td class="checkbox-id">
                                <?= $this->Form->checkbox('id', ['hiddenField' => false, 'value' => $item->id]); ?>
                            </td>
                            <td class="table-image">
                                <?= $this->Html->image($img, ['class' => 'img-responsive']); ?>
                            </td>
                            <td>
                                <?= h($item->placa) ?>
                                <br>
                                <?= h($item->modelo->nome) ?>
                                <br>
                                <?= h($item->modelo->marca->nome) ?>

                            </td>
                            <td>
                                <?= h($item->ano) ?>
                            </td>
                            <td>
                                <?= App\Enum\TipoCombustivelEnum::getType($item->combustivel) ?>
                            </td>
                            <td>
                                <?= h($item->core->nome) ?>
                            </td>
                            <td>
                                <?= dateToData($item->data_entrada) ?>
                            </td>
                            <td>
                                <?= h($item->renavam) ?>
                            </td>
                            <td class="actions">
                                <?= $this->element('admin/pesquisar/botoes-acoes', ['id' => $item->id]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>
