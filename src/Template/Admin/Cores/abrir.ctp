<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <div class="form-group col-md-4">
                <?= $this->Form->control('codigo'); ?>
            </div>
            <div class="form-group col-md-4">
                <?= $this->Form->control('nome'); ?>
            </div>
            <div class="form-group col-md-4">
                <label class=" no-padding" for="tipo-pintura">Tipo de Pintura</label>
                <?=
                $this->Form->select('tipo_pintura', \App\Enum\TipoPinturaEnum::ARRAY_STR, [
                    'multiple' => false,
                    'hiddenField' => false,
                    'required' => true,
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <label class=" no-padding" for="cor-denatran">Cor Denatran</label>
                <?=
                $this->Form->select('cor_denatran', \App\Enum\CorDenatranEnum::ARRAY_STR, [
                    'multiple' => false,
                    'hiddenField' => false,
                    'required' => true,
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <label>Cor:</label>
                <div class="input-group color-picker colorpicker-element">
                    <input type="text" name="cor" value="<?= $entidade->cor ?>" class="form-control">

                    <div class="input-group-addon">
                        <i style="background-color: <?= $entidade->cor ?>;"></i>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
