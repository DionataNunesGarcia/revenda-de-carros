<?php
$diasVencimento = [];
$i = 1;
while ($i <= 28) {
    $diasVencimento[$i] = $i;
    $i++;
}
?>
<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-4">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'Vendedores',
                        'name' => 'vendedor_id',
                        'label' => 'Vendedor',
                        'placeholder' => 'Pesquise pelo Vendedor',
                        'multiple' => false,
                        'required' => true,
                        'value' => $entidade->vendedor_id,
                    ])
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'Veiculos',
                        'name' => 'veiculo_id',
                        'label' => 'Veículo',
                        'placeholder' => 'Pesquise pela Placa',
                        'multiple' => false,
                        'required' => true,
                        'value' => $entidade->veiculo_id,
                    ])
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'Clientes',
                        'name' => 'cliente_id',
                        'label' => 'Cliente',
                        'multiple' => false,
                        'required' => true,
                        'value' => $entidade->cliente_id,
                    ])
                    ?>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-4">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'FormasPagamento',
                        'name' => 'forma_pagamento_id',
                        'label' => 'Formas de Pagamento',
                        'multiple' => false,
                        'required' => true,
                        'value' => $entidade->forma_pagamento_id,
                    ])
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('valor', [
                        'type' => 'text',
                        'class' => 'nf2Cifrado',
                        'required' => true,
                        'label' => 'Valor de Venda',
                        'value' => valueToValor($entidade->valor)
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('valor_desconto', [
                        'type' => 'text',
                        'class' => 'nf2Cifrado',
                        'required' => false,
                        'label' => 'Valor de Desconto',
                        'value' => valueToValor($entidade->valor_desconto)
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('valor_acrescimo', [
                        'type' => 'text',
                        'class' => 'nf2Cifrado',
                        'required' => false,
                        'label' => 'Valor de Acréscimo',
                        'value' => valueToValor($entidade->valor_acrescimo)
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('data_retirada', [
                        'label' => 'Data de Retirada',
                        'class' => 'datepicker',
                        'required' => true,
                        'type' => 'text',
                        'value' => dateToData($entidade->data_retirada)
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('data_retirada', [
                        'label' => 'Data de Retirada',
                        'class' => 'datepicker',
                        'required' => true,
                        'type' => 'text',
                        'value' => dateToData($entidade->data_retirada)
                    ]);
                    ?>
                </div>
            </div>
            <div class="col-md-12 no-padding">
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('primeiro_pagamento', [
                        'label' => 'Primeiro Pagamento',
                        'class' => 'datepicker',
                        'required' => false,
                        'type' => 'text',
                        'value' => dateToData($entidade->primeiro_pagamento)
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <?=
                    $this->Form->control('meses_garantia', [
                        'label' => 'Meses de Garantia',
                        'value' => $entidade->meses_garantia,
                        'class' => 'integer',
                        'type' => 'text'
                    ]);
                    ?>
                </div>
                <div class="form-group col-md-4">
                    <label for="dia-vencimento">Dia de Vencimento</label>
                    <?=
                    $this->Form->select('dia_vencimento', $diasVencimento, [
                        'value' => $entidade->dia_vencimento,
                        'required' => false,
                    ]);
                    ?>
                </div>
            </div>
            <?php if (!$entidade->id || $entidade->rascunho ) { ?>
                <div class="form-group col-md-12">
                    <label for="rascunho">Salvar como Rascunho</label>
                    <?=
                    $this->Form->select('rascunho',  [
                        1 => 'Sim, somente salvar',
                        0 => 'Não, salvar e gerar registros de pagamento',
                    ]);
                    ?>
                </div>
            <?php } ?>
            <div class="form-group col-md-12">
                <?=
                $this->Form->control('anotacoes', [
                    'type' => 'textarea',
                    'label' => 'Anotações',
                    'rows' => 7,
                    'class' => 'form-control'
                ]);
                ?>
            </div>

            <div class="col-md-12 no-padding">
                <div class="form-group col-md-4">
                    <?=
                    $this->element('admin/upload-arquivo', [
                        'id' => $entidade->id,
                        'field' => 'contrato_anexo',
                        'label' => 'Contrato',
                        'file' => $entidade->contrato_anexo
                    ])
                    ?>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
