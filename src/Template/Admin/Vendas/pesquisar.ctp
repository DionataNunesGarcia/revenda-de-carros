<?php
use App\Enum\ContasReceberSituacaoEnum;
?>
<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">

        <div class="col-md-6">
            <?=
            $this->Form->control('nome', [
                'class' => 'form-control',
                'label' => 'Nome',
                'placeholder' => 'Pesquise por Nome',
                'autofocus' => true,
                'value' => $this->request->getQuery('nome')
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?=
            $this->Form->control('datas_vencimento', [
                'class' => 'intervalo-tempo',
                'label' => 'Datas Vencimento',
                'autocomplete' => 'off',
                'value' => $this->request->getQuery('datas_vencimento')
            ]);
            ?>
            <?=
            $this->Form->hidden('datas_vencimento_load', [
                'class' => 'intervalo-tempo-load',
                'value' => $this->request->getQuery('datas_vencimento'),
                'data-id' => 'datas-vencimento',
                'disabled' => true
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?=
            $this->Form->control('datas_validade', [
                'class' => 'intervalo-tempo',
                'label' => 'Datas Validade',
                'autocomplete' => 'off',
                'value' => $this->request->getQuery('datas_validade')
            ]);
            ?>
            <?=
            $this->Form->hidden('datas_validade_load', [
                'class' => 'intervalo-tempo-load',
                'value' => $this->request->getQuery('datas_validade'),
                'data-id' => 'datas-validade',
                'disabled' => true
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?=
            $this->Form->control('datas_pagamento', [
                'class' => 'intervalo-tempo',
                'label' => 'Datas Pagamento',
                'autocomplete' => 'off',
                'value' => $this->request->getQuery('datas_pagamento')
            ]);
            ?>
            <?=
            $this->Form->hidden('datas_pagamento_load', [
                'class' => 'intervalo-tempo-load',
                'data-id' => 'datas-pagamento',
                'value' => $this->request->getQuery('datas_pagamento'),
                'disabled' => true
            ]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">
        <?= $this->element('admin/pesquisar/botoes') ?>
        <div class="table-responsive no-padding">
            <table class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                    <tr>
                        <th><?= $this->Form->checkbox('select-all', ['hiddenField' => false]); ?></th>
                        <th scope="col"><?= h('Veiculo') ?></th>
                        <th scope="col"><?= h('Cliente') ?></th>
                        <th scope="col"><?= h('Vendedor') ?></th>
                        <th scope="col"><?= h('Forma de Pagamento') ?></th>
                        <th scope="col"><?= h('Valor') ?></th>
                        <th scope="col"><?= h('Data Retirada') ?></th>
                        <th scope="col"><?= h('Meses Garantia') ?></th>
                        <th scope="col"><?= h('Criado') ?></th>
                        <th class="actions"><?= __('Ações') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($entidade as $item) {
                        $imgVeiculo = $item->veiculo->imagem ?? 'car-default.png';
                        ?>
                        <tr>
                            <td>
                                <?= $this->Form->checkbox('id', ['hiddenField' => false, 'value' => $item->id]); ?>
                            </td>
                            <td class="text-center">
                                <?= h("{$item->veiculo->placa} | {$item->veiculo->modelo->nome}") ?>
                                <br>
                                <?= $this->Html->image($imgVeiculo, ['class' => 'img-circle']); ?>
                            </td>
                            <td>
                                <?= h($item->cliente->nome) ?>
                                <br>
                                <?= $item->cliente->tipo == 'pf' ? 'CPF: ' : 'CNPJ: ' ?>
                                <?= cpfCnpj($item->cliente->cpf_cnpj) ?>
                                <br>
                                E-mail: <?= h($item->cliente->email) ?>
                            </td>
                            <td>
                                <?= h($item->vendedor->nome) ?>
                            </td>
                            <td>
                                <?= h($item->forma_pagamento->nome) ?>
                            </td>
                            <td>
                                <?= valueToValorCifrado($item->valor) ?>
                            </td>
                            <td>
                                <?= dateToData($item->data_retirada) ?>
                            </td>
                            <td>
                                <?= h($item->meses_garantia) ?>
                            </td>
                            <td>
                                <?= dateToData($item->criado) ?>
                            </td>
                            <td class="actions">
                                <?= $this->element('admin/pesquisar/botoes-acoes', ['id' => $item->id]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>
