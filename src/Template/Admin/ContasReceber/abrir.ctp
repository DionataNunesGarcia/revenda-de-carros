<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <?= $this->Form->hidden('model') ?>
            <div class="form-group col-md-4">
                <?= $this->Form->control('nome'); ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->element('admin/select2', [
                    'controller' => $entidade->model,
                    'name' => 'foreign_key',
                    'label' => $entidade->model,
                    'placeholder' => 'Pesquise por ' . $entidade->model,
                    'multiple' => false,
                    'required' => true,
                    'value' => $entidade->foreign_key,
                ])
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('valor', [
                    'type' => 'text',
                    'class' => 'nf2Cifrado',
                    'required' => true,
                    'label' => 'Valor de Venda',
                    'value' => valueToValor($entidade->valor)
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('data_vencimento', [
                    'label' => 'Data de Vencimento',
                    'class' => !empty($entidade->id) ? '' : 'datepicker',
                    'readonly' => !empty($entidade->id),
                    'required' => true,
                    'type' => 'text',
                    'value' => dateToData($entidade->data_vencimento)
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('data_validade', [
                    'label' => 'Data de Validade',
                    'class' => 'datepicker',
                    'required' => true,
                    'type' => 'text',
                    'value' => dateToData($entidade->data_validade)
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->control('data_pagamento', [
                    'label' => 'Data de Pagamento',
                    'class' => 'datepicker',
                    'required' => false,
                    'type' => 'text',
                    'value' => dateToData($entidade->data_pagamento)
                ]);
                ?>
            </div>
            <div class="form-group col-md-12">
                <?=
                $this->Form->control('anotacoes', [
                    'type' => 'textarea',
                    'label' => 'Anotações',
                    'rows' => 7,
                    'class' => 'form-control'
                ]);
                ?>
            </div>

            <div class="form-group col-md-4">
                <?=
                $this->element('admin/upload-arquivo', [
                    'id' => $entidade->id,
                    'field' => 'boleto',
                    'file' => $entidade->boleto
                ])
                ?>
            </div>

            <div class="form-group col-md-4">
                <?=
                $this->element('admin/upload-arquivo', [
                    'id' => $entidade->id,
                    'field' => 'nota_fiscal',
                    'file' => $entidade->nota_fiscal
                ])
                ?>
            </div>

            <div class="form-group col-md-4">
                <?=
                $this->element('admin/upload-arquivo', [
                    'id' => $entidade->id,
                    'field' => 'comprovante_pagamento',
                    'file' => $entidade->comprovante_pagamento
                ])
                ?>
            </div>
        </fieldset>
    </div>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
