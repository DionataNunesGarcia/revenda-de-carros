<?= $this->Form->create(null, ['action' => 'enviarMensagem', 'enctype' => 'multipart/form-data']) ?>
    <div class="box">
        <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
        <div class="box-body">
            <fieldset>
                <div class="form-group col-md-12">
                    <?=
                    $this->element('admin/select2', [
                        'controller' => 'Usuarios',
                        'name' => 'usuarios_id',
                        'label' => 'Enviar para',
                        'multiple' => true,
                        'required' => true,
                    ])
                    ?>
                </div>
                <div class="form-group col-md-6">
                    <?= $this->Form->control('titulo', []); ?>
                </div>
                <div class="form-group col-md-6">
                    <?= $this->Form->control('assunto', []); ?>
                </div>
                <div class="form-group col-md-12">
                    <?= $this->Form->control('mensagem', ['type' => 'textarea', 'class' => 'ckeditor']); ?>
                </div>
            </fieldset>
        </div>
        <div class="box-footer">
            <?= $this->element('admin/botoes-abrir') ?>
        </div>
    </div>
<?= $this->Form->end() ?>