<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <div class="form-group col-md-4">
                <?= $this->Form->control('nome'); ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->input('cnpj', [
                    'class' => 'form-control cnpj',
                    'label' => 'CNPJ',
                    'required' => true,
                    'value' => cnpj($entidade->cnpj)
                ]);
                ?>
            </div>
            <div class="form-group col-md-4">
                <?=
                $this->Form->input('telefone', [
                    'value' => fone($entidade->telefone),
                    'label' => 'Telefone',
                    'class' => 'fone',
                    'type' => 'text',
                ]);
                ?>
            </div>
            <div class="form-group col-md-12">
                <?=
                $this->Form->control('anotacoes', [
                    'type' => 'textarea',
                    'label' => 'Anotações',
                    'class' => 'form-control'
                ]);
                ?>
            </div>
        </fieldset>
    </div>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
