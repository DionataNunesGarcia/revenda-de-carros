<?php
use App\Enum\ContasReceberSituacaoEnum;
?>
<?= $this->Form->create(null, ['type' => 'get']); ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-filter"></i> Filtrar']) ?>
    <div class="box-body">

        <div class="col-md-6">
            <?=
            $this->Form->control('nome', [
                'class' => 'form-control',
                'label' => 'Nome',
                'placeholder' => 'Pesquise por Nome',
                'autofocus' => true,
                'value' => $this->request->getQuery('nome')
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?=
            $this->Form->control('datas_vencimento', [
                'class' => 'intervalo-tempo',
                'label' => 'Datas Vencimento',
                'autocomplete' => 'off',
                'value' => $this->request->getQuery('datas_vencimento')
            ]);
            ?>
            <?=
            $this->Form->hidden('datas_vencimento_load', [
                'class' => 'intervalo-tempo-load',
                'value' => $this->request->getQuery('datas_vencimento'),
                'data-id' => 'datas-vencimento',
                'disabled' => true
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?=
            $this->Form->control('datas_validade', [
                'class' => 'intervalo-tempo',
                'label' => 'Datas Validade',
                'autocomplete' => 'off',
                'value' => $this->request->getQuery('datas_validade')
            ]);
            ?>
            <?=
            $this->Form->hidden('datas_validade_load', [
                'class' => 'intervalo-tempo-load',
                'value' => $this->request->getQuery('datas_validade'),
                'data-id' => 'datas-validade',
                'disabled' => true
            ]);
            ?>
        </div>

        <div class="col-md-6">
            <?=
            $this->Form->control('datas_pagamento', [
                'class' => 'intervalo-tempo',
                'label' => 'Datas Pagamento',
                'autocomplete' => 'off',
                'value' => $this->request->getQuery('datas_pagamento')
            ]);
            ?>
            <?=
            $this->Form->hidden('datas_pagamento_load', [
                'class' => 'intervalo-tempo-load',
                'data-id' => 'datas-pagamento',
                'value' => $this->request->getQuery('datas_pagamento'),
                'disabled' => true
            ]);
            ?>
        </div>
    </div>
    <div class="box-footer">
        <div class="pull-right">
            <?= $this->element('admin/pesquisar/botoes-filtro') ?>
        </div>
    </div>
</div>
<?= $this->Form->end(); ?>
<!-- Default box -->
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => '<i class="fa fa-table"></i> Registros']) ?>
    <div class="box-body">
        <?= $this->element('admin/pesquisar/botoes') ?>
        <div class="table-responsive no-padding">
            <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover table-condensed">
                <thead>
                    <tr>
                        <th><?= $this->Form->checkbox('select-all', ['hiddenField' => false]); ?></th>
                        <th><?= $this->Paginator->sort('nome') ?></th>
                        <th scope="col" class="text-center"><?= h('Situação') ?></th>
                        <th scope="col" class="text-right"><?= $this->Paginator->sort('valor') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('data_vencimento') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('data_validade') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('data_pagamento') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('criado') ?></th>
                        <th class="actions"><?= __('Ações') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($entidade as $item) {
                        ?>
                        <tr>
                            <td>
                                <?= $this->Form->checkbox('id', ['hiddenField' => false, 'value' => $item->id]); ?>
                            </td>
                            <td>
                                <?= h($item->nome) ?>
                            </td>
                            <td class="text-center">
                                <?= ContasReceberSituacaoEnum::getSituacao(
                                    $item->data_vencimento,
                                    $item->data_validade,
                                    $item->data_pagamento)
                                ?>
                            </td>
                            <td class="text-right">
                                <?= valueToValor($item->valor) ?>
                            </td>
                            <td>
                                <?= dateToData($item->data_vencimento) ?>
                            </td>
                            <td>
                                <?= dateToData($item->data_validade) ?>
                            </td>
                            <td>
                                <?= dateToData($item->data_pagamento) ?>
                            </td>
                            <td>
                                <?= dateToData($item->criado) ?>
                            </td>
                            <td class="actions">
                                <?= $this->element('admin/pesquisar/botoes-acoes', ['id' => $item->id]) ?>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?= $this->element('admin/pesquisar/paginacao') ?>
        </div>
    </div>
</div>
