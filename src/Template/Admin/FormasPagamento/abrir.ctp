<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <div class="form-group col-md-6">
                <?= $this->Form->control('nome'); ?>
            </div>
            <div class="form-group col-md-6">
                <?=
                $this->Form->control('parcelas', [
                    'type' => 'text',
                    'class' => 'integer',
                    'required' => true,
                    'value' => $entidade->parcelas
                ]);
                ?>
            </div>
        </fieldset>
    </div>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
