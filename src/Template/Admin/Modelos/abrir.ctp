<?= $this->Form->create($entidade, ['action' => 'salvar', 'enctype' => 'multipart/form-data']) ?>
<div class="box">
    <?= $this->element('admin/titulo-box', ['titulo' => 'Cadastro']) ?>
    <div class="box-body">
        <fieldset>
            <?= $this->Form->hidden('id') ?>
            <div class="form-group col-md-6">
                <?= $this->Form->control('nome'); ?>
            </div>
            <div class="form-group col-md-6">
                <?=
                $this->element('admin/select2', [
                    'controller' => 'marcas',
                    'name' => 'marca_id',
                    'label' => 'Marca',
                    'multiple' => false,
                    'required' => true,
                    'value' => $entidade->marca_id,
                ])
                ?>
            </div>
        </fieldset>
    </div>
    <div class="box-footer">
        <?= $this->element('admin/botoes-abrir', ['id' => @$entidade->id]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
