<!--_______________________________________ Carousel__________________________________ -->
<?= $this->element('front/template/carousel') ?>

<div class="container-fluid landing-page">
    <div class="container">
        <!-- ____________________Featured Section ______________________________-->
        <?= $this->element('front/featured-cars', ['destaques' => $destaques]) ?>

        <!-- ____________________Latest Section ______________________________-->
        <?= $this->element('front/latest-cars', ['ultimos' => $ultimos]) ?>
        <div class="col-md-12 text-center">
            <a href="<?= $this->Url->build(['_name' => 'lista_veiculos'], true); ?>" class="btn btn-primary">
                Ver todos os veículos
            </a>
        </div>
    </div>
</div>
<!-- _______________________________News Letter ____________________-->
<?= $this->element('front/news-letter') ?>
