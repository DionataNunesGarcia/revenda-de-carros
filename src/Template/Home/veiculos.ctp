<!-- ________________________LATEST CARS SECTION _______________________-->
<div class="container">
    <?= $this->element('front/latest-cars-filters', ['title' => 'Veículos']) ?>
    <div class="text-center ">
        <?php if ($veiculos->count()) { ?>
            <div class="row">
                <?php foreach ($veiculos as $veiculo) { ?>
                    <?= $this->element('front/card', ['veiculo' => $veiculo]) ?>
                <?php } ?>
            </div>
            <?php echo $this->element('front/pagination') ?>
        <?php } else { ?>
            <div class="text-center">
                <strong class="d-none d-sm-block">Nenhum veiculo foi encontrado.</strong>
            </div>
        <?php } ?>
    </div>
</div>
