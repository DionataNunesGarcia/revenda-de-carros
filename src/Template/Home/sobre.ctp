<?php $sobre = $this->Sobre->buscar(); ?>
<div class="container">
    <div class="row">
        <div class="how-section1">
            <div class="row">
                <div class="col-md-6">
                    <img src="https://i.imgur.com/FDvR6pv.png"  alt=""/>

                    <h4>
                        Sobre Nós
                    </h4>
                    <h5>
                        <?= $sobre->nome ?>
                    </h5>

                    <p class="text-muted" style="font-size:19px">
                        <?= \Cake\Utility\Text::wordWrap($sobre->sobre) ?>
                    </p>
                </div>
                <div class="col-md-6 how-img">
                    <div class="single-item-carousel">
                        <ul id="lightSlider">
                            <?php foreach ($sobre->arquivos as $arquivo) { ?>
                                <li data-thumb="<?= $arquivo->arquivo ?>">
                                    <a data-thumb="<?= $arquivo->arquivo ?>" href="<?= $arquivo->arquivo ?>" data-fancybox="gallery" data-caption="<?= $arquivo->title ?>">
                                        <img src="<?= $arquivo->arquivo ?>" />
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>

                <div class="col-md-12" style="padding-top: 25px">
                    <?php if (!empty($sobre->missao) || !empty($sobre->visao) || !empty($sobre->valores) ) { ?>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php if (!empty($sobre->missao)) { ?>
                                <!-- panel 1 -->
                                <div class="panel panel-default">
                                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                                    <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
                                        <div class="panel-heading" role="tab" id="headingOne"data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <strong class="panel-title">Missão</strong>
                                        </div>
                                    </span>

                                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body">
                                            <!-- Tab content goes here -->
                                            <?= \Cake\Utility\Text::wordWrap($sobre->missao) ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- / panel 1 -->
                            <?php } ?>

                            <?php if (!empty($sobre->visao)) { ?>
                                <!-- panel 2 -->
                                <div class="panel panel-default">
                                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                                    <span class="side-tab" data-target="#tab2" data-toggle="tab" role="tab" aria-expanded="false">
                                        <div class="panel-heading" role="tab" id="headingTwo" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            <strong class="panel-title collapsed">Visão</strong>
                                        </div>
                                    </span>

                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body">
                                            <!-- Tab content goes here -->
                                            <?= \Cake\Utility\Text::wordWrap($sobre->visao) ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- / panel 2 -->
                            <?php } ?>

                            <?php if (!empty($sobre->valores)) { ?>
                                <!-- / panel 3 -->

                                <div class="panel panel-default">
                                    <!--wrap panel heading in span to trigger image change as well as collapse -->
                                    <span class="side-tab" data-target="#tab3" data-toggle="tab" role="tab" aria-expanded="false">
                                        <div class="panel-heading" role="tab" id="headingThree"  class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <strong class="panel-title">Valores </strong>
                                        </div>
                                    </span>

                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body">
                                            <!-- tab content goes here -->
                                            <?= \Cake\Utility\Text::wordWrap($sobre->valores) ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div> <!-- / panel-group -->
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>
</div>
