<?php
use Cake\Routing\Router;

$nome = $veiculo->modelo->nome . " - " . $veiculo->modelo->marca->nome;$url = Router::url('/', true);
$url = Router::url(['_name' => 'veiculo', $veiculo->id, slug($veiculo->modelo->nome)], true);
$sobre = $this->Sobre->buscar();
$urldamensagempronta = urlencode("Olá, tudo bem? Gostaria de saber mais sobre o carro {$nome} - $url");
$urlWhats = "https://wa.me/55{$sobre->celular}?text={$urldamensagempronta}";
$classValor = '';
$textoValor = 'Preço';
$valorPromocao = '';
if(!empty($veiculo->valor_promocao) && $veiculo->valor_promocao < $veiculo->valor) {
    $valorPromocao = '<strong class="card-price-promotion product-title">Por: ' . valueToValorCifrado($veiculo->valor_promocao) . '</strong>';
    $classValor = 'exist-promotion';
    $textoValor = 'De';
}
?>
<div class="container">
    <div class="card">
        <div class="wrapper row">
            <div class=" col-md-6">
                <div class="single-item-carousel">
                    <ul id="lightSlider">
                        <li data-thumb="<?= $veiculo->imagem ?>" >
                            <a href="<?= $veiculo->imagem ?>" data-fancybox="gallery" data-caption="<?= $nome ?>">
                                <img src="<?= $veiculo->imagem ?>" />
                            </a>
                        </li>
                        <?php foreach ($veiculo->arquivos as $arquivo) { ?>
                            <li data-thumb="<?= $arquivo->arquivo ?>">
                                <a data-thumb="<?= $arquivo->arquivo ?>" href="<?= $arquivo->arquivo ?>" data-fancybox="gallery" data-caption="<?= $arquivo->title ?>">
                                    <img src="<?= $arquivo->arquivo ?>" />
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>

                <div class="text-center">
                    <hr>
                    <strong class="subtitle">Para saber mais, entre em contato por mensagem ou entre em contato pelo nosso</strong>
                    <br/>
                    <button data-toggle="modal" data-target="#contact-modal" class="btn btn-primary center-block">
                        <i class="fa fa-envelope"></i> Clicando Aqui
                    </button>
                    <br/>
                    <a href="<?= $urlWhats ?>" target="_blank" class="btn btn-success ">
                        <i class="fa fa-whatsapp"></i> Whatsapp
                    </a>
                </div>
            </div>
            <div class="details col-md-6">
                <h3 class="product-title">
                    <?= $nome ?>
                </h3>
                <strong class="price <?= $classValor ?>">
                    <?= "{$textoValor}: " . valueToValorCifrado($veiculo->valor) ?>
                </strong>
                <?= $valorPromocao ?>

                <?php if($veiculo->opcionais) { ?>
                    <strong class="titulo-opcional">
                        <?= "Opcionais e acessórios" ?>
                    </strong>
                    <p class="produto-opcionais">
                        <?php foreach($veiculo->opcionais as $opcional) { ?>
                            <label class="label-primary label-opcional">
                                <?= h($opcional->nome) ?>
                            </label>
                        <?php } ?>
                    </p>
                <?php } ?>

                <p class="product-description">
                    <?= \Cake\Utility\Text::wordWrap($veiculo->descricao) ?>
                </p>
                <?=
                $this->element('botoes-compartilhar', [
                    'title' => $nome,
                    'description' => \Cake\Utility\Text::wordWrap($veiculo->descricao),
                    'img' => $veiculo->imagem,
                ]);
                ?>
            </div>
        </div>
    </div>
</div>


<!-- line modal -->
<div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">
                    Entre em contato sobre - <?= $veiculo->modelo->nome ?> - <?= $veiculo->modelo->marca->nome ?>
                </h3>
            </div>
            <div class="modal-body contact-form">
                <?= $this->element('front/form-contact', ['veiculoId' => $veiculo->id, 'assunto' => "{$veiculo->modelo->nome} - {$veiculo->modelo->marca->nome}"]) ?>
            </div>
        </div>
    </div>
</div>
