<div class="container" style="padding-top: 4%">
    <div class="col-md-12 text-center">
        <h1 class="title">
            Entre em contato conosco:
        </h1>
    </div>
    <div class="col-md-6">
        <div class="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d426.0955646765672!2d-54.089733242737985!3d-31.31025300337229!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95067581489151ef%3A0x5c4b70ade63661d5!2sJW%20Ve%C3%ADculos!5e0!3m2!1spt-BR!2sbr!4v1655141508723!5m2!1spt-BR!2sbr" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </div>
    </div>
    <div class="col-md-6">
        <div class="contact-form">
            <h2 class="subtitle">Estamos aqui para ajudalo</h2>
            <?= $this->element('front/form-contact') ?>
        </div>
    </div>
</div>
