<!DOCTYPE html>
<?php
use Cake\Core\Configure;
use Cake\Routing\Router;
$cakeDescription = Configure::read('Cliente.nome');
$url = Router::url('/', true);
?>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="description" content="<?= $cakeDescription ?>">
        <meta name="author" content="Diônata Garcia">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <?= $this->Html->css('../template-front/source/bootstrap-3.3.6-dist/css/bootstrap.css') ?>
        <?= $this->Html->css('../bower_components/font-awesome/css/font-awesome.min.css') ?>
        <?= $this->Html->css('../front/css/slider.css') ?>
        <?= $this->Html->css('../front/css/style-front.css') ?>
        <?= $this->Html->css('../plugins/lightslider-master/dist/css/lightslider.css') ?>
        <?= $this->Html->css('../plugins/fancyBox-master/dist/jquery.fancybox.css') ?>

        <?= $this->Html->script('../front/source/bootstrap-3.3.6-dist/js/jquery.js') ?>
        <?= $this->Html->script('../front/source/js/isotope.js') ?>
        <?= $this->Html->script('//code.jquery.com/ui/1.11.4/jquery-ui.js') ?>
        <?= $this->Html->script('../front/source/bootstrap-3.3.6-dist/js/bootstrap.js') ?>
        <?= $this->Html->script('../plugins/lightslider-master/dist/js/lightslider.js') ?>
        <?= $this->Html->script('../plugins/fancybox-master/dist/jquery.fancybox.js') ?>
        <?= $this->Html->script('../plugins/input-mask/jquery.inputmask.js') ?>
        <?= $this->Html->script('../plugins/input-mask/jquery.inputmask.date.extensions.js') ?>
        <?= $this->Html->script('../plugins/jquery-maskmoney-master/dist/jquery.maskMoney.js') ?>
        <?= $this->Html->script('../plugins/input-mask/jquery.inputmask.extensions.js') ?>
        <?= $this->Html->script('../front/source/js/myscript.js') ?>

        <?= $this->OpenGraph->html(); ?>

        <?= $this->fetch('script') ?>
        <!-- Fetch Custom Scripts -->
        <?= $this->fetch('custom') ?>
        <script>
            let saveNewsletters = "<?= $this->Url->build(['controller' => 'Home', 'action' => 'saveNewsletters'], true)?>";
        </script>

        <title>
            <?= $cakeDescription ?>
        </title>

        <?= $this->Html->meta('icon') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
    </head>
    <body data-url="<?= $url ?>">
        <!--MENU-->
        <?= $this->element('front/template/menu') ?>
        <div class="container" style="padding-top: 15px">
            <?= $this->Flash->render() ?>
        </div>
        <section class="content">
            <?= $this->fetch('content') ?>
        </section>

        <!-- ______________________________________________________Bottom Menu ______________________________-->
        <?= $this->element('front/template/footer') ?>
    </body>

</html>
