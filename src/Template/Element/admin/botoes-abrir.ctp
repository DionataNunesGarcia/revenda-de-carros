<?php
echo $this->Form->button('<i class="fa fa-save"></i> Salvar', ['class' => 'btn btn-primary', 'escape' => false]);

if(!isset($controller)){
    $controller = $this->request->controller;
}
?>
<a href="<?= $this->Url->build(['controller' => $controller, 'action' => 'pesquisar'], true); ?>" class="btn btn-default">
    <i class="fa fa-search"></i> Pesquisar 
</a>
<?php if (!empty($entidade->id)) { ?>
    <a href="<?= $this->Url->build(['controller' => $controller, 'action' => 'incluir'], true); ?>" class="btn btn-default">
        <i class="fa fa-plus-square"></i> Incluir 
    </a>
<?php } ?>