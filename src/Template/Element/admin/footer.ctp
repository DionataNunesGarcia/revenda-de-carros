<?php use Cake\Core\Configure; ?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        Desenvolvido por:
        <b>
            <a href="https://www.linkedin.com/in/dionata-garcia/" target="_blank" data-auth="false">
                Diônata Garcia
            </a>
        </b>
    </div>
    <strong>Copyright &copy;
        <?= date('Y') ?>
        <a href="<?= Configure::read('Cliente.link') ?>" target="_blank">
            <?= Configure::read('Cliente.nome') ?>
        </a>.
    </strong>
    Todos os direitos reservados
</footer>
