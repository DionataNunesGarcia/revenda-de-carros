<style>
    .treeview-menu li.disabled:hover, .treeview-menu li.disabled a:hover {
        cursor: no-drop;
    }
</style>
<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?php
                $imagem = 'user-default.png';
                if (!empty($userSession['imagem']) && file_exists(WWW_ROOT . $userSession['imagem'])) {
                    $imagem = '../' . $userSession['imagem'];
                }
                ?>
                <?= $this->Html->image($imagem, ['class' => 'img-circle']); ?>
            </div>
            <div class="pull-left info">
                <p>
                    <?= ucfirst($userSession['usuario']); ?>
                </p>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li>
                <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Veiculos', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Veiculos">
                    <i class="fa fa-car"></i>
                    <span>
                        <?= __('Veículos') ?>
                    </span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Clientes', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Clientes">
                    <i class="fa fa-users"></i>
                    <span>
                        <?= __('Clientes') ?>
                    </span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Proprietarios', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Proprietários">
                    <i class="fa fa-user-plus"></i>
                    <span>
                        <?= __('Proprietários') ?>
                    </span>
                </a>
            </li>
            <li>
                <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Vendas', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Proprietários">
                    <i class="fa fa-money"></i>
                    <span>
                        <?= __('Vendas') ?>
                    </span>
                </a>
            </li>
            <!-- Cadastros -->
            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-drivers-license"></i>
                    <span>
                        <?= __('Cadastros') ?>
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Cores', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Cores" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Cores') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Fabricantes', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Fabricantes" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Fabricantes') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Marcas', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Marcas" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Marcas') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Modelos', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Modelos" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Modelos') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Opcionais', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Opcionais" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Opcionais') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'TipoVeiculos', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Tipo de Veículos" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Tipo de Veículos') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'TipoVendas', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Tipo de Vendas" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Tipo de Vendas') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'FormasPagamento', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Formas de Pagamento" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Formas de Pagamento') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'Fornecedores', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Fornecedores" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Fornecedores') ?>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Financeiro -->
            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-dollar"></i>
                    <span>
                        <?= __('Finaceiro') ?>
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'ContasReceber', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Contas Receber" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Contas Receber') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['prefix' => 'admin', 'controller' => 'ContasPagar', 'action' => 'pesquisar'], true); ?>" title="Pesquisar Contas Pagar" data-placement="right">
                            <i class="fa fa-circle-o"></i>
                            <?= __('Contas Pagar') ?>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- Site -->
            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span>
                        <?= __('Site') ?>
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Sobre', 'action' => 'banners'], true); ?>" data-placement="right">
                            <i class="fa fa-picture-o"></i>
                            <?= __('Banners') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Sobre', 'action' => 'abrir'], true); ?>" data-placement="right">
                            <i class="fa fa-address-book"></i>
                            <?= __('Sobre') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Contatos', 'action' => 'pesquisar'], true); ?>" data-placement="right">
                            <i class="fa fa-users"></i>
                            <?= __('Contatos') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'ContatosNewsletters', 'action' => 'pesquisar'], true); ?>" data-placement="right">
                            <i class="fa fa-envelope"></i>
                            <?= __('Contatos Newsletter') ?>
                        </a>
                    </li>
                </ul>
            </li>

            <!--Administração-->
            <li class=" treeview">
                <a href="#">
                    <i class="fa fa-gears"></i> <span>
                        <?= __('Administração') ?>
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Usuarios', 'action' => 'pesquisar'], true); ?>" data-placement="right">
                            <i class="fa fa-users"></i>
                            <?= __('Usuários') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Vendedores', 'action' => 'pesquisar'], true); ?>" data-placement="right">
                            <i class="fa fa-users"></i>
                            <?= __('Vendedores') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'niveis', 'action' => 'pesquisar'], true); ?>" data-placement="right">
                            <i class="fa fa-bar-chart-o"></i>
                            <?= __('Níveis Permissões') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'ParametrosSistema', 'action' => 'abrir'], true); ?>" data-placement="right">
                            <i class="fa fa-gear"></i>
                            <?= __('Parâmetros do Sistema') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'LogsAlteracoes', 'action' => 'pesquisar'], true); ?>" data-placement="right">
                            <i class="fa fa-exclamation-triangle"></i>
                            <?= __('Logs Alterações') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'LogsAcesso', 'action' => 'pesquisar'], true); ?>" data-placement="right">
                            <i class="fa fa-check"></i>
                            <?= __('Logs Acessos') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'Mensagens', 'action' => 'escreverMensagem'], true); ?>" data-placement="right">
                            <i class="fa fa-envelope-o"></i>
                            <?= __('Enviar Mensagens') ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['controller' => 'usuarios', 'action' => 'logout'], true); ?>" data-auth="false" data-placement="right">
                            <i class="fa fa-sign-out"></i>
                            <?= __('Sair') ?>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
