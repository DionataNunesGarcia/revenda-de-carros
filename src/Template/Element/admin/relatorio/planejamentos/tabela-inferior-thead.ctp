<thead class="border-bottom border-right">

    <tr>
        <td colspan="10" class="no-bord"></td>
        <td colspan="10" class="text-center green border-left border-top border-bottom">
            AÇÕES DE TRADE
        </td>
        <td colspan="35" class="text-center green border-left border-top border-bottom">
            Cobertura
        </td>
        <td rowspan="3" class="text-center green border-left border-top border-bottom">
            FDS
        </td>
        <td colspan="3" class="text-center green border-left border-top border-bottom">
            MATERIAIS
        </td>
        <td colspan="3" class="text-center green border-left border-top border-bottom">
            FATURAMENTO
        </td>
        <td colspan="7" class="text-center green border-left border-top border-bottom">
            PLANEJAMENTO
        </td>
    </tr>

    <tr>
        <td colspan="10" class="no-bord"></td>
        <td colspan="4" class="text-center cinder border-left border-bottom">
            Temp. Tropical
        </td>
        <td colspan="4" class="text-center cinder border-left border-bottom">
            600 a bordo
        </td>
        <td rowspan="2" class="td-vertical cinder border-left">
            <span class="vertical-text">
                Cob. Fantástica
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                Calendário
            </span>
        </td>

        <!--Inicio Cobertura-->
        <td rowspan="2" class="td-vertical cinder border-left">
            <span class="vertical-text">
                TOTAL
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                M1
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                MAINSTREAN
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                PREMIUM
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                NÃO ALCOÓLICO
            </span>
        </td>

        <td rowspan="2" class="td-vertical cinder border-left">
            <span class="vertical-text">
                GLACIAL 300
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                GLACIAL 600
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                GLACIAL LITRO
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                GLACIAL 473
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                SCHIN CHOPP
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                SCHIN 300
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                SCHIN 600
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                SCHIN LITRO
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                SCHIN 350
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                SCHIN 473
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                DEVASSA CHOPP
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                DEVASSA 600
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                DEVASSA LITRO
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                DEVASSA 350
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                DEVASSA LN 355
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                DEVASSA 473
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                EISENBAHN CHOPP
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                EISENBAHN 600
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                EISENBAHN 350
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                EISENBAHN LN 355
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                EISENBAHN ESTILOS
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                BADEN 300
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                BADEN 600
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                FYS 350
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                FYS 2 LITROS
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                ITUBAINA 350
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                ITUBAINA LN 355
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                ITUBAINA 2 LITROS
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                SKINKA 450
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                DREHER
            </span>
        </td>
        <!--Fim Cobertura-->

        <!--Inicio Materiais-->
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                MATERIAL PESADO
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                MATERIAL LEVE
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                CHOPEIRA
            </span>
        </td>
        <!--Fim Materiais-->

        <!--Inicio Faturamento-->
        <td rowspan="2" class="cinder text-center border-left">
            <?= nomeMes($inferior['ultimosMeses'][1]->i18nFormat('MM')) ?>
            <br/>
            <?= $inferior['ultimosMeses'][1]->i18nFormat('yyyy') ?>
        </td>
        <td rowspan="2" class="cinder text-center border-left">
            <?= nomeMes($inferior['ultimosMeses'][2]->i18nFormat('MM')) ?>
            <br/>
            <?= $inferior['ultimosMeses'][2]->i18nFormat('yyyy') ?>
        </td>
        <td rowspan="2" class="cinder text-center border-left">
            <?= nomeMes($inferior['ultimosMeses'][3]->i18nFormat('MM')) ?>
            <br/>
            <?= $inferior['ultimosMeses'][3]->i18nFormat('yyyy') ?>
        </td>
        <!--Fim Faturamento-->

        <!--Inicio PLANEJAMENTO-->
        <td rowspan="2" class="td-vertical cinder border-left">
            <span class="vertical-text">
                CERVEJA 600
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                CERVEJA 350
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                CERVEJA 473
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                CERVEJA LN 355
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                CRAFTS
            </span>
        </td>
        <td rowspan="2" class="td-vertical cinder">
            <span class="vertical-text">
                NÃO ALCOÓLICO
            </span>
        </td>
        <!--Fim PLANEJAMENTO-->
    </tr>

    <tr>
        <td width="50" class="green border-top border-left text-center">
            Código
        </td>
        <td width="250" class="green border-top text-center">
            Razão
        </td>
        <td width="50" class="green border-top text-center">
            Pasta
        </td>
        <td width="50" class="green border-top text-center">
            Canal
        </td>
        <td width="50" class="green border-top text-center">
            Base
        </td>
        <td width="50" class="green border-top text-center">
            INAD
        </td>

        <td class="td-vertical cinder border-left">
            <span class="vertical-text vertical-text2">
                TOP 50
            </span>
        </td>
        <td class="td-vertical cinder">
            <span class="vertical-text vertical-text2">
                TOP 50 Ret
            </span>
        </td>
        <td class="td-vertical cinder border-left">
            <span class="vertical-text vertical-text2">
                Visita
            </span>
        </td>
        <td class="td-vertical cinder">
            <span class="vertical-text vertical-text2">
                Pedido
            </span>
        </td>

        <td class="cinder td-vertical border-left">
            <span class="vertical-text vertical-text2">
                Meta
            </span>
        </td>
        <td class="cinder td-vertical">
            <span class="vertical-text vertical-text2">
                Bonificação
            </span>
        </td>
        <td class="cinder td-vertical">
            <span class="vertical-text vertical-text2">
                Aderido
            </span>
        </td>
        <td class="cinder td-vertical">
            <span class="vertical-text vertical-text2">
                Batido
            </span>
        </td>

        <td class="cinder td-vertical border-left">
            <span class="vertical-text vertical-text2">
                Meta
            </span>
        </td>
        <td class="cinder td-vertical">
            <span class="vertical-text vertical-text2">
                Bonificação
            </span>
        </td>
        <td class="cinder td-vertical">
            <span class="vertical-text vertical-text2">
                Aderido
            </span>
        </td>
        <td class="cinder td-vertical">
            <span class="vertical-text vertical-text2">
                Batido
            </span>
        </td>
    </tr>
</thead>
