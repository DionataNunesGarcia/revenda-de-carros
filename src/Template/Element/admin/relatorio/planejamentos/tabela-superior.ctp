<div class="parte-superior">
    <div class="primeira-coluna">

        <div id="logo">
            <!--    <img src="img/logo.png" style="max-width: 70px;" alt="logo">-->
            <h1><small>Planejamento Diário</small></h1>
        </div>
        <div style="clear: both"></div>
        <table class="table dados-vendedor" border="0">
            <thead class="border-bottom border-right">
                <tr>
                    <td colspan="2" class="text-center green border-left border-top border-bottom">
                        VENDEDOR
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?= $superior['vendedor']->codigo ?>
                    </td>
                    <td>
                        <?= $superior['vendedor']->pessoaFisica->nome ?>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="table dados-vendedor" border="0">
            <thead class="border-bottom border-right">
                <tr>
                    <td class="text-center green border-left border-top border-bottom">
                        FOCOS DO DIA
                    </td>
                </tr>
            </thead>
            <tbody class="foco-dia">
                <tr>
                    <td>
                        1
                    </td>
                </tr>
                <tr>
                    <td>
                        2
                    </td>
                </tr>
                <tr>
                    <td>
                        3
                    </td>
                </tr>
                <tr>
                    <td>
                        4
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="segunda-coluna">
        <table class="table dados-vendedor" border="0">
            <tbody>
                <tr>
                    <td class="green bold">
                        DIA
                    </td>
                    <td class="valores text-center">
                        <?= $superior['data']->i18nFormat('dd/MM/yyyy') ?>
                    </td>
                </tr>
                <tr>
                    <td class="green bold">
                        DIAS ÚTEIS
                    </td>
                    <td class="text-center">
                        <?= $superior['totalDiasUteisMes']?>
                    </td>
                </tr>
                <tr>
                    <td class="green bold">
                        DIAS DECORRIDOS
                    </td>
                    <td class="text-center">
                        <?= $superior['totalDiasUteis']?>
                    </td>
                </tr>
            </tbody>
        </table>

        <table class="table dados-vendedor" border="0">
            <tbody>
                <tr>
                    <td class="no-bord">
                    </td>
                    <td class="green text-center">
                        META
                    </td>
                    <td class="green text-center">
                        REAL
                    </td>
                </tr>
                <tr>
                    <td class="green bold text-center">
                        IV
                    </td>
                    <td class="text-center">

                    </td>
                    <td class="text-center">

                    </td>
                </tr>
                <tr>
                    <td class="green bold text-center">
                        IAV
                    </td>
                    <td class="text-center">

                    </td>
                    <td class="text-center">

                    </td>
                </tr>
            </tbody>
        </table>

        <table class="table dados-vendedor" border="0">
            <tbody>
                <tr>
                    <td class="green bold">
                        PDV'S NA ROTA
                    </td>
                    <td class="valores text-center" style="min-width: 55px;">
                        <?= $superior['rotasVendedor']['pdvs'] ?>
                    </td>
                </tr>
                <tr>
                    <td class="green bold">
                        NÃO COBERTOS
                    </td>
                    <td class="text-center" style="min-width: 55px;">
                        <?= $superior['rotasVendedor']['naoCoberto'] ?>
                    </td>
                </tr>
                <tr>
                    <td class="green bold">
                        ... ESTÁVEIS
                    </td>
                    <td class="text-center" style="min-width: 55px;">
                        <?= $superior['rotasVendedor']['estaveis'] ?>
                    </td>
                </tr>
                <tr>
                    <td class="green bold">
                        ... INSTÁVEIS
                    </td>
                    <td class="text-center" style="min-width: 55px;">
                        <?= $superior['rotasVendedor']['instaveis'] ?>
                    </td>
                </tr>
                <tr>
                    <td class="green bold">
                        ... SEM COMPRA
                    </td>
                    <td class="text-center" style="min-width: 55px;">
                        <?= $superior['rotasVendedor']['semCompras'] ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="terceira-coluna">
        <table class="table dados-vendedor" border="0">
            <thead>
                <tr>
                    <td class="green bold text-center" colspan="2">
                        Faturamento Total
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="valores text-center">
                        REAL
                    </td>
                    <td class="text-right">
                        <?= valueToValor($superior['faturamentosVendedor']['total'], 2) ?>
                    </td>
                </tr>
            </tbody>
            <thead>
                <tr>
                    <td class="green bold text-center" colspan="2">
                        Faturamento Retornaveis
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="valores text-center">
                        REAL
                    </td>
                    <td class="text-right">
                        <?= valueToValor($superior['faturamentosVendedor']['retornaveis'], 2) ?>
                    </td>
                </tr>
            </tbody>
            <thead>
                <tr>
                    <td class="green bold text-center" colspan="2">
                        Faturamento Terceiros
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="valores text-center">
                        Meta
                    </td>
                    <td class="text-right">
                        <?= valueToValor($superior['faturamentosVendedor']['terceiros_meta'], 2) ?>
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        REAL
                    </td>
                    <td class="text-right">
                        <?= valueToValor($superior['faturamentosVendedor']['terceiros_real'], 2) ?>
                    </td>
                </tr>
            </tbody>
            <thead>
                <tr>
                    <td class="green bold text-center" colspan="2">
                        Devolução
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="valores text-center">
                        Meta
                    </td>
                    <td class="text-right">
                        <?= valueToValor($superior['faturamentosVendedor']['devolucoes_meta'], 2) ?>
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        REAL
                    </td>
                    <td class="text-right">
                        <?= valueToValor($superior['faturamentosVendedor']['devolucoes_real'], 2) ?>
                    </td>
                </tr>
            </tbody>
            <thead>
                <tr>
                    <td class="green bold text-center" colspan="2">
                        Trocas
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="valores text-center">
                        Meta
                    </td>
                    <td class="text-right">
                        <?= valueToValor($superior['faturamentosVendedor']['trocas_meta'], 2) ?>
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        REAL
                    </td>
                    <td class="text-right">
                        <?= valueToValor($superior['faturamentosVendedor']['trocas_real'], 2) ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="quarta-coluna">

        <table class="table dados-vendedor text-uppercase" border="0">
            <thead>
                <tr class="text-uppercase">
                    <td class="green bold text-center">
                        Volume
                    </td>
                    <td class="green bold text-center">
                        Meta
                    </td>
                    <td class="green bold text-center">
                        Real
                    </td>
                    <td class="green bold text-center">
                        Tend
                    </td>
                    <td class="green bold text-center">
                        Nec/Dia
                    </td>
                    <td class="green bold text-center">
                        Planejamento
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="valores text-center">
                        HL TOTAL
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Retornável
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        300ml+600ml+1Lt
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Premium
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Eisenbahn
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Devassa
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        600 ml
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Não Alcoólico
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        FYS
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table dados-vendedor table-refrigeracao" border="0">
            <thead>
                <tr class="text-uppercase">
                    <td rowspan="2" class="green bold text-center">
                        Refrigeração
                    </td>
                    <td class="green bold text-center">
                        Parque
                    </td>
                    <td class="green bold text-center">
                        Venda Zero
                    </td>
                    <td class="green bold text-center">
                        Batido
                    </td>
                    <td class="green bold text-center">
                        Meta
                    </td>
                    <td class="green bold text-center">
                        Giro
                    </td>
                </tr>
                <tr>
                    <td class="text-center">
                        0
                    </td>
                    <td class="text-center">
                        0
                    </td>
                    <td class="text-center">
                        0
                    </td>
                    <td class="text-center">
                        0
                    </td>
                    <td class="valores text-center">
                        0
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="quinta-coluna">

        <table class="table dados-vendedor text-uppercase" border="0">
            <thead>
                <tr class="text-uppercase">
                    <td class="green bold text-center">
                        Coberturas
                    </td>
                    <td class="green bold text-center">
                        Meta
                    </td>
                    <td class="green bold text-center">
                        Real
                    </td>
                    <td class="green bold text-center">
                        Tend
                    </td>
                    <td class="green bold text-center">
                        Nec/Dia
                    </td>
                    <td class="green bold text-center">
                        Planejamento
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="valores text-center">
                        HL TOTAL
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Retornável
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        300ml+600ml+1Lt
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Premium
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Eisenbahn
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Devassa
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        600 ml
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Não Alcoólico
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        FYS
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="valores text-center">
                    </td>
                </tr>
                <tr>
                    <td class="green text-center">
                        FDS
                    </td>
                    <td class="text-center">
                        0
                    </td>
                    <td class="text-center">
                        0
                    </td>
                    <td class="text-center">
                        0
                    </td>
                    <td class="text-center">
                        0
                    </td>
                    <td class="valores text-center">
                        0
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="sexta-coluna">

        <table class="table dados-vendedor text-uppercase" border="0">
            <thead>
                <tr class="text-uppercase">
                    <td class="">
                    </td>
                    <td colspan="3" class="green bold text-center">
                        Fundamentos
                    </td>
                    <td colspan="2" class="green bold text-center">
                        Desempenho
                    </td>
                </tr>
            </thead>
            <tbody class="fundamentos-superior">
                <tr>
                    <td class="green td-vertical" rowspan="5">
                        <span class="vertical-text">
                            VOLUME
                        </span>
                    </td>
                    <td class="valores text-center">
                        VOLUME TOTAL
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        300ml+600ml+1Lt
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        PREMIUM
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        C1 MARCA DEVASSA
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        C2 600 ML
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
                <tr>
                    <td class="green td-vertical" rowspan="2">
                        <span class="vertical-text">
                            EXEC
                        </span>
                    </td>
                    <td class="valores text-center">
                        REFRIGERÇÃO
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        IAV
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
                <tr>
                    <td class="green td-vertical" rowspan="2">
                        <span class="vertical-text">
                            DIST
                        </span>
                    </td>
                    <td class="valores text-center">
                        Coberturas Total
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Coberturas FDS
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
                <tr>
                    <td class="green td-vertical" rowspan="3">
                        <span class="vertical-text">
                            OUTROS
                        </span>
                    </td>
                    <td class="valores text-center">
                        Devolução
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Trocas
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
                <tr>
                    <td class="valores text-center">
                        Terceiros
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                    <td class="text-center">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
