
<thead>
<tr class="text-center">
    <td style="border: solid 1px #fff;">
    </td>
    <td style="border-left: solid 1px #fff; border-top: solid 1px #fff; border-bottom: solid 1px #fff;">
    </td>
    <td colspan="5">
        VOLUME
    </td>
    <td colspan="2">
        EXECUÇÃO
    </td>
    <td colspan="2">
        DISTRIBUIÇÃO
    </td>
    <td colspan="3">
        OUTROS
    </td>
</tr>

<tr class="text-center">
    <td style="border-top: solid 1px #fff; border-left: solid 1px #fff; border-right: solid 1px #fff;  border-bottom: solid 1px #fff;">
    </td>
    <td style="border-top: solid 1px #fff; border-left: solid 1px #fff; border-bottom: solid 1px #fff;">
    </td>
    <td colspan="5">
        55%
    </td>
    <td colspan="2">
        15%
    </td>
    <td colspan="2">
        20%
    </td>
    <td colspan="3">
        10%
    </td>
</tr>

<tr class="text-center">
    <td style="border: solid 1px #fff;">
    </td>
    <td style="border-top: solid 1px #fff; border-left: solid 1px #fff; border-bottom: solid 1px #fff;">
    </td>
    <td>
        35%
    </td>
    <td>
        20%
    </td>
    <td>
        25%
    </td>
    <td>
        10%
    </td>
    <td>
        10%
    </td>
    <td>
        50%
    </td>
    <td>
        50%
    </td>
    <td>
        50%
    </td>
    <td>
        50%
    </td>
    <td>
        25%
    </td>
    <td>
        25%
    </td>
    <td>
        50%
    </td>

</tr>

<tr class="text-center">
    <th valign="bottom">Cod</th>
    <th valign="bottom">Vendedor <?= $tipo ?></th>
    <th class="vertical">
        <div class="vertical vertical-600-1000-300">
            600ml + 1000ml + 300ml
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            Premium
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            Volume Total
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            Coringa1
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            Coringa2
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            Refrigeração
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            IAV
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            Cobertura Total
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            FDS
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            Devoluções
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            Trocas
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            Terceiros
        </div>
    </th>
    <th class="vertical">
        <div class="vertical">
            Totais
        </div>
    </th>
</tr>
</thead>
