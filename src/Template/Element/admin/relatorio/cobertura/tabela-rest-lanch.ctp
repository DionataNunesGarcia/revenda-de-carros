<div class="col-md-12">
    <h4>
        <?= $canal ?>
    </h4>
    <table cellpadding="0" cellspacing="0" class="table table-striped table-bordered table-hover table-condensed">
        <thead>
            <tr>
                <th class="text-left"><?= __('Código') ?></th>
                <th class="text-left"><?= __('Razão') ?></th>
                <th class="text-center"><?= __('Eisen 355') ?></th>
                <th class="text-center"><?= __('Dev 600') ?></th>
                <th class="text-center"><?= __('Schin 473') ?></th>
                <th class="text-center"><?= __('Itubaina 355') ?></th>
                <th class="text-center"><?= __('Skinka') ?></th>
                <th class="text-center"><?= __('%') ?></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($clientes as $item) {
            if ($item->canal_fds != $canal) {
                continue;
            }
            ?>
            <tr>
                <td>
                    <?= $item->codigo_str ?>
                </td>
                <td>
                    <?= $item->pessoasJuridica->nome_fantasia ?>
                </td>
                <td class="text-center">
                    <?= fdsX($item->fds->eisen355) ?>
                </td>
                <td class="text-center">
                    <?= fdsX($item->fds->dev600) ?>
                </td>
                <td class="text-center">
                    <?= fdsX($item->fds->schin473) ?>
                </td>
                <td class="text-center">
                    <?= fdsX($item->fds->itubaina355) ?>
                </td>
                <td class="text-center">
                    <?= fdsX($item->fds->skinka) ?>
                </td>
                <td class="text-center">
                    <?= h($item->fds->porcentagem) ?>%
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
