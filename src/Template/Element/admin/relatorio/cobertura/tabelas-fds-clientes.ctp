<?php
use Cake\Collection\Collection;
$clientes = $entidade['clientes'];

$collection = new Collection($clientes);
$clientesPorCanal = $collection->groupBy('canal_fds');

foreach ($clientesPorCanal->toArray() as $canal => $clientsCanal) {
    switch ($canal) {
        case 'ASI 1-4':
            echo $this->element('admin/relatorio/cobertura/tabela-asi14', [
                'clientes' => $clientsCanal,
                'canal' => $canal
            ]);
            break;
        case 'BAR A-B':
            echo $this->element('admin/relatorio/cobertura/tabela-barAB', [
                'clientes' => $clientsCanal,
                'canal' => $canal
            ]);
            break;
        case 'BAR C-D':
            echo $this->element('admin/relatorio/cobertura/tabela-barCD', [
                'clientes' => $clientsCanal,
                'canal' => $canal
            ]);
            break;
        case 'CHOPERIA':
            echo $this->element('admin/relatorio/cobertura/tabela-choperia', [
                'clientes' => $clientsCanal,
                'canal' => $canal
            ]);
            break;
        case 'CONVENIENCIA':
            echo $this->element('admin/relatorio/cobertura/tabela-conveniencia', [
                'clientes' => $clientsCanal,
                'canal' => $canal
            ]);
            break;
        case 'Outros':
            //Não mostrar Outros ainda
            break;
        case 'REST-LANCH':
            echo $this->element('admin/relatorio/cobertura/tabela-rest-lanch', [
                'clientes' => $clientsCanal,
                'canal' => $canal
            ]);
            break;
        case 'TRAD':
            echo $this->element('admin/relatorio/cobertura/tabela-trad', [
                'clientes' => $clientsCanal,
                'canal' => $canal
            ]);
            break;
    }
}
