<?php
$label = isset($label) ? $label : $field;
if (empty($file)) {

    echo $this->Form->label($label);
    echo $this->Form->file($field . '_upload', [
        'class' => '',
        'multiple' => false,
        'label' => 'Boleto'
    ]);
} else {
    ?>
    <strong>
        Formato: <?= strtoupper(extensao($file)) ?>
    </strong>
    <br/>
    <?php if(file_exists(WWW_ROOT . $file)){ ?>
        <i class="fa fa-4x <?= h(getIcon(extensao($file))) ?>"></i>
        <a href="<?= $this->Url->build(['action' => 'download', $field, base64_encode($file)]); ?>"
           class="btn btn-success btn-xs"
           data-auth="false"
           style="margin-left: 15px">
            <i class="fa fa-download"></i>
        </a>
    <?php } else { ?>
        <strong class="text-danger">
            ARQUIVO NÃO ENCONTRADO
        </strong>
    <?php } ?>
    <div class="col-md-4 text-rigth">
        <?=
        $this->Html->link("<i class='fa fa-trash'></i> Excluir", ['action' => 'excluir_arquivo', $id, $field], [
            "alt" => "Excluir Anexo",
            'escape' => false,
            'confirm' => __('Tem certeza de que deseja excluir o arquivo?'),
            'class' => 'btn btn-xs btn-danger tex'
        ]);
        ?>
    </div>
<?php } ?>
