<?= $this->element('admin/titulo-box', ['titulo' => 'Endereço']) ?>
<?= $this->Form->hidden('endereco.id', ['value' => @$entidade->id]) ?>
<?= $this->Form->hidden('endereco.model', ['value' => $model]) ?>
<div class="box-body">
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('endereco.cep', [
            'label' => 'CEP',
            'class' => 'cep',
            'id' => 'cep',
            'value' => cep(@$entidade['cep'])
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('endereco.rua', [
            'label' => 'Rua',
            'id' => 'rua',
            'value' => @$entidade['rua']
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('endereco.numero', [
            'label' => 'Nº',
            'id' => 'numero',
            'value' => @$entidade['numero']
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('endereco.complemento', [
            'label' => 'Complemento',
            'required' => false,
            'value' => @$entidade['complemento']
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('endereco.bairro', [
            'label' => 'Bairro',
            'id' => 'bairro',
            'value' => @$entidade['bairro']
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->element('admin/select2', [
            'controller' => 'Cidades',
            'name' => 'endereco.cidade_id',
            'label' => 'Cidade',
            'id' => 'select2-cidade',
            'multiple' => false,
            'required' => true,
            'value' => @$entidade['cidade_id'],
        ])
        ?>
    </div>
</div>
