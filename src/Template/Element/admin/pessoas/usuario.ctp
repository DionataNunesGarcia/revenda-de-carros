<?php
$placeholder = !empty($entidade->id) ? 'Só preencher se for alterar' : 'Digíte a senha';
$placeholderConfirm = !empty($entidade->id) ? 'Só preencher se for alterar' : 'Confirme a senha';
$required = empty($entidade->id) ? true : false;
?>
<?= $this->Form->hidden('usuario_id', ['value' => @$entidade->id]); ?>
<?= $this->Form->hidden('usuario.id', ['value' => @$entidade->id]); ?>
<?= $this->Form->hidden('usuario.usuario_save', ['value' => @$entidade->usuario, 'id' => 'usuario-save']); ?>
<div class="box-body">
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('usuario.usuario', [
            'label' => 'Usuário',
            'value' => @$entidade->usuario ?? '',
            'required' => true,
            'id' => 'usuario-usuario',
            'placeholder' => 'Login do Usuário'
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('usuario.senha', [
            'label' => 'Senha',
            'type' => 'password',
            'id' => 'usuario-senha',
            'value' => '',
            'placeholder' => $placeholder,
            'required' => $required,
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->Form->control('usuario.confirmar_senha', [
            'label' => 'Confirmar Senha',
            'type' => 'password',
            'id' => 'confirmar-senha',
            'value' => '',
            'placeholder' => $placeholderConfirm,
            'required' => $required,
        ]);
        ?>
    </div>
    <div class="form-group col-md-4">
        <?=
        $this->element('admin/select2', [
            'controller' => 'niveis',
            'name' => 'usuario.niveis_id',
            'label' => 'Níveis',
            'multiple' => false,
            'required' => true,
            'id' => 'usuario-nivel-id',
            'value' => @$entidade->niveis_id,
            'placeholder' => 'Pesquise o Nível de Permissão'
        ])
        ?>
    </div>
    <div class="form-group col-md-4 radio-booton">
        <label class=" no-padding" for="ativo">Ativo</label>
        <div class="radio">
            <?=
            $this->Form->radio('usuario.ativo', ['1' => 'Sim', '0' => 'Não'], [
                'label' => 'Ativo',
                'value' => !empty($entidade->ativo) ? $entidade->ativo : 1,
                'required' => true,
            ]);
            ?>
        </div>
    </div>
    <div class="form-group col-md-4">
        <?php
        if (empty($entidade->imagem)) {
            echo $this->Form->control('usuario.imagem_upload', [
                'type' => 'file',
                'class' => 'upload_crop',
                'multiple' => false,
                'accept' => 'image/*',
                'label' => 'Imagem'
            ]);
        } else {
            ?>
            <div class="col-md-4 no-padding">
                <strong>Imagem</strong>
                <?= $this->Html->image('../' . $entidade->imagem, ['class' => 'img-responsive img-usuario img-thumbnail']); ?>
            </div>
            <div class="col-md-2 text-rigth">
                <?=
                $this->Html->link("<i class='fa fa-trash'></i> Excluir", ['controller' => 'Usuarios', 'action' => 'excluir_imagem', $entidade->id, '_full' => true], [
                    "alt" => "Imagem",
                    'escape' => false,
                    'confirm' => __('Tem certeza de que deseja excluir o arquivo?'),
                    'class' => 'btn btn-xs btn-danger tex'
                ]);
                ?>
            </div>
        <?php } ?>
        <div id="uploaded_image"></div>
    </div>
</div>
<?= $this->element('admin/imagem-crop-modal') ?>
