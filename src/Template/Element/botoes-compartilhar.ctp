<?php
$services = [
    'facebook' => __('Compartilhar no Facebook'),
    'whatsapp' => __('Compartilhar no WhatsApp'),
    'linkedin' => __('Compartilhar no LinkedIn'),
    'twitter' => __('Compartilhar no Twitter')
];
?>
<div class="mobile-social-share">
    <div id="socialHolder" class="col-md-12">
        <div id="socialShare">
            <a data-toggle="dropdown" class="btn btn-primary btn-share">
                Compartilhar <i class="fa fa-share-alt fa-inverse"></i>
            </a>
            <ul class="dropdown-menu">
                <?php
                foreach ($services as $service => $linkText) {
                    echo '<li>' . $this->SocialShare->fa(
                            $service, null, ['icon_class' => 'fa fa-'.$service, 'data-original-title' => $linkText, 'class' => 'btn btn-'.$service]
                    ) . '</li>';
                }
                ?>
            </ul>
        </div>
    </div>
</div>
<?php ?>

<?php
$sobre = $this->Sobre->buscar();
$title = isset($title) ? "{$title} - {$sobre->nome}" : $sobre->nome;
echo $this->OpenGraph
    ->setTitle($title)
    ->setDescription($description)
    ->setImage($img, ['width' => '200', 'height' => '300'])
    ->render();
?>
<style>
    .mobile-social-share {
        background: none repeat scroll 0 0 #EEEEEE;
        display: block !important;
        min-height: 70px !important;
        margin: 50px 0;
    }

    .mobile-social-share h3 {
        color: inherit;
        float: left;
        font-size: 15px;
        line-height: 20px;
        margin: 25px 25px 0 25px;
    }

    .share-group {
        float: right;
        margin: 18px 25px 0 0;
    }

    .btn-group {
        display: inline-block;
        font-size: 0;
        position: relative;
        vertical-align: middle;
        white-space: nowrap;
    }

    .mobile-social-share ul {
        float: right;
        list-style: none outside none;
        margin: 0;
        min-width: 61px;
        padding: 0;
    }

    .share {
        min-width: 17px;
    }

    .mobile-social-share li {
        display: block;
        font-size: 18px;
        list-style: none outside none;
        margin-bottom: 3px;
        margin-left: 4px;
        margin-top: 3px;
    }
    .mobile-social-share .open > .dropdown-menu {
        display: block;
        margin-top: -20px;
    }

    .btn-twitter {
        background-color: #3399CC !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-facebook {
        background-color: #3D5B96 !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-facebook {
        background-color: #3D5B96 !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-google {
        background-color: #DD3F34 !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-linkedin {
        background-color: #1884BB !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-whatsapp {
        background-color: #5cb75c !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-pinterest {
        background-color: #CC1E2D !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .btn-mail {
        background-color: #FFC90E !important;
        width: 51px;
        color:#FFFFFF!important;
    }

    .caret {
        border-left: 4px solid rgba(0, 0, 0, 0);
        border-right: 4px solid rgba(0, 0, 0, 0);
        border-top: 4px solid;
        display: inline-block;
        height: 0;
        margin-left: 2px;
        vertical-align: middle;
        width: 0;
    }

    #socialShare {
        max-width:59px;
        margin-bottom:18px;
    }

    #socialShare > a{
        padding: 6px 10px 6px 10px;
    }

    @media (max-width : 320px) {
        #socialHolder{
            padding-left:5px;
            padding-right:5px;
        }

        .mobile-social-share h3 {
            margin-left: 0;
            margin-right: 0;
        }

        #socialShare{
            margin-left:5px;
            margin-right:5px;
        }

        .mobile-social-share h3 {
            font-size: 15px;
        }
    }

    @media (max-width : 238px) {
        .mobile-social-share h3 {
            font-size: 12px;
        }
    }
</style>
