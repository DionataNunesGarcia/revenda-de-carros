<?php
$veiculoId = isset($veiculoId) ? $veiculoId : null;
?>
<?= $this->Form->create(null, ['action' => 'salvarContato', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal', 'required' => true]) ?>
    <?= $this->Form->control('veiculo_id', ['label' => false, 'type' => 'hidden', 'value' => $veiculoId]); ?>
    <?= $this->Form->control('nome', ['label' => false, 'placeholder' => 'Seu Nome:', 'class' => 'form-control', 'required' => true]); ?>
    <?= $this->Form->control('email', ['label' => false, 'placeholder' => 'E-mail', 'class' => 'form-control', 'required' => true, 'type' => 'email']); ?>
    <?= $this->Form->control('telefone', ['label' => false, 'placeholder' => 'Seu Telefone/Whatsapp:', 'class' => 'form-control fone', 'required' => true]); ?>
    <?php
    if (isset($assunto)) {
        echo $this->Form->control('assunto', [
            'label' => false,
            'type' => 'hidden',
            'value' => $assunto
        ]);
    } else {
        echo $this->Form->control('assunto', ['label' => false, 'placeholder' => 'Assunto:', 'class' => 'form-control', 'required' => true]);
    }
    ?>
    <?= $this->Form->control('mensagem', ['label' => false, 'placeholder' => 'Sua Menssagem:', 'class' => 'form-control', 'type' => 'textArea', 'rows' => 5, 'required' => true]); ?>
    <?= $this->Form->button('<i class="fa send"></i> Enviar', ['class' => 'btn btn-primary', 'escape' => false]); ?>
<?= $this->Form->end() ?>
