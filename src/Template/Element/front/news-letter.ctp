<div class="newslettercontent">
    <div class="leftside">
        <img src="/template-front/image/border.png" alt="border">
        <h1>NEWSLETTER</h1>
        <p>
            Inscreva-se para receber atualizações sobre novos veículos, ofertas especiais e outras informações de desconto.
        </p>
    </div>
    <div class="rightside">
        <img class="newsimage" src="/img/cars-newsletter.jpg" alt="newsletter">
        <input type="text" class="form-control" id="subemail" placeholder="E-mail">
        <button>
            Enviar
        </button>
    </div>
</div>
