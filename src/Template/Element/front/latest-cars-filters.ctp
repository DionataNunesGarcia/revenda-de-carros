<?php
use App\Enum\YearsEnum;
use App\Enum\SimNaoEnum;
use App\Enum\TipoCombustivelEnum;
$title = isset($title) ? $title : "ULTIMOS   VEÍCULOS";
?>
<div class="latestcars">
    <h1 class="text-center">&bullet; <?= $title ?> &bullet;</h1>
</div>

<div id="filter-panel" class="">
    <div class="panel panel-primary panel-collapse">
        <span class="side-tab" data-target="#tab1" data-toggle="tab" role="tab" aria-expanded="false">
            <div class="panel-heading" role="tab" id="heading"data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <h4 class="panel-title">
                    Filtro Avançado <i class="fa fa-filter"></i>
                </h4>
            </div>
        </span>

        <div id="collapseOne" class="panel-collapse collapse <?= !empty($this->request->getQuery()) ? 'in' : 'off' ?>" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">
                <?= $this->Form->create(null, ['type' => 'get', 'class' => 'form-horizontal', 'url' => ['_name' => 'lista_veiculos']]); ?>

                    <!-- form group [min year] -->
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label class="filter-col" style="margin-right:0;" for="modelos">Modelos</label>
                        <?=
                        $this->Form->select('modelo', $modelos, [
                            'empty' => ['' => ''],
                            'class' => 'form-control',
                            'value' => $this->request->getQuery('modelo'),
                        ]);
                        ?>
                    </div>

                    <!-- form group [min year] -->
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label class="filter-col" style="margin-right:0;" for="categorias">Categorias</label>
                        <?=
                        $this->Form->select('categoria', $categorias, [
                            'empty' => ['' => ''],
                            'class' => 'form-control',
                            'value' => $this->request->getQuery('categoria'),
                        ]);
                        ?>
                    </div>

                    <!-- form group [min year] -->
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label class="filter-col" style="margin-right:0;" for="marca">Marcas</label>
                        <?=
                        $this->Form->select('marca', $marcas, [
                            'empty' => ['' => ''],
                            'class' => 'form-control',
                            'value' => $this->request->getQuery('marca'),
                        ]);
                        ?>
                    </div>

                    <!-- form group [min year] -->
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label class="filter-col" style="margin-right:0;" for="marca">Combustivel</label>
                        <?=
                        $this->Form->select('combustivel', TipoCombustivelEnum::ARRAY_STR, [
                            'empty' => ['' => ''],
                            'class' => 'form-control',
                            'value' => $this->request->getQuery('combustivel'),
                        ]);
                        ?>
                    </div>

                    <!-- form group [order by] -->
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label class="filter-col" style="margin-right:0;" for="pref-orderby">Ordenar por:</label>
                        <?=
                        $this->Form->select('ordem_preco', [
                            'asc' => 'Menor Preço',
                            'desc' => 'Maior Preço'
                        ], [
                            'empty' => ['' => ''],
                            'class' => 'form-control',
                            'value' => $this->request->getQuery('ordem_preco'),
                        ]);
                        ?>
                    </div>

                    <!-- form group [importado] -->
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label class="filter-col" style="margin-right:0;" for="importado">Importado:</label>
                        <?=
                        $this->Form->select('importado', SimNaoEnum::getArray(), [
                            'empty' => ['' => ''],
                            'class' => 'form-control',
                            'value' => $this->request->getQuery('importado'),
                        ]);
                        ?>
                    </div>

                    <!-- form group [min year] -->
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label class="filter-col" style="margin-right:0;" for="ano-min">Ano Min.:</label>
                        <?=
                        $this->Form->select('ano_min', YearsEnum::getList(), [
                            'empty' => ['' => ''],
                            'class' => 'form-control',
                            'value' => $this->request->getQuery('ano_min'),
                        ]);
                        ?>
                    </div>
                    <div class="form-group col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label class="filter-col" style="margin-right:0;" for="ano-max">Ano Máx.:</label>
                        <?=
                        $this->Form->select('ano_max', YearsEnum::getList(), [
                            'empty' => ['' => ''],
                            'class' => 'form-control',
                            'value' => $this->request->getQuery('ano_max'),
                        ]);
                        ?>
                    </div>
                    <div class="col-md-12 text-right">
                        <?= $this->Form->button('<i class="fa fa-search"></i> Pesquisar', ['class' => 'btn btn-primary', 'escape' => FALSE]); ?>
                        <?= $this->Form->button('<i class="fa fa-eraser"></i> Limpar', ['class' => 'btn btn-default','type' => 'reset', 'escape' => FALSE]); ?>
                    </div>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</div>
