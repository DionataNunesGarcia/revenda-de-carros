<div class="feturedsection">
    <h1 class="text-center">
        <span class="bdots">&bullet;</span>
        <span class="carstxt">
            D E S T A Q U E S
        </span>&bullet;
    </h1>
</div>
<div class="row upcoming">
    <?php foreach ($destaques as $veiculo) { ?>
        <?= $this->element('front/card', ['veiculo' => $veiculo]) ?>
    <?php } ?>
</div>
