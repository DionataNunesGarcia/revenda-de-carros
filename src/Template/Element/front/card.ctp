<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 box border py-4 car-card text-center">
    <div class="car">
        <div class="box-carimage">
            <img src="<?= $veiculo->imagem ?>" alt="" class="img-responsive">
        </div>
        <div class="box-cartitle">
            <h4> <?= $veiculo->modelo->nome ?> - <?= $veiculo->ano ?></h4>
            <span>
                <?= h($veiculo->modelo->marca->nome)?>
            </span>
        </div>
        <div class="box-carprice">
            <?php
            $classValor = '';
            $valorPromocao = '';
            if(!empty($veiculo->valor_promocao) && $veiculo->valor_promocao < $veiculo->valor) {
                $valorPromocao = '<strong class="card-price-promotion">' . valueToValorCifrado($veiculo->valor_promocao) . '</strong>';
                $classValor = 'exist-promotion';
                ?>
            <?php } ?>
            <strong class="card-price <?= $classValor ?>" >
                <?= valueToValorCifrado($veiculo->valor) ?>
            </strong>
            <?= $valorPromocao ?>
        </div>
        <div class="box-date pb-3">
            <small>
                Combustível: <?= \App\Enum\TipoCombustivelEnum::getType($veiculo->combustivel) ?>
            </small>
        </div>
        <a class="btn btn-primary" href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'veiculo', $veiculo->id, slug($veiculo->modelo->nome) ], true); ?>">
            Ver detalhes
        </a>
    </div>
</div>
