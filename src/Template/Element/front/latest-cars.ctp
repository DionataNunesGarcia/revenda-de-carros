<div class="latestcars">
    <h1 class="text-center">&bullet; ADICIONADOS RECENTEMENTE &bullet;</h1>
    <div class="row">
        <?php foreach ($ultimos as $veiculo) { ?>
            <?= $this->element('front/card', ['veiculo' => $veiculo]) ?>
        <?php } ?>
    </div>
</div>
