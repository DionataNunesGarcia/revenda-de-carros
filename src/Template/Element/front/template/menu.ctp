<!-- Header -->
<div class="allcontain">
<!--    <div  000-->
    <!-- Navbar Up -->
    <nav class="topnavbar navbar-default topnav">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed toggle-costume" data-toggle="collapse" data-target="#upmenu" aria-expanded="false">
                    <span class="sr-only"> Toggle navigaion</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand logo" href="<?= $this->Url->build('/', true); ?>">
                    <?= $this->Html->image('logo.png', ['class' => 'logo']); ?>
                </a>
            </div>
        </div>
        <div class="collapse navbar-collapse" id="upmenu">
            <ul class="nav navbar-nav" id="navbarontop">
                <li>
                    <a href="<?= $this->Url->build('/', true); ?>">
                        Home
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?= $this->Url->build(['_name' => 'sobre'], true); ?>">
                        Sobre Nós
                    </a>
                </li>
                <li>
                    <a href="<?= $this->Url->build(['_name' => 'lista_veiculos'], true); ?>">
                        Veículos
                    </a>
                </li>
<!--                <li class="dropdown">-->
<!--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">NEGOCIANTES <span class="caret"></span></a>-->
<!--                    <ul class="dropdown-menu dropdowncostume">-->
<!--                        <li><a href="#">1</a></li>-->
<!--                        <li><a href="#">2</a></li>-->
<!--                        <li><a href="3">3</a></li>-->
<!--                    </ul>-->
<!--                </li>-->
                <li>
                    <a href="<?= $this->Url->build(['_name' => 'contato'], true); ?>">
                        Contato
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>
