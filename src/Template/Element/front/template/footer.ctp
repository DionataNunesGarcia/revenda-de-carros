<?php $sobre = $this->Sobre->buscar(); ?>
<footer id="dk-footer" class="dk-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-4">
                <div class="dk-footer-box-info">
                    <a href="<?= $this->Url->build('/', true); ?>" class="footer-logo">
                        <?= $this->Html->image('logo.png', ['class' => 'logo-footer', 'alt' => 'logo1']); ?>
                    </a>
                    <div class="footer-social-link text-center">
                        <h3></h3>
                        <ul>
                            <?php if (!empty($sobre->facebook)) { ?>
                                <li>
                                    <a href="<?= $sobre->facebook ?>" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if (!empty($sobre->linkedin)) { ?>
                                <li>
                                    <a href="<?= $sobre->linkedin ?>" target="_blank">
                                        <i class="fa fa-linkedin-square"></i>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if (!empty($sobre->instagram)) { ?>
                                <li>
                                    <a href="<?= $sobre->instagram ?>" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!-- End Social link -->
                </div>
            </div>
            <!-- End Col -->
            <div class="col-md-12 col-lg-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="contact-us">
                            <div class="contact-icon">
                                <i class="fa fa-map-o" aria-hidden="true"></i>
                            </div>
                            <!-- End contact Icon -->
                            <div class="contact-info">
                                <h3><?= $sobre->endereco->rua ?>, Nº <?= $sobre->endereco->numero ?></h3>
                                <p>Bairro:<?= $sobre->endereco->bairro ?></p>
                                <p><?= ($sobre->endereco->cidade->nome) ?>/<?= $sobre->endereco->cidade->uf ?></p>
                            </div>
                            <!-- End Contact Info -->
                        </div>
                        <!-- End Contact Us -->
                    </div>
                    <!-- End Col -->
                    <div class="col-md-6">
                        <div class="contact-us contact-us-last">
                            <div class="contact-icon">
                                <i class="fa fa-volume-control-phone" aria-hidden="true"></i>
                            </div>
                            <!-- End contact Icon -->
                            <div class="contact-info">
                                <?php if (!empty($sobre->telefone)) { ?>
                                    <strong>
                                        <?= fone($sobre->telefone) ?>
                                    </strong>
                                    <br/>
                                <?php } ?>
                                <?php if (!empty($sobre->celular)) { ?>
                                    <strong>
                                        <?= fone($sobre->celular) ?>
                                    </strong>
                                    <br/>
                                <?php } ?>
                                <?php if (!empty($sobre->celular_2)) { ?>
                                    <strong>
                                        <?= fone($sobre->celular_2) ?>
                                    </strong>
                                    <br/>
                                <?php } ?>
                                <p>Entre em contato conosco</p>
                            </div>
                            <!-- End Contact Info -->
                        </div>
                        <!-- End Contact Us -->
                    </div>
                    <!-- End Col -->
                </div>
            </div>
            <!-- End Col -->
        </div>
        <!-- End Widget Row -->
    </div>
    <!-- End Contact Container -->


    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <span>Copyright © <?= date('Y') ?>, Todos os direitos reservados</span>
                </div>
                <!-- End Col -->
                <div class="col-md-6">
                    <div class="copyright-menu">
                        <ul>
                            <li>
                                <a href="<?= $this->Url->build('/', true); ?>">
                                    Home
                                </a>
                            </li>
                            <li>
                                <a href="<?= $this->Url->build(['_name' => 'sobre'], true); ?>">
                                    Sobre Nós
                                </a>
                            </li>
                            <li>
                                <a href="<?= $this->Url->build(['_name' => 'lista_veiculos'], true); ?>">
                                    Veículos
                                </a>
                            </li>
                            <li>
                                <a href="<?= $this->Url->build(['_name' => 'contato'], true); ?>">
                                    Contato
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- End col -->
            </div>
            <!-- End Row -->
        </div>
        <!-- End Copyright Container -->
    <!-- End Copyright -->
    </div>
</footer>
