<?php if ($banners = $this->Sobre->banners()) { ?>
    <div class="allcontain">
        <ul id="banner-adaptive">
            <?php foreach ($this->Sobre->banners() as $banner) { ?>
                <li data-src="<?= $banner->arquivo ?>">
                    <?php if ($banner->link) { ?>
                        <a href="<?= $banner->link ?>">
                    <?php } ?>
                    <img src="<?= $banner->arquivo ?>" />
                    <?php if ($banner->link) { ?>
                        </a>
                    <?php } ?>
                </li>
            <?php  } ?>
        </ul>
    </div>
<?php } ?>
