$(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 1000000,
      values: [ 500000, 1000000 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
  });

	var container = $('.grid').isotope({
		itemSelector: '.col-xs-12',
		getSortData: {
			name: '.name',
			price: '.price parseInt'
		}
	});
	$('.alphSort').on('click', function(e){
		e.preventDefault();
		container.isotope({ sortBy: 'name'});
	});
	$('.prcBtnH').on('click', function(e){
		e.preventDefault();
		container.isotope({ sortBy: 'price', sortAscending: false});
	});
	$('.prcBtnL').on('click', function(e){
		e.preventDefault();
		container.isotope({ sortBy: 'price', sortAscending: true});
	});
	$('.prcBtnR').on('click',function(e){
		e.preventDefault();
		container.isotope({sortBy:'random'});
	});
	$('.prcBtnO').on('click',function(e){
		e.preventDefault();
		container.isotope({sortBy:'original-order'});
	});

$('#btnRM').click(function(){
    $('#readmore').animate({height:'322px'}, 500);
});
$('#btnRL').click(function(){
	$('#readmore').animate({height:'0px'}, 500);
});
$('#btnRM2').click(function(){
    $('#readmore2').animate({height:'322px'}, 500);
});
$('#btnRL2').click(function(){
	$('#readmore2').animate({height:'0px'}, 500);
});

	$(function () {
  $("#mydd a").on('click',function (e) {
  	e.preventDefault();
    $("#dropdownMenu1").html($(this).html() + ' <span class="downicon"></span>');
  });
});

$(function () {
  $("#mydd2 a").on('click',function (e) {
  	e.preventDefault();
    $("#dropdownMenu2").html($(this).html() + ' <span class="downicon"></span>');
  });
});
$(function () {
  $("#mydd3 a").on('click',function (e) {
  	e.preventDefault();
    $("#dropdownMenu3").html($(this).html() + ' <span class="downicon"></span>');
  });
});

$(function () {
    $(".read-more").on('click',function (e) {
        e.preventDefault();
        $(this).before(".read-more-text").fadeIn();
    });

    $(".read-less").on('click',function (e) {
        e.preventDefault();
        $(this).before(".read-more-text").fadeOut();
    });

    $(".newslettercontent button").on('click',function (e) {
        e.preventDefault();
        let email = $("#subemail").val();
        if (email == '') {
            alert('O campo e-mail não pode ficar vázio.');
            return;
        }
        if( !validateEmail(email)) {
            alert('O e-mail digitado é inválido.');
            return;
        }

        $.ajax({
            url: saveNewsletters,
            type: 'POST',
            data: {
                email: email
            },
            success: function(response){
                if (!response.status) {
                    alert(response.mensagem);
                    return;
                }
                alert('E-mail cadastrado com sucesso, aguarde para receber nossas novidades.')
            }
        }).done(function(){

        });
    });

    $(':reset').click(function (e) {
        var form = $(this).closest("form");
        form.find('input').each(function () {
            $(this).val('');
        });
        form.find('select').each(function () {
            $(this).val('');
        });
        $(".select2-ajax").each(function () {
            $(this).val('').trigger('change');
        });
        return false;
    });
});

$( document ).ready(function() {

    $h_slider_options =  {
        gallery: true,
        item: 1,
        loop:true,
        slideMargin: 0,
        thumbItem: 6,
        galleryMargin: 10,
        thumbMargin: 10,
    };

    h_slider = $('#lightSlider').lightSlider($h_slider_options);

    /* Fancybox & lightSlider Sync - Bug Fix */
    $selector = '#lightSlider li:not(".clone") a';
    $selector += ',#lightSliderVertical li:not(".clone") a';
    $().fancybox({
        selector : $selector,
        backFocus : false, //The most important options for sync bug fix
        buttons : [
            'slideShow',
            'share',
            'zoom',
            'fullScreen',
            'thumbs',
            'download',
            'close'
        ]
    });

    $('#banner-adaptive').lightSlider({
        adaptiveHeight: true,
        item: 1,
        slideMargin: 0,
        loop:true,
        pauseOnHover: true,
        auto:true,
    });

    activeMenu();

    //Mascara para telefones
    $(".fone").inputmask({
        mask: ["(99) 9999-9999", "(99) 9999-99999", ],
        keepStatic: true
    });
});

function activeMenu() {
    var url = window.location;
    // Will only work if string in href matches with location
    $('nav #navbarontop li a[href="' + url.href + '"]')
        .parents('li')
        .addClass('active');

    // Will also work for relative and absolute hrefs
    $('nav #navbarontop li a')
        .filter(function () {
            return this.href === url.href;
        })
        .parents('li')
        .addClass('active');
}

function validateEmail($email) {
    let emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test( $email );
}
